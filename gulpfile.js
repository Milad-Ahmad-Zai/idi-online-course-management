//
// WebHiro gulp config file
// www.webhiro.com
//

var gulp = require('gulp'),
  $ = require('gulp-load-plugins')();
var path = require('path');

$.sync = require('browser-sync');
$.run = require('run-sequence');
$.vinylPaths = require('vinyl-paths');
$.del = require('del');
$.merge = require('merge-stream');
$.lazypipe = require('lazypipe');
$.historyApiFallback = require('connect-history-api-fallback');
$.karma = require('karma').Server;
$.concat = require('gulp-concat');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';


/**
 * @using
 * $ gulp build --param value
 *
 * argv.param === value // true
 */
var argv = require('yargs').argv;

var isDev = process.env.NODE_ENV == 'development';
var isProd = !isDev;

var config = {
  src: 'app',
  dest: 'build',

  html: {
    src: ['app/index.html', 'app/partials/**/*.html', 'app/assets/js/**/*.html'],
    dest: 'build'
  },
  js: {
    src: 'app/assets/js/**/*.js',
    dest: 'build/assets/js/'
  },
  app: {
    name: 'app.js',
    src: [
      './app/assets/js/router/routes.js',
      './app/assets/js/services/auth.service.js',
      './app/assets/js/app.run.js',
      './app/assets/js/services/interceptor.service.js',
      './app/assets/js/application.js',
      './app/assets/js/**/route.js'
    ]
  },
  vendor: {
    name: 'vendor.js',
    src: [
      './bower_components/angular/angular.js',
      './bower_components/angular-animate/angular-animate.js',
      './bower_components/angular-sanitize/angular-sanitize.js',
      './bower_components/angular-touch/angular-touch.js',
      './bower_components/angular-ui-router/release/angular-ui-router.js',
      './bower_components/angular-permission/dist/angular-permission.js',
      './bower_components/angular-local-storage/dist/angular-local-storage.js',
      './bower_components/angular-strap/dist/angular-strap.min.js',
      './bower_components/angular-strap/dist/angular-strap.tpl.min.js',
      './bower_components/oclazyload/dist/ocLazyLoad.js',
      './app/assets/js/vendor/thirdparty/lodash.js',
      './app/assets/js/vendor/thirdparty/jquery.min.js',
      './app/assets/js/vendor/thirdparty/moment.js',
      './app/assets/js/vendor/thirdparty/fullcalendar.min.js',
      './app/assets/js/vendor/clipboard.js',
      './app/assets/js/vendor/api-check.min.js',
      './app/assets/js/vendor/formly.js',
      './app/assets/js/vendor/formly-templates-bootstrap-edited.js',
      './bower_components/angular-ui-select/dist/select.js'
    ]
  },
  css: {
    watch: 'app/assets/css/**/*.{css,less}',
    src: 'app/assets/css/' + (argv.theme || 'idi') + '.less',
    dest: 'build/assets/css',
    name: 'application.css'
  },
  img: {
    src: 'app/assets/img/**/*',
    dest: 'build/assets/img'
  },
  font: {
    src: 'app/assets/font/**/*',
    dest: 'build/assets/font'
  },
  other: {
    src: 'app/assets/other/**/*',
    dest: 'build/assets/other'
  },
  personalImages: {
    watch: 'app/assets/logos/' + (argv.theme || 'idi') + '/**/**',
    src: 'app/assets/logos/' + (argv.theme || 'idi') + '/**/*',
    dest: 'build/media/'
  },
  config: {
    src: ['app/config.js'],
    dest: 'build'
  },
  locale: {
    src: ['app/assets/locale/*.json'],
    dest: 'build/assets/locale'
  }
};


var notify = {
  html: { errorHandler: $.notify.onError('VIEWS: BUILD FAILED!\n' + 'Error:\n<%= error.message %>') },
  css: { errorHandler: $.notify.onError('STYLES: BUILD FAILED!\n' + 'Error:\n<%= error.message %>') },
  js: { errorHandler: $.notify.onError('SCRIPTS: BUILD FAILED!\n' + 'Error:\n<%= error.message %>') },
  img: { errorHandler: $.notify.onError('IMAGES: SOMETHING WRONG!\n' + 'Error:\n<%= error.message %>') },
  personalImages: { errorHandler: $.notify.onError('PERSONAL_IMAGES: SOMETHING WRONG!\n' + 'Error:\n<%= error.message %>') },
  font: { errorHandler: $.notify.onError('FONTS: SOMETHING WRONG!\n' + 'Error:\n<%= error.message %>') },
  other: { errorHandler: $.notify.onError('OTHER: SOMETHING WRONG!\n' + 'Error:\n<%= error.message %>') },
  locale: { errorHandler: $.notify.onError('LOCALE: SOMETHING WRONG!\n' + 'Error:\n<%= error.message %>') }
};

gulp.task('font', function () {
  return gulp
    .src(config.font.src)
    .pipe( $.newer(config.font.dest) )
    .pipe( $.plumber(notify.font) )
    .pipe( gulp.dest(config.font.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});

/**
 * Here we concat all vendor files
 */
gulp.task('vendor', function() {
  return gulp
    .src(config.vendor.src)
    // dest is the same as for js files
    .pipe($.newer(path.join(config.js.dest, config.vendor.name)))
    .pipe($.concat(config.vendor.name))
    .pipe($.if(isProd, $.ngAnnotate()))
    .pipe($.if(isProd, $.uglify({ mangle: false })))
    // same path as for js files
    .pipe(gulp.dest(config.js.dest));
})

/**
 * Here we build and concat application's
 * main files
 */
gulp.task('app', function() {
  return gulp
    .src(config.app.src)
    // dest is the same as for js files
    .pipe($.newer(path.join(config.js.dest, config.app.name)))
    .pipe($.concat(config.app.name, {newLine: '\r\n;'}))
    .pipe($.if(isProd, $.ngAnnotate()))
    .pipe($.if(isProd, $.uglify({ mangle: false})))
    .pipe(gulp.dest(config.js.dest));
})

gulp.task('other', function () {
  return gulp
    .src(config.other.src)
    .pipe( $.newer(config.other.dest) )
    .pipe( $.plumber(notify.other) )
    .pipe( gulp.dest(config.other.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});

gulp.task('image', function () {
  return gulp
    .src(config.img.src)
    .pipe( $.newer(config.img.dest) )
    .pipe( $.plumber(notify.img) )
    .pipe( $.imagemin({
      optimizationLevel: 5,
      progressive: true,
      interlaced: true
    }))
    .pipe( gulp.dest(config.img.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});

gulp.task('personalImages', function () {
  return gulp
    .src(config.personalImages.src)
    .pipe( $.newer(config.personalImages.dest) )
    .pipe( $.plumber(notify.personalImages) )
    .pipe( $.imagemin({
      optimizationLevel: 5,
      progressive: true,
      interlaced: true
    }))
    .pipe( gulp.dest(config.personalImages.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});

gulp.task('html', function () {
  return gulp
    .src(config.html.src, { base: 'app' })
    .pipe( $.newer(config.html.dest) )
    .pipe( $.plumber(notify.html ) )
    .pipe( $.preprocess() )
    .pipe(gulp.dest(config.html.dest))
    .pipe($.sync.reload({ stream: true }));
});

gulp.task('css', function () {
  return gulp
    .src(config.css.src)
    .pipe( $.newer(config.css.dest) )
    .pipe( $.plumber(notify.css) )
    .pipe( $.less() )
    .pipe( $.rename(config.css.name) )
    .pipe( $.autoprefixer() )
    .pipe( gulp.dest(config.css.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});



gulp.task('js', function () {
  return gulp.src([
    config.js.src, '!app/config.*.js',
    '!app/config.js',
    '!app/assets/js/application.js',
    '!app/assets/js/router/routes.js',
    '!app/assets/js/app.run.js',
    '!app/assets/js/services/auth.service.js',
    '!app/assets/js/services/interceptor.service.js'
  ])
    .pipe( $.newer(config.js.dest) )
    .pipe( $.plumber(notify.js) )
    .pipe( $.if( isProd, $.ngAnnotate()))
    .pipe( $.if( isProd, $.uglify()))
    .pipe( gulp.dest(config.js.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});

gulp.task('config', function () {
  return gulp.src(config.config.src)
    .pipe( $.newer(config.config.dest) )
    .pipe( $.plumber(notify.js) )
    .pipe( gulp.dest(config.config.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});

gulp.task('locale', function () {
  return gulp
    .src(config.locale.src)
    .pipe( $.newer(config.locale.dest) )
    .pipe( $.plumber(notify.locale) )
    .pipe( gulp.dest(config.locale.dest) )
    .pipe( $.sync.reload({ stream: true }) );
});

gulp.task('test', function() {
  $.karma.start({
    configFile: __dirname + '/test/karma.conf.js',
    singleRun: true
  });
});

gulp.task('clean', function (cb) {
  return gulp
    .src([config.dest])
    .pipe($.vinylPaths($.del));
});

gulp.task('serve', function () {
  $.sync.init(null, {
    server: {
      open: false,
      baseDir: config.dest,
      middleware: [$.historyApiFallback() ]
    }
  });
});

gulp.task('watch', function() {
  gulp.watch(config.html.src, ['html']);
  gulp.watch(config.js.src, ['js']);
  gulp.watch(config.app.src, ['app']);
  gulp.watch(config.css.watch, ['css']);
  gulp.watch(config.config.src, ['config']);
  gulp.watch(config.locale.src, ['locale']);
});

gulp.task('build', function (cb) {
  $.run(['font', 'vendor', 'app', 'other',
  'image', 'personalImages', 'html',
  'css', 'js', 'config', 'locale'], cb);
});


gulp.task('default', ['build', 'watch'], function() {
  $.run('serve')
});