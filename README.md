IDI - Online Course and Users Management Web App.

``` 
git pull
npm install
bower install
gulp build
``` 

# Prepare

1. Install node.js and npm package manager for node

``` 
   sudo apt-get install nodejs
```

2. Install gulp

```
   npm install gulp -g
```

3. Get dependencies

```
   bower install 
   npm install
```

4. Run application in development environment  


```
   gulp serve [--theme=(abs|idi|aberdeen)]
```

5. Build app for production environment

```
   gulp build [--theme=(abs|idi|aberdeen)]
```

## Styleguide

We use 2 spaces indentation. Use [editor config](http://editorconfig.org/) for applying this settings to your IDE 

### Directives
* Directives should have prefix `idi` in their name and file name.
**Example:**
```js
// idi.forum-message.js
/**
 * @using <idi-forum-message/>
 */
angular
.module('idi.forums.forum-message', [])
.directive('idiForumMessage', directive);

function directive () {
 // code
}

```
* Directives should be placed in  
`app/assets/js/directives/{namespace}/{directive-name}`  
**Example:**  
`app/assets/js/directives/forums/forum-message/`  


* Template file for directive should be placed in the same folder as a js  
**Example:**  
`/forums/forum-message/idi.forum-message.js`  
`/forums/forum-message/idi.forum-message.html`  


* Styles for directive should be placed in  
`app/assets/css/directives/{namespace}/`  
and use the same naming rules as a js files  
**Example:**  
`app/assets/css/directives/forums/idi.forum-message.less`


## Favicons

To add a new set of favicons to the app, follow these steps:

1. Get an icon with size at least 260x260px
2. Upload to [https://realfavicongenerator.net/](https://realfavicongenerator.net/)
3. Follow steps and download .zip containing all necessary files
4. Extract and copy contents to desired themes logo directory. For example for ABS copy files into `app/assets/logos/abs`