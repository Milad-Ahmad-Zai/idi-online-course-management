(function () {
  'use strict';

  angular
    .module('login.ctrl', [])

    .controller('LoginCtrl', function ($scope, $state, $rootScope, $window, $sce, $modal, $q, $stateParams, CONFIG, cfpLoadingBar, Notification, AuthService, ProfilesService) {
      $scope.credentials = {};
      if ($stateParams.path) {
        $scope.path = $window.atob($stateParams.path);
        $scope.path = decodeURIComponent($scope.path);
      }
      if ($stateParams.username && $stateParams.password) {
        $scope.credentials = {
          username: $stateParams.username,
          password: $stateParams.password
        }
      }
      if ($stateParams.accessToken && $stateParams.refreshToken) {
        var tokenConfig = CONFIG.client;

        tokenConfig.access_token = $stateParams.accessToken;
        tokenConfig.refresh_token = $stateParams.refreshToken;

        AuthService.getAccessToken('refresh_token', tokenConfig).then(function(res) {
          if (!res.error) {
            $scope.permissionsRequest().then(function (res) {
              $scope.goToState();
            });
          } else {
            $window.localStorage.token = '';
            $state.go('app.login');
          }
        });
      }

      $scope.permissionsRequest = function () {
        return AuthService.getPermissions().then(function (res) {
          $rootScope.currentPermissions = res.data.permissions;
          return res;
        });
      };

      if ($state.is('app.login')) {
        $scope.isLogin = true;
      } else if ($state.is('app.forgotpassword')) {
        $scope.isForgotPassword = true;
      }  else if ($state.is('app.recoverUsername')) {
        $scope.isRecoverUsername = true;
      } else if ($state.is('app.resetpassword')) {
        cfpLoadingBar.start();
        var hash = $state.params.hash;
        AuthService.checkPasswordResetRequest(hash).then(function (res) {
          if (res.data.success) {
            $scope.isResetPassword = true;
          } else {
            $state.go('app.login');
          }

          cfpLoadingBar.complete();
        });
      }

      $scope.finishRequest = function (res) {
        if (res.data.profile_image && res.data.profile_image.type == 'profileimage') {
          $rootScope.profileImage = res.data.profile_image.url;
        }
        $rootScope.oldProfile = angular.copy(res.data);
        $rootScope.profile = angular.copy(res.data);

        if($rootScope.profile && $rootScope.profile.age) $rootScope.profile.age = +$rootScope.profile.age;
        if($rootScope.profile && $rootScope.profile.file && $rootScope.profile.file.url) $rootScope.profile.image = $sce.trustAsResourceUrl($rootScope.profile.file.url);

        var firstName = res.data.first_name || '';
        var lastName = res.data.last_name || '';
        $rootScope.fullName = firstName + ' ' + lastName;


        $scope.permissionsRequest().then(function (res) {
          $scope.goToState();
        });
      };

      $scope.getMe = function () {
        AuthService.me().then(function (res) {
          if (res.status !== 403) {
            $rootScope.user = $scope.user = res.data.user;
            $rootScope.userId = $scope.userId = $scope.user.id;

            ProfilesService.getProfile($rootScope.userId).then(function(res) {
              if (!res.data.error) {
                $scope.finishRequest(res);
              }
            });
          }
        }, function () {
          Notification.error({ message: 'Cannot get your data', title: 'Error'});
        })
      };

      $scope.goToState = function () {
        var previousState = $rootScope.previousState;
        if (previousState) {
          $state.go(previousState.state, previousState.params)
        } else if (this.currentPermissions.indexOf('admissions.role.advisor') > -1) {
          $state.go('app.admin.admissions.advisor');
        } else if (this.currentPermissions.indexOf('admissions.role.applicant') > -1) {
          $state.go('app.admin.admissions.student');
        } else {
          $state.go('app.admin.dashboard');
        }
      };

      $scope.login = function () {
        var dataToSend = angular.copy(CONFIG.client);
        dataToSend['username'] = $scope.credentials.username;
        dataToSend['password'] = $scope.credentials.password;
        AuthService.getAccessToken('password', dataToSend).then(function(res) {
          var token;
          if (res.error) {
            Notification.error({title: 'Error', message: res.msg});
          } else {
            token = res.data;
            token.timestamp = new Date().getTime() + token.expires_in * 1000;
            $window.localStorage.token = angular.toJson(res.data);
            $scope.getMe();
          }
        }, function (res) {
          Notification.error({title: 'Error', message: res.data.msg});
        });
      };

      $scope.sendEmail = function (form) {
        if (form.$invalid) {
          return;
        }

        cfpLoadingBar.start();
        AuthService.sendEmail($scope.credentials.username).then(function (res) {
          if (res.status === 500) {
            Notification.error({title: 'Error', message: 'User does not exists.'});
          } else if (!res.data.error){
            Notification.success({title: 'Success', message: res.data.msg});
          }

          cfpLoadingBar.complete();
        });
      };

      $scope.recoverUsername = function (form) {
        if (form.$invalid) {
          return;
        }

        cfpLoadingBar.start();
        AuthService.remindEmail($scope.credentials.email).then(function (res) {
          if (res.error) {
            Notification.error({title: 'Error', message: 'Email does not exists.'});
          } else {
            Notification.success({title: 'Success', message: res.data.msg});
          }

          cfpLoadingBar.complete();
        });
      };

      $scope.resetPassword = function (form) {
        if (form.$invalid) {
          return;
        }

        cfpLoadingBar.start();
        var pwdConfig = {
          hash: $state.params.hash,
          password: $scope.credentials.password
        };
        AuthService.passwordReset(pwdConfig).then(function (res) {
          if (res.error) {
            Notification.error({title: 'Error', message: 'An error occured while processing your request'});
          } else {
            Notification.success({title: 'Success', message: res.data.msg});
            $state.go('app.login');
          }

          cfpLoadingBar.complete();
        });
      };
      var date = 1459278000000;
      if (new Date().getTime() < date) {
        Notification.warning(
          {
            message: 'Dear students, <br>' +
            'Due to some essential maintenance, the Admissions portal will be closed for a short period of time on 29 March 2016 from 6pm. <br>' +
            'If you find you cannot log in, please try again later. <br>' +
            'Kind regards, <br>' +
            'IDI Admissions Team'
          });
      }
    });

})();