(function() {

  angular
  .module('ui.routes')
  .config(function($stateProvider) {
    $stateProvider
    .state('app.admin.error', {
      url: '/error',
      abstract: true,
      views: {
        'content@app': {
          template: '<div ui-view=""></div>'
        }
      },
      data: {
        permissions: {
          except: ['anonymous'],
          redirectTo: 'app.login'
        }
      }
    })
    .state('app.admin.error.403', {
      url: '/403?back',
      templateUrl: '/partials/errors/403.html',
      controller: 'Error403Ctrl',
      resolve: {
        load: function($ocLazyLoad) {
          return $ocLazyLoad.load([{
            name: 'ctrl.403',
            files: ['/assets/js/controllers/errors/403.js']
          }])
        }
      }
    })
    .state('app.admin.error.404', {
      url: '/404?back',
      templateUrl: '/partials/errors/404.html',
      controller: 'Error404Ctrl',
      resolve: {
        load: function($ocLazyLoad) {
          return $ocLazyLoad.load([{
            name: 'ctrl.404',
            files: ['/assets/js/controllers/errors/404.js']
          }])
        }
      }
    })
  })

})()

