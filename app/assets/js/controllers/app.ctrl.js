(function () {

  'use strict';

  angular
    .module('ctrl.app', ['ui-notification', 'angular.filter'])
    .controller('AppCtrl', AppCtrl);

    function AppCtrl ($scope, $rootScope, $timeout, $interpolate, $interval, $state, $window, $sce, cfpLoadingBar, MessageService) {

      // Permissions
      var PagesPermissions = {
        //
        'auth.permissions.available': ['auth.group.edit', 'auth.userPermissions.edit'],
        'auth.groups.list': ['auth.group.list'],
        // profiles sidebar
        'profiles': ['auth.user.list', 'auth.group.list', 'auth.user.get', 'auth.user.getSelf', 'profiles.profileAny.get', 'profiles.profileSelf.get'],
        'profiles.profile': ['profiles.profileSelf.get'],
        'profiles.anyProfile': ['profiles.profileAny.get'],
        'profiles.users': ['auth.user.list'],
        'profiles.groups': ['auth.group.list'],
        // tabs
        'profiles.personal': ['profiles.tabPersonalSelf.see'],
        'profiles.history': ['profiles.tabHistorySelf.see'],
        'profiles.experience': ['profiles.tabEmploymentSelf.see'],
        'profiles.creative': ['profiles.tabCreativeSelf.see'],
        'profiles.summary': ['profiles.tabSummarySelf.see'],
        'profiles.decision': ['profiles.tabDecisionSelf.see'],
        'profiles.uploaded': ['profiles.tabFilesSelf.see'],
        'profiles.uhform': ['profiles.tabUhSelf.see'],
        'profiles.notes_history': ['profiles.tabNotesSelf.see'],
        'profiles.managers': ['messages.manager.manage', 'messages.managers.manage'],
        'profiles.otherTabs': [
          'profiles.tabPersonalOther.see',
          'profiles.tabHistoryOther.see',
          'profiles.tabEmploymentOther.see',
          'profiles.tabCreativeOther.see',
          'profiles.tabSummaryOther.see',
          'profiles.tabDecisionOther.see',
          'profiles.tabFilesOther.see',
          'profiles.tabUhOther.see',
          'profiles.tabNotesOther.see'
        ],
        'profiles.other.personal': ['profiles.tabPersonalOther.see'],
        'profiles.other.history': ['profiles.tabHistoryOther.see'],
        'profiles.other.experience': ['profiles.tabEmploymentOther.see'],
        'profiles.other.creative': ['profiles.tabCreativeOther.see'],
        'profiles.other.summary': ['profiles.tabSummaryOther.see'],
        'profiles.other.decision': ['profiles.tabDecisionOther.see'],
        'profiles.other.uploaded': ['profiles.tabFilesOther.see'],
        'profiles.other.uhform': ['profiles.tabUhOther.see'],
        'profiles.other.notes_history': ['profiles.tabNotesOther.see'],
        'profiles.other.managers': ['messages.manager.manage', 'messages.managers.manage'],
        // study path tabs
        'profiles.programmes': ['profiles.tabProgramme.see'],
        'profiles.academic_history': ['profiles.tabStudentLog.see'],
        'profiles.messages': ['profiles.tabMessagesHistory.see'],
        'profiles.academic_notes': ['profiles.tabNotes.see'],
        // profiles user
        'profiles.user.editPermission': ['auth.userPermissions.edit'],
        // other profile things
        'profiles.versions': ['profiles.versions.get'],
        'profiles.versions.restore': ['profiles.versions.restore'],
        // admissions
        'admissions.apply': ['admissions.admissionSelf.get', 'admissions.admissionAny.get'],
        'admissions.get': ['admissions.admissionSelf.get', 'admissions.admissionAny.get'],
        'admissions.anyGet': ['admissions.admissionAny.get'],
        'admissions.advisor': ['admissions.admissionAny.get'],
        'admissions.document': ['admissions.supportDocument.edit'],
        'admissions.chat': ['messages.scope.admissions_advisor'],
        'admissions.decision': ['admissions.decisionSheet.get'],
        'admissions.decision.delete': ['admissions.decisionSheets.delete'],
        'admissions.summary': ['admissions.summarySheet.get'],
        'admissions.summary.edit': ['admissions.summarySheet.edit'],
        'messages.compose': ['messages.messageAnyone.create', 'messages.messageAnyGroup.create'],
        'messages.settings': ['messages.group.getall', 'messages.templateAny.get'],
        'messages.other': ['messages.scope.tutor', 'messages.scope.finance', 'messages.scope.course_coordinator', 'messages.scope.student_support', 'messages.scope.technical_support', 'messages.scope.academic_administrator', 'messages.scope.admissions_advisor'],
        // forums
        'forums.manager': ['forums.forum.publish', 'forums.forum.unpublish'],
        'forums.addTopic': ['forums.topicAny.edit', 'forums.topicSelf.edit'],
        'forums.editTopic': ['forums.topicAny.edit'],
        'forums.editTopicState': ['forums.topicState.edit'],
        // studio
        'studio': ['courses.semester.get'],
        //studypath
        'studypath.any': ['studypath.role.student', 'studypath.role.tutor', 'studypath.role.manager'],
        'studypath.programmes': ['studypath.role.student', 'studypath.role.manager'],
        'studypath.student': ['studypath.role.student'],
        'studypath.tutor': ['studypath.role.tutor'],
        'studypath.manager': ['studypath.role.manager']
      };

      var PagesBans = {
        'admissions.apply': ['admissions.admissionAny.get']
      };

      $scope.checkPermission = function (permission) {
        return _.isArray($rootScope.currentPermissions) && $rootScope.currentPermissions.indexOf(permission) > -1;
      };

      $scope.checkRights = function (type) {
        var result = true;

        if (PagesPermissions[type]) {
          result = false;
          PagesPermissions[type].forEach(function (element, index, array) {
            if ($scope.checkPermission(element)){
              result = true;
            }
          });
        }
        if (PagesBans[type]) {
          PagesBans[type].forEach(function (element, index, array) {
            if ($scope.checkPermission(element)) {
              result = false;
            }
          });
        }

        return result;
      };

      $scope.getHomeLink = function () {
        var state;
        if ($scope.checkRights('admissions.get')) {
          state = $state.href('app.admin.admissions.student');
        } else if ($scope.checkRights('studypath.tutor')) {
          state = $state.href('app.admin.courses.tutor');
        } else if ($scope.checkRights('studypath.student')) {
          state = $state.href('app.admin.dashboard');
        } else {
          state = $state.href('app.admin.profile.public');
        }

        return state;
      };

      // Course Builder

      var courseBuilderUrl = '';

      $scope.createCourseBuilderUrl = function () {
        function buildUrl(url) {
          courseBuilderUrl = url + '/courses/land/{{token}}/{{refreshToken}}{{ path ? "/" + path : ""}}';
          $rootScope.urlToCourseBuilder = $interpolate(courseBuilderUrl)({
            token: angular.fromJson($window.localStorage.token).access_token,
            refreshToken: angular.fromJson($window.localStorage.token).refresh_token
          });
        }
        if (location.origin.indexOf('localhost') > -1) {
          buildUrl('http://lms3.idi-demo.com');
        } else {
          buildUrl(location.origin);
        }
      };

      $scope.createCourseBuilderUrl();
      $interval($scope.createCourseBuilderUrl, 60 * 60 * 1000);

      // get message scopes
      var scopeOrder = ['Admissions Advisor', 'Admission Advisor', 'Tutor', 'Student', 'Student Support', 'Course Coordinator', 'Finance', 'Technical Support', 'Academic Administrator', 'Other', 'Other - Admissions Advisor', 'Other - Tutor', 'Other - Student Support', 'Other - Course Coordinator', 'Other - Finance', 'Other - Technical Support', 'Other - Academic Administrator', 'Private Message', 'Private Messages', 'Sent Box', 'Announcements'];
      $rootScope.messageScopes = [];
      $rootScope.getMessageScopes = function () {
        MessageService.getUserMessageScope().then(function (res) {
          if (res) {
            var unorderedMessageScope = res.data;

            // check if scope has only one conversation
            unorderedMessageScope.forEach(function (item) {
              item.expanded = false;

              if (item.name_alias == 'private_messages') {
                $rootScope.privateMessageScope = item;
              }

              if (item.name_alias == 'tutor' || item.name_alias == 'module') {
                $rootScope.tutorMessageScope = item;
              }

              if (item.name_alias == 'admissions_advisor') {
                $rootScope.admissionAdvisorScope = item;
              }

              // check if account is student. if yes, all scopes except module/tutor should display the message right away
              if ($rootScope.currentPermissions.indexOf('messages.scope.private_messages') == -1
                && (item.name_alias != 'tutor' && item.name_alias != 'module')) {
                item.expanded = true;
                item.conversationId = null;

                var data = {
                  userId: $scope.userId,
                  scopeId: item.id,
                  sourceId: $scope.userId,
                  category: 'none',
                  page: '1/1'
                };

                MessageService.getConversations(data).then(function (res) {
                  if (!res.error) {
                    var items = res.data.items;
                    if (items.length > 0) {
                      item.conversationId = items[0].Conversation.id;
                    }
                  }
                });
              }
            });

            if ($scope.checkRights('messages.other') && $rootScope.currentPermissions.indexOf('messages.scope.private_messages') > -1) {
              unorderedMessageScope.forEach(function (item) {
                if (item.name == 'Tutor' || item.name == 'Finance' || item.name == 'Course Coordinator'
                  || item.name == 'Student Support' || item.name == 'Technical Support' || item.name == 'Academic Administrator'
                  || item.name == 'Academic Administrator' || item.name == 'Admissions Advisor') {
                  item.hasSubCategories = true;
                }
              });
            }

            if ($rootScope.currentPermissions.indexOf('messages.messages.sent') > -1) {
              unorderedMessageScope.push({
                name: 'Sent Box',
                name_alias: 'sent-box',
                expanded: false
              });
            }

            // update ordering of message scopes
            $rootScope.messageScopes = [];
            scopeOrder.forEach(function (order) {
              unorderedMessageScope.forEach(function (scope) {
                if (order == scope.name) {
                  $rootScope.messageScopes.push(scope);
                }
              });
            });

            $rootScope.getUnreadMessageCount();
          }
        });
      };

      $scope.getLastFiveMessages = function () {
        MessageService.lastFiveMessages().then(function (res) {
          if (res) {
            $rootScope.fiveMessages = [];
            res.data.forEach(function (mes) {
              // date duration
              var createdMoment = moment.utc(mes.created_at);
              mes.date = createdMoment.toDate();
              mes.duration = createdMoment.fromNow(true);

              // user picture
              if (mes.user_picture) {
                mes.pic = $sce.trustAsResourceUrl(mes.user_picture.url);
              }

              // set message
              if ($rootScope.userId == mes.user_id) {
                mes.msg = 'You sent a message to "' + mes.topic + '"';
                mes.isSelf = true;
              } else {
                var firstName = mes.user_first_name || '';
                var lastName = mes.user_last_name || '';
                mes.msg = firstName + ' ' + lastName + ' sent you a message on "' + mes.topic + '"';
                mes.isSelf = false;
              }

              $rootScope.fiveMessages.push(mes);
            });
          }
        });
      };

      $rootScope.getUnreadMessageCount = function () {
        $rootScope.totalUnreadCtr = 0;
        MessageService.countUnread({user_id: $scope.userId}).then(function (res) {
          if ( res && res.error !== true ) {
            if (res.data.scope_level.length > 0) {
              var scope_level = res.data.scope_level;

              $rootScope.messageScopes.forEach(function (item) {
                if (item.id) {
                  scope_level.forEach(function (scope) {
                    if (scope.scope_id == item.id) {
                      item.unreadCtr = parseInt(scope.unread_count);
                      if (item.unreadCtr > 0) {
                        $rootScope.totalUnreadCtr += item.unreadCtr;
                      }
                    }
                  });
                }
              });
            }
          }
        });
      };

      $scope.scrollbarConfig = {
        autoResize: true,
        scrollbar: {
          width: 2,
          hoverWidth: 2,
          color: '#65cac0',
          show: false
        },
        scrollbarContainer: {
          width: 4,
          color: '#e9f0f5'
        },
        scrollTo: null
      };

      $scope.isRead = function () {
        return $rootScope.currentPermissions.indexOf('messages.scope.private_messages') > -1 && $state.is('app.admin.mail.category.read') && !$rootScope.messageScopeObj.expanded && $rootScope.messageScopeObj.name_alias;
      };

      $scope.isSearch = function () {
        return $state.is('app.admin.mail.search.read');
      };

      // for images
      $scope.isImage = function (ext) {
        ext = '|' + ext + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(ext) !== -1;
      };

      // router changed state
      $rootScope.$on('$stateChangeSuccess', function (event, data) {
        $timeout(function () {
          $scope.show_page = true;
          cfpLoadingBar.complete();
        }, 500);
      });

      $rootScope.$on('$stateChangeStart', function (event, data) {
        window.scrollTo(0, 0);
        $scope.show_page = false;
        cfpLoadingBar.start();
      });

      $rootScope.logout = function () {
        $window.localStorage.removeItem('token');
        $rootScope.previousState = null;
        location.reload()
      };

      $scope.notifications = [
        {"id":"001", "type":"message", "date":"2016-12-14 09:38:27", "message":{ "id":"25", "name":"Vitalij", "content":"Do you like the new notifications widget"}},
        {"id":"002", "type":"alert", "date":"2016-12-14 07:38:27", "content":"Site will be down for maintenance on 25/12/2016" },
        {"id":"003", "type":"calendar", "date":"2016-12-12 09:38:27", "calendar":{ "title":"Very important event", "start":"11:00", "end":"13:30"}},
        // {"id":"004", "type":"upload", "date":"2016-12-7 09:38:27", "upload":{ "title":"eps1.7_wh1ter0se.m4v", "url":"http://www.google.com"}},
        // {"id":"005", "type":"forum", "date":"2016-11-14 09:38:27", "topic":{ "name":"Important module discussion", "url":"http://www.google.com"}},
      ]

      $scope.firstInitial = $rootScope.firstInitial;

    }
})();