(function () {
  'use strict';

  angular
    .module('ctrl.groups', [])

    .controller('GroupsCtrl', function ($scope, $state, $rootScope, $window, $q, $modal, AuthService, NgTableParams, Notification) {
      var vm = this;
      var groups = [];
      var oldGroup = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: 'id'
      };
      if ($window.localStorage.users) {
        usersConfig = JSON.parse($window.localStorage.users);
      } else {
        usersConfig = {
          start: 0,
          limit: 10,
          filter: '',
          sort: 'id',
          page: 1
        };
        $window.localStorage.users = JSON.stringify(usersConfig);
      }
      var groupsConfig;
      if ($window.localStorage.groups) {
        groupsConfig = JSON.parse($window.localStorage.groups);
      } else {
        groupsConfig = {
          start: 0,
          limit: 100,
          filter: '',
          sort: 'id',
          page: 1
        };
        $window.localStorage.groups = JSON.stringify(groupsConfig);
      }

      $scope.$on('editGroupModal.hide', $scope.cancelEditGroup);

      $scope.getUsersList = function (config) {
        return AuthService.getUsersList(config);
      };

      $scope.getGroupsList = function (config) {
        return AuthService.getGroupsList(config);
      };

      $scope.getData = function () {
        var arrayOfRequests = [];
        var permissions = AuthService.getPermissionsList();
        var groups = $scope.getGroupsList({
          start: 0,
          limit: 100,
          filter: '',
          sort: 'id'
        });

        if ($scope.checkRights('auth.groups.list')) arrayOfRequests[0] = groups;
        if ($scope.checkRights('auth.permissions.available')) arrayOfRequests[1] = permissions;

        $q.all(arrayOfRequests).then(function(arrayOfResults) {
          if(arrayOfResults[0]) {
            $scope.groups = angular.copy(arrayOfResults[0].data.list);
          }
          if (arrayOfResults[1]) {
            $scope.services = angular.copy(arrayOfResults[1].data.services);
          }
          $scope.answers = angular.copy(AuthService.getHelp());
          $scope.cleanForm();
        });
      };

      $scope.populateGroupsList = function () {
        var sort = {};
        var splittedSort;

        if (groupsConfig.sort) {
          if (groupsConfig.sort.indexOf('-') > -1) {
            splittedSort = groupsConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[groupsConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        $scope.groupsTable = new NgTableParams({
          page: groupsConfig.page || 1,
          count: groupsConfig.limit,
          sorting: sort
        }, {
          getData: function($defer, params) {
            groupsConfig.limit = params.count();
            groupsConfig.start = (params.page() - 1) * params.count();
            groupsConfig.sort = Object.keys(params.sorting())[0];
            groupsConfig.page = params.page();

            if (params.sorting()[groupsConfig.sort] == 'desc') {
              groupsConfig.sort = '-' + groupsConfig.sort;
            }

            $window.localStorage.groups = JSON.stringify(groupsConfig);
            return $scope.getGroupsList(groupsConfig).then(function (res) {
              params.total(parseInt(res.data.pagination.totalItems));
              $scope.groupsTable.total(parseInt(res.data.pagination.totalItems));
              groups = res.data.list;
              return groups;
            });
            //var orderedData = params.sorting() ? $filter('orderBy')(groups, params.orderBy()) : groups;
            //return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
          }
        });
      };

      $scope.cleanForm = function () {
        $scope.service = {};
        $scope.servicesNames = [];
        if ($scope.services.length) {
          $scope.currentServiceName = $scope.services[Object.keys($scope.services)[0]].name;
        }

        $scope.services.forEach(function (element, index, array) {
          $scope.service[element.name] = element.permissions;
          $scope.servicesNames.push(element.name);
        });

        $scope.newGroup = {
          allServices: angular.copy($scope.services),
          service: angular.copy($scope.service),
          permissions: [],
          description: '',
          name: ''
        };
        $scope.newUser = {
          allGroups: angular.copy($scope.groups),
          allServices: angular.copy($scope.services),
          service: angular.copy($scope.service),
          groups: [],
          permissions: [],
          name: '',
          username: '',
          email: '',
          password: ''
        };
      };

      $scope.cancelChanges = function (e) {
        var scope = angular.element(e.target).scope();
        scope.userProfile = angular.copy(scope.oldProfile);
      };

      $scope.changeService = function (e) {
        var selectedService = angular.element(e.target).scope().service;
        $scope.currentServiceName = selectedService.name;
        if (!(selectedService.name == $scope.currentService && $scope.currentService.name)) {
          var filtered = $scope.services.filter(function (service) {
            return service.name == service.name;
          });
          $scope.currentService = filtered[0];
        }
      };

      $scope.filterByKeyword = function (e) {
        console.log(true);
        if (e.which === 13) {
          if ($scope.filter.name.length > 0) {
            usersConfig.filter = 'search=' + $scope.filter.name;
          } else {
            usersConfig.filter = '';
          }

          if ($scope.filter.name.length > 0) {
            groupsConfig.filter = 'name==' + $scope.filter.name;
          } else {
            groupsConfig.filter = '';
          }

          usersConfig.start = 0;
          groupsConfig.start = 0;
          $scope.populateUsersList();
          $scope.populateGroupsList();
        }
      };

      $scope.addGroup = function () {
        for(var i = 0; i < $scope.newGroup.allServices.length; i++) {
          _.assign($scope.newGroup.allServices[i], { selected: false})
        }

        var modal = $modal({
          scope: $scope,
          templateUrl: 'partials/modals/new.group.modal.html',
          show: false
        });

        modal.$promise.then(modal.show);
      };

      $scope.updateGroup = function (e) {
        var group = angular.element(e.target).scope().group;
        var permissions = [];
        group.allServices.forEach(function (element, index, array) {
          element.permissions.forEach(function (el, i, arr) {
            if (el.selected) {
              permissions.push(el.permission);
            }
          })
        });
        group.permissions = permissions;
        AuthService.updateGroup(group).then(function (res) {
          if (res.data.status == 500 || res.data.error) {
            Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
            scope.group = oldGroup;
          } else {
            Notification.success({ message: 'Group successfully updated' });
          }
          $scope.getData();
        });
      };

      $scope.createGroup = function () {
        $scope.newGroup.allServices.forEach(function (element, index, array) {
          element.permissions.forEach(function (el, i, arr) {
            if (el.selected) {
              $scope.newGroup.permissions.push(el.permission);
            }
          })
        });
        AuthService.createGroup($scope.newGroup).then(function (res) {
          if (res.data.status == 500 || res.data.error) {
            Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
          } else {
            Notification.success({ message: res.data.msg });

            groups.push(angular.copy(res.data.group));
            $scope.groupsTable.reload();
            $scope.getData();
          }
        });
      };

      $scope.editGroup = function (e) {
        var scope = angular.element(e.target).scope();

        oldGroup = scope.group;

        AuthService.getGroup(scope.group.id).then(function (res) {

          scope.group = res.data.group;

          var groupPermissions = scope.group.permissions;

          scope.group.allServices = angular.copy($scope.services);
          scope.group.allServices.forEach(function (element, index, array) {
            element.permissions.forEach(function (el, i, arr) {
              for (var j = 0; j < groupPermissions.length; j++) {
                var permission = groupPermissions[j];
                if (permission === el.permission) {
                  el.selected = true;
                }
              }
            })
          });

          var modal = $modal({
            scope: scope,
            templateUrl: 'partials/modals/edit.group.modal.html',
            prefixEvent: 'editGroupModal',
            show: false
          });

          modal.$promise.then(modal.show);
        });
      };

      $scope.deleteGroup = function (e) {
        var scope = angular.element(e.target).scope();
        var group = scope.group;

        AuthService.deleteGroup(group.id).then(function (res) {
          if (res.data && res.data.status == 500 || res.data && res.data.error) {
            Notification.error({ message: 'User haven\'t deleted', title: 'Error: ' + res.data.status});
          } else {
            Notification.success({ message: res.data.msg });

            $scope.getGroupsList(groupsConfig).then(function (res) {
              groups = res.data.list;
              $scope.groupsTable.reload();
            });
          }
        });
      };

      $scope.cancelEditGroup = function (e) {
        var scope = e.targetScope;
        scope.group = oldGroup;
        scope.group.permissions = _.pluck(scope.group.permissions, 'permission');
      };

      init();

      function init() {
        $scope.services = [];
        $scope.groups = [];
        $scope.service = {};
        $scope.servicesNames = [];
        $scope.filter = {};
        $scope.isBlockView = false;
        $scope.getData();
        $scope.populateGroupsList();
      }
    });
})();
