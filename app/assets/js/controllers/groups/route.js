(function() {
  angular
  .module('ui.routes')
  .config(config)

  function config($stateProvider) {
    $stateProvider
    .state('app.admin.groups', {
      url: '/groups',
      views: {
        'content@app': {
          controller: 'GroupsCtrl',
          templateUrl: '/partials/groups/groups.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.groups', files: ['/assets/js/controllers/groups/groups.ctrl.js']
            }
          ])
        }
      },
      data: {
        permissions: {
          only: ['auth.user.list']
        }
      }
    })
  }
})()