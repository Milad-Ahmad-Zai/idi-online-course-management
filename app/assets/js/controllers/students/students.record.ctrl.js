(function () {
  'use strict';

  angular
    .module('ctrl.students.record', [])

    .controller('StudentsRecordCtrl', ['$scope', '$state', '$rootScope', 'AuthService', function ($scope, $state, $rootScope, AuthService) {
      
      $rootScope.studentManagerConfig = {
        start: 0,
        limit: 10,
        page: 1,
        filter: '',
        sort: {
          id: 'asc'
        }
      };
      
      $scope.updateTitle = function () {
        if ($state.current.name == 'app.admin.studentrecord.list') {
          $scope.title = 'Student Manager';
        } else {
          $scope.title = 'Student Manager - Edit Student';
        }
      };
      
      $scope.updateTitle();
      
      $scope.getSemesters = function () {
      
      };
      
      $rootScope.$on('$stateChangeSuccess', function (event, data) {
        $scope.updateTitle();
      });
      
    }]);
})();