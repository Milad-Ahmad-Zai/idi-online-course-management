(function () {
  'use strict';

  angular
    .module('ctrl.students.studypath', [])

    .controller('StudentsStudyPathCtrl', function ($scope, $state, $rootScope, $sce, $modal, cfpLoadingBar, NgTableParams, Notification, AuthService, CoursesService, StudyPath2Service, SemestersService, ProfilesService) {
      $scope.studentId = $state.params.id | $scope.userProfileId;
      $scope.course_status = [];
      $scope.module_status = [];
      $scope.section_status = [];

      $scope.tutors = [];

      $scope.collapse_programme = {};
      $scope.collapse_course = {};
      $scope.collapse_module = {};

      $scope.showProgrammes = false;
      $scope.showCourseTypes = false;
      $scope.canSave = false;

      $scope.currentStudent = {};

      //get current student
      ProfilesService.getProfile($scope.studentId).then(setCurrentStudent);

      function setCurrentStudent(currentStudent) {
        $scope.currentStudent = currentStudent.data;
        $scope.currentStudent.enrolment_date = $scope.currentStudent.enrolment_date ? moment($scope.currentStudent.enrolment_date).format("YYYY-MM-DD") : null;
        $scope.currentEnrolmentDate = $scope.currentStudent.enrolment_date;
      }

      // get study path
      $scope.getStudyPath = function () {

        StudyPath2Service.getStudentProgrammesWithModules($scope.studentId).then(function (res) {
          $scope.studypath = res;

          $scope.studypath.forEach(function (prog) {
            prog.isEdit = false;

            // get programme start date
            if (prog.semester_start_date) {
              prog.start_date = moment.utc(prog.semester_start_date);
              prog.start_date_string = prog.start_date.format('DD/MM/YYYY');
            }

            prog.programme.courses.forEach(function (course) {

              course.modules.forEach(function (mod) {
                mod.isEdit = false;

                var startDateSemester = _.result(_.find($rootScope.semesterList, {'id': mod.semester_id}), 'start_date');

                // compute start date
                mod.start_date = moment.utc(startDateSemester).add(parseInt(mod.offset), 'days');
                mod.start_date_string = mod.start_date.format('DD/MM/YYYY');
                mod.start_date_string_2 = mod.start_date.format('YYYY-MM-DD');

                // compute end date
                var days = parseInt(mod.offset) + parseInt(mod.duration);
                mod.end_date = prog.start_date.add(days, 'days');
                mod.end_date_string = mod.end_date.format('DD/MM/YYYY');
                mod.end_date_string_2 = mod.end_date.format('YYYY-MM-DD');

              });
            });
          });
        });
      };

//get all tutors
      $scope.getAllTutors = function (semester) {
        var tutorConfig = {
          filter: 'permission=studypath.role.tutor',
          start: 0,
          limit: 999
        };
        AuthService.getUsersList(tutorConfig).then(function (res) {
          if (res) {
            $scope.tutors = res.data.list;
          }
        });
      };

// get programme list
      $scope.getProgrammeList = function (semester) {
        var programmeConfig = {
          filter: 'export=1/get.course=true/get.content=false/get.module=false/semester_id=' + semester.id,
          start: 0
        };
        CoursesService.getEntities(programmeConfig, 'programmes').then(function (res) {

          if (res) {
            $scope.programmeList = res.data.list;
            $scope.showProgrammes = true;
          }
        });
      };

// get course type
      $scope.getCourseTypes = function () {
        CoursesService.getCourseTypes().then(function (res) {
          $scope.courseTypes = res.data;
          $scope.showCourseTypes = true;
        });
      };

// get semester list
      $scope.getSemesterList = function () {
        var semesterConfig = {
          start: 0,
          limit: 9999,
          sort: 'id'
        };
        if ($rootScope.semesterList.length == 0) {
          SemestersService.getSemestersList(semesterConfig).then(function (res) {
            if (res) {
              $rootScope.semesterList = res.data.list;
            }

            $scope.getStudyPath();

          });
        }
      };

      $scope.getProgrammeStatuses = function () {
        StudyPath2Service.getProgrammeStatuses().then(function (res) {
          if (res) {
            $scope.programmeStatuses = res
          }
        })
      };

      $scope.getLevelStatuses = function () {
        StudyPath2Service.getLevelStatuses().then(function (res) {
          if (res) {
            $scope.levelStatuses = res
          }
        })
      };

      $scope.getModuleStatuses = function () {
        StudyPath2Service.getModuleStatuses().then(function (res) {
          if (res) {
            $scope.moduleStatuses = res
          }
        })
      };

      $scope.updateStudentStatus = function (form) {
        cfpLoadingBar.start();
        ProfilesService.updateProfile($scope.currentStudent.id, $scope.currentStudent).then(function (res) {
          setCurrentStudent(res);
          cfpLoadingBar.complete();
          form.$setPristine();
          form.$setUntouched();
          if (!res.error) {
            Notification.success({
              message: 'Student status successfully updated.',
              title: 'Success'
            });
          } else {
            Notification.error({
              message: 'There was an error processing your request.',
              title: 'Error'
            });
          }
        });
      };

      $scope.addProgramme = function (form) {
        cfpLoadingBar.start();
        var data = {
          user_id: $scope.studentId,
          created_by: $rootScope.userId,
          semester_id: $scope.addProgramme.semester.id,
          programme_id: $scope.addProgramme.programme.id,
          course_type: $scope.addProgramme.study_mode.id
        };
        StudyPath2Service.createNewStudyPath(data).then(function (res) {
          cfpLoadingBar.complete();
          form.$setPristine();
          form.$setUntouched();

          $scope.showAddProgrammeModal.$promise.then($scope.showAddProgrammeModal.hide);

          if (!res.error) {
            $scope.getStudyPath();
            Notification.success({
              message: 'Programme successfuly added.',
              title: 'Success'
            });
          } else {
            Notification.error({
              message: 'There was an error processing your request.',
              title: 'Error'
            });
          }
        });
      };

      $scope.deleteStudyPath = function (index) {
        cfpLoadingBar.start();
        var prog = $scope.studypath[index];
        StudyPath2Service.deleteStudyPath(prog.id).then(function (res) {
          cfpLoadingBar.complete();

          if (!res.error) {
            $scope.studypath.splice(index, 1);
            Notification.success({
              message: 'Programme successfuly removed.',
              title: 'Success'
            });
          } else {
            Notification.error({
              message: 'There was an error processing your request.',
              title: 'Error'
            });
          }
        });
      };

      $scope.showAddProgramme = function () {
        $scope.showAddProgrammeModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/student.programme.modal.html',
          show: false
        });

        $scope.showAddProgrammeModal.$promise.then($scope.showAddProgrammeModal.show);
      };

      $scope.editStudentProgramme = function (programme) {
        programme.isEdit = !programme.isEdit;
      };

      $scope.editStudentCourse = function (course) {
        course.isEdit = !course.isEdit;
      };

      $scope.editStudentModule = function (module) {
        module.isEdit = !module.isEdit;
      };

      $scope.semesterSelected = function () {
        $scope.showProgrammes = false;
        $scope.showCourseTypes = false;
        $scope.courseTypes = [];
        $scope.programmeList = [];
        delete $scope.addProgramme.programme;
        delete $scope.addProgramme.study_mode;
        if ($scope.addProgramme.semester) {
          $scope.getProgrammeList($scope.addProgramme.semester);
        }
      };

      $scope.programmeSelected = function () {
        $scope.showCourseTypes = false;
        $scope.courseTypes = [];
        delete $scope.addProgramme.study_mode;
        if ($scope.addProgramme.programme) {
          $scope.getCourseTypes($scope.addProgramme.programme);
        }
      };

      $scope.courseTypeSelected = function () {
        if ($scope.addProgramme.study_mode) {
          $scope.canSave = true;
        } else {
          $scope.canSave = false;
        }
      };
      if ($scope.studentId && $scope.checkRights('profiles.tabProgramme.see')) {
        $scope.getSemesterList();
        $scope.getProgrammeStatuses();
        $scope.getLevelStatuses();
        $scope.getModuleStatuses();
        //$scope.getStudyPath();
        $scope.getAllTutors();
      }
    })
  ;
})
();