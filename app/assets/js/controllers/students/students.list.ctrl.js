(function () {
  'use strict';

  angular
    .module('ctrl.students.record.list', ['dndLists', 'angularjs-dropdown-multiselect'])

    .controller('StudentsListCtrl', ['$scope', '$state', '$rootScope', '$window', '$timeout', 'NgTableParams', 'AuthService', 'CoursesService', 'SemestersService', 'ListcacheService', function ($scope, $state, $rootScope, $window, $timeout, NgTableParams, AuthService, CoursesService, SemestersService, ListcacheService) {
      var studentsList = this;
      var sort = {};
      var students = [];
      studentsList.totalStudents = 0;
      studentsList.firstLoad = true;

      studentsList.stringFilter = '';

      studentsList.models = {
        selected: null,
        lists: {
          'filters': [], 
          'column1': [],
          'column2': [],
          'column3': []
        }
      };

      studentsList.columnsHover = {};
      studentsList.columnsClick = {};

      studentsList.filters = {};

      studentsList.clearAllColumns = function () {
        var columns = ['column1', 'column2', 'column3'];
        for (var i = 0; i <= columns.length - 1; i++) {
          studentsList.models.lists[columns[i]].forEach(function (element, index, array) {
            studentsList.models.lists.filters.push(element);
          });
          studentsList.models.lists[columns[i]] = [];
        }
      };

      studentsList.clearAllFilters = function () {
        studentsList.filters = {
          programme_id: []
        };
      };

      studentsList.clearAll = function () {
        studentsList.clearAllFilters();
        studentsList.clearAllColumns();
      };

      studentsList.setFilterStyles = function (item) {
        var str = '';
        if (studentsList.models.selected === item) {
          str += 'selected';
        }
        return str;
      };

      $scope.$watch('models', function(model) {
        studentsList.modelAsJson = angular.toJson(model, true);
      }, true);

      // get programme name
      studentsList.getProgrammes = function () {
        CoursesService.getUniqueProgrammes().then(function (res) {
          if (res) {
            studentsList.programmeList = res.data;
          }
        });
      };

      studentsList.getProgrammes();

      // get level
      studentsList.levels = [];
      studentsList.getLevel = function () {
        studentsList.levels = [
          {
            id: 1,
            title: 'Level 1'
          },
          {
            id: 2,
            title: 'Level 2'
          },
          {
            id: 3,
            title: 'Level 3'
          },
          {
            id: 4,
            title: 'Level 4'
          }];
      };

      studentsList.getLevel();

      // get semester list
      var semesterConfig = {
        start: 0,
        limit: 9999,
        sort: 'id'
      };
      studentsList.getSemesterList = function () {
        if ($rootScope.semesterList.length == 0) {
          SemestersService.getSemestersList(semesterConfig).then(function (res) {
            if (res) {
              $rootScope.semesterList = res.data.list;
            }
          });
        }
      };

      studentsList.getSemesterList();

      // get tutors
      studentsList.advisors = [];
      var advisorConfig = {
        start: 0,
        limit: 100,
        filter: 'group=Admissions Advisor',
        sort: 'name'
      };
      studentsList.getTutors = function () {
        AuthService.getUsersList(advisorConfig).then(function (res) {
          if (res.data) {
            studentsList.advisors = res.data.list;
          }
        });
      };

      studentsList.getTutors();

      // get course coordinators
      studentsList.course_coordinators = [];
      var courseCoordinatorConfig = {
        start: 0,
        limit: 100,
        filter: 'group=Admissions Advisor',
        sort: 'name'
      };
      studentsList.getCourseCoordinator = function () {
        AuthService.getUsersList(courseCoordinatorConfig).then(function (res) {
          if (res.data) {
            studentsList.course_coordinators = res.data.list;
          }
        });
      };

      studentsList.getCourseCoordinator();

      // filters
      studentsList.studentFilter = {};
      if ($window.localStorage.studentFilter) {
        studentsList.studentFilter = angular.fromJson($window.localStorage.studentFilter);
      } else {
        $window.localStorage.studentFilter = angular.toJson(studentsList.studentFilter);
      }

      studentsList.filterByKeyword = function (e) {
        if (e.which === 13) studentsList.filterStudentList();
      };

      studentsList.resetFilter = function () {
        studentsList.studentFilter = {};
        studentsList.filterStudentList();
      };

      studentsList.filterStudentList = function () {
        $window.localStorage.studentFilter = angular.toJson(studentsList.studentFilter);
      };

      studentsList.loadData = function () {
        var studentsListConfig = {
          start: 0,
          limit: 10,
          filter: studentsList.stringFilter
        };
        studentsList.studentsTable = new NgTableParams({
          page: studentsListConfig.page || 1,
          count: studentsListConfig.limit,
          sorting: sort
        }, {
          getData: function($defer, params) {
            studentsListConfig.limit = params.count();
            studentsListConfig.start = (params.page() - 1) * params.count();
            studentsListConfig.sort = Object.keys(params.sorting())[0];
            studentsListConfig.page = params.page();

            if (params.sorting()[studentsListConfig.sort] === 'desc') {
              studentsListConfig.sort = '-' + studentsListConfig.sort;
            }

            return ListcacheService.getStudentsCachedList(studentsListConfig).then(function (res) {
              studentsList.totalStudents = parseInt(res.data.pagination.totalItems);
              params.total($scope.totalStudents);
              studentsList.studentsTable.total(parseInt(res.data.pagination.totalItems));
              students = res.data.list;
              studentsList.studentsKeys = _.keys(students[0]);

              studentsList.studentsKeys.forEach(function (key) {
                studentsList.columnsClick[key] = true;
              });

              var filtersKeys = Object.keys(res.data.filters);

              filtersKeys.forEach(function (key) {
                var obj = res.data.filters[key];
                obj.filterId = key;
                if (obj.type === 'multiselect') {

                  var options = obj.options;
                  var arrayOfOptions = [];
                  for (var prop in options) {
                    arrayOfOptions.push({
                      id: prop,
                      label: options[prop]
                    });
                  }
                  obj.options = arrayOfOptions;
                  if (studentsList.firstLoad) {
                    studentsList.filters[key] = [];
                  }
                }
                if (studentsList.firstLoad) {
                  studentsList.models.lists.filters.push(obj);
                }
              });
              studentsList.firstLoad = false;
              return students;
            });
          }
        });
      };

      studentsList.reloadFilters = function () {
        if (!studentsList.timeoutOn) {
          $timeout(function () {
            studentsList.timeoutOn = false;
            studentsList.stringFilter = '';
            for (var key in studentsList.filters) {
              if (_.isArray(studentsList.filters[key])) {
                if (studentsList.filters[key].length > 0) {
                  var arrayString = '';
                  for (var i = 0; i < studentsList.filters[key].length; i++) {
                    if (i > 0) { arrayString += ','; }
                    arrayString += studentsList.filters[key][i].id;
                  }
                  studentsList.stringFilter += '/' + key + '=IN=' + arrayString;
                }
              } else {
                if (studentsList.filters[key]) {
                  studentsList.stringFilter += '/' + key + '==' + studentsList.filters[key];
                }
              }
            }
            studentsList.stringFilter = studentsList.stringFilter.slice(1);
            studentsList.loadData();
          }, 200);
        }
        studentsList.timeoutOn = true;
      };

      studentsList.loadData();
    }]);
})();
