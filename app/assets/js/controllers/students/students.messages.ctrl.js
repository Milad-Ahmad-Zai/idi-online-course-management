(function () {
  'use strict';

  angular
    .module('ctrl.students.messages', [])

    .controller('StudentsMessagesCtrl', ['$scope', '$state', '$rootScope', '$sce', '$modal', 'cfpLoadingBar', 'NgTableParams', 'Notification', 'AuthService', 'MessageService', function ($scope, $state, $rootScope, $sce, $modal, cfpLoadingBar, NgTableParams, Notification, AuthService, MessageService) {
      
      $scope.studentId = $state.params.id;
      
      // get all message scopes
      if (!$rootScope.allMessageScopes) {
        $rootScope.allMessageScopes = [{
          id: '',
          name: ''
        }];
        MessageService.getAllScope().then(function (res) {
          if (res) {
            res.data.forEach(function (item) {
              if (item.deleted == '0') {
                item.title = item.name;
                $rootScope.allMessageScopes.push(item);
              }
            });
          }
        });
      }
      
      // get messages
      $scope.getMessages = function () {
        var messageData = {
          userId : $scope.studentId,
          page : 1,
          order: 'DESC'
        };
        MessageService.getConversations(messageData).then(function (messageRes) {
          if (messageRes) {
            $scope.messages = [];
            messageRes.data.items.forEach(function (conversation) {
              if (conversation.Messages instanceof Array) {
                conversation.Messages.forEach(function (message) {
                  if (message.deleted == '0') {
                    message.id = parseInt(message.id);
                    message.scope = conversation.Conversation.scope_id;
                    message.source_id = conversation.Conversation.source_id;
                    $rootScope.allMessageScopes.forEach(function (item) {
                      if (item.id == message.scope) message.scope_name = item.name;
                    });
                    
                    var createdMoment = moment.utc(message.created_at);
                    message.date = createdMoment.toDate();
                    message.date_text = createdMoment.format('DD/MM/YYYY HH:mm:ss A');
                    message.org_text = message.text;
                    message.text = $sce.trustAsHtml(message.text);
                    
                    $scope.messages.push(message);
                  }
                });
              }
            });
            
            $scope.messagesTable = new NgTableParams({
              sorting: { date: "desc" }
            }, {
              dataset: $scope.messages
            });
          }
        });
      };
      
      if ($scope.studentId && $scope.checkRights('profiles.messages')) {
        $scope.getMessages();
      }
      
    }]);
})();