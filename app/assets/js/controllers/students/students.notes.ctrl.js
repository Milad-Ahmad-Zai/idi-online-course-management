(function () {
  'use strict';

  angular
    .module('ctrl.students.notes', [])

    .controller('StudentsNotesCtrl', ['$scope', '$state', '$rootScope', '$sce', '$modal', 'cfpLoadingBar', 'NgTableParams', 'Notification', 'AuthService', 'CoursesService', 'AdmissionsService', 'NotesService', 'SemestersService', function ($scope, $state, $rootScope, $sce, $modal, cfpLoadingBar, NgTableParams, Notification, AuthService, CoursesService, MessageService, NotesService, SemestersService) {

      $scope.studentId = $state.params.id;

      // get academic notes
      $scope.note = {};
      $scope.noteActions = [
        { id: 'General', title: 'General'},
        { id: 'Email', title: 'Email' },
        { id: 'Phone Call', title: 'Phone Call'},
        { id: 'Retake', title: 'Retake'},
        { id: 'Deferral', title: 'Deferral'},
        { id: 'Rests', title: 'Rests'},
        { id: 'Study Concern', title: 'Study Concern'},
        { id: 'Complaint', title: 'Complaint'},
        { id: 'Technical', title: 'Technical'}
      ];
      $scope.getNotes = function () {
        $scope.notes = [];
        NotesService.getLatestNoteSourceId($scope.studentId, 'student').then(function(res) {
          if (res) {
            res.data.forEach(function (item) {
              var createdMoment = moment.utc(item.created_at);
              item.date = createdMoment.toDate();
              item.date_text = createdMoment.format('DD/MM/YYYY HH:mm:ss A');

              $scope.notes.push({
                id: item.id,
                action: item.title,
                comments: item.body,
                logged_by: (item.created_by_data.first_name || '') + ' ' + (item.created_by_data.last_name || ''),
                date_string: item.date_text,
                date: item.date
              });
            });

            $scope.notesTable = new NgTableParams({
              sorting: { date: "desc" }
            }, {
              dataset: $scope.notes
            });
          }
        });
      };

      // create new note
      $scope.addNote = function () {
        $scope.addNoteModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/note.modal.html',
          show: false
        });

        $scope.addNoteModal.$promise.then($scope.addNoteModal.show);
      };

      // save notes
      $scope.saveNotes = function (form, isClose) {
        cfpLoadingBar.start();

        var data = {
          source: 'student',
          source_id: $scope.studentId,
          body: $scope.note.comment,
          title: $scope.note.action,
          created_by: $rootScope.userId
        };

        NotesService.createNote(data).then(function (res) {
          if (res) {
            $scope.notes.push({
              id: res.data.id,
              action: $scope.note.action,
              comments: $scope.note.comment,
              logged_by: $rootScope.fullName,
              date: new Date(),
              date_string: moment().format('DD/MM/YYYY HH:mm:ss A')
            });

            $scope.notesTable = new NgTableParams({
              sorting: { date: "desc" }
            }, {
              dataset: $scope.notes
            });

            form.$setPristine();
            form.$setUntouched();
            $scope.note = {};

            if (isClose) {
              $scope.addNoteModal.$promise.then($scope.addNoteModal.hide);
            }

            cfpLoadingBar.complete();
            Notification.success({
              title: 'Success',
              message: 'Topic successfully updated.'
            });
          }
        });
      };

      // get semester list
      var semesterConfig = {
        start: 0,
        limit: 9999,
        sort: 'id'
      };
      $scope.getSemesterList = function () {
        if ($rootScope.semesterList.length == 0) {
          SemestersService.getSemestersList(semesterConfig).then(function (res) {
            if (res) {
              $rootScope.semesterList = res.data.list;
            }
          });
        }
      };

      if ($scope.studentId && $scope.checkRights('profiles.academic_notes')) {
        $scope.getNotes();
      }

    }]);
})();