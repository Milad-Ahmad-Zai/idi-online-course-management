(function() {

  angular
  .module('ui.routes')
  .config(function($stateProvider) {
    $stateProvider
    .state('app.admin.students', {
      url: '/students',
      abstract: true,
      views: {
        'content@app': {
          controller: 'StudentsRecordCtrl',
          templateUrl: '/partials/students/index.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.students.record', files: ['/assets/js/controllers/students/students.record.ctrl.js']},
            {
              name: 'angularjs-dropdown-multiselect',
              files: ['/assets/js/vendor/angular/angularjs-dropdown-multiselect.js']
            },
            {name: 'dndLists', files: ['/assets/js/vendor/angular/angular-drag-and-drop-lists.js']},
            {name: 'srs.filter', files: ['/assets/js/directives/srs/filter.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.get', 'forums.forumSelf.get'],
        }
      }
    })
    .state('app.admin.students.list', {
      url: '',
      views: {
        'subcontent@app.admin.students': {
          controller: 'StudentsListCtrl as studentsList',
          templateUrl: '/partials/students/students.list.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.students.record.list', files: ['/assets/js/controllers/students/students.list.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.get', 'forums.forumSelf.get'],
        }
      }
    })
  })
})();