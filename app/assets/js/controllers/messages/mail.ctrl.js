(function () {
  'use strict';

  angular
    .module('ctrl.mail', ['cfp.loadingBar', 'message.service'])
    .controller('MailCtrl',
      function ($scope, $rootScope, cfpLoadingBar, $timeout, $state, $sce, user, permissions, MessageService, ProfilesService, AuthService, PermissionsService) {

      cfpLoadingBar.start();
      $rootScope.expanded = true;
      $scope.short = 360;
      $scope.isDesc = true;

      $scope.messages = [];
      $scope.userList = [];
      $scope.groupList = [];
      $scope.filter = {};
      $scope.searchBy = 'keyword';
      $scope.messageScopes = [];
      $scope.pagination = {};
      $scope.limit = 25;
      $scope.newMessageScope = [];

      var usersConfig = {
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id'
      };

      $rootScope.currentScope = $state.params.category;
      if ( $state.is('app.admin.mail.search') ) {
        $scope.category = 'Search';
      }

      // update conversation
      $scope.loadConversations = function (param) {
        if ($state.params.category) {
          $scope.category = $rootScope.messageScopeObj.name;
        }

        MessageService.getConversations(param).then(function (res) {
          if (res) {
            $scope.messages = [];
            var items = res.data.items;
            var filter = $state.params.keyword;

            // pagination of conversation list
            $scope.pagination = res.data.pagination;

            items.forEach(function (conv, index) {
              var count = conv.Messages.length;
              // get last message
              for (var ctr = count - 1; ctr >= 0; ctr--) {
                var mes = conv.Messages[ctr];
                if (mes.deleted == 0 || (mes.deleted == 1 && $scope.currentPermissions.indexOf('messages.messageAny.get') > -1)) {
                  var createdMoment = moment.utc(mes.created_at);
                  mes.date = createdMoment.toDate();

                  if (filter) {
                    mes.text = mes.text.replace(filter, '<span class="filtered-text">' + filter + '</span>');
                  }
                  mes.text = $sce.trustAsHtml(mes.text);

                  conv.LastMessage = mes;
                  conv.additionalData = {};
                  var tempUserData = [];
                  conv.additionalData.convUsersData = conv.ConversationUsers.filter(function (сonversationUser) {
                    if (сonversationUser.user_id !== user.id) tempUserData.push(сonversationUser.user_name);

                    return сonversationUser.user_id !== user.id;
                  });
                  conv.additionalData.convUsers = tempUserData.join(', ');
                  conv.date = new Date(mes.date);

                  break;
                }
              }

              conv.read = true;
              $scope.checkFlagAndRead(conv);
              $scope.messages.push(conv);
            });
            cfpLoadingBar.complete();
          }
        });
      };

      $scope.checkFlagAndRead = function (conversation) {
        conversation.ConversationUsers.forEach(function (сonversationUser) {
          var userId = сonversationUser.user_id;
          if (user.id == userId) {
            conversation.userIsSubscribed = true;
            if (сonversationUser.ReadUnread) { // unread messages counter
              var unread = parseInt(conversation.Conversation.message_count) - parseInt(сonversationUser.ReadUnread.message_count);

              if (unread > 0) {
                conversation.read = false;
              }
            } else { // conversation is unread if ReadUnread json object is null
              conversation.read = false;
            }

            if (сonversationUser.flagged == 1) { // flag messages
              conversation.isFlag = true;
            }
          }
        });
      };

      $rootScope.messageScopeObj = {};
      $scope.updateMessageScope = function () {
        //$rootScope.oldMessageScopeName = $rootScope.messageScopeObj.name_alias;
        $scope.messageScopes.forEach(function (mes) {
          if ($state.params.category.indexOf(mes.name_alias) > -1) {
            $rootScope.messageScopeObj = mes;
          }
        });

        if ($rootScope.oldMessageScopeName != $state.params.category) {
          $scope.isDesc = true;
          $scope.pagination = {};
        }
      };

      $scope.getMessageConversation = function () {
        $scope.updateMessageScope();

        var order = $scope.isDesc ? 'DESC' : 'ASC';
        if ($rootScope.messageScopeObj.id) {
          if (!$scope.pagination.current_page) $scope.pagination.current_page = 1;
          var data = {
            userId : user.id,
            scopeId : $rootScope.messageScopeObj.id,
            page : $scope.pagination.current_page,
            order: order,
            category: 'none'
          };

          if ($state.params.category.indexOf('other') > -1) {
            data.category = 'other';
          } else if ($state.params.category.indexOf('new') > -1) {
            data.userId = 'all';
            data.category = 'unreplied';
          }

          $scope.loadConversations(data);
        } else if ($rootScope.messageScopeObj.name_alias == 'sent-box'){
          if (!$scope.pagination.current_page) $scope.pagination.current_page = 1;
          var data = {
            userId : user.id,
            keyword : 'sent',
            page : $scope.pagination.current_page,
            limit: $scope.limit,
            order: order,
            category: 'none'
          };

          $scope.loadConversations(data);
        } else if ($rootScope.messageScopeObj.name_alias == 'starred'){
          if (!$scope.pagination.current_page) $scope.pagination.current_page = 1;
          var data = {
            userId : user.id,
            keyword : 'flagged',
            page : $scope.pagination.current_page,
            limit: $scope.limit,
            order: order,
            category: 'none'
          };

          $scope.loadConversations(data);
        }
      };

      $scope.getMessageScopes = function () {
        MessageService.getUserMessageScope().then(function (res) {
          if (res) {
            $scope.manipulateMessageScopes(res);
          }
        });
      };

      $scope.getMessageHref = function (messageScope, message) {
        if (message.Conversation.scope_id == "13") {
          return $state.href('app.admin.courses.module.studio', {
            id: message.Conversation.source_id
          })
        } else {
          if ($scope.category.indexOf('search') == -1) {
            return $state.href('app.admin.mail.category.read', {
              category: messageScope,
              id: message.Conversation.id
            })
          } else {
            return $state.href('app.admin.mail.search.read', {
              id: message.Conversation.id
            })
          }
        }
      };

      $scope.manipulateMessageScopes = function (res) {
        $scope.messageScopes = res.data;

        // check if scope has only one conversation
        $scope.messageScopes.forEach(function (item) {
          item.expanded = false;

          // check if account is student. if yes, all scopes except module/tutor should display the message right away
          if ($scope.currentPermissions.indexOf('messages.scope.private_messages') == -1
              && (item.name_alias != 'tutor' && item.name_alias != 'module')) {
            item.expanded = true;
            item.conversationId = null;

            var data = {
              userId: user.id,
              scopeId: item.id,
              sourceId: user.id,
              category: 'none',
              page: '1/1'
            };
            MessageService.getConversations(data).then(function (res) {
              if (!res.error) {
                var items = res.data.items;
                if (items.length > 0) {
                  item.conversationId = items[0].Conversation.id;
                }
              }
            });
          }
        });

        var possibleScopes = ['Tutor', 'Finance', 'Course Coordinator', 'Student Support',
          'Technical Support', 'Academic Administrator', 'Admissions Advisor']

        if ($scope.checkRights('messages.other') && PermissionsService.has('messages.scope.private_messages')) {
          $scope.messageScopes.forEach(function (item) {
            if (_.include(possibleScopes, item.name)) {
              item.hasSubCategories = true;

              $scope.newMessageScope.push({
                id: item.id,
                name: item.name
              });
            }
          });
        }

        if (PermissionsService.has('messages.messages.sent')) {
          $scope.messageScopes.push({
            name: 'Sent Box',
            name_alias: 'sent-box',
            expanded: false
          });
        }

        $scope.messageScopes.push({
          name: 'Starred',
          name_alias: 'starred',
          expanded: false
        });
      };

      $scope.getAllGroup = function () {
        if (PermissionsService.has('messages.conversationAny.search')) {
          /*$scope.updateUserList();*/

          MessageService.getAllGroup().then(function (res) {
            if (res) {
              var groupList = res.data;
              groupList.forEach(function (item) {
                $scope.groupList.push({
                  id: item.id,
                  name: item.name,
                  type: 'group'
                });
              });
            }
          });
        }
      };

      // get current user
        MessageService.getUserMessageScope().then(function (res) {
          if (res) {
            $scope.manipulateMessageScopes(res);

            if ( $state.is('app.admin.mail.category') ) {
              $rootScope.oldMessageScopeName = $state.params.category;
              $scope.getMessageConversation();
            }

            if ( $state.is('app.admin.mail.category.read') ) {
              $scope.updateMessageScope();
            }
          }
        });

        delete $scope.searchScope;
        delete $scope.searchCategory;

        if ( $state.is('app.admin.mail.search') ) {
          $scope.category = 'Search ' + $state.params.keyword;
          $scope.getFilterMessages();
        }

      $scope.getAllGroup();

      $rootScope.$on('$stateChangeSuccess', function (event, data) {
        $timeout(function () {
          cfpLoadingBar.complete();
        }, 500);

        $scope.toggle = null;

        if( $state.is('app.admin.mail.category') ) {
          $rootScope.expanded = true;
          $scope.filter = {};
          $scope.searchBy = 'keyword';
          $rootScope.currentScope = $state.params.category;
          $scope.getMessageConversation();
          $scope.messages = [];
        }

        if( $state.is('app.admin.mail.search.read') ) {

        }

        if ( $state.is('app.admin.mail.category.read') ) {
          $scope.updateMessageScope();
        }

        delete $scope.searchScope;
        delete $scope.searchCategory;

        delete $scope.searchScope;
        delete $scope.searchCategory;

        if ( $state.is('app.admin.mail.search') ) {
          $rootScope.expanded = true;
          $scope.messages = [];
          $scope.getFilterMessages();
          $rootScope.messageScopeObj = {};
        }
      });

      $rootScope.$on('$stateChangeStart', function (event, data) {
        window.scrollTo(0,0);
        if ( $state.is('app.admin.mail.category') && $state.params.category != 'starred' ) {
          $rootScope.oldMessageScopeName = $state.params.category;
        }
        $scope.show_page = false;
        cfpLoadingBar.start();
      });

      $scope.expandToggle = function (e) {
        e.preventDefault();
        $scope.expanded = !$scope.expanded;
      };

      $scope.toggleAll = function () {
        $scope.messages.forEach(function (messages) {
          messages.check = !messages.check;
        });
      };

      // show read and unread buttons
      $scope.showMarkMessages = function(status) {
        var hasStatus = false;
        var hasCheck = false;
        $scope.messages.forEach(function (message) {
          if (message.check) {
            hasCheck = true;
            if (status != message.read) {
              hasStatus = true;
            }
          }
        });

        if (hasCheck) {
          return hasStatus;
        } else {
          return true;
        }
      };

      // mark messages as read/unread
      var lastCheckedMessage = '';
      $scope.markMessageAsReadUnread = function(status) {
        // get last checked message
        lastCheckedMessage = '';
        $scope.messages.forEach(function (message, index) {
          if (message.check) {
            lastCheckedMessage = message.Conversation.id;
          }
        });

        $scope.messages.forEach(function (message, index) {
          if (message.check) {
            $scope.readMessage(status, message);
          }
        });
      };

      $scope.deleteConversation = function () {
        $scope.messages.forEach(function (message, index) {
          if (message.check) {
            MessageService.deleteConversation(message.Conversation.id).then(function (res) {});
          }
        });
      };

      $scope.readMessage = function(status, message) {
        var data = {
          userId: user.id,
          conversationId: message.Conversation.id,
          param: status ? 'read' : 'unread'
        };
        MessageService.optionMessage(data).then(function (res) {
          if (res) {
            if (res.data.Success) {
              message.read = status;
              if (message.Conversation.id == lastCheckedMessage) {
                $rootScope.getUnreadMessageCount();
              }
            }
          }
        });
      };

      // flag/unflag messages
      $scope.flagMessage = function (message) {
        var data = {
          userId: user.id,
          conversationId: message.Conversation.id,
          param: message.isFlag ? 'unflag' : 'flag'
        };
        MessageService.optionMessage(data).then(function (res) {
          if (res) {
            if (res.data.Success) {
              message.isFlag = !message.isFlag;
            }
          }
        });
      };

      $scope.pagingConversation = function (dir) {
        var page = 1;
        var isPaging = false;
        var order = $scope.isDesc ? 'DESC' : 'ASC';
        if (dir == 'right' && $scope.pagination.current_page < $scope.pagination.total_pages) {
          page = $scope.pagination.current_page + 1;
          isPaging = true;
        } else if (dir == 'left' && $scope.pagination.current_page > 1) {
          page = $scope.pagination.current_page - 1;
          isPaging = true;
        }

        if (isPaging) {
          var data = {
            userId : user.id,
            page : page,
            limit: $scope.limit,
            order: order,
            category: 'none'
          };

          var category = $state.params.category;
          if ($state.is('app.admin.mail.search')) {
            category = $state.params.searchCategory;
          }

          if (category.indexOf('other') > -1) {
            data.category = 'other';
          } else if (category.indexOf('new') > -1) {
            data.userId = 'all';
            data.category = 'unreplied';
          }

          if ($scope.filter.keyword) {
            data.keyword = $scope.filter.keyword;
          }

          if ($rootScope.messageScopeObj) {
            data.scopeId = $rootScope.messageScopeObj.id;
          }

          if ($state.is('app.admin.mail.search')) {
            data.scopeId = $scope.searchScope;
          }

          if ($scope.searchBy == 'user') {
            data.userId = $scope.filter.user;
          }

          cfpLoadingBar.start();
          $scope.loadConversations(data);
          $timeout(function () {
            window.scrollTo(0,0);
            cfpLoadingBar.complete();
          }, 500);
        }
      };

      $scope.sortConversation = function () {
        $scope.isDesc = !$scope.isDesc;
        var order = $scope.isDesc ? 'DESC' : 'ASC';

        var data = {
          userId : user.id,
          page : $scope.pagination.current_page,
          limit: $scope.limit,
          order: order,
          category: 'none'
        };

        var category = $state.params.category;
        if ($state.is('app.admin.mail.search')) {
          category = $state.params.searchCategory;
        }

        if (category.indexOf('other') > -1) {
          data.category = 'other';
        } else if (category.indexOf('new') > -1) {
          data.userId = 'all';
          data.category = 'unreplied';
        }

        if ($scope.filter.keyword) {
          data.keyword = $scope.filter.keyword;
        }

        if ($rootScope.messageScopeObj) {
          data.scopeId = $rootScope.messageScopeObj.id;
        }

        if ($state.is('app.admin.mail.search')) {
          data.scopeId = $scope.searchScope;
        }

        if ($state.params.category == 'starred') {
          data.keyword = 'flagged';
        }

        if ($scope.searchBy == 'user') {
          data.userId = $scope.filter.user;
        }

        cfpLoadingBar.start();
        $scope.loadConversations(data);
        $timeout(function () {
          window.scrollTo(0,0);
          cfpLoadingBar.complete();
        }, 500);
      };

      $scope.getFilterMessages = function () {
        cfpLoadingBar.start();
        $scope.searchBy = $state.params.searchBy;

        $scope.searchScope = $state.params.searchScope;
        $scope.searchCategory = $state.params.searchCategory;

        if ($scope.searchBy == 'keyword') {
          $scope.category = 'search-' + $state.params.keyword;
          $scope.filter.keyword = $state.params.keyword;

          var data = {
            userId : user.id,
            scopeId : $scope.searchScope,
            keyword: $state.params.keyword,
            page: 1,
            order: $scope.isDesc ? 'DESC' : 'ASC'
          };

          if ($scope.searchCategory.indexOf('new') > -1) {
            data.userId = 'all';
            data.category = 'unreplied';
          } else if ($scope.searchCategory.indexOf('other') > -1) {
            data.category = 'other';
          }

          $scope.loadConversations(data);

        } else if ($scope.searchBy == 'user') {
          $scope.category = 'search-by-user';
          $scope.filter.user = $state.params.keyword;

          var data = {
            userId : $scope.filter.user,
            scopeId : $scope.searchScope,
            page: 1,
            order: $scope.isDesc ? 'DESC' : 'ASC'
          };

          if ($scope.searchCategory.indexOf('other') == -1 && $scope.searchCategory.indexOf('new') == -1) {
            $scope.loadFilteredConversation($state.params.keyword);
          } else {
            if ($scope.searchCategory.indexOf('new') > -1) {
              data.category = 'unreplied';
            }
            $scope.loadConversations(data);
          }

        } else if ($scope.searchBy == 'group') {
          $scope.category = 'search-by-group';
          $scope.filter.group = $state.params.keyword;
          MessageService.getGroupUsers($state.params.keyword).then(function (res) {
            var list = res.data;
            var group = '';
            list.forEach(function (groupUser, index) {
              group += groupUser.user_id;
              if (index < list.length - 1) {
                group += '/';
              }
            });

            $scope.loadFilteredConversation(group);
          });
        }
      };

      $scope.updateFilterBy = function (searchBy) {
        $scope.searchBy = searchBy;
      };

      $scope.filterByKeyword = function (e) {
        if (e.which === 13) {
          if ($scope.filter.keyword.length > 0 && $scope.filter.keyword.length < 4) {
            return false;
          }

          var searchScope = $rootScope.messageScopeObj.id;
          if ($scope.searchScope) {
            searchScope = $scope.searchScope;
          }

          $state.go('app.admin.mail.search', {searchScope: searchScope, searchCategory: $rootScope.currentScope, searchBy: $scope.searchBy, keyword: $scope.filter.keyword});
        }
      };

      $scope.filterByUser = function (item) {
        $scope.filter.user = item.id;
        var searchScope = $rootScope.messageScopeObj.id;
        if ($scope.searchScope) {
          searchScope = $scope.searchScope;
        }

        $state.go('app.admin.mail.search', {searchScope: searchScope, searchCategory: $rootScope.currentScope, searchBy: $scope.searchBy, keyword: $scope.filter.user});
      };

      $scope.filterByGroup = function (item) {
        $scope.filter.group = item.id;
        $state.go('app.admin.mail.search', {searchScope: $rootScope.messageScopeObj.id, searchCategory: $rootScope.currentScope, searchBy: $scope.searchBy, keyword: $scope.filter.group});
      };

      $scope.loadFilteredConversation = function (fromUsers) {
        $scope.messages = [];
        MessageService.getConversationFromUser(user.id, fromUsers).then(function (res) {
          if (res) {
            var items = res.data;
            var filter = $state.params.keyword;

            // pagination of conversation list
            $scope.pagination = res.data.pagination;

            items.forEach(function (conv, index) {
              var count = conv.Messages.length;
              if (count > 0) { // check if there are messages
                // get last message
                for (var ctr = count - 1; ctr >= 0; ctr--) {
                  var mes = conv.Messages[ctr];
                  if (mes.deleted == 0 || (mes.deleted == 1 && $rootScope.currentPermissions.indexOf('messages.conversationAny.search') > -1)) {
                    var createdMoment = moment.utc(mes.created_at);
                    mes.date = createdMoment.toDate();

                    if (filter) {
                      mes.text = mes.text.replace(filter, '<span class="filtered-text">' + filter + '</span>');
                    }
                    mes.text = $sce.trustAsHtml(mes.text);

                    conv.LastMessage = mes;
                    conv.additionalData = {};
                    var tempUserData = [];
                    conv.additionalData.convUsersData = conv.ConversationUsers.filter(function (conversationUser) {
                      if (conversationUser.user_id !== user.id) tempUserData.push(conversationUser.user_name);

                      return conversationUser.user_id !== user.id;
                    });
                    conv.additionalData.convUsers = tempUserData.join(', ');
                    conv.date = new Date(mes.date);

                    break;
                  }
                }

                conv.read = true;
                $scope.checkFlagAndRead(conv);
                $scope.messages.push(conv);
              }
            });
          }

          cfpLoadingBar.complete();
        });
      };

      $scope.loadConversationFromUsers = function () {
        if ($scope.userMessage) {
          var order = $scope.isDesc ? 'DESC' : 'ASC';
          var data = {
            userId : $scope.userMessage.id,
            page : $scope.pagination.current_page,
            order: order
          };

          $scope.loadConversations(data);
        } else {
          $scope.messages = [];
          $scope.pagination = {
            current_page: 1,
            items_count: 0,
            limit: 25,
            start: 0,
            total_items: 0,
            total_pages: 0
          };
          cfpLoadingBar.complete();
        }
      };

      $scope.getUserMessages = function (item) {
        cfpLoadingBar.start();
        $scope.userMessage = item;
        $scope.loadConversationFromUsers();
      };

      $scope.refreshUsers = function (userText) {
        usersConfig.filter = 'name==' + userText;
        usersConfig.sort = 'name';
        $scope.updateUserList();
      };

      $scope.updateUserList = function () {
        AuthService.getUsersList(usersConfig).then(function (res) {
          var userList = res.data.list;
          userList.forEach(function (userFromList) {
            ProfilesService.getBasicProfile(userFromList.id).then(function (userRes) {
              if (!userRes.error && userRes.data) {
                if (userRes.data.first_name || userRes.data.last_name) {
                  if (!userRes.data.first_name) userRes.data.first_name = '';
                  if (!userRes.data.last_name) userRes.data.last_name = '';
                  var isUserExist = false;
                  $scope.userList.forEach(function (user) {
                    if (user.id == userRes.data.id) {
                      isUserExist = true;
                    }
                  });

                  if (!isUserExist) {
                    $scope.userList.push({
                      id: userRes.data.id,
                      name: userRes.data.first_name + ' ' + userRes.data.last_name
                    });
                  }
                }
              }
            });
          });
        });
      };
    });
})();