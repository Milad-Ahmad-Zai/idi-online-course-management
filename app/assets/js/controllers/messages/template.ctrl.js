(function () {
  'use strict';
  
  angular
    .module('ctrl.mail.template', ['cfp.loadingBar', 'message.service'])
    .controller('MessageTemplateCtrl', ['$scope', '$state', '$rootScope', '$filter', '$sce', '$interval', 'cfpLoadingBar', 'MessageService', 'ngTableParams', 'Notification', function ($scope, $state, $rootScope, $filter, $sce, $interval, cfpLoadingBar, MessageService, ngTableParams, Notification) {
      $scope.$parent.category = 'Templates';
      $rootScope.expanded = false;
      $scope.isListing = true;
      $scope.userId = '';
      
      $scope.displayTemplates = function () {
        MessageService.getTemplates($scope.userId).then(function (res) {
          if (res) {
            $scope.templates = [];
            res.data.items.forEach(function (template) {
              if (template.deleted == '0') {
                template.text = $sce.trustAsHtml(template.text);
                $scope.templates.push(template);
              }
            });
            
            $scope.templateTable = new ngTableParams({
              page: 1,
              count: 10,
              sorting: {
                id: 'desc'
              }
            }, {
              total: $scope.templates.length,
              getData: function($defer, params) {
                var orderedData = params.sorting() ? $filter('orderBy')($scope.templates, params.orderBy()) : $scope.templates;
                return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
              }
            });

            cfpLoadingBar.complete();
          }
        });
      };

      var userInterval = $interval(function () {
        if ($scope.$parent.userId) {
          cfpLoadingBar.start();
          $scope.userId = $scope.$parent.userId;
          $scope.displayTemplates();
          $interval.cancel(userInterval);
        }
      }, 500);
      
      $scope.deleteTemplate = function (id) {
        cfpLoadingBar.start();
        MessageService.deleteTemplate(id).then(function (res) {
          if (res) {
            $scope.displayTemplates();
            cfpLoadingBar.complete();
            Notification.success({
              message: 'Template has been successfully deleted.',
              title: "Success"
            });
          }
        });
      };
      
      $scope.editTemplate = function (id) {
        $scope.isListing = false;

        $scope.template = {};
        $scope.title = 'Add new template';
        if (id > 0) {
          $scope.templates.forEach( function (item) {
            if (item.id == id) {
              $scope.template = {
                subject: item.topic,
                message: item.text,
                id: item.id,
              };
            }
          });

          $scope.title = 'Edit template';
        }
      };
      
      $scope.cancelEdit = function () {
        $scope.template = {};
        $scope.isListing = true;
      };
      
      $scope.submitTemplateForm = function (form) {
        if (form.$invalid) {
          return;
        }
        
        cfpLoadingBar.start();
        $scope.templateFormSubmitted = true;
        
        if ($scope.template.id) {
          var data = {
            'id': $scope.template.id,
            'topic': $scope.template.subject,
            'text': $scope.template.message,
            'scope_id': $scope.userId,
            'deleted': false
          };
          
          MessageService.updateTemplate(data).then(function (res) {
            if (res) {
              $scope.editSuccessful(true, 'Template has been successfully updated.');
            } else {
              $scope.editSuccessful(false, "There's an error processing your request.");
            }
          });
        } else {
          var data = {
            'topic': $scope.template.subject,
            'text': $scope.template.message,
            'scope_id': $scope.userId
          };
          
          MessageService.addTemplate(data).then(function (res) {
            if (res) {
              $scope.editSuccessful(true, 'Template has been successfully created.');
            } else {
              $scope.editSuccessful(false, "There's an error processing your request.");
            }
          });
        }
      };
      
      $scope.editSuccessful = function (status, message) {
        $scope.displayTemplates();
        $scope.isListing = true;
        $scope.templateFormSubmitted = false;
        $scope.template = {};

        cfpLoadingBar.complete();
        if (status) {
          Notification.success({ 
            message: message,
            title: "Success"
          });
        } else {
          Notification.error({ 
            message: message,
            title: "Error"
          });
        }
      };
      
    }]);
})();