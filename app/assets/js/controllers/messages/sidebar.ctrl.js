(function () {
  'use strict';
  
  angular
    .module('ctrl.mail.sidebar', ['cfp.loadingBar', 'message.service'])
    .controller('SideBarMailCtrl', ['$scope', '$state', '$rootScope', 'MessageService', 'MessageScope', function ($scope, $state, $rootScope, MessageService, MessageScope) {
      $scope.profileImage = '';
      $scope.me = {};
      $scope.$watch(function() {
        return MessageScope.get('me');
      }, function(newValue, oldValue) {
        if (!_.isUndefined(newValue)) {
          $scope.me = MessageScope.get('me');
          $scope.profileImage = $scope.me.profileImage;
          $scope.categoryScopes = MessageService.getCategoryScope($scope.me.id);
          $scope.categoryScopes.forEach( function (cat) {
            if (cat.expanded) {
              var data = { 
                userId : $scope.me.id,
                scopeId : cat.scope_id,
                page : '1/1'
              };
            
              if (cat.source_id) {
                data.sourceId = cat.source_id;
              }
              
              MessageService.getConversations(data).then(function (res) {
                if (res) {
                  var items = res.data.items;
                  if (items.length > 0) {
                    cat.conversationId = items[0].Conversation.id;
                  }
                }
              });
            }
          });
          MessageScope.store('categoryScopes', $scope.categoryScopes);
        }
      });
    }])
})();