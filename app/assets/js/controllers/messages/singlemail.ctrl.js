(function () {
  'use strict';

  angular
    .module('ctrl.mail.single', ['cfp.loadingBar', 'message.service', 'angularFileUpload'])
    .controller('SingleMailCtrl', function ($scope, $state, $stateParams, $rootScope, $interval, $timeout, $sce, $window, $modal, $filter, cfpLoadingBar, Notification, user, MessageService, FilesService, FileUploader, AuthService, ProfilesService) {
      $rootScope.expanded = false;
      $scope.isForwarded = false;
      $scope.messages = [];
      $scope.files = [];
      $scope.users = [];
      $scope.fileIds = [];
      $scope.tempFileIds = [];
      $scope.reply = {};
      //$rootScope.messageScopeObj = {};
      $scope.templates = [];
      $scope.userList = [];
      $scope.userGroupList = [];
      $scope.recipients = {
        to: []
      }


      $scope.conversation = {};
      $scope.messageIsDesc = false;

      var usersConfig = {
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id'
      };


      if ($stateParams.to) {
        $scope.userGroupList.push({
          id: $stateParams.to.id,
          name: ($stateParams.to.first_name || '') + ' ' + ($stateParams.to.last_name || ''),
          type: 'user'
        });

        $scope.recipients.to = [$scope.userGroupList[0]];
      }

      $scope.loadConversation = function () {
        cfpLoadingBar.start();
        $scope.conversation_id = $state.params.id;

        MessageService.getConversation($scope.conversation_id).then(function (res) {
          if ($rootScope.messageScopeObj.name) {
            $scope.$parent.category = $rootScope.messageScopeObj.name;
          }

          if (res) {
            var message = res.data;

            if ($rootScope.messageScopeObj) {
              //if ($state.params.category.indexOf('search') == -1 &&
              if (!$state.is('app.admin.mail.search.read') &&
                  (message.Conversation.scope_id != $rootScope.messageScopeObj.id && $rootScope.messageScopeObj.name_alias != 'sent-box' && $rootScope.messageScopeObj.name_alias != 'starred')) {
                $state.go('app.admin.mail.category', {category: $state.params.category});
                Notification.error({
                  message: 'Could not load the message.',
                  title: "Error"
                });

              } else {
                $scope.topic = message.Conversation.topic;
                // get messages on the conversation
                var messages = [];
                message.Messages.forEach(function (mes) {
                  if (mes.deleted == 0 || (mes.deleted == 1 && $rootScope.currentPermissions.indexOf('messages.messageAny.get') > -1)) {
                    var createdMoment = moment.utc(mes.created_at);
                    mes.date = createdMoment.toDate();

                    var filter = $scope.keyword;
                    if (filter) {
                      mes.text = mes.text.replace(filter, '<span class="filtered-text">' + filter + '</span>');
                    }

                    if (mes.text.indexOf('has uploaded file') > -1) {
                      mes.isUploadNotification = true;
                    }
                    mes.text = $sce.trustAsHtml(mes.text);

                    if (mes.modified_at) {
                      var modifiedMoment = moment.utc(mes.modified_at);
                      mes.modifiedDate = modifiedMoment.toDate();
                      mes.isUpdated = true;
                    }
                    messages.push(mes);
                  }
                });
                $scope.conversation = messages;

                // get users on the conversation
                $scope.isSubscribed = false;
                $scope.conversationUsers = message.ConversationUsers;
                $scope.loadUserProfiles($scope.conversationUsers)

                if ($scope.isSubscribed) {
                  $scope.$parent.readMessage(true, message);
                }

                $scope.updatePagination();

                // load attachments
                $scope.conversation.forEach(function (mes) {
                  mes.MessageAttachments.forEach(function (file) {
                    mes.files = [];
                    if (file.deleted == 0) {
                      FilesService.getFile(file.file_id).then(function (res) {
                        if (res.data.id > 0) {
                          mes.files.push(file.file_id);
                          $scope.fileIds.push(file.file_id);
                          $scope.files[file.file_id] = res.data;
                          if ($scope.$parent.textFilter) {
                            $scope.files[file.file_id].title = $scope.files[file.file_id].title.replace($scope.$parent.textFilter, '<span class="filtered-text">' + $scope.$parent.textFilter + '</span>');
                          }
                        }
                      });
                    }
                  });
                });
              }

              cfpLoadingBar.complete();
            }
          }
        });
      };

      $scope.prepareNewMessage = function () {
        if ( $state.is('app.admin.mail.new') ) {
          $scope.$parent.category = 'New Message';

          if ($rootScope.currentPermissions.indexOf('messages.messageAnyGroup.create') > -1) {
            MessageService.getAllGroup().then(function (res) {
              if (res) {
                var groupList = res.data;
                groupList.forEach(function (item) {
                  $scope.userGroupList.push({
                    id: item.id,
                    name: item.name,
                    type: 'group'
                  });
                });
              }
            });
          }

          $scope.reply.messageScope = '';
          if ($state.params.scope) {
            $scope.reply.messageScope = $state.params.scope;
          }

          cfpLoadingBar.complete();
        }
      };

      $scope.getTemplates = function () {
        MessageService.getTemplates(user.id).then(function (res) {
          if (res) {
            $scope.templates = [];

            if (res.data) {
              res.data.items.forEach(function (template) {
                if (template.deleted == '0') {
                  template.text = $sce.trustAsHtml(template.text);
                  $scope.templates.push(template);
                }
              });
            }

            $scope.reply.template = -1;
          }
        });
      };

      $scope.setUp = function () {
        if ($scope.currentPermissions.indexOf('messages.templateAny.get') > -1 || $scope.currentPermissions.indexOf('messages.templateOwn.get') > -1) {
          $scope.getTemplates(); // get user templates
        }

        if ($state.is('app.admin.mail.category.read') || $state.is('app.admin.mail.search.read')) { // read message
          if ($state.is('app.admin.mail.search.read')) {
            $scope.loadConversation();
          } else if ($state.params.category.indexOf('sent-box') > -1) {
            $rootScope.messageScopeObj = {
              name: 'Sent Box',
              name_alias: 'sent-box',
              expanded: false
            };

            $scope.loadConversation();
          } else if ($state.params.category.indexOf('starred') > -1) {
            $rootScope.messageScopeObj = {
              name: 'Starred',
              name_alias: 'starred',
              expanded: false
            };

            $scope.loadConversation();
          } else {
            MessageService.getUserMessageScope().then(function (res) {
              if (res) {
                if ($state.params.category.indexOf('other') > -1 || $state.params.category.indexOf('new') > -1) {
                  $scope.loadConversation();
                } else {
                  res.data.forEach(function (messageScope) {
                    if (messageScope.name_alias == $state.params.category) {
                      $rootScope.messageScopeObj = messageScope;

                      if ($scope.currentPermissions.indexOf('messages.scope.private_messages') == -1
                          && ($rootScope.messageScopeObj.name_alias != 'tutor' && $rootScope.messageScopeObj.name_alias != 'module')) {
                        if ($rootScope.messageScopeObj.name) {
                          $scope.$parent.category = $rootScope.messageScopeObj.name;
                        }

                        $rootScope.messageScopeObj.expanded = true;
                        $rootScope.messageScopeObj.conversationId = null;

                        var data = {
                          userId: user.id,
                          scopeId: $rootScope.messageScopeObj.id,
                          sourceId: user.id,
                          category: 'none',
                          page: '1/1'
                        };
                        MessageService.getConversations(data).then(function (res) {
                          if (!res.error) {
                            var items = res.data.items;
                            if (items.length > 0) {
                              $rootScope.messageScopeObj.conversationId = items[0].Conversation.id;
                            }

                            if ($rootScope.messageScopeObj.conversationId) {
                              if ($rootScope.messageScopeObj.conversationId == $state.params.id) {
                                $scope.loadConversation();
                              } else {
                                $state.go('app.admin.mail.category.read', {category: $state.params.category, id: $rootScope.messageScopeObj.conversationId});
                              }
                            } else {
                              if ($state.params.id) {
                                $state.go('app.admin.mail.category.read', {category: $state.params.category, id: null});
                              } else {
                                cfpLoadingBar.complete();
                              }
                            }
                          }
                        });
                      } else {
                        $scope.loadConversation();
                      }
                    }
                  });
                }
              }
            });
          }
        } else if ( $state.is('app.admin.mail.new') ) {
          $scope.prepareNewMessage();
        }
      };

      if ($state.is('app.admin.mail.search.read')) {
        if ($scope.currentPermissions.indexOf('messages.templateAny.get') > -1 || $scope.currentPermissions.indexOf('messages.templateOwn.get') > -1) {
          $scope.getTemplates(); // get user templates
        }
        $scope.loadConversation();
      } else if ($rootScope.messageScopeObj.name && $state.is('app.admin.mail.category.read')) {
        if ($scope.currentPermissions.indexOf('messages.templateAny.get') > -1 || $scope.currentPermissions.indexOf('messages.templateOwn.get') > -1) {
          $scope.getTemplates(); // get user templates
        }
        $scope.loadConversation();
      } else if ($rootScope.messageScopeObj.name && $state.is('app.admin.mail.new')) {
        if ($scope.currentPermissions.indexOf('messages.templateAny.get') > -1 || $scope.currentPermissions.indexOf('messages.templateOwn.get') > -1) {
          $scope.getTemplates(); // get user templates
        }
        $scope.prepareNewMessage();
      } else {
        $scope.setUp();
      }

      $scope.loadUserProfiles = function (users) {
        users.forEach(function (user) {
          if (user.cc == "1") {
            users.hasCC = true;
          }

          var userId = user.user_id;

          if (userId == $rootScope.user.id) {
            $scope.conversationUserId = user.id;
            $scope.isSubscribed = true;
          }
        });
      };

      $scope.limit = 10;
      $scope.updatePagination = function () {
        var total = $scope.conversation.length;
        $scope.totalPages = Math.ceil( total / $scope.limit );
        if (!$scope.messageIsDesc) {
          $scope.currentPage = $scope.totalPages;
        } else {
          $scope.currentPage = 1;
        }
        $scope.pagingMessage($scope.currentPage);
      };

      $scope.sortMessages = function () {
        $scope.messageIsDesc = !$scope.messageIsDesc;
        $scope.conversation.reverse();
        $scope.updatePagination();
      }

      $scope.getNumber = function (num) {
        return new Array(num);
      };

      $scope.pagingMessage = function (num) {
        $scope.currentPage = num;
        var startIndex = ((num - 1) * $scope.limit);
        var endIndex = (num * $scope.limit);
        $scope.messages = $scope.conversation.slice(startIndex, endIndex);
        window.scrollTo(0,0);
      };

      $scope.displayPage = function (num, isClickable) {
        if (!isClickable) {
          cfpLoadingBar.start();
          $timeout(function () {
            $scope.pagingMessage(num);
            cfpLoadingBar.complete();
          }, 100);
        }
      };

      $scope.cancel = function (e) {
        e.preventDefault();

        $scope.isForwarded = false;
        if ( $state.is('app.admin.mail.new') ) {
          $rootScope.expanded = true;
          $state.go('app.admin.mail.category', {category: $rootScope.privateMessageScope.name_alias});
        } else if ($scope.$parent.messageScopeObj.expanded) {
          $scope.reply.message = '';
          $scope.tempFileIds = [];
          uploader.clearQueue();
        } else {
          $rootScope.expanded = true;
          $state.go('app.admin.mail.category', {category: $scope.params.category});
        }
      };

      // add attachments
      var uploader = $scope.uploader = FilesService.getUploader('message', 'message', true);

      uploader.onCompleteItem = function(fileItem, response, status, headers) {
        if (!response.error || status == 200) {
          $scope.tempFileIds.push(response.data.id);
          $scope.files[response.data.id] = response.data;
        } else {
          fileItem.msg = response.msg;
        }
      };

      uploader.onBeforeUploadItem = function(item) {
        item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
      };

      FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
        return false; // true|false
      };

      $scope.removeFileId = function (item) {
        var index = uploader.queue.indexOf(item);
        var fileId = $scope.tempFileIds[index];
        if (fileId) {
          $scope.tempFileIds.splice(index, 1);
          FilesService.deleteFile(fileId).then(function(res) {});
        }
      };

      // submit the reply message form
      $scope.replyFormSubmitted = false;
      $scope.addReplyMessage = function(form) {
        if (form.$invalid) {
          return;
        }

        cfpLoadingBar.start();
        $scope.replyFormSubmitted = true;

        // send data to api
        var data = {
          'conversation_id': $scope.conversation_id,
          'user_id': $scope.$parent.userId,
          'text': $scope.reply.message + '',
          'created_at_local': new Date(),
          'deleted': 0,
          'file_ids': $scope.tempFileIds
        };

        if ($scope.conversation_id) {
          $scope.replyMessage(data, form);
        } else {
          $scope.addMessage(data);
        }
      };

      $scope.replyMessage = function (data, form) {
        MessageService.sendMessage(data).then(function (res) {
          if (res) {
            // add to scope directly
            var reply = {
              id: res.data.id,
              conversation_id: $scope.conversation_id,
              text: $sce.trustAsHtml($scope.reply.message + ''),
              read: true,
              date: new Date(),
              user_name: $rootScope.fullName,
              user_picture: $rootScope.profileImage,
              files: $scope.tempFileIds,
              user_id: $scope.$parent.userId
            };
            $scope.fileIds = $scope.fileIds.concat($scope.tempFileIds);
            if (!$scope.messageIsDesc) {
              $scope.conversation.push(reply);
            } else {
              $scope.conversation.unshift(reply);
            }
            $scope.updatePagination();

            // send notification
            var notificationData = {
              conversation_id: $scope.conversation_id,
              user_id: $scope.$parent.userId,
              msg_text: $scope.reply.message + ''
            };
            $scope.sendNotification(notificationData);

            // reset reply inputs
            form.$setPristine();
            $scope.reply.message = '';
            $scope.tempFileIds = [];
            $scope.isForwarded = false;
            $scope.isSubscribed = true;
            uploader.clearQueue();

            Notification.success({
              message: 'Your message has been successfully sent.',
              title: "Success"
            });
          } else {
            Notification.error({
              message: "There's an error processing your request.",
              title: "Error"
            });
          }

          cfpLoadingBar.complete();
          $scope.replyFormSubmitted = false;
        });
      };

      $scope.addMessage = function (data) {
        data.conversation_id = null;
        data.scope_id = $rootScope.messageScopeObj.id;
        data.source_id = $scope.$parent.userId;
        data.topic = $scope.$parent.category;

        MessageService.sendMessage(data).then(function (res) {
          if (res) {
            $rootScope.getMessageScopes();
            $scope.$parent.getMessageConversation();

            MessageService.getMessage(res.data.id).then(function (res) {
              $state.go('app.admin.mail.category.read', {category: $rootScope.messageScopeObj.name_alias, id: res.data.conversation_id});

              // send notification
              var notificationData = {
                conversation_id: res.data.conversation_id,
                user_id: $scope.$parent.userId,
                msg_text: $scope.reply.message + ''
              };
              $scope.sendNotification(notificationData);
            });

            Notification.success({
              message: 'Your message has been successfully sent.',
              title: "Success"
            });

            cfpLoadingBar.complete();
          } else {
            Notification.error({
              message: "There's an error processing your request.",
              title: "Error"
            });

            cfpLoadingBar.complete();
          }

          $scope.replyFormSubmitted = false;
        });
      };

      $scope.sendNotification = function (data) {
        MessageService.sendNotification(data).then(function (res) {});
      };

      // show edit and delete options
      $scope.showOptions = function(item) {
        var flag = false;

        var isDeleted = item.deleted + '';

        if (($rootScope.currentPermissions.indexOf('messages.messageAny.delete') > -1 || $rootScope.currentPermissions.indexOf('messages.messageAny.edit') > -1) && isDeleted == '0') {
          flag = true;
        } else if (user.id == item.user_id) {
          var diffMs = new Date() - item.date;
          var diffMins = diffMs / 60000; // minutes

          if (diffMins < 10) {
            flag = true;
          }
        }

        return flag;
      };

      // delete message
      $scope.deleteMessage = function(index) {
        cfpLoadingBar.start();
        var messageId = this.message.id;
        var messageWeNeed;
        if ($scope.messages && $scope.messages.length > 0) {
          messageWeNeed = $scope.messages.filter(function (message) {
            return message.id == messageId;
          });
          messageWeNeed = messageWeNeed[0];
        }
        MessageService.deleteMessage(messageWeNeed.id).then(function (res) {
          if (res) {
            var attachments = messageWeNeed.files;
            _.remove($scope.fileIds, function(n){
              return _.indexOf(attachments, n) > -1;
            });

            var convIndex = (($scope.currentPage - 1) * $scope.limit) + index;
            $scope.conversation.splice(convIndex, 1);
            _.remove($scope.messages, function(message){
              return message.id == messageId;
            });
            if ($scope.messages.length > 0) {
              $scope.updatePagination();

            } else if (!$rootScope.messageScopeObj.expanded) {
              $scope.$parent.getMessageScopes();
              $state.go('app.admin.mail.category', {category: $rootScope.messageScopeObj.name_alias});

            } else if ($rootScope.messageScopeObj.expanded) {
              $scope.conversation_id = null;
              $rootScope.getMessageScopes();
              $scope.$parent.getMessageScopes();
              $state.go('app.admin.mail.category.read', {category: $rootScope.messageScopeObj.name_alias, id: $scope.conversation_id});
            }

            Notification.success({
              message: 'Message has been successfully deleted.',
              title: "Success"
            });
          } else {
            Notification.error({
              message: "There's an error processing your request.",
              title: "Error"
            });
          }

          cfpLoadingBar.complete();
        });
      };

      // display edit form
      $scope.displayEditForm = function(index) {
        var messageId = this.message.id;
        var messageWeNeed;
        if ($scope.messages && $scope.messages.length > 0) {
          messageWeNeed = $scope.messages.filter(function (message) {
            return message.id == messageId;
          });
          messageWeNeed = messageWeNeed[0];
          messageWeNeed.isEdit = true;
        }
      };

      $scope.isImage = function (ext) {
        ext = '|' + ext + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(ext) !== -1;
      };

      $scope.createMessage = function (form) {
        if (form.$invalid) {
          return;
        }

        if ($scope.recipients.to.length == 0) {
          return;
        }

        cfpLoadingBar.start();
        $scope.createFormSubmitted = true;

        var toArray = [];
        var toGroupArray = [];
        var userId = $scope.$parent.userId;
        var messageText = $scope.reply.message + '';
        $scope.recipients.to.forEach(function (item) {
          if ( item.type == 'group' ) {
            toGroupArray.push(item.id);
          }

          if ( item.type == 'user' ) {
            toArray.push(item.id);
          }
        });

        // send data to api
        var data = {
          'user_id': userId,
          'text': messageText,
          'created_at_local': new Date(),
          'deleted': 0,
          'file_ids': $scope.tempFileIds,
          'topic': $scope.reply.subject,
          'to': toArray,
          'cc': $scope.recipients.cc,
          'bcc': $scope.recipients.bcc,
          'to_group': toGroupArray,
          'scope_id': $scope.reply.messageScope,
          'source_id': null
        };

        MessageService.sendMessage(data).then(function (res) {
          if (res) {
            // send notification
            MessageService.getMessage(res.data.id).then(function (res) {
              var notificationData = {
                conversation_id: res.data.conversation_id,
                user_id: userId,
                msg_text: messageText
              };
              $scope.sendNotification(notificationData);
            });

            var scope_alias = $scope.privateMessageScope.name_alias;
            $rootScope.messageScopes.forEach(function (item) {
              if (item.id == $scope.reply.messageScope) {
                scope_alias = item.name_alias;
              }
            });

            // reset reply inputs
            form.$setPristine();
            $scope.reply = {};
            $scope.recipients = {};
            $scope.tempFileIds = [];
            uploader.clearQueue();

            $state.go('app.admin.mail.category', {category: scope_alias});

            Notification.success({
              message: 'Your message has been successfully sent.',
              title: "Success"
            });
          } else {
            Notification.error({
              message: "There's an error processing your request.",
              title: "Error"
            });
          }

          cfpLoadingBar.complete();
          $scope.createFormSubmitted = false;
        });
      };

      $scope.useTemplate = function (e) {
        var scope = e ? angular.element(e.target).scope() : $scope ;
        var templateId = scope.reply.template;
        $scope.templates.forEach( function (item) {
          if (item.id == templateId) {
            $scope.reply.subject = item.topic;
            $scope.reply.message = item.text;
          }
        });
      };

      $scope.groupUsers = function (item) {
        if ( item.type == 'user' ) {
          return 'Users';
        }

        if ( item.type == 'group' ) {
          return 'Groups';
        }
      };

      $scope.setForwarded = function () {
        $scope.isForwarded = true;
      };

      $scope.unsubscribeConversation = function () {
        cfpLoadingBar.start();
        MessageService.deleteConversationUser($scope.conversationUserId).then(function (res) {
          if (res.data.Success) {
            //$state.go('app.admin.mail.category', {category: $state.params.category});
            $scope.isSubscribed = false;

            Notification.success({
              message: 'You have successfully unsubscribe on the conversation.',
              title: "Success"
            });

            cfpLoadingBar.complete();
          }
        });
      };

      $scope.subscribeConversation = function () {
        cfpLoadingBar.start();
        MessageService.subscribeConversation(user.id, $scope.conversation_id).then(function (res) {
          if (res.data) {
            //$state.go('app.admin.mail.category', {category: $state.params.category});
            $scope.isSubscribed = true;

            Notification.success({
              message: 'You have successfully subscribe on the conversation.',
              title: "Success"
            });

            cfpLoadingBar.complete();
          }
        });
      };

      $scope.refreshUsers = function (userText) {
        usersConfig.filter = 'name==' + userText;
        usersConfig.sort = 'name';
        $scope.updateUserList();
      };

      $scope.updateUserList = function () {
        AuthService.getUsersList(usersConfig).then(function (res) {
          var userList = res.data.list;
          userList.forEach(function (user) {
            ProfilesService.getBasicProfile(user.id).then(function (userRes) {
              if (!userRes.error && userRes.data) {
                if (userRes.data.first_name || userRes.data.last_name) {
                  if (!userRes.data.first_name) userRes.data.first_name = '';
                  if (!userRes.data.last_name) userRes.data.last_name = '';
                  var isUserGroupExist = false;
                  $scope.userGroupList.forEach(function (userGroup) {
                    if (userGroup.id == userRes.data.id) {
                      isUserGroupExist = true;
                    }
                  });

                  if (!isUserGroupExist) {
                    $scope.userGroupList.push({
                      id: userRes.data.id,
                      name: userRes.data.first_name + ' ' + userRes.data.last_name,
                      type: 'user'
                    });
                  }

                  var isUserExist = false;
                  $scope.userList.forEach(function (user) {
                    if (user.id == userRes.data.id) {
                      isUserExist = true;
                    }
                  });

                  if (!isUserExist) {
                    $scope.userList.push({
                      id: userRes.data.id,
                      name: userRes.data.first_name + ' ' + userRes.data.last_name
                    });
                  }
                }
              }
            });
          });
        });
      };

      $scope.displayImage = function (item) {
        $scope.imageItem = {file: item};
        if ($state.is('app.admin.mail.search.read')) {
          $scope.displayImageModal = $modal({
            scope: $scope,
            templateUrl: '/partials/modals/image.modal.html',
            show: false
          });
        } else {
          $scope.displayImageModal = $modal({
            scope: $scope,
            templateUrl: '/partials/modals/image.modal.html',
            show: false
          });
        }

        $scope.displayImageModal.$promise.then($scope.displayImageModal.show);
      };
    });
})();