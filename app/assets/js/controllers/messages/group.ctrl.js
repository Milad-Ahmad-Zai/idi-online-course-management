(function () {
  'use strict';

  angular
    .module('ctrl.mail.group', ['cfp.loadingBar', 'message.service'])
    .controller('MessageGroupCtrl', ['$scope', '$state', '$rootScope', '$interval', '$filter', 'cfpLoadingBar', 'MessageService', 'ngTableParams', 'Notification', 'AuthService', 'ProfilesService', function ($scope, $state, $rootScope, $interval, $filter, cfpLoadingBar, MessageService, ngTableParams, Notification, AuthService, ProfilesService) {
      $scope.isListing = true;
      $scope.userId = '';
      
      var usersConfig = {
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id'
      };

      var userInterval = $interval(function () {
        if ($scope.$parent.userId) {
          $scope.userId = $scope.$parent.userId;
          $interval.cancel(userInterval);
        }
      }, 500);

      // get all groups
      $scope.getAllGroup = function () {
        MessageService.getAllGroup().then(function (res) {
          $scope.groups = res.data;

          $scope.groups.forEach(function (item) {
            // get creator profile
            ProfilesService.getProfile(item.owner_id).then(function (userRes) {
              if (!userRes.error) {
                item.owner = {
                  'id': userRes.data.id + '',
                  'name': (userRes.data.first_name || '') + ' ' + (userRes.data.last_name || '')
                };
              }
            });

            // get user list on group
            $scope.getGroupUsers(item);
          });

          $scope.groupTable = new ngTableParams({
            page: 1,
            count: 10,
            sorting: {
              id: 'desc'
            }
          }, {
            total: $scope.groups.length,
            getData: function($defer, params) {
              var orderedData = params.sorting() ? $filter('orderBy')($scope.groups, params.orderBy()) : $scope.groups;
              return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
            }
          });
        });
      };
      $scope.getAllGroup();

      $scope.getGroupUsers = function (item) {
        item.userList = [];
        MessageService.getGroupUsers(item.id).then(function (res) {
          if (res) {
            var list = res.data;
            list.forEach(function (user) {
              ProfilesService.getProfile(user.user_id).then(function (userRes) {
                if (!userRes.error) {
                  item.userList.push({
                    'id': userRes.data.id + '',
                    'name': (userRes.data.first_name || '') + ' ' + (userRes.data.last_name || '')
                  });
                }
              });
            });
          }
        });
      };

      // get all users
      $scope.userList = [];
      AuthService.getUsersList(usersConfig).then(function (res) {
        var userList = res.data.list;
        userList.forEach(function (user) {
          ProfilesService.getProfile(user.id).then(function (userRes) {
            if (!userRes.error && userRes.data) {
              $scope.userList.push({
                'id': userRes.data.id + '',
                'name': (userRes.data.first_name || '') + ' ' + (userRes.data.last_name || '')
              });
            }
          });
        });
      });

      $scope.deleteGroup = function (id) {
        cfpLoadingBar.start();
        MessageService.deleteGroup(id).then(function (res) {
          if (res) {
            $scope.getAllGroup();
            cfpLoadingBar.complete();
            Notification.success({
              message: 'Message Group has been successfully deleted.',
              title: "Success"
            });
          }
        });
      };

      $scope.editGroup = function (id) {
        $scope.isListing = false;

        $scope.group = {};
        $scope.title = 'Add new group';
        if (id > 0) {
          $scope.groups.forEach( function (item) {
            if (item.id == id) {
              $scope.group = {
                id : item.id,
                name: item.name
              };

              $scope.group.userIds = [];
              item.userList.forEach(function (user) {
                $scope.group.userIds.push(user.id);
              });

              $scope.group.userIds = _.uniq($scope.group.userIds);
              $scope.group.oldUserIds = $scope.group.userIds;
            }
          });

          $scope.title = 'Edit group';
        }
      };

      $scope.cancelEdit = function () {
        $scope.group = {};
        $scope.isListing = true;
      };

      $scope.addGroupUser = function (groupId) {
        $scope.group.userIds.forEach(function (id, index) {
          var data = {
            group_id: groupId,
            user_id: id
          };

          MessageService.addGroupUser(data).then(function (res) {
            if (!res) {
              $scope.errorUpdate('An error occurred while adding user on group.');
            } else {
              if ( index == ($scope.group.userIds.length - 1) ) {
                $scope.successfulUpdate();
              }
            }
          });
        });
      };

      $scope.removeGroupUser = function () {
        if ($scope.group.oldUserIds.length > 0) {
          $scope.group.oldUserIds.forEach(function (id, index) {
            MessageService.deleteGroupUser($scope.group.id, id).then( function (res) {
              if ( index == ($scope.group.oldUserIds.length - 1) ) {
                $scope.addGroupUser($scope.group.id);
              }
            });
          });
        } else {
          $scope.addGroupUser($scope.group.id);
        }
      };

      $scope.successfulUpdate = function () {
        $scope.group = {};
        $scope.isListing = true;
        $scope.groupFormSubmitted = false;
        $scope.getAllGroup();
        cfpLoadingBar.complete();
        Notification.success({
          message: 'Message group has been successfully saved.',
          title: "Success"
        });
      };

      $scope.errorUpdate = function (msg) {
        $scope.group = {};
        $scope.isListing = true;
        $scope.groupFormSubmitted = false;
        $scope.getAllGroup();
        cfpLoadingBar.complete();
        Notification.error({
          message: msg,
          title: "Error"
        });
      };
      
      $scope.deleteUser = function (item) {
        if ($scope.group.id) {
          MessageService.deleteGroupUser($scope.group.id, item.id).then( function (res) {});
        }
      };
      
      $scope.addUser = function (item) {
        if ($scope.group.id) {
          var data = {
            group_id: $scope.group.id,
            user_id: item.id
          };

          MessageService.addGroupUser(data).then(function (res) {});
        }
      };

      $scope.submitGroupForm = function (form) {
        if (form.$invalid) {
          return;
        }

        cfpLoadingBar.start();
        $scope.groupFormSubmitted = true;

        var data = {
          name: $scope.group.name,
          owner_id: $scope.userId
        };

        if ($scope.group.id) {
          MessageService.updateGroup($scope.group.id, data).then(function (res) {
            if (res) {
              $scope.successfulUpdate();
            } else {
              $scope.errorUpdate('An error occurred while updating the group.');
            }
          });
        } else {
          MessageService.createGroup(data).then(function (res) {
            if (res) {
              $scope.addGroupUser(res.data.id);
            } else {
              $scope.errorUpdate('An error occurred while creating the group.');
            }
          });
        }
      };
      
      $scope.refreshUsers = function (userText) {
        usersConfig.filter = 'name==' + userText;
        usersConfig.sort = 'name';
        AuthService.getUsersList(usersConfig).then(function (res) {
          var userList = res.data.list;
          userList.forEach(function (user) {
            ProfilesService.getProfile(user.id).then(function (userRes) {
              if (!userRes.error && userRes.data) {
                if (userRes.data.first_name || userRes.data.last_name) {
                  $scope.userList.push({
                    id: userRes.data.id,
                    name: (userRes.data.first_name || '') + ' ' + (userRes.data.last_name || ''),
                    type: 'user'
                  });
                }
              }
            });
          });
        });
      };
    }]);
})();