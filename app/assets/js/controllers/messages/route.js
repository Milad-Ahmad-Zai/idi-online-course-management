(function () {
  angular
    .module('ui.routes')
    .config(config);

  function config($stateProvider) {
    $stateProvider
    // mail
      .state('app.admin.mail', {
        url: '/messages',
        abstract: true,
        views: {
          'content@app': {
            controller: 'MailCtrl',
            templateUrl: '/partials/messages/index.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail', files: ['/assets/js/controllers/messages/mail.ctrl.js']}
            ]);
          }
        },
        data: {
          permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }
        }
      })
      .state('app.admin.mail.new', {
        url: '/new/:scope',
        params: {
          to: null
        },
        views: {
          'subcontent': {
            controller: 'SingleMailCtrl',
            templateUrl: '/partials/messages/new.html'
          }
        },
        data: {
          permissions: {
            only: ['messages.messageAnyone.create', 'messages.messageAnyGroup.create'],
            redirectTo: 'app.login'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail.single', files: ['/assets/js/controllers/messages/singlemail.ctrl.js']}
            ]);
          },
          conversation: function (load, $state, $rootScope, $stateParams, MessageService) {
            if ($stateParams.to) {
              return MessageService.getConversationFromUser($rootScope.userId, $stateParams.to.id)
                .then(function (response) {
                  var conversations = response.data.filter(function (item) {
                    return item.Conversation.scope_id == $rootScope.admissionAdvisorScope.id;
                  })
                  if (conversations.length) {
                    $state.go('app.admin.mail.category.read', {
                      category: $rootScope.admissionAdvisorScope.name_alias,
                      id: conversations[0].Conversation.id
                    })
                  }
                })
            }
            return true;
          }
        }
      })
      .state('app.admin.mail.settings', {
        url: '/settings',
        views: {
          'content@app': {templateUrl: '/partials/messages/settings.html'},
          'templates@app.admin.mail.settings': {
            controller: 'MessageTemplateCtrl',
            templateUrl: '/partials/messages/template.html'
          },
          'groups@app.admin.mail.settings': {
            controller: 'MessageGroupCtrl',
            templateUrl: '/partials/messages/group.html'
          }
        },
        data: {
          permissions: {
            only: ['messages.group.getall', 'messages.templateAny.get'],
            redirectTo: 'app.login'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail.template', files: ['/assets/js/controllers/messages/template.ctrl.js']},
              {name: 'ctrl.mail.group', files: ['/assets/js/controllers/messages/group.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail.search', {
        url: '/search/:searchScope/:searchCategory/:searchBy/:keyword',
        data: {
          permissions: {
            only: ['messages.message.search', 'messages.users.search', 'messages.group.search'],
            redirectTo: 'app.login'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail', files: ['/assets/js/controllers/messages/mail.ctrl.js']},
            ]);
          }
        }
      })
      .state('app.admin.mail.category', {
        url: '/:category',
        data: {
          permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail.single', files: ['/assets/js/controllers/messages/singlemail.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail.category.read', {
        url: '/:id',
        views: {
          'subcontent@app.admin.mail': {
            controller: 'SingleMailCtrl',
            templateUrl: '/partials/messages/read.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail.single', files: ['/assets/js/controllers/messages/singlemail.ctrl.js']}
            ]);
          }
        },
        data: {
          permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }
        }
      })
      .state('app.admin.mail.search.read', {
        url: '/:id',
        views: {
          'subcontent@app.admin.mail': {
            controller: 'SingleMailCtrl',
            templateUrl: '/partials/messages/read.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail.single', files: ['/assets/js/controllers/messages/singlemail.ctrl.js']}
            ]);
          }
        },
        data: {
          permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }
        }
      })
    // end
  }
})()