(function () {
  'use strict';

  angular
    .module('ctrl.edit.profile', ['angularFileUpload'])

    .controller('EditProfileCtrl', function ($scope, $state, $rootScope, $filter, $modal, $sce, $window, $stateParams, user, Notification, AuthService, ProfilesService, FilesService, AdmissionsService, MessageService, publicWork, editProfile, countryList) {
      var vm = this;
      var oldProfileImage = {};
      var profileUploader = {};
      var oldCover = {}
      var uploadImage = ['profileimage', 'profilecover'];
      vm.hideUploads = true;
      vm.fbRegex = /^(https?:\/\/)?(w{3}\.)?(facebook|fb)\.[a-z\.]{2,6}\/$/;
      vm.twRegex = /^(https?:\/\/)?(w{3}\.)?(twitter)\.[a-z\.]{2,6}\/$/;
      vm.liRegex = /^(https?:\/\/)?(w{3}\.)?(linkedin)\.[a-z\.]{2,6}\/$/;
      vm.igRegex = /^(https?:\/\/)?(w{3}\.)?(instagram)\.[a-z\.]{2,6}\/$/;

      vm.getPublicFiles = function() {
        return FilesService.getFileList('publicwork',editProfile.id).then(function (res) {
          return vm.filterPublicFiles(res.data);
        });
      }

      vm.filterPublicFiles = function(files) {
        $scope.publicWork = _.filter(files.items, function (file) {
          return file.state == 1;
        });
      }

      vm.deletePublicFile = function (id) {
        FilesService.deleteFile(id).then(function (res) {
          Notification.success({message: 'File successfully deleted', title: 'Success'});
          vm.getPublicFiles();
        }).catch(function(res) {
          Notification.error({message: 'File not deleted', title: 'Error'});
        })
      };

      $scope.initializeUploader = function () {
        var uploadID = editProfile.id
        // public work uploader
        profileUploader['publicwork'] = $scope.profileUploader['publicwork'] = FilesService.getUploader('publicwork', uploadID, true);
        profileUploader['publicwork'].onCompleteItem = function(fileItem, response, status) {
          if (!response.error || status == 200) {
            var file_id = response.data.id;
            var entity_id = response.data.entity_id;
            Notification.success({ message: 'Successfully uploaded file', title: 'Success'});
            vm.getPublicFiles();
          } else {
            Notification.error({ message: 'Could not upload file', title: 'Error: ' + response.data.status});
          }
        }
        profileUploader['publicwork'].onBeforeUploadItem = function(item) {
          item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
        }
        profileUploader['publicwork'].filters.push({
          name: 'imageFilter',
          fn: function(item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
          }
        });
        $scope.puplicUploaderLoaded = true;

        uploadImage.forEach(function (image) {
          if(image == 'profilecover'){
            profileUploader[image] = $scope.profileUploader[image] = FilesService.getUploader(image, uploadID, true);
          }else{
            profileUploader[image] = $scope.profileUploader[image] = FilesService.getUploader(image, uploadID, false);
          }

          profileUploader[image].onCompleteItem = function(fileItem, response, status) {
            if (!response.error || status == 200) {
              var file_id = response.data.id;
              var entity_id = response.data.entity_id;

              if (image == 'profileimage') {
                oldProfileImage = vm.userProfile.profile_image;
                vm.userProfile.profile_image = response.data;

                if(vm.userProfile.profile_image.url) {
                  vm.userProfile.image = $sce.trustAsResourceUrl(vm.userProfile.profile_image.url);
                  $rootScope.user.profileImage = vm.userProfile.profile_image.url;
                }
                ProfilesService.updateProfileImage(entity_id, file_id).then(function(res) {

                  if (res.data.status == 500 || res.data.error) {
                    Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
                    vm.userProfile.profile_image = oldProfileImage;
                    oldProfileImage = {};
                  } else {
                    Notification.success({ message: 'Profile successfully updated' });
                  }
                }, function(res){
                  Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
                  vm.userProfile.profile_image = oldProfileImage;
                  oldProfileImage = {};
                });

              } else if (image == 'profilecover') {
                var oldCover = vm.userProfile.cover_image;
                vm.userProfile.cover_image = response.data;

                ProfilesService.updateCoverImage(entity_id, file_id).then(function(res) {
                  if (res.data.status == 500 || res.data.error) {
                    Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
                    vm.userProfile.cover_image = oldCover;
                    oldCover = {};
                  } else {
                    Notification.success({ message: 'Profile successfully updated' });
                    if(vm.userProfile.cover_image.url) vm.userProfile.cover = $sce.trustAsResourceUrl(vm.userProfile.cover_image.url);
                  }
                }, function(res){
                  Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
                  vm.userProfile.cover_image = oldCover;
                  oldCover = {};
                });
              }

              profileUploader[image].clearQueue();
            }
          };

          profileUploader[image].onBeforeUploadItem = function(item) {
            if(item.croppedImage)item._file = dataURItoBlob(item.croppedImage);
            item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
          };

          profileUploader["profileimage"].onAfterAddingFile = function(item) {
            $scope.cropProfileImageModal.$promise.then($scope.cropProfileImageModal.show);
            item.croppedImage = '';
            var reader = new FileReader();
            reader.onload = function(event) {
              $scope.$apply(function(){
                item.image = event.target.result;
              });
            };
            reader.readAsDataURL(item._file);
          };

          profileUploader[image].filters.push({
            name: 'imageFilter',
            fn: function(item) {
              var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
              return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
          });

          $scope.profileUploaderLoaded[image] = true;
        });
      };

      vm.loadProgrammeList = function () {
        ProfilesService.getProgrammeList().then(function (res) {
          $scope.programmeList = res.data;
        });
      };

      vm.updateUserProfile = function (e) {
        if(vm.socialLinks.$valid) {
          vm.userProfile.age = $filter('ageFilter')(vm.userProfile.date_of_birth);
          ProfilesService.updateProfile(vm.userProfile.id, vm.userProfile).then(function(res) {
            if (res.status == 500 || res.error) {
              Notification.error({ message: 'Profile is not updated', title: 'Error: ' + res.status});
            } else {
              Notification.success({ message: 'Profile is successfully updated' });
            }
          });
        }
      };

      vm.cancelChanges = function (e) {
        if ($stateParams.id) {
          $state.go('app.admin.profile.public', { id: $stateParams.id })
        } else {
          $state.go('app.admin.profile.public')
        }
      };

      vm.openHelp = function () {
        var modal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/help.modal.profile.html',
          show: false
        });

        modal.$promise.then(modal.show);
      };

      //profile image crop
      $scope.cropProfileImageModal = $modal({
        scope: $scope,
        templateUrl: '/partials/modals/crop.profile.image.modal.html',
        show: false
      });

      /**
       * Converts data uri to Blob. Necessary for uploading.
       * @see
       *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
       * @param  {String} dataURI
       * @return {Blob}
       */
      var dataURItoBlob = function(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for(var i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: mimeString});
      };

      $scope.getUserSummarySheet = function (id) {
        AdmissionsService.getUserSummarySheets(id).then(function (res) {
          if (!(res.error || res.data && res.data.error) && res.data) {
            var tempData;
            if (_.isArray(res.data) && res.data.length === 0) {
              $scope.summary = {};
            } else {
              tempData = res.data[0];
              for (var prop in tempData) {
                if(_.isString(tempData[prop]) && prop != 'created' && prop != 'updated') {
                  tempData[prop] = JSON.parse(tempData[prop])
                }
              }
              $scope.summary = tempData;
             // $scope.loadNote();
            }
          }
        });
      };

      vm.getAdmission = function () {
        AdmissionsService.getAdmission(editProfile.id).then(function (res) {
          if (res && res.status !== 204) {
            $scope.admissionData = res.data;
            vm.userProfile.user.state = $scope.admissionData.state = parseInt($scope.admissionData.state) == 1;
          }
        });
      };

      $scope.changeSubmitConfirmation = function () {
        var data = {};
        if ($scope.admissionData.state) {
          data = { state: 1 };
        } else {
          data = { state: 0 };
        }

        AdmissionsService.saveAdmission(editProfile.id, data).then(function (res) {
          if(res.data.error) {
            Notification.error({message: 'Submit confirmation was not updated', title: 'Error: ' + res.status});
          } else {
            Notification.success({message: 'Submit confirmation changed.', title: 'Success'});
            vm.userProfile.user.state = res.data.state;
          }
        }, function (res) {
          Notification.error({ message: 'Submit confirmation was not updated', title: 'Error'});
        });
      };

      var groupNames = ['Course Coordinator', 'Student Support', 'Technical Support', 'Academic Administrator', 'Admissions Advisor', 'Finance Advisor'];
      vm.loadManagers = function () {
        $scope.usersByGroup = [];

        groupNames.forEach(function (group) {
          var groupName = group == 'Finance Advisor' ? 'Finance' : group;
          var usersByGroupConfig = {
            start: 0,
            limit: 100,
            filter: 'group=' + groupName,
            sort: 'name'
          };
          AuthService.getUsersList(usersByGroupConfig).then(function (res) {
            if (res.data) {
              $scope.usersByGroup[group] = res.data.list
            }
          });
        });
      };

      vm.loadStudentManagers = function (profileId) {
        $scope.managers = {};
        $scope.managersId = {};
        MessageService.getStudentManagers(profileId).then(function (res) {
          if (res) {
            res.data.forEach(function (manager) {
              $scope.managers[manager.duty] = manager.user_id;
              $scope.managersId[manager.duty] = {
                id: manager.id,
                user_id: manager.user_id
              };
            });
          }
        });
      };

      init();

      function init() {
        // set variables
        vm.userProfile = editProfile;
        $scope.userProfileId = editProfile.id;
        $scope.profileUploaderLoaded = {};
        $scope.profileUploader = {};
        $scope.profilesPage = true;
        $scope.showProfileImage = true;
        $scope.wizardDisabled = true;
        vm.countries = countryList;

        $scope.ownProfile = editProfile.id == user.id;
        // set current tab
        $rootScope.currentTab = 'personal';
        if ($stateParams.type == 'decision') {
          $rootScope.currentTab = $stateParams.type;
        } else if ($stateParams.type == 'programmes') {
          $rootScope.currentTab = $stateParams.type;
        }
        // Filter out user deleted files from preloaded public work
        vm.filterPublicFiles(publicWork);
        // load necessary data
        $scope.initializeUploader();
        vm.loadProgrammeList();
        if ($scope.checkRights('profiles.managers')) {
          vm.loadManagers();
          vm.loadStudentManagers(vm.userProfile.id);
        }
        if ($scope.checkRights('admissions.summary')) {
          $scope.getUserSummarySheet(vm.userProfile.id);
          vm.getAdmission();
        }
      }
    });
})();
