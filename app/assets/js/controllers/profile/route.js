(function () {
  angular
  .module('ui.routes')
  .config(config);

  function config($stateProvider) {
    $stateProvider
    // profiles
    .state('app.admin.profile', {
      url: '/profile',
      abstract: true,
      views: {
        'content@app': {
          controller: 'ProfileCtrl',
          template: '<ui-view=""></ui-view>'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            { name: 'ctrl.profile', files: ['/assets/js/controllers/profile/profile.ctrl.js']}
          ])
        }
      },
    })
    .state('app.admin.profile.edit', {
      url: '/edit/:id/:type',
      params: {
        id: {squash: true, value: null},
        type: {squash: true, value: null}
      },
      views: {
        'content@app': {
          controller: 'EditProfileCtrl as editProfile',
          templateUrl: '/partials/profiles/index.html'
        },
        'profile@app.admin.profile.edit': {
          templateUrl: '/partials/profiles/profile.html'
        },
        'experience@app.admin.profile.edit': {
          templateUrl: '/partials/admissions/wizard/experience.html'
        },
        'history@app.admin.profile.edit': {
          templateUrl: '/partials/admissions/wizard/history.html'
        },
        'creative@app.admin.profile.edit': {
          templateUrl: '/partials/admissions/wizard/creative.html'
        },
        /*ADMI-89 'summary@app.admin.profile.edit': {
          controller: 'AdmissionsSummaryCtrl',
          templateUrl: '/partials/admissions/summary.html'
        },*/
        'decision@app.admin.profile.edit': {
          controller: 'AdmissionsDecisionCtrl',
          templateUrl: '/partials/admissions/decision.html'
        },
        'uploaded@app.admin.profile.edit': {
          controller: 'AdmissionsUploadedCtrl',
          templateUrl: '/partials/admissions/uploaded.html'
        },
        'notes_history@app.admin.profile.edit': {
          controller: 'NotesHistoryCtrl',
          templateUrl: '/partials/profiles/notes_history.html'
        },
        /* ADMI-87'uhform@app.admin.profile.edit': {
          templateUrl: '/partials/profiles/uhform.html'
        },*/
        'managers@app.admin.profile.edit': {
          templateUrl: '/partials/profiles/managers.html'
        },
        'study_path@app.admin.profile.edit': {
          controller: 'StudentsStudyPathCtrl',
          templateUrl: '/partials/students/study-path.html'
        },
        'student_log@app.admin.profile.edit': {
          controller: 'StudentsStudyPathCtrl',
          templateUrl: '/partials/students/history.html'
        },
        'messages@app.admin.profile.edit': {
          controller: 'StudentsMessagesCtrl',
          templateUrl: '/partials/students/messages.html'
        },
        'notes@app.admin.profile.edit': {
          controller: 'StudentsNotesCtrl',
          templateUrl: '/partials/students/notes.html'
        }
      },
      resolve: {
        publicWork: function (load, $stateParams, user, FilesService) {
          var userID = user.id;
          if($stateParams.id) {
            userID = $stateParams.id
          }
          return FilesService.getFileList('publicwork',userID).then(function (res) {
            return res.data;
          });
        },
        editProfile: function (load, $stateParams, user, ProfilesService) {
          var userID = user.id;
          if($stateParams.id) {
            userID = $stateParams.id
          }
          return ProfilesService.getProfile(userID).then(function(res) {
            return res.data;
          });
        },
        countryList: function(load, AdmissionsService) {
          return AdmissionsService.getCountries().then(function(res) {
            return res.data;
          });
        },
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            { name: 'ctrl.edit.profile', files: ['/assets/js/controllers/profile/profile.edit.ctrl.js']},
            { name: 'ctrl.profiles.notes_history', files: ['/assets/js/controllers/notes_history.ctrl.js']},
            { name: 'ctrl.admissions.student', files: ['/assets/js/controllers/admissions/student.ctrl.js']},
            { name: 'ctrl.admissions.summary', files: ['/assets/js/controllers/admissions/summary.ctrl.js']},
            { name: 'ctrl.admissions.decision', files: ['/assets/js/controllers/admissions/decision.ctrl.js']},
            { name: 'ctrl.admissions.uploaded', files: ['/assets/js/controllers/admissions/uploaded.ctrl.js']},
            { name: 'ctrl.students.studypath', files: ['/assets/js/controllers/students/students.studypath.ctrl.js']},
            { name: 'ctrl.students.messages', files: ['/assets/js/controllers/students/students.messages.ctrl.js']},
            { name: 'ctrl.students.notes', files: ['/assets/js/controllers/students/students.notes.ctrl.js']},
            { name: 'idi.profile.widget', files: ['/assets/js/directives/common/profile/idi.profileWidget.js']},
            { name: 'ngImgCrop', files: ['/assets/js/vendor/ng-img-crop.js']}
          ])
        }
      },
      data: {
        permissions: {
          only: ['auth.user.get', 'auth.user.getSelf', 'profiles.profileAny.get', 'profiles.profileSelf.get'],
          redirectTo: {
            'default': 'app.login'
          }
        }
      }
    })
    .state('app.admin.profile.public', {
      url: '/:id',
      params: {
        id: {squash: true, value: null}
      },
      views: {
        'content@app': {
          controller: 'ProfilePublicCtrl',
          templateUrl: '/partials/profiles/public.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            { name: 'ctrl.public.profile', files: ['/assets/js/controllers/profile/profile.public.ctrl.js']},
            { name: 'idi.profile.widget', files: ['/assets/js/directives/common/profile/idi.profileWidget.js']}
          ])
        },
        publicProfile: function (load, $stateParams, user, ProfilesService) {
          var userID = user.id;
          if($stateParams.id) {
            userID = $stateParams.id
          }
          return ProfilesService.getPublicProfile(userID).then(function (res) {
            return res.data;
          });
        },
        publicWork: function (load, $stateParams, user, FilesService) {
          var userID = user.id;
          if($stateParams.id) {
            userID = $stateParams.id
          }
          return FilesService.getFileList('publicwork',userID).then(function (res) {
            return res.data;
          });
        }
      },
      data: {
        permissions: {
          only: ['auth.user.get', 'auth.user.getSelf', 'profiles.profileAny.get', 'profiles.profileSelf.get'],
          redirectTo: {
            'default': 'app.login'
          }
        }
      }
    })
  }
})()