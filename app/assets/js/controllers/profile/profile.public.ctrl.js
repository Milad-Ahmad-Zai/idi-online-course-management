(function () {
  'use strict';

  angular
    .module('ctrl.public.profile', [])

    .controller('ProfilePublicCtrl', function ($scope, $state, $rootScope, publicProfile, publicWork, user) {
      function init() {
        $scope.userProfile = publicProfile;
        $scope.ownProfile = $scope.userProfile.id == user.id;
        $scope.filterPublicFiles(publicWork);
      }

      $scope.filterPublicFiles = function(files) {
        $scope.publicWork = _.filter(files.items, function (file) {
          return file.state == 1;
        });
      }

      init();
    })
})()