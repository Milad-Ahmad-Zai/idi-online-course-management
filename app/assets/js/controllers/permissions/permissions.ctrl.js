(function () {
  'use strict';

  angular
  .module('app.permissions', [])
  .controller('PermissionsCtrl', PermissionsCtrl);

  function PermissionsCtrl(permissions, groups, AuthService, NgTableParams, Notification) {
    var vm = this;

    init();

    function init() {
      vm.tableCols = getColumns([])
      vm.groups = groups;

      vm.tableParams = new NgTableParams({
        page: 1,
        count: 9999,
        group: {
          group: 'asc'
        }
      }, {
        dataset: permissions,
        groupOptions: {
          isExpanded: false
        }
      })
    }

    /**
     * Postponing execution of callback to
     * avoid multiple calls to backend
     *
     * @param {Function} callback - function that should be executed
     * @param {Number} time - milliseconds to wait before execution
     */
    function debounce(callback, time) {
      var timeout;
      return function() {
        var args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
          callback.apply(null, args);
        }, time);
      }
    }

    /**
     * We postpone execution of update, to wait for all changes
     */
    var debouncedUpdatePermissions = debounce(updatePermissions, 1000)

    vm.onGroupSelect = function() {
      var selected = [];
      for (var key in vm.selectedGroups) {
        if (vm.selectedGroups[key]) {
          selected.push(key);
        }
      }

      vm.tableCols = getColumns(selected);
    }

    /**
     * On group's permission change
     */
    vm.onChange = function(group) {
      var listOfPermissions = permissions.reduce(function(prev, current) {
        if (group.permissions2[current.permission] == true)
          prev.push(current.permission);
        return prev;
      }, [])

      debouncedUpdatePermissions(group, listOfPermissions);
    }

    function getColumns(roles) {
      var columns = roles.map(function(role) {
        return {
          title: role,
          getValue: function(row) {
            return groups[role].permissions.indexOf(row.permission) > -1;
          }
        }
      })
      columns.unshift({
        title: 'Permission',
        field: 'name',
        show: true
      })
      return columns;
    }

    function updatePermissions(group, listOfPermissions) {
      AuthService.updateGroup({
        id: group.id,
        permissions: listOfPermissions
      })
      .then(function() {
        Notification.success({title: 'Success', message: 'New permissions have been saved'});
      })
    }
  }
})()