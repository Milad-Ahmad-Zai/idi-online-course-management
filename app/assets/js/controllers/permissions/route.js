(function() {
  angular
  .module('ui.routes')
  .config(config)

  function config($stateProvider) {
    $stateProvider
    .state('app.admin.permissions', {
      url: '/permissions',
      views: {
        'content@app': {
          controller: 'PermissionsCtrl as permissions',
          templateUrl: '/partials/permissions/permissions.html'
        }
      },
      resolve: {
        permissions: function(load, AuthService) {
          return AuthService.getPermissionsList()
          .then(function (res) {
            return res.data.services.reduce(function (prev, current) {
              current.permissions.forEach(function (item) {
                item.group = current.name;
              });
              return prev.concat(current.permissions)
            }, []);
          })
        },
        groups: function(load, AuthService) {
          return AuthService.getGroupsList({
            start: 0,
            limit: 100,
            sort: 'id',
            page: 0
          })
          .then(function (res) {
            return res.data.list.reduce(function(dict, current) {
              dict[current.name] = current;
              current.permissions2 = current.permissions.reduce(function(prev, current) {
                prev[current] = true
                return prev;
              }, {})
              return dict;
            }, {})
          })
        },
        load: function($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'app.permissions',
              files: ['/assets/js/controllers/permissions/permissions.ctrl.js']
            }
          ])
        }
      },
      data: {
        permissions: {
          only: ['auth.group.list']
        }
      }
    })
  }
})()