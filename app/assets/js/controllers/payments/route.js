(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments', {
          url: '/payments',
          abstract: true,
          views: {
            'content@app': {
              controller: 'PaymentsDashboardCtrl as dashboard',
              template: '<ui-view=""></ui-view>'
            }
          },
          resolve: {
            stripePublicKey: function(load, PaymentsStudentService) {
              return PaymentsStudentService.getStripePublicKey().then(function(res) {
                return res.data.key;
              })
            },
            userPaymentsData: function(load, PaymentsStudentService, user) {
              return PaymentsStudentService.getUserByID(user.id).then(function(res) {
                return res;
              })
            },
            userSubscriptionData: function(load, PaymentsStudentService, user) {
              return PaymentsStudentService.getUserSubscriptions(user.id).then(function(res) {
                return res;
              })
            },
            load: function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                { name: 'ctrl.payments.dashboard', files: ['/assets/js/controllers/payments/student/payments.dashboard.ctrl.js'] },
                { name: 'idi.payments.card', files: ['/assets/js/directives/payments/card/idi.paymentCard.js'] },
                { name: 'idi.payments.subscriptions', files: ['/assets/js/directives/payments/subscriptions/idi.paymentSubscriptions.js'] },
              ])
            }
          }
        })
    }
})();