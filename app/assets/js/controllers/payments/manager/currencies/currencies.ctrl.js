(function () {
  'use strict';

  angular
    .module('ctrl.payments.manager.currencies', [])

    .controller('ManagerCurrenciesCtrl', ManagerCurrenciesCtrl);

    function ManagerCurrenciesCtrl($scope, $rootScope, NgTableParams, PaymentsManagerCurrenciesService, Notification, $modal) {

      var vm = this;

      $scope.$parent.manager.index = 4;
      vm.newCurrency = {};
      vm.filter = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: 'name',
        page: 1
      };

      init();

      function init(){
        populateCurrenciesList();
      }
      
      vm.getCurrenciesList = function (config) {;
        return PaymentsManagerCurrenciesService.getCurrenciesList(config);
      };

      function populateCurrenciesList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            currency: 'asc'
          }
        }

        vm.currenciesTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return vm.getCurrenciesList(usersConfig).then(function (res) {
              vm.currencies = res.data;
              
              params.total(parseInt(res.pagination.total_items));
              vm.currenciesTable.total(parseInt(res.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populateCurrenciesList = populateCurrenciesList;

      vm.filterByKeyword = function (e) {
        if (e.which === 13) {
          if (vm.filter.name.length > 0) {
            usersConfig.filter = 'search=' + vm.filter.name;
          } else {
            usersConfig.filter = '';
          }         

          usersConfig.start = 0;
          vm.populateCurrenciesList();
        }
      };
            
      vm.cbChecked = function(){
        var unchecked = vm.currencies.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.currencies, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/currencies/currencies_modal.html', show: false});
      vm.showAddNewModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };

      vm.initNewCurrency = function(){
        if(vm.method !== 'save'){
          vm.method='save'; 
          vm.newCurrency = {};
        }
        vm.showAddNewModal();
      }

      vm.addNew = function(){
        if(vm.newCurrency.code){
          if(vm.method === 'save'){
            PaymentsManagerCurrenciesService.saveCurrency(vm.newCurrency).then(function(response){
              if(response.error === false){
                vm.currencies.push(response.data);
                vm.newCurrency = {};
                addNewModal.hide();
                Notification.success({ message: 'New currency created.', title: 'Success'});
              }else{
                Notification.error({ message: 'Form submission failed.', title: 'Error' });
              }
            });
          }else{
            PaymentsManagerCurrenciesService.updateCurrency(vm.newCurrency).then(function(response){
              if(response.error === false){
                vm.newCurrency = {};
                addNewModal.hide();
                Notification.success({ message: 'Currency updated.', title: 'Success'});
              }else{
                Notification.error({ message: 'Form submission failed.', title: 'Error' });
              }
            });
          }
        }
      }

      vm.update = function(code){
        angular.forEach(vm.currencies, function(item){
          if(item.code === code){
            vm.method = 'update';
            vm.newCurrency = item;
            vm.showAddNewModal();
          }
        });
      }

      vm.delete = function(){
        angular.forEach(vm.currencies, function(v, k) {
          if(v.checked){
            PaymentsManagerCurrenciesService.deleteCurrency(v.code).then(function(response){
              if(response.error === false){
                var index = vm.currencies.indexOf(v);
                vm.currencies.splice(index, 1);
                Notification.success({ message: 'Currency deleted.', title: 'Success'});
              }else{
                Notification.error({ message: 'Currency deletion failed.', title: 'Error' });
              }
            });
          }
        });
      }

    }

})();