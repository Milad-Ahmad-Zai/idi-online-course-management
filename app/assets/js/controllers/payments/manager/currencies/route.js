(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.manager.currencies', {
        url: '/currencies',
        templateUrl: '/partials/payments/manager/currencies/currencies.html',
        controller: 'ManagerCurrenciesCtrl as currencies',
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.payments.manager.currencies', files: ['/assets/js/controllers/payments/manager/currencies/currencies.ctrl.js'] },
              { name: 'payments.manager.currencies.service', files: ['/assets/js/services/payments/manager/currencies.service.js'] }
            ])
          }
        }
      });
    }

})();
