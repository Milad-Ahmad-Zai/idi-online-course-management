(function () {
  'use strict';

  angular
    .module('ctrl.payments.manager.customers', [])

    .controller('ManagerCustomersCtrl', ManagerCustomersCtrl);

    function ManagerCustomersCtrl($scope, $rootScope, NgTableParams, AuthService, PaymentsManagerCustomersService, PaymentsManagerCardsService, PaymentsManagerCurrenciesService, PaymentsManagerCoursesService, PaymentsManagerPlansService, stripePublicKey, Notification, $modal) {

      var vm = this;

      $scope.$parent.manager.index = 0;
      vm.customer = {};
      vm.totalAmount = 0;
      vm.cards = {};
      vm.defaultCard = {};
      vm.newCard = {};
      vm.cardUserId;
      vm.loading = false;
      vm.filteredItems = null;
      vm.payment = {};
      vm.payment.user_id = null;
      vm.payment.installment = {};
      vm.plan = {};
      vm.plan.course_id = null;
      vm.plan.plan_id = null;
      vm.plan.get_first_installment = true;
      vm.sortedPlans = [];
      vm.sortedCurrencies = [];
      vm.filter = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: '-id',
        page: 1
      };

      init();

      function init(){

        populateCustomersList();
        
        PaymentsManagerCustomersService.getUsersList({limit: 100}).then(function(response){
          if(response.error === false){
            vm.users = response.data.list;
          }
        });
        PaymentsManagerCurrenciesService.getCurrenciesList({limit: 100}).then(function(response){
          if(response.error === false){
            vm.currencies = response.data;
          }
        });
        PaymentsManagerCoursesService.getCoursesList({limit: 100}).then(function(response){
          if(response.error === false){
            vm.courses = response.data;
          }
        });
        PaymentsManagerPlansService.getPlansList({limit: 100}).then(function(response){
          if(response.error === false){
            vm.plans = response.data;
          }
        });
      }      

      vm.getCustomersList = function (config) {;
        return PaymentsManagerCustomersService.getCustomersList(config);
      };

      function populateCustomersList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'desc'
          }
        }

        vm.customersTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return vm.getCustomersList(usersConfig).then(function (res) {
              vm.customers = res.data;
              
              params.total(parseInt(res.pagination.total_items));
              vm.customersTable.total(parseInt(res.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populateCustomersList = populateCustomersList;

      vm.filterByKeyword = function (e) {
        if (e.which === 13) {
          if (vm.filter.name.length > 0) {
            usersConfig.filter = 'search=' + vm.filter.name;
          } else {
            usersConfig.filter = '';
          }         

          usersConfig.start = 0;
          vm.populateCustomersList();
        }
      };
            
      vm.cbChecked = function(){
        var unchecked = vm.customers.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.customers, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      vm.calculateTotal = function(){
        vm.totalAmount = 0;
        angular.forEach(vm.customer.installments, function(item){
          vm.totalAmount += parseFloat(item.amount);
        });
      }

      vm.addInstallment = function(){
        vm.customer.installments.push({amount: 0, date:null});
      }
      
      vm.loadUsers = function(str){
        PaymentsManagerCustomersService.getUsersList(str).then(function(response){
          if(response.error === false){
            vm.users = response.data.list;
          }
        });
      }

      vm.loadCourses = function(str){
        PaymentsManagerCoursesService.getCoursesList({filter: 'title==' + str}).then(function(response){
          if(response.error === false){
            vm.courses = response.data;
          }
        });
      }

      vm.loadInstallment = function(){
        var requestData = {};
        requestData.id = vm.plan.plan_id;
        requestData.code = vm.plan.code;
        requestData.start_date = vm.plan.start_date;
        PaymentsManagerCustomersService.recalculatePlan(requestData).then(function(response){
          if(response.error === false){
            vm.installments = response.data;
            vm.totalAmount = 0;
            angular.forEach(vm.installments.installments, function(item){
              vm.totalAmount+= parseFloat(item.amount);
            });
          }
        });
      }

      vm.loadPlans = function(id){
        vm.sortedPlans = [];
        vm.sortedCurrencies = [];
        vm.plan.plan_id = null;
        vm.plan.currency_code = null;
        vm.installments = {};
        vm.totalAmount = 0;
        angular.forEach(vm.plans,function(item){
          if(item.course.id === id){
            vm.sortedPlans.push(item);
            if(vm.sortedCurrencies.indexOf(item.currency.code) < 0) {
              vm.sortedCurrencies.push(item.currency.code);
            }
          }
        });
      }

      vm.sortPlans = function(code){
        vm.sortedPlans = [];
        vm.plan.plan_id = null;
        vm.installments = {};
        vm.totalAmount = 0;
        angular.forEach(vm.plans,function(item){
          if(item.course.id === vm.plan.course_id && item.currency.code === code){
            vm.sortedPlans.push(item);
          }
        });
      }
      
      vm.getCutomerCards = function(id){
        PaymentsManagerCardsService.getCardsByUserId(id).then(function(response){
          if(response.error === false){
            vm.defaultCard = response.data.stripe_data.default_source;
            vm.cards = response.data.stripe_data.sources.data;
          }else{
            vm.cards = {};
            vm.defaultCard = {};
          }
        });
      }

      
      var addNewCustomerModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/customers/plan_modal.html', show: false});
      vm.showNewCustomerModal = function() {
        addNewCustomerModal.$promise.then(addNewCustomerModal.show);
      };

      var editCustomerModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/customers/customers_modal.html', show: false});
      vm.showeditCustomerModal = function() {
        editCustomerModal.$promise.then(editCustomerModal.show);
      };

      var addNewCardModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/customers/card_modal.html', show: false});
      vm.showAddNewCardModal = function(user_id) {
        vm.cardUserId = user_id;
        addNewCardModal.$promise.then(addNewCardModal.show);
      };

      var addNewPaymentModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/customers/payment_modal.html', show: false});
      vm.showAddNewPaymentModal = function() {
        addNewPaymentModal.$promise.then(addNewPaymentModal.show);
      };

      vm.initNewCustomer = function(){
        if(vm.method !== 'save'){
          vm.method='save'; 
          vm.customer = {};
          vm.customer.installments = [];
          vm.cards = {};
          vm.calculateTotal();
        }
        vm.showNewCustomerModal();
      }

      vm.addNew = function(){
        if(vm.method === 'save'){
          PaymentsManagerCustomersService.saveCustomer(vm.plan).then(function(response){
            if(response.error === false){
              vm.customers.push(response.data);
              addNewCustomerModal.hide();
              vm.plan = {};
              vm.installments = {};
              vm.totalAmount = 0;
              Notification.success({ message: 'New customer created.', title: 'Success'});
            }else{
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            }
          });
        }else{
          PaymentsManagerCustomersService.updateCustomer(vm.customer).then(function(response){
            if(response.error === false){
              populateCustomersList();
              editCustomerModal.hide();
              vm.customer = {};
              vm.customer.installments = [];
              Notification.success({ message: 'Customer updated.', title: 'Success'});
            }else{
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            }
          });
        }
      }

      vm.addNewCard = function(){
        vm.loading = true;
            
        Stripe.setPublishableKey(stripePublicKey);

        Stripe.card.createToken(vm.newCard, function stripeResponseHandler(status, response) {

          if (response.error) { // Problem!

            Notification.error({ message: 'Form submission failed.', title: 'Error' });
            vm.loading = false;

          } else { // Token was created!

            var addNewCard = {};
            addNewCard.card = response.id;
            addNewCard.user_id =  vm.cardUserId;

            PaymentsManagerCardsService.createNewCard(addNewCard).then(function(response){

              if(response.error === false){
                vm.getCutomerCards(vm.cardUserId);
                addNewCardModal.hide();
                vm.newCard = {};
                Notification.success({ message: 'Card created.', title: 'Success'});
              }else{
                Notification.error({ message: 'Form submission failed.', title: 'Error' });
              }
              
              vm.loading = false;

            });

          }
        });
         
      }

      vm.newPayment = function(){
        vm.cards = {};
        vm.defaultCard = {};
        vm.payment.installment = {};
        vm.payment.user_id = null;
        vm.showAddNewPaymentModal();
      }

      vm.submitPayment = function(){
        vm.payment.installment.date = new Date();
        vm.payment.get_first_installment = true;
        vm.payment.installments = [];
        vm.payment.installments.push(vm.payment.installment);
         PaymentsManagerCustomersService.takePayment(vm.payment).then(function(response){
          if(response.error === false){
            vm.cards = {};
            vm.defaultCard = {};
            vm.payment.installment = {};
            vm.payment.user_id = null;
            addNewPaymentModal.hide();
            Notification.success({ message: 'Payment successful.', title: 'Success'});
          }else{
            Notification.error({ message: 'Form submission failed.', title: 'Error' });
          }

        });
      }

      vm.makeCardDefault = function(card, user_id){
        var requestData = {};
        requestData.card = card.id;
        requestData.user_id = user_id;
        PaymentsManagerCardsService.makeCardDefault(requestData).then(function(response){
          if(response.error === false){
            Notification.success({ message: 'Default card change successful.', title: 'Success'});
            vm.getCutomerCards(requestData.user_id);
          }else{
            Notification.error({ message: 'Change default card failed.', title: 'Error' });
          }
        });
      }

      vm.removeCard = function(card, user_id){
        if (confirm('Are you sure you want to remove card?')) {
          var requestData = {};
          requestData.card = card.id;
          requestData.user_id = user_id;
          
          PaymentsManagerCardsService.removeCard(requestData).then(function(response){
            Notification.success({ message: 'Card removed successful.', title: 'Success'});
            vm.getCutomerCards(requestData.user_id);
          })
          .catch(function(){
            Notification.error({ message: 'Failed to remove card.', title: 'Error' });
          });
        }
      }

      vm.chargeInstallment = function(installment){
        if (confirm('Are you sure you want to charge installment?')) {
          PaymentsManagerCustomersService.chargeInstallment(installment).then(function(response){
            if(response.error === false){
              if(response.data.installment.transaction.status === "SUCCESS"){
                Notification.success({ message: 'Installment charge successful.', title: 'Success'});
              }else{
                Notification.error({ message: 'Installment charge failed.', title: 'Error' });
              }
            }else{
              Notification.error({ message: 'Installment charge failed.', title: 'Error' });
            }
          });
        }
      }

      vm.removeInstallment = function(installment){
        var index = vm.customer.installments.indexOf(installment);
        vm.customer.installments.splice(index, 1);
      }

      vm.update = function(id){
        angular.forEach(vm.customers, function(item){
          if(item.id === id){
            vm.method = 'update';
            vm.customer = item;
            vm.calculateTotal();
            vm.getCutomerCards(vm.customer.user_id);
            vm.showeditCustomerModal();
          }
        });
      }

      vm.export = function(){
        var filtered = vm.filteredItems.map(function(item){
          return item.id;
        });

        if (filtered.length > 0) {
          PaymentsManagerCustomersService.getCustomersListCSV(filtered).then(function(response){

              var anchor = angular.element('<a/>');
              anchor.attr({
                 href: 'data:attachment/csv;charset=utf-8,' + encodeURI(response),
                 target: '_blank',
                 download: 'subscribers.csv'
              })[0].click();

          });
        }

      }
      
      vm.duplicate = function(){
        angular.forEach(vm.plans, function(v, k) {
          if(v.checked){
            var obj = angular.copy(v);
            obj.id = null;
            obj.course_id = obj.course.id;
            obj.currency_code = obj.currency.code;
            PaymentsManagerCustomersService.savePlan(obj).then(function(response){
              if(response.error === false){
                vm.plans.push(response.data);
                Notification.success({ message: 'Customer duplicated.', title: 'Success'});
              }else{
                Notification.error({ message: 'Duplication failed.', title: 'Error' });
              }
            });
          }
        });
      }

      vm.delete = function(){
        angular.forEach(vm.customers, function(v, k) {
          if(v.checked){
            PaymentsManagerCustomersService.deleteCustomer(v.id).then(function(response){
              if(response.error === false){
                var index = vm.customers.indexOf(v);
                vm.customers.splice(index, 1);
                Notification.success({ message: 'Customer deleted.', title: 'Success'});
              }else{
                Notification.error({ message: 'Deletion failed.', title: 'Error' });
              }
            });
          }
        });
      }

      vm.archive = function(){
        if (confirm('Are you sure you want to archive selected?')) {
          angular.forEach(vm.customers, function(v, k) {
            if(v.checked){
              v.archived = true;
              PaymentsManagerCustomersService.updateCustomer(v).then(function(response){
                if(response.error === false){
                  Notification.success({ message: 'Customer Archived.', title: 'Success'});
                }else{
                  Notification.error({ message: 'Archiving customer failed.', title: 'Error' });
                }
              });
            }
          });
        }
      }

    }

})();