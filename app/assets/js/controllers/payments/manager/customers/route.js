(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.manager.customers', {
        url: '/customers',
        templateUrl: '/partials/payments/manager/customers/customers.html',
        controller: 'ManagerCustomersCtrl as customers',
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.payments.manager.customers', files: ['/assets/js/controllers/payments/manager/customers/customers.ctrl.js'] },
              { name: 'payments.manager.customers.service', files: ['/assets/js/services/payments/manager/customers.service.js'] },
              { name: 'payments.manager.cards.service', files: ['/assets/js/services/payments/manager/cards.service.js'] },
              { name: 'payments.student.service', files: ['/assets/js/services/payments/student/payments.service.js'] },
              { name: 'payments.manager.currencies.service', files: ['/assets/js/services/payments/manager/currencies.service.js'] },
              { name: 'payments.manager.courses.service', files: ['/assets/js/services/payments/manager/courses.service.js'] },
              { name: 'payments.manager.plans.service', files: ['/assets/js/services/payments/manager/plans.service.js'] },
              { name: 'idi.payments.card', files: ['/assets/js/directives/payments/card/idi.paymentCard.js'] },
              { name: 'validators.stripe-card-number', files: ['/assets/js/directives/validators/stripe-card-number.js'] },
              { name: 'validators.stripe-card-cvc', files: ['/assets/js/directives/validators/stripe-card-cvc.js'] },
              { name: 'validators.stripe-card-expire', files: ['/assets/js/directives/validators/stripe-card-expire.js'] },
              { name: 'ui.toInteger', files: ['/assets/js/directives/ui.toInteger.js'] },
              { name: 'ui.toDate', files: ['/assets/js/directives/ui.toDate.js'] }
            ])
          }
        }
      });
    }

})();
