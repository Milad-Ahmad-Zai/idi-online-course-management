(function () {
  'use strict';

  angular
    .module('ctrl.payments.manager.transactions', [])

    .controller('ManagerTransactionsCtrl', ManagerTransactionsCtrl);

    function ManagerTransactionsCtrl($scope, $rootScope, NgTableParams, PaymentsManagerTransactionsService, ProfilesService, Notification, $modal) {

      var vm = this;

      $scope.$parent.manager.index = 1;
      vm.transaction = {};
      vm.filteredItems = null;
      vm.filter = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: '-id',
        page: 1
      };

      init();

      function init(){
        populateTransactionsList();
      }

      vm.getTransactionsList = function (config) {        
        return PaymentsManagerTransactionsService.getTransactionsList(config);
      };

      function populateTransactionsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'desc'
          }
        }

        vm.transactionsTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return vm.getTransactionsList(usersConfig).then(function (res) {
              vm.transactions = res.data;

              angular.forEach(vm.transactions, function(item){
                item.date = moment(item.date).format('DD/MM/YYYY');
                ProfilesService.getProfile(item.user_id, {cache: true}).then(function(response){
                  item.profile = response.data;
                });
              });
              params.total(parseInt(res.pagination.total_items));
              vm.transactionsTable.total(parseInt(res.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populateTransactionsList = populateTransactionsList;

      vm.filterByKeyword = function (e) {
        if (e.which === 13) {
          if (vm.filter.name.length > 0) {
            usersConfig.filter = 'search=' + vm.filter.name;
          } else {
            usersConfig.filter = '';
          }         

          usersConfig.start = 0;
          vm.populateTransactionsList();
        }
      };
            
      vm.cbChecked = function(){
        var unchecked = vm.transactions.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.transactions, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/transactions/transactions_modal.html', show: false});
      vm.showAddNewModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };


      vm.addNew = function(){
        PaymentsManagerTransactionsService.updateTransaction(vm.transaction).then(function(response){
          if(response.error === false){
            vm.transaction = {};
            addNewModal.hide();
            Notification.success({ message: 'Transaction updated.', title: 'Success'});
          }else{
            Notification.error({ message: 'Form submission failed.', title: 'Error' });
          }
        });
      }

      vm.update = function(id){
        angular.forEach(vm.transactions, function(item){
          if(item.id === id){
            vm.transaction = item;
            if(vm.transaction.amount){
              vm.transaction.amount = parseFloat(vm.transaction.amount);
            }
            vm.showAddNewModal();
          }
        });
      }

      vm.retakePayment = function(transaction){
        if (confirm("Are you sure you want to retake payment?")) {
          PaymentsManagerTransactionsService.takeTransaction(transaction.id).then(function(response){
            if(response.data.status === "SUCCESS"){
              transaction.status = response.data.status;
              Notification.success({ message: 'Transaction successful.', title: 'Success'});
            }else{
              Notification.error({ message: 'Transaction failed.', title: 'Error' });
            }
          })
          .catch(function(){
            Notification.error({ message: 'Transaction failed.', title: 'Error' });
          });
        }
      }

      vm.export = function(){
        var filtered = vm.filteredItems.map(function(item){
          return item.id;
        });

        if (filtered.length > 0) {
          PaymentsManagerTransactionsService.getTransactionsListCSV(filtered).then(function(response){
            var anchor = angular.element('<a/>');
            anchor.attr({
               href: 'data:attachment/csv;charset=utf-8,' + encodeURI(response),
               target: '_blank',
               download: 'subscribers.csv'
            })[0].click();
          });
        }

      }

    }

})();