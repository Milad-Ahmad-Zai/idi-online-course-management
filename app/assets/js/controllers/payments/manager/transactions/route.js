(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.manager.transactions', { 
        url: '/transactions',
        templateUrl: '/partials/payments/manager/transactions/transactions.html',
        controller: 'ManagerTransactionsCtrl as transactions',
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.payments.manager.transactions', files: ['/assets/js/controllers/payments/manager/transactions/transactions.ctrl.js'] },
              { name: 'payments.manager.transactions.service', files: ['/assets/js/services/payments/manager/transactions.service.js'] },
              { name: 'profiles.service', files: ['/assets/js/services/profiles.service.js'] }
            ])
          }
        }
      });
    }

})();
