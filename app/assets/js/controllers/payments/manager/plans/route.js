(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.manager.plans', {
        url: '/plans',
        templateUrl: '/partials/payments/manager/plans/plans.html',
        controller: 'ManagerPlansCtrl as plans',
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.payments.manager.plans', files: ['/assets/js/controllers/payments/manager/plans/plans.ctrl.js'] },
              { name: 'payments.manager.plans.service', files: ['/assets/js/services/payments/manager/plans.service.js'] },
              { name: 'payments.manager.courses.service', files: ['/assets/js/services/payments/manager/courses.service.js'] },
              { name: 'payments.manager.currencies.service', files: ['/assets/js/services/payments/manager/currencies.service.js'] },
              { name: 'ui.toInteger', files: ['/assets/js/directives/ui.toInteger.js'] },
              { name: 'ui.toDate', files: ['/assets/js/directives/ui.toDate.js'] }
            ])
          }
        }
      });
    }

})();
