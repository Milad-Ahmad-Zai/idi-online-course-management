(function () {
  'use strict';

  angular
    .module('ctrl.payments.manager.plans', [])

    .controller('ManagerPlansCtrl', ManagerPlansCtrl);

    function ManagerPlansCtrl($scope, $rootScope, NgTableParams, PaymentsManagerPlansService, PaymentsManagerCoursesService, PaymentsManagerCurrenciesService, Notification, $modal) {

      var vm = this;

      $scope.$parent.manager.index = 2;
      vm.plan = {};
      vm.plan.early_discounts = [];
      vm.early_discount = {};
      vm.filter = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: '-id',
        page: 1
      };
      
      init();

      function init(){
        
        populatePlansList();

        PaymentsManagerCoursesService.getCoursesList({limit: 100}).then(function(response){
          if(response.error === false){
            vm.courses = response.data;
          }
        });

        PaymentsManagerCurrenciesService.getCurrenciesList({limit: 100}).then(function(response){
          if(response.error === false){
            vm.currencies = response.data;
          }
        });
      }     

      vm.getPlansList = function (config) {;
        return PaymentsManagerPlansService.getPlansList(config);
      };

      function populatePlansList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'desc'
          }
        }

        vm.plansTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return vm.getPlansList(usersConfig).then(function (res) {
              vm.plans = res.data;
              
              params.total(parseInt(res.pagination.total_items));
              vm.plansTable.total(parseInt(res.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populatePlansList = populatePlansList;

      vm.filterByKeyword = function (e) {
        if (e.which === 13) {
          if (vm.filter.name.length > 0) {
            usersConfig.filter = 'search=' + vm.filter.name;
          } else {
            usersConfig.filter = '';
          }         

          usersConfig.start = 0;
          vm.populatePlansList();
        }
      };
            
      vm.cbChecked = function(){
        var unchecked = vm.plans.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.plans, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }
      //this is begening
      vm.calculateInstallments = function(){
        if(vm.plan.numInstallments && vm.plan.start_date){
          var total = 0;

          if(vm.plan.admin_fee_type === 'percent'){
            total = ((parseFloat(vm.plan.admin_fee)/100) * parseFloat(vm.plan.price)) + parseFloat(vm.plan.price);
          }else{
            total = parseFloat(vm.plan.admin_fee) + parseFloat(vm.plan.price);
          }
          
          var monthly_amount = total / vm.plan.numInstallments;
          var start_date = new Date(vm.plan.start_date);

          vm.plan.installments = [];
          var x = 1;
          var initial_date = angular.copy(start_date);

          for(var i=0; i<vm.plan.numInstallments; i++){
            vm.plan.installments.push({amount: monthly_amount, date: angular.copy(start_date)});
            start_date = addMonthsUTC(initial_date, x);
            x++;
          }
        }
      }

      var addMonthsUTC = function(initial_date, monthOffset){
        var dt = new Date(initial_date);
        dt.setMonth(dt.getMonth() + monthOffset) ;
        if (dt.getDate() < initial_date.getDate()) { dt.setDate(0); }
        return dt;
      };

      vm.installmentTotal = function(){
        var total = 0;
        angular.forEach(vm.plan.installments, function(item){
          total+=parseFloat(item.amount);
        });
        var planTotal = 0;
        if(vm.plan.admin_fee_type === 'percent'){
          planTotal = ((parseFloat(vm.plan.admin_fee)/100) * parseFloat(vm.plan.price)) + parseFloat(vm.plan.price);
        }else{
          planTotal = parseFloat(vm.plan.admin_fee) + parseFloat(vm.plan.price);
        }
        vm.total = total;
        if(total !== planTotal){
          vm.installmentsError = true;
        }else{
          vm.installmentsError = false;
        }
      }

      vm.recalculateInstallments = function(){
        var total = 0;
        if(vm.plan.admin_fee_type === 'percent'){
          total = ((parseFloat(vm.plan.admin_fee)/100) * parseFloat(vm.plan.price)) + parseFloat(vm.plan.price);
        }else{
          total = parseFloat(vm.plan.admin_fee) + parseFloat(vm.plan.price);
        }

        var firstInstallment = parseFloat(vm.plan.installments[0].amount);
        var diff = total - firstInstallment;

        var len = vm.plan.installments.length -1;
        var finalAmount = diff / len;

        angular.forEach(vm.plan.installments, function(item, index){
          if(index !== 0){
            item.amount = finalAmount;
          }
        });
      }

      vm.removeDiscount = function(item){
        var index = vm.plan.early_discounts.indexOf(item);
        vm.plan.early_discounts.splice(index, 1);
      }

      vm.initNewPlan = function(){
        if(vm.method !== 'save'){
          vm.method='save'; 
          vm.plan = {};
          vm.plan.early_discounts = [];
          vm.plan.enabled = true;
          vm.plan.admin_fee = 0;
          vm.plan.price = 0;
          vm.plan.admin_fee_type = 'value';
          vm.installmentsError = false;
        }
        vm.showAddNewModal();
      }

      vm.addDiscount = function(){
        vm.plan.early_discounts.push(angular.copy(vm.early_discount));
        vm.early_discount = {};
      }


      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/plans/plans_modal.html', show: false});
      vm.showAddNewModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };

      vm.addNew = function(){
        if(vm.method === 'save'){
          PaymentsManagerPlansService.savePlan(vm.plan).then(function(response){
            if(response.error === false){
              vm.plans.push(response.data);
              addNewModal.hide();
              vm.plan = {};
              vm.plan.early_discounts = [];
              vm.early_discount = {};
              vm.plan.enabled = true;
              vm.plan.admin_fee = 0;
              vm.plan.price = 0;
              vm.plan.admin_fee_type = 'value';
              vm.installmentsError = false;
              Notification.success({ message: 'Plan created.', title: 'Success'});
            }else{
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            }
          });
        }else{
          PaymentsManagerPlansService.updatePlan(vm.plan).then(function(response){
            if(response.error === false){
              addNewModal.hide();
              vm.plan = {};
              vm.plan.early_discounts = [];
              vm.early_discount = {};
              vm.plan.enabled = true;
              vm.plan.admin_fee = 0;
              vm.plan.price = 0;
              vm.plan.admin_fee_type = 'value';
              vm.installmentsError = false;
              Notification.success({ message: 'Plan updated.', title: 'Success'});
            }else{
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            }
          });
        }
      }
      
      vm.update = function(id){
        angular.forEach(vm.plans, function(item){
          if(item.id === id){
            vm.method = 'update';
            vm.plan = item;
            vm.plan.numInstallments = vm.plan.installments.length;
            vm.plan.price = parseFloat(vm.plan.price);
            vm.plan.admin_fee = parseFloat(vm.plan.admin_fee);
            vm.plan.course_id = vm.plan.course.id;
            vm.plan.currency_code = vm.plan.currency.code;
            vm.installmentsError = false;
            vm.showAddNewModal();
          }
        });
      }
      
      vm.duplicate = function(){
        angular.forEach(vm.plans, function(v, k) {
          if(v.checked){
            var obj = angular.copy(v);
            obj.id = null;
            obj.course_id = obj.course.id;
            obj.currency_code = obj.currency.code;
            PaymentsManagerPlansService.savePlan(obj).then(function(response){
              if(response.error === false){
                vm.plans.push(response.data);
                Notification.success({ message: 'Plan duplicated.', title: 'Success'});
              }else{
                Notification.error({ message: 'Plan duplication failed.', title: 'Error' });
              }
            });
          }
        });
      }

      vm.delete = function(){
        angular.forEach(vm.plans, function(v, k) {
          if(v.checked){
            PaymentsManagerPlansService.deletePlan(v.id).then(function(response){
              if(response.error === false){
                var index = vm.plans.indexOf(v);
                vm.plans.splice(index, 1);
                Notification.success({ message: 'Plan Deleted.', title: 'Success'});
              }else{
                Notification.error({ message: 'Plan deletion failed.', title: 'Error' });
              }
            });
          }
        });
      }

      vm.archive = function(){
        angular.forEach(vm.plans, function(v, k) {
          if(v.checked){
            v.enabled = false;
            v.course_id = v.course.id;
            v.currency_code = v.currency.code;
            PaymentsManagerPlansService.savePlan(v).then(function(response){
              if(response.error === false){
                Notification.success({ message: 'Plan Archived.', title: 'Success'});
              }else{
                Notification.error({ message: 'Archiving plan failed.', title: 'Error' });
              }
            });
          }
        });
      }

    }

})();