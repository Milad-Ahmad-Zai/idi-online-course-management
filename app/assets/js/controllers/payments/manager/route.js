(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.manager', {
        url: '/management',
        //abstract: true,
        views: {
          'content@app': {
            templateUrl: '/partials/payments/manager/manager.html',
            controller: 'PaymentsManagerCtrl as manager'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.payments.manager', files: ['/assets/js/controllers/payments/manager/manager.ctrl.js'] }
            ])
          }
        },
        data: {
          permissions: {
            only: ['payments.role.manager'],
            redirectTo: 'app.login'
          }
        }
      });
    }

})();
