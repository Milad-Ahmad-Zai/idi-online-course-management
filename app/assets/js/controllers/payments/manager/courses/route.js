(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.manager.courses', { 
        url: '/courses',
        templateUrl: '/partials/payments/manager/courses/courses.html',
        controller: 'ManagerCoursesCtrl as courses',
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.payments.manager.courses', files: ['/assets/js/controllers/payments/manager/courses/courses.ctrl.js'] },
              { name: 'payments.manager.courses.service', files: ['/assets/js/services/payments/manager/courses.service.js'] }
            ])
          }
        }
      });
    }

})();
