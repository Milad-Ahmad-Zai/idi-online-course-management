(function () {
  'use strict';

  angular
    .module('ctrl.payments.manager.courses', [])

    .controller('ManagerCoursesCtrl', ManagerCoursesCtrl);

    function ManagerCoursesCtrl($scope, $rootScope, NgTableParams, PaymentsManagerCoursesService, Notification, $modal) {

      var vm = this;

      $scope.$parent.manager.index = 3;
      vm.newCourse = {};
      vm.filter = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: '-id',
        page: 1
      };

      init();

      function init(){
        populateCoursesList();
      }
      
      vm.getCoursesList = function (config) {;
        return PaymentsManagerCoursesService.getCoursesList(config);
      };

      function populateCoursesList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'desc'
          }
        }

        vm.coursesTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return vm.getCoursesList(usersConfig).then(function (res) {
              vm.courses = res.data;
              
              params.total(parseInt(res.pagination.total_items));
              vm.coursesTable.total(parseInt(res.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populateCoursesList = populateCoursesList;

      vm.filterByKeyword = function (e) {
        if (e.which === 13) {
          if (vm.filter.name.length > 0) {
            usersConfig.filter = 'search=' + vm.filter.name;
          } else {
            usersConfig.filter = '';
          }         

          usersConfig.start = 0;
          vm.populateCoursesList();
        }
      };
            
      vm.cbChecked = function(){
        var unchecked = vm.courses.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.courses, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/courses/courses_modal.html', show: false});
      vm.showAddNewModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };

      vm.initNewCourse = function(){
        if(vm.method !== 'save'){
          vm.method='save'; 
          vm.newCourse = {};
        }
        vm.showAddNewModal();
      }

      vm.addNew = function(){
        if(vm.newCourse.title){
          if(vm.method === 'save'){
            PaymentsManagerCoursesService.saveCourse(vm.newCourse).then(function(response){
              if(response.error === false){
                vm.courses.push(response.data);
                vm.newCourse = {};
                addNewModal.hide();
                Notification.success({ message: 'New course created.', title: 'Success'});
              }else{
                Notification.error({ message: 'Form submission failed.', title: 'Error' });
              }
            });
          }else{
            PaymentsManagerCoursesService.updateCourse(vm.newCourse).then(function(response){
              if(response.error === false){
                vm.newCourse = {};
                addNewModal.hide();
                Notification.success({ message: 'Course updated.', title: 'Success'});
              }else{
                Notification.error({ message: 'Form submission failed.', title: 'Error' });
              }
            });
          }
        }
      }

      vm.update = function(id){
        angular.forEach(vm.courses, function(item){
          if(item.id === id){
            vm.method = 'update';
            vm.newCourse = item;
            vm.showAddNewModal();
          }
        });
      }
      
      vm.duplicate = function(){
        angular.forEach(vm.courses, function(v, k) {
          if(v.checked){
            var obj = angular.copy(v);
            obj.id = null;
            PaymentsManagerCoursesService.saveCourse(obj).then(function(response){
              if(response.error === false){
                vm.courses.push(response.data);
                Notification.success({ message: 'Course duplicated.', title: 'Success'});
              }else{
                Notification.error({ message: 'Course duplication failed.', title: 'Error' });
              }
            });
          }
        });
      }

      vm.delete = function(){
        angular.forEach(vm.courses, function(v, k) {
          if(v.checked){
            PaymentsManagerCoursesService.deleteCourse(v.id).then(function(response){
              if(response.error === false){
                var index = vm.courses.indexOf(v);
                vm.courses.splice(index, 1);
                Notification.success({ message: 'Course deleted.', title: 'Success'});
              }else{
                Notification.error({ message: 'Course deletion failed.', title: 'Error' });
              }
            });
          }
        });
      }

      vm.archive = function(){
        angular.forEach(vm.courses, function(v, k) {
          if(v.checked){
            v.archived = true;
            PaymentsManagerCoursesService.updateCourse(v).then(function(response){
              if(response.error === false){
                Notification.success({ message: 'Course Archived.', title: 'Success'});
              }else{
                Notification.error({ message: 'Archiving course failed.', title: 'Error' });
              }
            });
          }
        });
      }

    }

})();