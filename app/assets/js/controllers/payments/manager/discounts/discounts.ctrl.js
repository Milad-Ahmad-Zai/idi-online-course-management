(function () {
  'use strict';

  angular
    .module('ctrl.payments.manager.discounts', [])

    .controller('ManagerDiscountsCtrl', ManagerDiscountsCtrl);

    function ManagerDiscountsCtrl($scope, $rootScope, NgTableParams, PaymentsManagerDiscountsService, PaymentsManagerPlansService, Notification, $modal) {

      var vm = this;

      $scope.$parent.manager.index = 5;
      vm.newDiscount = {};
      vm.selectedPlans = {};
      vm.filter = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: 'code',
        page: 1
      };

      init();

      function init(){
        populateDiscountsList();

        PaymentsManagerPlansService.getPlansList({limit: 100}).then(function(response){
          if(response.error === false){
            vm.plans = response.data;
          }
        });
      }
      
      vm.getDiscountsList = function (config) {;
        return PaymentsManagerDiscountsService.getDiscountsList(config);
      };

      function populateDiscountsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            code: 'asc'
          }
        }

        vm.discountsTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return vm.getDiscountsList(usersConfig).then(function (res) {
              vm.discounts = res.data;
              
              params.total(parseInt(res.pagination.total_items));
              vm.discountsTable.total(parseInt(res.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populateDiscountsList = populateDiscountsList;

      vm.filterByKeyword = function (e) {
        if (e.which === 13) {
          if (vm.filter.name.length > 0) {
            usersConfig.filter = 'search=' + vm.filter.name;
          } else {
            usersConfig.filter = '';
          }         

          usersConfig.start = 0;
          vm.populateDiscountsList();
        }
      };
            
      vm.cbChecked = function(){
        var unchecked = vm.discounts.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.discounts, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/payments/manager/discounts/discounts_modal.html', show: false, tag: 'discountModal'});
      vm.showAddNewModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };

      vm.initNewDiscount = function(){
        if(vm.method !== 'save'){
          vm.method='save'; 
          vm.newDiscount = {};
          vm.newDiscount.for_all_plans = true;
        }
        vm.showAddNewModal();
      }

      $scope.$on('modal.hide', function(e, target) {
        if (target.$options.tag == 'discountModal') {
          vm.newDiscount.valid_until = moment(vm.newDiscount.valid_until).format('DD/MM/YYYY');
        }
      })
      
      vm.addNew = function(){
        vm.newDiscount.plans = vm.selectedPlans;
        if(vm.method === 'save'){
          PaymentsManagerDiscountsService.saveDiscount(vm.newDiscount).then(function(response){
            if(response.error === false){
              vm.discounts.push(response.data);
              addNewModal.hide();
              vm.newDiscount = {};
              Notification.success({ message: 'New discount created.', title: 'Success'});
            }else{
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            }
          });
        }else{
          PaymentsManagerDiscountsService.updateDiscount(vm.newDiscount).then(function(response){
            if(response.error === false){
              addNewModal.hide();
              vm.newDiscount.valid_until = moment(vm.newDiscount.valid_until).format('DD/MM/YYYY');
              vm.newDiscount = {};
              Notification.success({ message: 'Discount updated.', title: 'Success'});
            }else{
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            }
          });
        }
      }

      vm.update = function(code){
        angular.forEach(vm.discounts, function(item){
          if(item.code === code){
            item.amount = parseInt(item.amount);
            item.valid_until = new Date(item.valid_until) || null;
            vm.selectedPlans = item.plans.map(function (plan){ return plan.id});
            vm.method = 'update';
            vm.newDiscount = item;
            vm.showAddNewModal();
          }
        });
      }
     
      vm.delete = function(){
        angular.forEach(vm.discounts, function(v, k) {
          if(v.checked){
            PaymentsManagerDiscountsService.deleteDiscount(v.id).then(function(response){
              if(response.error === false){
                var index = vm.discounts.indexOf(v);
                vm.discounts.splice(index, 1);
                Notification.success({ message: 'Discount deleted.', title: 'Success'});
              }else{
                Notification.error({ message: 'Discount deletion failed.', title: 'Error' });
              }
            });
          }
        });
      }

      vm.archive = function(){
        angular.forEach(vm.discounts, function(v, k) {
          if(v.checked){
            v.enabled = false;
            PaymentsManagerDiscountsService.saveDiscount(v).then(function(response){
              if(response.error === false){
                Notification.success({ message: 'Discount Archived.', title: 'Success'});
              }else{
                Notification.error({ message: 'Archiving discount failed.', title: 'Error' });
              }
            });
          }
        });
      }

    }

})();