(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.manager.discounts', {
        url: '/discounts',
        templateUrl: '/partials/payments/manager/discounts/discounts.html',
        controller: 'ManagerDiscountsCtrl as discounts',
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.payments.manager.discounts', files: ['/assets/js/controllers/payments/manager/discounts/discounts.ctrl.js'] },
              { name: 'payments.manager.discounts.service', files: ['/assets/js/services/payments/manager/discounts.service.js'] },
              { name: 'payments.manager.plans.service', files: ['/assets/js/services/payments/manager/plans.service.js'] }
            ])
          }
        }
      });
    }

})();
