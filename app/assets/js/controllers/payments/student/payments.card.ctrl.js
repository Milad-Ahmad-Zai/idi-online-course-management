(function () {
  'use strict';

  angular
    .module('ctrl.payments.card', [])

    .controller('PaymentsCardCtrl', function ($state, user, $element, Notification, countryList, PaymentsStudentService, stripePublicKey) {
      var vm = this;

      init();

      function init() {
        // data models to hold form data
        vm.cardInfo = {};
        vm.billing = {
          name: user.name
        };
        // Set country list
        vm.countries = countryList;
        // Set stripe publishable key
        Stripe.setPublishableKey(stripePublicKey);
        // Set initial state of adding card as false
        vm.addingCard = false;
      }

      vm.onSubmitted = function(card, billing) {
        vm.addingCard = true;
        addNewCard();
      }

      vm.onCancel = function() {
        $state.go('app.admin.payments.dashboard');
      }

      function addNewCard() {
        Stripe.card.createToken({
          number: vm.cardInfo.number,
          cvc: vm.cardInfo.cvc,
          exp_month: vm.cardInfo.expiryMonth,
          exp_year: vm.cardInfo.expiryYear,
          name: vm.billing.name,
          address_line1: vm.billing.address1,
          address_city: vm.billing.city,
          address_zip: vm.billing.postcode,
          address_country: vm.billing.country
        }, addCardResponse);
      }

      function addCardResponse(status, response) {
        // If error from stripe, show notification else proceed to API
        if (response.error) {
          Notification.error({ message: response.error.message, title: 'Error'})
          // re-enable card form
          vm.addingCard = false;
        } else {
          // Add card to user using tok_(token)
          var data = { card: response.id, user_id: user.id };
          PaymentsStudentService.addCustomerByCard(data).then(function(res1) {
            if (res1.error===false) {
              // Set new card as default using card_(token)
              data = { card: response.card.id, user_id: user.id };
              PaymentsStudentService.setDefaultCard(data).then(function(res2) {
                if (res2.error===false) {
                  Notification.success({ message: 'Card was created', title: 'Success'})
                  // return to dashboard and reload resolve data
                  $state.go('app.admin.payments.dashboard', {}, {reload: true});
                }
              })
            } else {
              Notification.error({ message: 'Card was not created', title: 'Error'})
              vm.addingCard = false;
            }
          });
        }
      }
    });
})();