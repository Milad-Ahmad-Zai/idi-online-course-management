(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.purchase', {
          url: '/purchase',
          views: {
            'content@app': {
              controller: 'PaymentsPurchaseCtrl as purchase',
              templateUrl: '/partials/payments/student/purchase/payments.purchase.html'
            },
            'course@app.admin.payments.purchase': {
              templateUrl: '/partials/payments/student/purchase/payments.purchase.course.html'
            },
            'card@app.admin.payments.purchase': {
              templateUrl: '/partials/payments/student/purchase/payments.purchase.card.html'
            },
            'summary@app.admin.payments.purchase': {
              templateUrl: '/partials/payments/student/purchase/payments.purchase.summary.html'
            },
            'confirmation@app.admin.payments.purchase': {
              templateUrl: '/partials/payments/student/purchase/payments.purchase.confirmation.html'
            }
          },
          resolve: {
            countryList: function(load, AdmissionsService) {
              return AdmissionsService.getCountries().then(function(res) {
                return res.data;
              });
            },
            courseList: function(load, PaymentsStudentService) {
              return PaymentsStudentService.getCourseList().then(function(res) {
                return res.data;
              });
            },
            planList: function(load, PaymentsStudentService) {
              return PaymentsStudentService.getPlanList().then(function(res) {
                return res.data;
              });
            },
            load: function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                { name: 'ctrl.payments.purchase', files: ['/assets/js/controllers/payments/student/purchase/payments.purchase.ctrl.js'] },
                { name: 'idi.payments.card', files: ['/assets/js/directives/payments/card/idi.paymentCard.js']},
                { name: 'idi.payments.summary', files: ['/assets/js/directives/payments/summary/idi.paymentSummary.js']},
                { name: 'idi.payments.card-form', files: ['/assets/js/directives/payments/card-form/idi.paymentCardForm.js']},
                { name: 'idi.wizard.progress', files: ['/assets/js/directives/common/progress/idi.wizardProgress.js']},
                { name: 'validators.stripe-card-expire', files: ['/assets/js/directives/validators/stripe-card-expire.js']},
                { name: 'validators.stripe-card-number', files: ['/assets/js/directives/validators/stripe-card-number.js'] },
                { name: 'validators.stripe-card-cvc', files: ['/assets/js/directives/validators/stripe-card-cvc.js'] },
                { name: 'validators.purchase-course-choice', files: ['/assets/js/directives/validators/purchase-course-choice.js'] }
              ])
            }
          }
        })
    }
})()