(function () {
  'use strict';

  angular
    .module('ctrl.payments.purchase', [])

    .controller('PaymentsPurchaseCtrl', function ($scope, $state, user, PaymentsStudentService, $element, Notification, userPaymentsData, countryList, courseList, planList, stripePublicKey) {
      var vm = this;

      /**
       * 
       * Initialise
       * 
       */
      init();

      function init() {
        vm.index = 0;
        // data models to hold form data
        vm.course = {};
        vm.card = {};
        vm.billing = {};
        // Load course list
        vm.courses = courseList;
        // Set plan list
        vm.plans = planList;
        // Set country list
        vm.countries = countryList;
        // Set stripe publishable key
        Stripe.setPublishableKey(stripePublicKey);
        // Parse user data
        parseUserData();
        // Set initial state of adding card as false
        vm.addingCard = false;
        // Set array of tab titles for wizard progress directive
        vm.titles = ['Choose a Plan','Billing Details','Review & Confirm','Complete'];
        // Define initial values for select boxes
        vm.initialValues = ['--Select Programme--', '--Select Currency--', '--Select Plan--'];
      }

      // parse user payments data from payments student service
      function parseUserData() {
        if (userPaymentsData.status===404) {
          vm.newCustomer = true;
          vm.billing.name = user.name;
        } else {
          vm.newCustomer = false;
          vm.userData = userPaymentsData.data;
          // Set card details
          var cards = vm.userData.stripe_data.sources.data;
          var defaultCard = vm.userData.stripe_data.default_source;
          for (var i=0; i < cards.length; i++) {
            if (cards[i].id == defaultCard) {
              vm.defaultCard = cards[i];
              vm.card = vm.defaultCard;
              break;
            }
          }
          // use card details to set values for card pane
          initialiseCardDetails();
        }
      }

      // if user has a default card then sset these details in data models
      function initialiseCardDetails() {
        vm.card.number = '**** **** **** ' + vm.defaultCard.last4;
        vm.card.expiryMonth = vm.defaultCard.exp_month;
        vm.card.expiryYear = vm.defaultCard.exp_year;
        vm.card.cvc = '***';
        vm.billing.name = vm.defaultCard.name;
        vm.billing.address1 = vm.defaultCard.address_line1;
        if (vm.defaultCard.address_line2) {
          vm.billing.address2 = vm.defaultCard.address_line2;
        }
        vm.billing.city = vm.defaultCard.address_city;
        vm.billing.country = vm.defaultCard.address_country;
        vm.billing.postcode = vm.defaultCard.address_zip;
      }

      /**
       * 
       * Wizard Stage 1 - Choose course, currency & plan
       * 
       */
      
      // Determines wether the required conditions are met to display next stage of form
      // Checks if any of the select inputs are undefined, null or set as initial value
      vm.showNext = function(index) {
        var attrs = [vm.course.name, vm.course.currency, vm.course.plan];
        var check1 = angular.isUndefined(attrs[index]);
        var check2 = attrs[index] == vm.initialValues[index];
        var check3 = attrs[index] == null;
        if ( !check1 && !check2 && !check3) { 
          return true;
        } else {
          return false;
        }
      }
      
      // When selected course is changed:
      //  1. Get all plans that match that course ID
      //  2. Generate list of all currencies available from filtered plan list
      vm.courseChange = function() {
        vm.course.currency = vm.initialValues[1];
        vm.course.plan = vm.initialValues[2];
        vm.coursePlans = vm.plans.filter(function(plan){
          return plan.course.id == parseInt(vm.course.name);
        })
        vm.availableCurrencies = getAvailableCurrencies();
      }

      // When selected currency is changed
      //  1. Filter availablePlans to only show plans that match selected currency
      vm.currencyChange = function() {
        vm.course.plan = vm.initialValues[2];
        vm.selectedPlan = null;
        vm.summaryPlan = null;
        vm.availablePlans = vm.coursePlans.filter(function(plan){
          return plan.currency.code == vm.course.currency && plan.enabled == true;
        });
      }

      // When selected plan is changed set selected plan & available discounts
      vm.planChange = function() {
        if (vm.showNext(2)) {
          vm.selectedPlan = vm.availablePlans[vm.course.plan];
          // set summary plan equal to selected plan, this will be shown in the payment summary table
          vm.summaryPlan = vm.selectedPlan;
          getRecalculatedPlan();
        }
      }

      // when user clicks to apply a discount code
      vm.applyCode = function() {
        getRecalculatedPlan(vm.course.promo);
      }

      // get recalculated plan from payments student service
      // then calculate total to be paid and set as attribute
      function getRecalculatedPlan(discountCode) {
        var id = vm.selectedPlan.id;
        // discount code will be set if called from add discount code click
        return PaymentsStudentService.getRecalculatedPlan(id, discountCode).then(function(res) {
          // set table data as result of recalculation
          vm.summaryPlan = res.data;
          vm.summaryPlan.id = id;
          showPlanDiscount(discountCode);
          calculatePlanTotal();
        })
      }

      // calculates total amount payable in plan
      function calculatePlanTotal() {
        var total = 0.0;
        for(var i=0; i < vm.summaryPlan.installments.length; i++) {
          total += parseFloat(vm.summaryPlan.installments[i].amount);
        }
        vm.summaryPlan.total = total;
      }

      // based on summaryPlan discount attribute, show relevant discount if any
      function showPlanDiscount(discountCode) {
        if (vm.summaryPlan.discount) {
          // Some discount was applied
          if(angular.isUndefined(vm.summaryPlan.discount.code)) {
            // No code attribute => early discount
            vm.summaryPlan.earlyDiscount = true;
            vm.summaryPlan.codeDiscount = false;
            // If there was an invalid discount code show notification
            if (discountCode) {
              Notification.error({ message: 'Invalid discount code', title: 'Error'});
            }
          } else {
            // Code attribute => discount code
            Notification.success({ message: 'Discount code was applied', title: 'Success'})
            vm.summaryPlan.earlyDiscount = false;
            vm.summaryPlan.codeDiscount = true;
          }
        } else {
          // No discount was applied
          // If there was an invalid discount code show notification
          if (discountCode) {
            Notification.error({ message: 'Invalid discount code', title: 'Error'});
          }
          vm.summaryPlan.earlyDiscount = false;
          vm.summaryPlan.codeDiscount = false;
        }
      }

      // Get all distinct currencies from plans that match selected course
      function getAvailableCurrencies() {
        var unique = {};
        var distinct = [];
        for( var i in vm.coursePlans ){
          if( angular.isUndefined(unique[vm.coursePlans[i].currency.code]) ){
            distinct.push(vm.coursePlans[i].currency);
          }
          unique[vm.coursePlans[i].currency.code] = 0;
        }
        return distinct;
      }

      /**
       *
       * Wizard Stage 2 - communicate with directive and if new customer, add card
       * 
       */
      
      // Get response from directive submit.
      vm.onSubmitted = function(card, billing) {
        if (vm.newCustomer==true) {
          // As we don't have stripe details to form widget, add necessary attrs
          vm.card=card;
          vm.card.exp_month = card.expiryMonth;
          vm.card.exp_year = card.expiryYear;
          vm.card.last4 = card.number.substr(card.number.length - 4);
          vm.card.name = billing.name;
          vm.card.brand = Stripe.card.cardType(card.number);
        }
        vm.submit(2);
      }

      /**
       *
       * Wizard stage 3 - add new course, and if new customer add new default card
       * 
       */
      vm.processWizard = function() {
        vm.addingCard = true;
        if (vm.newCustomer==true) {
          addNewCard();
        } else {
          purchasePlan()
        }
      }

      // add new course to user
      function purchasePlan() {
        var data = {
          plan_id: vm.summaryPlan.id,
          user_id: user.id,
          get_first_installment: true
        }
        if (vm.summaryPlan.codeDiscount) {
          data.code = vm.summaryPlan.discount.code;
        }
        // var myData = JSON.stringify(data);
        return PaymentsStudentService.createSubscription(data).then(function(res) {
          if(res.error===false) {
            // if success then redirect to confirmation pane
            vm.submit(3);
          } else {
            // if error do something
            Notification.error({title: 'Error', message: res.data.msg});
            vm.addingCard = false;
          }
        })
      }

      function addNewCard() {
        Stripe.card.createToken({
          number: vm.card.number,
          cvc: vm.card.cvc,
          exp_month: vm.card.expiryMonth,
          exp_year: vm.card.expiryYear,
          name: vm.billing.name,
          address_line1: vm.billing.address1,
          address_city: vm.billing.city,
          address_zip: vm.billing.postcode,
          address_country: vm.billing.country
        }, addCardResponse);
      }

      function addCardResponse(status, response) {
        // If error from stripe, show notification else proceed to API
        if (response.error) {
          Notification.error({ message: response.error.message, title: 'Error'})
          // re-enable card form
          vm.addingCard = false;
        } else {
          // Add card to user using tok_(token)
          var data = { card: response.id, user_id: user.id };
          PaymentsStudentService.addCustomerByCard(data).then(function(res1) {
            if (res1.error===false) {
              // Set new card as default using card_(token)
              data = { card: response.card.id, user_id: user.id };
              PaymentsStudentService.setDefaultCard(data).then(function(res2) {
                if (res2.error===false) {
                  // If card is scucessfully added and set as default then purchase plan
                  purchasePlan();
                }
              })
            } else {
              Notification.error({ message: 'Card was not created', title: 'Error'})
              vm.addingCard = false;
            }
          });
        }
      }

      /**
       *
       * Functions to manage wizard status and progression
       * 
       */
      
      // change the active ui-view by adding active class to correct ui-view
      function changePane(index) {
        var panes = $element[0].querySelectorAll('.wizard_pane');
        [].forEach.call(panes, function (pane, i) {
          if ( i === index ) {
            pane.classList.add('active');
          } else {
            pane.classList.remove('active');
          }
        });
      }

      // When stage of form is submitted check all required attributes are set before proceeding
      vm.submit = function(index) {
        var proceed = true;
        var panes = $element[0].querySelectorAll('.wizard_pane');
        if(index===-1) {
          // index===-1 => go back a stage
          vm.index--;
          changePane(vm.index);
          return;
        } else if(index===4) {
          // index===4 => exit wizard and reload resolve data
          $state.go('app.admin.payments.dashboard', {}, {reload: true});
          return;
        } else if (index===1) {
          // check validity of stage 1 of wizard
          proceed =  vm.courseForm.$valid
        }
        if(proceed===true) {
          if (vm.index < panes.length-1) {
            vm.index++;
          }
          changePane(vm.index);
        }
      }     
    });
})();