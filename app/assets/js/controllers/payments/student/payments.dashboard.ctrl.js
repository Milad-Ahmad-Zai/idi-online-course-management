(function () {
  'use strict';

  angular
    .module('ctrl.payments.dashboard', [])

    .controller('PaymentsDashboardCtrl', function (userPaymentsData, userSubscriptionData, CONFIG, $window) {
      var vm = this;

      // get user payments data from payments student service
      function parseUserData() {
        if (userPaymentsData.status===404) {
          vm.newCustomer = true;
        } else {
          vm.userData = userPaymentsData.data;
          // Set card details
          var cards = vm.userData.stripe_data.sources.data;
          var defaultCard = vm.userData.stripe_data.default_source;
          for (var i=0; i < cards.length; i++) {
            if (cards[i].id == defaultCard) {
              vm.card = cards[i]
              break;
            }
          }
        }
      }

      function parseUserSubscriptions() {
        vm.subscriptions = userSubscriptionData.data
        if (userSubscriptionData.data.length===0) {
          vm.noCourse = true;
        }
      }

      vm.getPDF = function(id) {
        return CONFIG.url + 'payments/subscription/' + id + '.pdf?access_token=' + vm.accessToken;
      }

      function init() {
        // parse user payments data
        parseUserData();
        // parse user subscription data
        parseUserSubscriptions();
        // set access token
        vm.accessToken = JSON.parse($window.localStorage.token).access_token;
      }

      init();
    });
})();