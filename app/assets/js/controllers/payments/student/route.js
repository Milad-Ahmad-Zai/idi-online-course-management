(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.payments.dashboard', {
          url: '',
          views: {
            'content@app': {
              controller: 'PaymentsDashboardCtrl as dashboard',
              templateUrl: '/partials/payments/student/payments.dashboard.html'
            }
          },
          resolve: {
            load: function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                { name: 'ctrl.payments.dashboard', files: ['/assets/js/controllers/payments/student/payments.dashboard.ctrl.js'] },
                { name: 'idi.payments.card', files: ['/assets/js/directives/payments/card/idi.paymentCard.js']},
                { name: 'idi.pages-slug', files: ['/assets/js/directives/common/pages-slug/idi.pages-slug.js']}
              ])
            }
          }
        })
        .state('app.admin.payments.card', {
          url: '/card',
          views: {
            'content@app': {
              controller: 'PaymentsCardCtrl as addCard',
              templateUrl: '/partials/payments/student/payments.card.html'
            }
          },
          resolve: {
            countryList: function(load, AdmissionsService) {
              return AdmissionsService.getCountries().then(function(res) {
                return res.data;
              });
            },
            load: function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                { name: 'ctrl.payments.card', files: ['/assets/js/controllers/payments/student/payments.card.ctrl.js'] },
                { name: 'validators.stripe-card-expire', files: ['/assets/js/directives/validators/stripe-card-expire.js']},
                { name: 'validators.stripe-card-number', files: ['/assets/js/directives/validators/stripe-card-number.js'] },
                { name: 'validators.stripe-card-cvc', files: ['/assets/js/directives/validators/stripe-card-cvc.js'] },
                { name: 'idi.payments.card-form', files: ['/assets/js/directives/payments/card-form/idi.paymentCardForm.js']}
              ])
            }
          }
        })
    }
})();