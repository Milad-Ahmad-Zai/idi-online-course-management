(function () {
  'use strict';

  angular
    .module('ctrl.users', [])

    .controller('UsersCtrl', function ($scope, $state, $rootScope, $window, $q, $modal, AuthService, NgTableParams, ProfilesService, Notification) {
      var vm = this;
      var users = [];
      var oldUser = {};
      var usersConfig = {
        start: 0,
        limit: 10,
        filter: '',
        sort: 'id'
      };
      if ($window.localStorage.users) {
        usersConfig = JSON.parse($window.localStorage.users);
      } else {
        usersConfig = {
          start: 0,
          limit: 10,
          filter: '',
          sort: 'id',
          page: 1
        };
        $window.localStorage.users = JSON.stringify(usersConfig);
      }
      var groupsConfig;
      if ($window.localStorage.groups) {
        groupsConfig = JSON.parse($window.localStorage.groups);
      } else {
        groupsConfig = {
          start: 0,
          limit: 100,
          filter: '',
          sort: 'id',
          page: 1
        };
        $window.localStorage.groups = JSON.stringify(groupsConfig);
      }

      $scope.$on('editUserModal.hide', $scope.cancelEditUserPermissions);

      $scope.getUsersList = function (config) {
        return AuthService.getUsersList(config);
      };

      $scope.getGroupsList = function (config) {
        return AuthService.getGroupsList(config);
      };

      $scope.getData = function () {
        var arrayOfRequests = [];
        var permissions = AuthService.getPermissionsList();
        var groups = $scope.getGroupsList({
          start: 0,
          limit: 100,
          filter: '',
          sort: 'id'
        });

        if ($scope.checkRights('auth.groups.list')) arrayOfRequests[0] = groups;
        if ($scope.checkRights('auth.permissions.available')) arrayOfRequests[1] = permissions;

        $q.all(arrayOfRequests).then(function(arrayOfResults) {
          if(arrayOfResults[0]) {
            $scope.groups = angular.copy(arrayOfResults[0].data.list);
          }
          if (arrayOfResults[1]) {
            $scope.services = angular.copy(arrayOfResults[1].data.services);
          }
          $scope.answers = angular.copy(AuthService.getHelp());
          $scope.cleanForm();
        });
      };

      $scope.populateUsersList = function () {
        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        $scope.usersTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            if (!$scope.isBlockView) {
              $window.localStorage.users = JSON.stringify(usersConfig);
            }
            return $scope.getUsersList(usersConfig).then(function (res) {

              users = res.data.list;
              for (var i = 0; i < users.length; i++) {
                users[i].id = +users[i].id;
              }
              params.total(parseInt(res.data.pagination.totalItems));
              $scope.usersTable.total(parseInt(res.data.pagination.totalItems));
              return res.data.list;
            });
          }
        });
      };

      $scope.cleanForm = function () {
        $scope.service = {};
        $scope.servicesNames = [];
        if ($scope.services.length) {
          $scope.currentServiceName = $scope.services[Object.keys($scope.services)[0]].name;
        }

        $scope.services.forEach(function (element, index, array) {
          $scope.service[element.name] = element.permissions;
          $scope.servicesNames.push(element.name);
        });

        $scope.newGroup = {
          allServices: angular.copy($scope.services),
          service: angular.copy($scope.service),
          permissions: [],
          description: '',
          name: ''
        };
        $scope.newUser = {
          allGroups: angular.copy($scope.groups),
          allServices: angular.copy($scope.services),
          service: angular.copy($scope.service),
          groups: [],
          permissions: [],
          name: '',
          username: '',
          email: '',
          password: ''
        };
      };

      $scope.addUser = function () {

        $scope.addUserModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/new.user.modal.html',
          show: false
        });

        $scope.addUserModal.$promise.then($scope.addUserModal.show);
      };

      $scope.createUser = function () {
        $scope.newUser.groups = $scope.newUser.allGroups.filter(function (group) { return group.selected; });
        $scope.newUser.allServices.forEach(function (element, index, array) {
          element.permissions.forEach(function (el, i, arr) {
            if (el.selected) {
              $scope.newUser.permissions.push(el.permission);
            }
          })
        });
        if ($scope.newUser.groups.length > 0) {
          AuthService.createUser($scope.newUser).then(function (res) {
            if (res.data.status == 500 || res.data.error) {
              Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
            } else {
              ProfilesService.updateProfile(res.data.user.id, {}).then(function(res) {
                if (res.status == 500 || res.error) {
                } else {
                  Notification.success({ message: 'User successfully created', title: 'Success' });
                  $scope.addUserModal.$promise.then($scope.addUserModal.hide);
                }
              });
              users.push(angular.copy(res.data.user));
              $scope.usersTable.reload();
              $scope.getData();
            }
          });
        } else {
          Notification.error({ message: 'You need to specify group', title: 'Error' });
        }
      };

      $scope.updateUser = function (e) {
        var scope = angular.element(e.target).scope();
        var user = scope.user;
        user.permissions = [];
        user.groups = user.allGroups.filter(function (group) { return group.selected; });
        user.allServices.forEach(function (element, index, array) {
          element.permissions.forEach(function (el, i, arr) {
            if (el.selected) {
              user.permissions.push(el.permission);
            }
          })
        });
        if (user.groups.length > 0) {
          var userIndex = _.findIndex(scope.$data, {"id": user.id});
          scope.$data[userIndex] = user;
          AuthService.updateUser(user).then(function (res) {
            if (res.data && res.data.status == 500 || res.data && res.data.error) {
              Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
              user = angular.copy(oldUser);
              scope.$data[userIndex] = angular.copy(oldUser);
            } else {
              Notification.success({ message: res.data.msg });
              $scope.editUserModal.$promise.then($scope.editUserModal.hide);
            }
            $scope.getData();
          });
        } else {
          Notification.error({ message: 'You need to specify group', title: 'Error' });
        }
      };

      $scope.deleteUser = function (e) {
        var scope = angular.element(e.target).scope();
        var user = scope.user;

        AuthService.deleteUser(user.id).then(function (res) {
          if (res.data && res.data.status == 500 || res.data && res.data.error) {
            Notification.error({ message: 'User haven\'t deleted', title: 'Error: ' + res.data.status});
          } else {
            Notification.success({ message: res.data.msg });
            AuthService.getUsersList().then(function (res) {
              users = res.data.list;
              $scope.usersTable.reload();
            });
          }
        });
      };

      $scope.editUserPermissions = function (e) {
        var scope = angular.element(e.target).scope();
        //scope.user.editing = true;
        oldUser = angular.copy(scope.user);

        AuthService.getFullUser(scope.user.id).then(function (res) {
          scope.user = res.data.user;
          //scope.user.editing = true;
          oldUser = angular.copy(scope.user);

          if (!scope.user.permissions) scope.user.permissions = [];
          if (!scope.user.groups) scope.user.groups = [];

          var userPermissions = scope.user.permissions;

          scope.user.allServices = angular.copy($scope.services);
          scope.user.allServices.forEach(function (element, index, array) {
            element.permissions.forEach(function (el, i, arr) {
              for (var j = 0; j < userPermissions.length; j++) {
                var permission = userPermissions[j];
                if (permission === el.permission) {
                  el.selected = true;
                }
              }
            })
          });

          var userGroups = scope.user.groups;
          scope.user.allGroups = angular.copy($scope.groups);
          scope.user.allGroups.forEach(function (element, index, array) {
            for (var j = 0; j < userGroups.length; j++) {
              var group = userGroups[j];
              if (group.id === element.id) {
                element.selected = true;
              }
            }
          });

          $scope.editUserModal = $modal({
            scope: scope,
            templateUrl: '/partials/modals/edit.user.modal.html',
            prefixEvent: 'editUserModal',
            show: false
          });

          $scope.editUserModal.$promise.then($scope.editUserModal.show);
        });
      };

      $scope.cancelEditUserPermissions = function (e) {
        var scope = e.targetScope;
        //scope.user.editing = false;
        //scope.user = oldUser;
        //scope.user.groups = _.pluck(scope.user.groups, 'name');
        //scope.user.permissions = _.pluck(scope.user.permissions, 'permission');
      };

      $scope.updateUserProfile = function (e) {
        var scope = angular.element(e.target).scope();
        scope.userProfile.age = $filter('ageFilter')(scope.userProfile.date_of_birth);
        ProfilesService.updateProfile(scope.userProfile.id, scope.userProfile).then(function(res) {

          if (res.status == 500 || res.error) {
            Notification.error({ message: 'Profile is not updated', title: 'Error: ' + res.status});
          } else {
            Notification.success({ message: 'Profile is successfully updated' });
          }
        });
      };

      $scope.cancelChanges = function (e) {
        var scope = angular.element(e.target).scope();
        scope.userProfile = angular.copy(scope.oldProfile);
      };

      $scope.changeService = function (e) {
        var selectedService = angular.element(e.target).scope().service;
        $scope.currentServiceName = selectedService.name;
        if (!(selectedService.name == $scope.currentService && $scope.currentService.name)) {
          var filtered = $scope.services.filter(function (service) {
            return service.name == service.name;
          });
          $scope.currentService = filtered[0];
        }
      };

      $scope.filterByKeyword = function (e) {
        console.log(true);
        if (e.which === 13) {
          if ($scope.filter.name.length > 0) {
            usersConfig.filter = 'search=' + $scope.filter.name;
          } else {
            usersConfig.filter = '';
          }

          if ($scope.filter.name.length > 0) {
            groupsConfig.filter = 'name==' + $scope.filter.name;
          } else {
            groupsConfig.filter = '';
          }

          usersConfig.start = 0;
          groupsConfig.start = 0;
          $scope.populateUsersList();
          $scope.populateGroupsList();
        }
      };

      $scope.viewBlockedUsers = function (isBlock) {
        if (isBlock) {
          usersConfig = {
            start: 0,
            limit: 10,
            filter: 'deleted=1',
            sort: 'id'
          };
          $scope.isBlockView = true;
        } else {
          usersConfig = {
            start: 0,
            limit: 10,
            filter: '',
            sort: 'id'
          };
          $scope.isBlockView = false;
        }
        $scope.filter.name = '';
        $scope.populateUsersList();
      };

      $scope.blockUsers = function (e) {
        var scope = angular.element(e.target).scope();
        var user = scope.user;
        user.deleted = user.deleted == '0' ? '1' : '0';

        //AuthService.updateUser(user)
        AuthService.updateUser(user).then(function (res) {
          if (res.data && res.data.status == 500 || res.data && res.data.error) {
            Notification.error({ message: res.data.msg, title: 'Error: ' + res.data.status});
            user.deleted = user.deleted == '0' ? '1' : '0';
          } else {
            Notification.success({ message: res.data.msg });
          }
        });
      };

      init();

      function init() {
        $scope.services = [];
        $scope.groups = [];
        $scope.service = {};
        $scope.servicesNames = [];
        $scope.filter = {};
        $scope.isBlockView = false;
        $scope.getData();
        $scope.populateUsersList();
      }
    });
})();
