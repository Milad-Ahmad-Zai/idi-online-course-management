(function() {
  angular
  .module('ui.routes')
  .config(config)

  function config($stateProvider) {
    $stateProvider
    .state('app.admin.users', {
      url: '/users',
      views: {
        'content@app': {
          controller: 'UsersCtrl',
          templateUrl: '/partials/users/users.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.users', files: ['/assets/js/controllers/users/users.ctrl.js']
            }
          ])
        }
      },
      data: {
        permissions: {
          only: ['auth.user.list']
        }
      }
    })
  }
})()