(function () {
  'use strict';

  angular
    .module('ctrl.files', [])

    .controller('FilesCtrl', ['$scope', '$state', '$rootScope', '$window', '$q', 'FileUploader', 'FilesService', function ($scope, $state, $rootScope, $window, $q, FileUploader, FilesService) {
      $scope.files = [
        {
          name: 'root',
          type: 'folder',
          id: 0,
          nodes: []
        }
      ];
      // $scope.files = [
      //   {
      //     name: 'test',
      //     type: 'folder',
      //     lastModifiedDate: new Date(),
      //     size: 1234567,
      //     id: 234,
      //     nodes: [
      //       {
      //         name: 'subfolder',
      //         type: 'folder',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 2345
      //       },
      //       {
      //         name: 'file',
      //         type: 'pdf',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 111
      //       }
      //     ]
      //   },
      //   {
      //     name: 'test2',
      //     type: 'folder',
      //     lastModifiedDate: new Date(),
      //     size: 1234567,
      //     id: 538
      //   },
      //   {
      //     name: 'test',
      //     type: 'folder',
      //     lastModifiedDate: new Date(),
      //     size: 1234567,
      //     id: 4,
      //     nodes: [
      //       {
      //         name: 'subfolder',
      //         type: 'folder',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 6
      //       },
      //       {
      //         name: 'file',
      //         type: 'pdf',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 8
      //       }
      //     ]
      //   },
      //   {
      //     name: 'test',
      //     type: 'folder',
      //     lastModifiedDate: new Date(),
      //     size: 1234567,
      //     id: 9,
      //     nodes: [
      //       {
      //         name: 'subfolder',
      //         type: 'folder',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 90
      //       },
      //       {
      //         name: 'file',
      //         type: 'pdf',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 890
      //       }
      //     ]
      //   },
      //   {
      //     name: 'test',
      //     type: 'folder',
      //     lastModifiedDate: new Date(),
      //     size: 1234567,
      //     id: 324,
      //     nodes: [
      //       {
      //         name: 'subfolder',
      //         type: 'folder',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 321
      //       },
      //       {
      //         name: 'file',
      //         type: 'pdf',
      //         lastModifiedDate: new Date(),
      //         size: 1234567,
      //         id: 2
      //       }
      //     ]
      //   },
      //   {
      //     name: '123',
      //     type: 'txt',
      //     lastModifiedDate: new Date(),
      //     size: 1234567,
      //     id: 112
      //   }
      // ];

      var folderConfig = {
        filter: 'parent_id=0'
      };
      $rootScope.fileUploaderDeferred = $q.defer();
      $scope.getFolder = function (id) {
        var config = {
          id: id,
          start: 0,
          limit: 100
        };
        FilesService.getFolder(config).then(function (res) {
          if(res.data){
            res.data.structure.list.forEach(function (folder) {
              var node = folder;
              if (node.type === 'folder') {
                node.nodes = [];
              }
              $scope.files[0].nodes.push(node);
            });
          }

          $rootScope.fileUploaderDeferred.resolve();
        });
      };
      $scope.getFolder(0);
    }]);
})();