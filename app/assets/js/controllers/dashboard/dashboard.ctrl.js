(function () {
  'use strict';

  angular
    .module('ctrl.dashboard', [])
    .controller('DashboardCtrl', DashboardCtrl);

  function DashboardCtrl($scope, $rootScope, $interval, cfpLoadingBar, profile, PermissionsService, Notification, $modal, $compile, userDefaultCalendar, userDefaultCalendarEvents, CalendarService, $timeout, clipboard, userSubscriptionData) {
    var vm = this;
    init();

    function init() {
      getDate();
      $scope.profile = profile;
      $scope.events = userDefaultCalendarEvents;
      /* event sources array*/
      $scope.eventSources = [$scope.events];
      $scope.urlICalendar = "";
      getUrlICalendar();

      if (!clipboard.supported) {
        $scope.errorCopy = true;
      }
      parseUserSubscriptions();
      $scope.todaysEvents = getTodaysEvents();
    }

    function getDate() {
      $scope.date = new Date();
    }

    function parseUserSubscriptions() {
      $scope.subscriptions = userSubscriptionData.data
      if (userSubscriptionData.data.length===0) {
        $scope.noCourse = true;
      }
    }

    function getTodaysEvents() {
      var theEvents = _.filter($scope.events, function (o) {
        var check1 = moment($scope.date).isBetween(o.start, o.end, null, '[]');
        var check2 = moment($scope.date).isSame(o.start,'day') && o.fullday==true;
        return check1 || check2;
      });
      return theEvents;
    }

    // When user clicks on event on today widget
    vm.todayClick = function(id) {
      eventClick($scope.todaysEvents[id]);
    }

    //Helper reinit calendar
    $scope.flagCalendar = true;

    //Calendar option
    $scope.calendarOptions = {
      customClass: getDayClass,
      showWeeks: false
    };

    /* config object */
    $scope.uiConfig = {
      calendar: {
        customButtons: {
          iCalButton: {
            text: 'iCal',
            click: clickHandler
          }
        },
        buttonText: {
          today: 'Today'
        },
        displayEventTime:false,
        height: 450,
        editable: false,
        firstDay:1,
        header: {
          left: 'title',
          center: '',
          right: 'iCalButton today prev,next'
        },
        eventClick: eventClick,
        eventRender: eventRender,
        dayClick: eventAddModal
      }
    };

    $scope.hasPermissions = function (permission) {
      return PermissionsService.has(permission);
    };

    //Add event modal form submit
    $scope.addEvent = function () {
      var eventToSend = $scope.event;
      if (eventToSend.start) eventToSend.start = moment(eventToSend.start).format("YYYY-MM-DD HH:mm:ss");
      if (eventToSend.end) eventToSend.end = moment(eventToSend.end).format("YYYY-MM-DD HH:mm:ss");

      CalendarService.createEvent(eventToSend).then(function (res) {
        $scope.events.push(res.data);
        reinitCalendar();
      });
    };

    //Update event modal form submit
    $scope.updateEvent = function () {
      var eventToSend = $scope.event;
      if (eventToSend.start) eventToSend.start = moment(eventToSend.start).format("YYYY-MM-DD HH:mm:ss");
      if (eventToSend.end) eventToSend.end = moment(eventToSend.end).format("YYYY-MM-DD HH:mm:ss");
      
      CalendarService.updateEvent(eventToSend).then(function (res) {
        $scope.events.splice(_.findIndex($scope.events, {'uuid': res.data.uuid}), 1);
        $scope.events.push(res.data);
        reinitCalendar();
      });
    };

    /* remove event */
    $scope.remove = function (uuid) {
      CalendarService.deleteEvent(uuid).then(function (res) {
        $scope.events.splice(_.findIndex($scope.events, {'uuid': uuid}), 1);
        reinitCalendar();
      });
    };

    /* Render Tooltip */
    function eventRender(event, element, view) {
      element.attr({'tooltip': event.title, 'tooltip-append-to-body': true});
      $compile(element)($scope);
    }

    /* alert on eventClick */
    function eventClick(date, jsEvent, view) {
      $scope.event = date;
      $scope.event.start = moment($scope.event.start).format("YYYY-MM-DD HH:mm:ss");
      if ($scope.event.end) $scope.event.end = moment($scope.event.end).format("YYYY-MM-DD HH:mm:ss");
      $scope.editEventModal.$promise.then($scope.editEventModal.show);
    }

    function eventAddModal(start, end) {
      var curMin = 0;
      var curHour = moment().get('hour');
      curMin = moment().get('minute') < 30 ? 0 : 30;

      $scope.event = {
        start: moment(start).set('hour', curHour).set('minute', curMin),
        end: moment(start).set('hour', moment().add(1, 'h').get('hour')).set('minute', curMin)
      };

      $scope.addEventModal.$promise.then($scope.addEventModal.show);
    }

    //Set calendar day class & tooltip
    function getDayClass(data) {
      var dayEvents = _.filter($scope.events, function (o) {
        return moment(data.date).isBetween(o.start, o.end, null, '[]');
      });

      if (dayEvents.length > -1) {
        var toollTip = "", eventClass;
        for (var i = 0; i < dayEvents.length; i++) {
          toollTip += '<p>' + dayEvents[i].title + '</p>';
        }
        if (dayEvents.length == 1) {
          eventClass = dayEvents[0].className;
        } else {
          eventClass = _.map(dayEvents, function (event) {
            return event.className;
          });
          if (_.uniq(eventClass).length > 1) {
            eventClass = "many";
          } else {
            eventClass = eventClass[0];
          }
        }

        return {
          "toollTip": toollTip,
          "className": eventClass
        }
      }
    }

    function reinitCalendar() {
      $scope.flagCalendar = false;
      $scope.dt = _.now();
      $timeout(function () {
        $scope.flagCalendar = true;
      });
      // refresh todaysEvents to show new/updated events in today widget
      $scope.todaysEvents = getTodaysEvents();
    }

    //Add event modal
    $scope.addEventModal = $modal({
      scope: $scope,
      templateUrl: '../../partials/modals/add.event.modal.html',
      show: false
    });

    //Edit event modal
    $scope.editEventModal = $modal({
      scope: $scope,
      templateUrl: '../../partials/modals/edit.event.modal.html',
      show: false
    });

    function getUrlICalendar() {
      if (!userDefaultCalendar.token) {
        CalendarService.renewCalendarToken(userDefaultCalendar.id).then(function (res) {
          $scope.urlICalendar = res.data.token;
        });
      } else {
        $scope.urlICalendar = userDefaultCalendar.token;
      }
    }

    function clickHandler () {
      if (!$scope.errorCopy){
        clipboard.copyText($scope.urlICalendar);
        Notification.success({title: 'Success', message: $rootScope.locale.dashboard.calendar.icalexport.success});
      }else{
        Notification.error({title: 'Error', message: $rootScope.locale.dashboard.calendar.icalexport.error});
      }
    }

    vm.calSize = calendarSize();

    function calendarSize() {
      // Check user has ability to add a todo task
      var check1 = $scope.hasPermissions('todo.task.create');
      // Check user can buy courses
      var check2 = $scope.hasPermissions('payments.role.subscriber');
      if(!check1 && !check2) {
        return 12;
      } else if(!check1 || !check2) {
        return 9;
      } else if(check1 && check2) {
        return 6;
      }
    }

    vm.notifications = [
      {"id":"001", "type":"message", "date":"2016-12-14 09:38:27", "message":{ "id":"25", "name":"Vitalij", "content":"Do you like the new notifications widget"}},
      {"id":"002", "type":"alert", "date":"2016-12-14 07:38:27", "content":"Site will be down for maintenance on 25/12/2016" },
      {"id":"003", "type":"calendar", "date":"2016-12-12 09:38:27", "calendar":{ "title":"Very important event", "start":"11:00", "end":"13:30"}},
      // {"id":"004", "type":"upload", "date":"2016-12-7 09:38:27", "upload":{ "title":"eps1.7_wh1ter0se.m4v", "url":"http://www.google.com"}},
      // {"id":"005", "type":"forum", "date":"2016-11-14 09:38:27", "topic":{ "name":"Important module discussion", "url":"http://www.google.com"}},
    ]
  }
})();