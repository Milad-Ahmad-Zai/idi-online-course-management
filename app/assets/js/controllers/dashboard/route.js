(function () {
  angular
    .module('ui.routes')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('app.admin.dashboard', {
        url: '/dashboard',
        views: {
          'content@app': {
            controller: 'DashboardCtrl as dashboard',
            templateUrl: '/partials/admin/dashboard.html'
          }
        },
        resolve: {
          userDefaultCalendar: function (load, CalendarService, user) {
            return CalendarService.getUserDefaultCalendar(user.id)
              .then(function (res) {
                  return res.data;
              })
          },
          userDefaultCalendarEvents: function (load, CalendarService, userDefaultCalendar) {
            return CalendarService.getEventList().then(function (res) {
               return res.data;
            })
          },
          userSubscriptionData: function(load, PaymentsStudentService, user) {
            return PaymentsStudentService.getUserSubscriptions(user.id).then(function(res) {
              return res;
            })
          },
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.dashboard', files: ['/assets/js/controllers/dashboard/dashboard.ctrl.js']},
              {name: 'idi.profile.widget', files: ['/assets/js/directives/common/profile/idi.profileWidget.js']},
              {name: 'idi.notifications.widget', files: ['/assets/js/directives/common/notifications/idi.notificationsWidget.js']},
              {name: 'idi.bioCard', files: ['/assets/js/directives/common/bio-card/idi.bio-card.js']},
              {name: 'ui.dashboard.today', files: ['/assets/js/directives/dashboard/today/ui.dashboardToday.js']},
              {name: 'ui.dashboard.programmesTable', files: ['/assets/js/directives/dashboard/programmesTable/ui.programmesTable.js']},
              {name: 'ui.dashboard.todo', files: ['/assets/js/directives/dashboard/todo/idi.todo-list.js']},
              {name: 'ui.courses.tutorModulesTable', files: ['/assets/js/directives/courses/ui.tutorModulesTable.js']},
              {name: 'calendar.service', files: ['/assets/js/services/calendar.service.js']},
              {name: 'ui.calendar', files: ['/assets/js/vendor/angular/angular-ui-calendar.js']},
              {name: 'angular-clipboard', files: ['/assets/js/vendor/angular/angular-clipboard.js']},
              {name: 'idi.payments.subscriptions', files: ['/assets/js/directives/payments/subscriptions/idi.paymentSubscriptions.js']}
            ]);
          }
        },
        data: {
          permissions: {
            except: ['anonymous'],
            redirectTo: 'app.login'
          }
        }
      })
  }
})()