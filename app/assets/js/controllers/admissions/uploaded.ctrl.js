(function () {
  'use strict';

  angular
    .module('ctrl.admissions.uploaded', [])

    .controller('AdmissionsUploadedCtrl', ['$scope', '$rootScope', '$state', '$stateParams', '$filter', '$modal', 'Notification', 'AdmissionsService', function ($scope, $rootScope, $state, $stateParams, $filter, $modal, Notification, AdmissionsService) {
      var id = $stateParams.id || $rootScope.userId;
      $scope.sheetData = [];
      $scope.admissionData = {};

      var fileCategories = [
        'Personal',
        'Academic',
        'Employment',
        'Creative'
      ];

      var admissionFilesConfig = {
        start: 0,
        limit: 100,
      };

      $scope.categoryNames = {
        'Personal': 'Personal',
        'Academic': 'Academic History',
        'Employment': 'Employment & Experience',
        'Creative': 'Creative work'
      };

      $scope.getFiles = function () {
        AdmissionsService.getAdmissionFiles(id, admissionFilesConfig).then(function (res) {
          $scope.allFiles = res.data.items;
          $scope.files = {};

          // update 'seen' of files
          $scope.allFiles.forEach(function (item) {
            if (!item.seen || item.seen == "0") {
              var data = {
                admissionId: id,
                fileId: item.file_id,
                admissionFileId: item.id,
                categoryId: item.category_id,
                seen: 1
              };
              AdmissionsService.updateAdmissionFile(data).then(function (res) { });
            }
          });

          fileCategories.forEach(function (element, index, array) {
            AdmissionsService.getFileCategoryId(element).then(function (res) {
              $scope.files[element] = _.filter($scope.allFiles, function(file) {
                return file.category_id === res.data.toString() && file.file.id > 0;
              });
            });
          });
        });
      };

      $scope.displayImage = function (item) {
        $scope.imageItem = item;
        $scope.displayImageModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/image.modal.html',
          show: false
        });

        $scope.displayImageModal.$promise.then($scope.displayImageModal.show);
      };

      if (id && !$scope.userProfileId && $scope.checkRights('profiles.uploaded') || id && $scope.userProfileId && $scope.checkRights('profiles.other.uploaded')) {
        $scope.getFiles();
      }
    }]);
})();