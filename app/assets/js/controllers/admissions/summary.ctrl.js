(function () {
  'use strict';

  angular
    .module('ctrl.admissions.summary', [])

    .controller('AdmissionsSummaryCtrl', function ($scope, $rootScope, $state, $stateParams, $filter, Notification, user, AdmissionsService, AuthService, NotesService) {
      var id = $stateParams.id || user.id;
      $scope.sheetData = [];
      $scope.admissionData = {};
      $scope.summary = {};
      $scope.summaryNote = {};

      $scope.getUserSummarySheet = function (id) {
        AdmissionsService.getUserSummarySheets(id).then(function (res) {
          if (!(res.error || res.data && res.data.error) && res.data) {
            var tempData;
            if (_.isArray(res.data) && res.data.length === 0) {
              $scope.summary = {};
            } else {
              tempData = res.data[0];
              for (var prop in tempData) {
                if(_.isString(tempData[prop]) && prop != 'created' && prop != 'updated') {
                  tempData[prop] = JSON.parse(tempData[prop])
                }
              }
              $scope.summary = tempData;
              $scope.loadNote();
            }
          }
        });
      };

      $scope.getAdmission = function () {
        AdmissionsService.getAdmission(id).then(function (res) {
          if (res && res.status !== 204) {
            $scope.admissionData = res.data;
            $scope.admissionData.state = parseInt($scope.admissionData.state) == 1;
          }
        });
      };

      if ($scope.checkRights('admissions.summary')) {
        if (id) {
          $scope.getUserSummarySheet(id);
          $scope.getAdmission();
        } else {
          id = user.id;

          $scope.getUserSummarySheet(id);
          $scope.getAdmission();
        }
      }

      $scope.changeSubmitConfirmation = function () {
        var data = {};
        if ($scope.admissionData.state) {
          data = { state: 1 };
        } else {
          data = { state: 0 };
        }
        
        AdmissionsService.saveAdmission(id, data).then(function (res) {
          if(res.data.error) {
            Notification.error({message: 'Submit confirmation was not updated', title: 'Error: ' + res.status});
          } else {
            Notification.success({message: 'Submit confirmation changed.', title: 'Success'});
          }
        }, function (res) {
          Notification.error({ message: 'Submit confirmation was not updated', title: 'Error'});
        });
      };

      $scope.saveNote = function (e) {
        if ($scope.summaryNote.note_id) {
          $scope.updateNote();
        } else {
          $scope.createNote();
        }
      };

      $scope.createNote = function (e) {
        var data = {
          source: 'summary',
          source_id: $scope.summary.id,
          body: $scope.summaryNote.body,
          created_by: user.id
        };
        NotesService.createNote(data).then(function (res) {
          if (res.error) {
            Notification.error({ message: 'Note was not created', title: 'Error'})
          } else {
            Notification.success({ message: 'Note was created', title: 'Success'})
          }
        })
      };

      $scope.updateNote = function (e) {
        var data = {
          source: 'summary',
          source_id: $scope.summary.id,
          body: $scope.summaryNote.body,
          created_by: user.id
        };
        NotesService.updateNote($scope.summaryNote.note_id, data).then(function (res) {
          if (res.error) {
            Notification.error({ message: 'Note was not updated', title: 'Error'})
          } else {
            Notification.success({ message: 'Note was updated', title: 'Success'})
          }
        })
      };

      $scope.deleteNote = function (e) {
        if ($scope.summaryNote.note_id) {
          NotesService.getLatestNoteSourceId($scope.summary.id, 'summary').then(function (res) {
            if (!res.error) {
              //$scope.summaryNote = res.data && res.data[0];
              $scope.summaryNote = {};
              if (res.data.length === 0) {
                Notification.error({message: 'You don\'t have a note', title: 'Error'})
              } else {
                NotesService.deleteNote($scope.summaryNote.note_id).then(function (res) {
                  if (res.error) {
                    Notification.error({message: 'Note was not deleted', title: 'Error'})
                  } else {
                    Notification.success({message: 'Note was deleted', title: 'Success'})
                  }
                });
              }
            }
          });
        } else {
          $scope.summaryNote.body = '';
        }
      };

      $scope.loadNote = function () {
        if ($scope.summary) {
          NotesService.getLatestNoteSourceId($scope.summary.id, 'summary').then(function (res) {
            if (!res.error) {
              $scope.summaryNote = res.data && res.data[0];
            }
          });
        }
      };
    });
})();