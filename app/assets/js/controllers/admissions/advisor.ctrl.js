(function () {
  'use strict';

  angular
    .module('ctrl.admissions.advisor', ['angular-svg-round-progress', 'cfp.hotkeys'])

    .controller('AdmissionsAdvisorCtrl', function ($scope, $rootScope, $state, $timeout, $filter, $modal, $window, $q, NgTableParams, AdmissionsService, AuthService, NotesService, ProfilesService, FilesService, SalesforceService, ListcacheService, Notification, user, cfpLoadingBar, MessageService, hotkeys) {
      $scope.assignedData = [];
      $scope.list = 'all';
      $scope.filterBy = {
        'name': {
          type: 'name',
          query: ''
        }
      };
      $scope.filter = {};

      $scope.loadedPage = true;

      $scope.applicantsImages = {};
      $scope.applicantsIds = [];
      $scope.advisors = [];
      $scope.usersForTask = [];

      var applicantItems;

      var bulk = {
        wind: 'cold',
        fire: 'hot',
        cloud: 'neutral'
      };

      var fileCategories = [
        'Personal',
        'Academic',
        'Employment'/*ADMI-96,
        'Creative'*/
      ];

      var applicantsConfig;

      if ($window.localStorage.applicants) {
        applicantsConfig = JSON.parse($window.localStorage.applicants);
      } else {
        applicantsConfig = {
          start: 0,
          limit: 10,
          filter: '',
          sort: {
            id: 'asc'
          },
          page: 1
        };
      }

      $scope.$watch(function () {
        return applicantsConfig;
      }, function (newValue) {
        $window.localStorage.applicants = JSON.stringify(newValue || '{}');
      }, true);

      $scope.onEnter = function (e, hotkey) {
        e.preventDefault();
        $scope.applicants.reload();
      };

      hotkeys.add({
        combo: 'enter',
        description: 'Searching on enter',
        callback: $scope.onEnter
      });

      $scope.clearFilters = function () {
        angular.extend($scope.filter, {
          name: '',
          id: '',
          advisor: -1,
          temperature: -1,
          dateFrom: null,
          dateTo: null,
          programme: -1,
          opportunity_stage: -1,
          unseen: false,
          submitted: false,
          apl: -1
        })
        $scope.list = 'all';
        $scope.applicants.reload();
      }

      // FILTERS

      var filtersArray = {};

      var filterConfigFromString = function (filtersString) {
        filtersString = filtersString || '';
        filtersString = filtersString.replace('==', '=');
        filtersString = filtersString.replace('>', '=>');
        filtersString = filtersString.replace('<', '=<');
        var filterArray = filtersString.split('/');
        var filteredArray = filterArray.map(function(item) {
          var obj = {
            values: []
          };
          var splittedItem;
          var values;
          if (_.contains(item, '||')) {
            values = item.split('||');
            values.forEach(function(value) {
              splittedItem = value.split('=');
              obj.type = splittedItem[0];
              obj.values.push(splittedItem[1]);
            });
          } else if (_.contains(item, '=')) {
            splittedItem = item.split('=');
            obj.type = splittedItem[0];
            obj.values.push(splittedItem[1]);
          }
          return obj.type ? obj : {type: item};
        });
        // ❌ TO REMOVE: avoid side effects
        // filtersArray = _.compact(filteredArray);
        return _.compact(filteredArray);
      };

      var filterConfigToString = function (array) {
        var string = '';
        array.forEach(function(element) {
          var elementString = '/';
          if (element.values && element.values.length > 0) {
            element.values.forEach(function (value, index) {
              if (index !== 0) {
                elementString += '||';
              }

              elementString += element.type;
              switch (value[0]) {
                case '!':
                  elementString += '!=';
                  break;
                case '>':
                case '<':
                  break;
                default:
                  elementString += '=';
              }
              elementString += value;

            });
          } else {
            elementString += element.type;
          }
          string += elementString;
        });
        return string;
      };

      var tempAdvisor, tempStage, tempApl, tempProgramme;

      function setFilter (filterString) {
        var filters = filterConfigFromString(filterString);
        filters.forEach(function (item) {
          switch (item.type) {
            case 'id':
            case 'apl':
            case 'name':
              $scope.filter[item.type] = item.values[0];
              break;
            case 'temperature':
              $scope.filter[item.type] = item.values[0];
              break;
            case 'opportunity_stage':
              tempStage = decodeURI(item.values[0]);
              break;
            case 'programme':
              tempProgramme = decodeURI(item.values[0]);
              break;
            case 'created':
              if (item.values[0][0] === '>') {
                $scope.filter.dateFrom = new Date(item.values[0].substring(1));
              } else if (item.values[0][0] === '<') {
                $scope.filter.dateTo = new Date(item.values[0].substring(1));
              }
              break;
            case 'unseen':
              $scope.filter.unseen = true;
              break;
            case 'state':
              if (item.values[0] == 1) {
                $scope.filter.submitted = true
              }
              break;
            case 'advisor_id':
              var isNot = item.values[0][0] === '!';
              if (isNot) {
                item.values[0] = item.values[0].substring(1);
              }
              if (item.values[0] == user.id) {
                if (isNot) {
                  $scope.list = 'not-assigned';
                } else {
                  $scope.list = 'assigned';
                }
              } else {
                tempAdvisor = item.values[0];
              }
              break;
            case 'completed':
              if (item.values[0] == 1) {
                $scope.list = 'completed';
              }
              break;

          }
        });
        $scope.filter.temperature = $scope.filter.temperature || -1;
        $scope.filter.apl = $scope.filter.apl || -1;
      }

      setFilter(applicantsConfig.filter);

      function getFilter () {
        var types = ['id', 'temperature', 'opportunity_stage', 'apl', 'programme'];
        var searchArray = types.reduce(function (prev, key) {
          if ($scope.filter[key] && $scope.filter[key] != -1) {
            prev.push({
              type: key,
              values: [encodeURI($scope.filter[key])]
            });

          }
          return prev;
        }, []);

        if ($scope.filter.name) {
          searchArray.push({
            type: 'name',
            values: ['=' + $scope.filter.name]
          })
        }

        if ($scope.filter.dateFrom) {
          searchArray.push({
            type: 'created',
            values: ['>' + $filter('date')($scope.filter.dateFrom, 'yyyy-MM-dd')]
          })
        }

        if ($scope.filter.dateTo) {
          searchArray.push({
            type: 'created',
            values: ['<' + $filter('date')($scope.filter.dateTo, 'yyyy-MM-dd')]
          })
        }

        if ($scope.filter.unseen) {
          searchArray.push({
            type: 'unseen'
          })
        }

        if ($scope.filter.submitted) {
          searchArray.push({
            type: 'state',
            values: ['1']
          })
        }

        switch ($scope.list) {
          case 'assigned':
            searchArray.push({
              type: 'advisor_id',
              values: [user.id]
            });
            break;
          case 'not-assigned':
            searchArray.push({
              type: 'advisor_id',
              values: ['!' + user.id, 'null']
            })
            break;
          case 'completed':
            searchArray.push({
              type: 'completed',
              values: ['1']
            })
            break;
          default:
            searchArray.push({
              type: 'completed',
              values: ['null', '0']
            });
            if ($scope.filter.advisor && $scope.filter.advisor != '-1') {
              searchArray.push({
                type: 'advisor_id',
                values: [$scope.filter.advisor]
              })
            }
        }
        return filterConfigToString(searchArray);
      }

      $scope.filterList = function (list) {
        $scope.list = list;
        var lastLoginFilter = '-';
        var createdFilter = '';
        var updateData = function (items) {
          $scope.assignedData = items;
          $scope.applicantsIds = [];
          $scope.assignedData.forEach(function (item) {
            item.id = parseInt(item.id);
            item.unseen = parseInt(item.unseen);
            item.total_files = parseInt(item.total_files);
            $scope.applicantsIds.push(parseInt(item.id));

            if ((item.last_login !== '0000-00-00 00:00:00') && !_.isNull(item.last_login)) {
              var lastLogin = moment.utc(item.last_login);
              item.lastLogin = lastLogin.toDate();
              item.lastLoginDuration = lastLogin.fromNow(true);
              lastLoginFilter = $filter('date')(item.lastLogin, 'dd/MM/yyyy hh:mm a');
            } else {
              item.lastLoginDuration = '-';
            }

            if (item.created && (item.created !== '0000-00-00 00:00:00') && !_.isNull(item.created)) {
              var created = moment.utc(item.created);
              item.created = created.toDate();
              createdFilter = $filter('date')(item.created, 'dd/MM/yyyy');
              item.createdDuration = $filter('date')(item.created, 'dd/MM/yy');
            } else {
              item.createdDuration = '-';
            }

            if (!item.completness) {
              item.completness = 0;
            }

            item.tooltip = {
              title: '<div class="applicant-info-tooltip">' +
              '<div>Date Created: ' + createdFilter + '</div>' +
              '<div>Last Visited: ' + lastLoginFilter + '</div>' +
              '<div>Application ID: ' + item.id + '</div>' +
              '<div>E-mail: ' + item.email + '</div>' +
              '</div>'
            };

            // count of messages sent since last login
            $scope.getSentMessages(item);
          });
          ProfilesService.getBulkBasicProfiles($scope.applicantsIds).then(function(res) {
            $scope.applicantsImages = res.data;
          });
          return $scope.assignedData;
        };

        var sort = {};
        var splittedSort;

        if (applicantsConfig.sort && !_.isObject(applicantsConfig.sort)) {
          if (applicantsConfig.sort.indexOf('-') > -1) {
            splittedSort = applicantsConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[applicantsConfig.sort] = 'asc';
          }
        } else if (applicantsConfig.sort && _.isObject(applicantsConfig.sort)) {
          applicantsConfig.sort = convertSortFromTable();
          sort = applicantsConfig.sort;
        } else {
          sort = {
            id: 'asc'
          };
        }



        $scope.applicants = new NgTableParams({
          page: applicantsConfig.page || 1,
          count: applicantsConfig.limit,
          sorting: sort
        }, {
          getData: function ($defer, params) {
            applicantsConfig.filter = getFilter();
            if ($scope.loadedPage) {
              $scope.isListNew = true;
              $scope.loadedPage = false;
            } else {
              $scope.isListNew = false;
            }
            applicantsConfig.limit = params.count();
            applicantsConfig.start = ((params.page() || 1) - 1) * params.count();
            applicantsConfig.page = params.page();
            if (params.sorting() && !_.isEmpty(params.sorting())) {
              applicantsConfig.sort = params.sorting();
              applicantsConfig.sort = convertSortFromTable();
            } else {
              delete applicantsConfig.sort;
            }
            $window.localStorage.applicants = JSON.stringify(applicantsConfig || {});
            cfpLoadingBar.start();

            return $scope.getApplicants($scope.isListNew).then(function (res) {
              if (res.status == 204) {
                applicantItems = [];
              } else {
                applicantItems = res.data.list;

                if (_.contains($scope.filterBy, 'id') && $scope.filter.id > 0) {
                  applicantItems = [res.data];
                  res.data.pagination = {
                    currentPage: 1,
                    itemsCount: 1,
                    limit: '10',
                    start: '0',
                    totalItems: 1,
                    totalPages: 1
                  };
                }

                if (res.data.pagination.totalPages !== 0 && res.data.pagination.currentPage > res.data.pagination.totalPages) {
                  params.page(res.data.pagination.totalPages);
                  $scope.applicants.reload();
                }
              }
              cfpLoadingBar.complete();
              if (_.contains($scope.filterBy, 'id') && $scope.filter.id > 0) {
                params.total(1);
                $scope.applicants.total(1);
                if (res.status == 204) {
                  return updateData(applicantItems);
                }
                return updateData([res.data]);
              } else {
                $scope.totalApplicants = parseInt(res.data.pagination.totalItems);
                params.total($scope.totalApplicants);
                $scope.applicants.total($scope.totalApplicants);
                return updateData(res.data.list);
              }
            });
          }
        });

      };

      $scope.bulkAction = function (type, option) {
        var ids = [];
        var data = $scope.assignedData;

        data.forEach(function(item){
          if (item.checked) {
            ids.push(item.id);
          }
        });
        var dataToSend = {
          ids: ids,
          params: {}
        };

        if (type === 'delete') {
          $scope.deleteAdmission(ids);
        }
        if (type.indexOf('completed') > -1) {

          if (type.indexOf('completed=1') > -1) {
            dataToSend.params.completed = '1';
          } else {
            dataToSend.params.completed = '0';
          }
          AdmissionsService.bulkMethod(dataToSend).then(function(res) {
            $scope.applicants.reload();
          });
        } else if (type === 'wind' || type === 'fire' || type === 'cloud') {
          dataToSend.params.temperature = bulk[type];
          AdmissionsService.bulkMethod(dataToSend).then(function(res) {
            if(res.data.error) {
              Notification.error({ message: 'Temperature was not changed', title: 'Error: ' + res.status});
            } else {
              $scope.applicants.reload();
            }
          });
        } else if (type === 'opportunity_stage') {
          dataToSend.params.opportunity_stage = option;
          AdmissionsService.bulkMethod(dataToSend).then(function(res) {
            if(res.data.error) {
              Notification.error({ message: 'Opportunity stage was not changed', title: 'Error: ' + res.status});
            } else {
              $scope.applicants.reload();
            }
          });
        }
      };

      $scope.filterByKeyword = function (e) {
        if (e.which === 13) {
          applicantsConfig.start = 0;
          applicantsConfig.limit = 10;
          applicantsConfig.filter = '';

          $scope.filter.id = parseInt($scope.filter.id);
          $scope.filterList('all');
        }
      };

      $scope.filterBySelect = function () {
        applicantsConfig.start = 0;
        applicantsConfig.limit = 10;
        applicantsConfig.filter = '';

        $scope.filterList('all');
      };

      if (!$rootScope.advisorsDeferred) {
        $scope.newAdvisorQuery = true;
        $rootScope.advisorsDeferred = $q.defer();
      }
      if ($scope.newAdvisorQuery) {
        var advisorConfig = {
          start: 0,
          limit: 100,
          filter: 'group=Admissions Advisor',
          sort: 'name'
        };
        AuthService.getUsersList(advisorConfig).then(function (res) {
          if (res.data) {
            $rootScope.advisorsDeferred.resolve(res);
            $scope.advisors = res.data.list;
            $timeout(function () {
              $scope.filter.advisor = tempAdvisor || -1;
            })
          }
        });
      } else {
        $rootScope.advisorsDeferred.promise.then(function (res) {
          if (res.data) {
            $scope.advisors = res.data.list;
            $timeout(function () {
              $scope.filter.advisor = tempAdvisor || -1;
            })
          }
        });
      }

      $scope.filterList('all');

      AdmissionsService.getOptionsByField('opportunity_stage').then(function (res) {
        $scope.opportunityStageOptions = Object.keys(res.data);
        $timeout(function () {
          $scope.filter.opportunity_stage = tempStage || -1;
        })
      });

      ProfilesService.getProgrammeList().then(function (res) {
        $scope.preferredProgrammeOptions = res.data;
        $timeout(function () {
          $scope.filter.programme = tempProgramme || -1;
        })
      })

      $scope.reportButton = ListcacheService.getReportUrl();

      function convertSortFromTable () {
        var data = {};
        if (!_.isObject(applicantsConfig.sort)) {
          return applicantsConfig.sort;
        }
        if (applicantsConfig.sort) {
          data.sort = Object.keys(applicantsConfig.sort)[0];
          if (applicantsConfig.sort[Object.keys(applicantsConfig.sort)[0]] == 'desc') {
            data.sort = '-' + data.sort;
          }
        }
        if (!data.sort) {
          data.sort = '';
        }
        return data.sort;
      }

      $scope.getApplicants = function () {
        return ListcacheService.getApplicantsCachedList(applicantsConfig);
      };

      $scope.deleteAdmission = function (ids, applicant) {
        var single = false;

        if (ids.length === 1) single = true;

        var data = $scope.assignedData;

        AdmissionsService.deleteAdmission(ids).then(function (res) {
          var message = '';
          if(res.data.error) {
            message = single ? 'Admissions was not deleted' : 'Admission was not deleted';

            Notification.error({message: message, title: 'Error: ' + res.status});
          } else {
            message = single ? 'Admissions successfully deleted' : 'Admission successfully deleted';

            Notification.success({message: message, title: 'Success'});
            if (applicant) {
              _.remove(data, function(item) {
                return item.id === applicant.id;
              });
            } else {
              _.remove(data, function(applicant) {
                return applicant.checked === true;
              });
            }

            $scope.applicants.reload();
          }
        }, function (res) {
          var message = single ? 'Admissions was not deleted' : 'Admission was not deleted';

          Notification.error({ message: message, title: 'Error'});
        });
      };

      $scope.deleteApplicant = function (applicant) {
        var ids = [];
        ids.push(applicant.id);

        $scope.deleteAdmission(ids, applicant);
      };

      $scope.previewApplicant = function (applicant) {
        $state.go('app.admin.admissions.student', {id: applicant.id});
      };


      $scope.addTask = function (item) {
        SalesforceService.getUsersList().then(function (users) {
          $scope.usersForTask = users;
          $scope.task = {
            studentId: item.id
          }

          var modal = $modal({
            scope: $scope,
            templateUrl: '/partials/modals/admission.task.modal.html',
            show: true
          })
        })

      }

      $scope.submitTask = function () {
        var isValid = $scope.task.assignedTo && $scope.task.subject
          && $scope.task.comments;
        if (!isValid) return false;

        SalesforceService.addTask({
          profile_id: $scope.task.studentId,
          subject: $scope.task.subject,
          comment: $scope.task.comments,
          assigned_to: $scope.task.assignedTo,
          due_date: $scope.task.dueDate,
          reminder_date: $scope.task.reminderDate
        }).then(function () {
          Notification.success({ message: 'Task was created', title: 'Success'})
        }, function () {
          Notification.error({ message: 'Task was not created', title: 'Error'})
        })
        return true;
      }

      // NOTES
      $scope.editNote = function (e, viewNote) {
        var scope = angular.element(e.target).scope();
        var modal;

        NotesService.getLatestNoteSourceId(scope.item.id, 'applicant').then(function (res) {
          if (!res.error) {
            $scope.currentNote = res.data && res.data[0];
            $scope.viewNote = viewNote ? true : false;
            if (res.data.length === 0) {
              scope.note = {
                text: '',
                isNew: true
              };
            } else {
              scope.note = {
                text: res.data[0].body,
                isNew: false
              };
            }

            modal = $modal({
              scope: scope,
              templateUrl: '/partials/modals/admission.note.modal.html',
              show: false
            });
            modal.$promise.then(modal.show);
          }
        });
      };

      $scope.createNote = function (e) {
        var scope = angular.element(e.target).scope();
        var data = {
          source: 'applicant',
          source_id: scope.item.id,
          body: scope.note.text,
          created_by: user.id,
          prefixEvent: 'editNote'
        };
        NotesService.createNote(data).then(function (res) {
          if (res.error) {
            Notification.error({ message: 'Note was not created', title: 'Error'})
          } else {
            Notification.success({ message: 'Note was created', title: 'Success'})
          }
        })
      };

      $scope.updateNote = function (e) {
        var scope = angular.element(e.target).scope();
        var data = {
          source: 'applicant',
          source_id: scope.item.id,
          body: scope.note.text,
          created_by: user.id,
          prefixEvent: 'editNote'
        };
        NotesService.updateNote($scope.currentNote.note_id, data).then(function (res) {
          if (res.error) {
            Notification.error({ message: 'Note was not updated', title: 'Error'});
          } else {
            Notification.success({ message: 'Note was updated', title: 'Success'});
          }
        });
      };

      $scope.deleteNote = function (e) {
        var scope = angular.element(e.target).scope();
        var modal;
        NotesService.getLatestNoteSourceId(scope.item.id, 'applicant').then(function (res) {
          if (!res.error) {
            $scope.currentNote = res.data && res.data[0];
            if (res.data.length === 0) {
              Notification.error({ message: 'You don\'t have a note', title: 'Error'});
            } else {
              NotesService.deleteNote($scope.currentNote.note_id).then(function (res) {
                if (res.error) {
                  Notification.error({ message: 'Note was not deleted', title: 'Error'});
                } else {
                  Notification.success({ message: 'Note was deleted', title: 'Success'});
                }
              });
            }
          }
        });
      };

      // FILES

      $scope.displayFiles = function (e) {
        cfpLoadingBar.start();
        var scope = angular.element(e.target).scope();

        var admissionFilesConfig = {
          start: 0,
          limit: 100
        };

        AdmissionsService.getAdmissionFiles(scope.item.id, admissionFilesConfig).then(function (res) {
          scope.allFiles = res.data.items;
          scope.files = {};

          // update uploaded date
          scope.allFiles.forEach(function (item) {
            if (item.file.created) {
              var created = moment(item.file.created);
              if (!_.isString(item.file)) {
                item.file.uploaded = created.toDate();
              }
            }

            // update 'seen' of files
            if (!item.seen || item.seen == '0') {
              var data = {
                admissionId: scope.item.id,
                fileId: item.file_id,
                admissionFileId: item.id,
                categoryId: item.category_id,
                seen: 1
              };
              AdmissionsService.updateAdmissionFile(data).then(function (res) { });
            }
          });

          fileCategories.forEach(function (element, index, array) {
            AdmissionsService.getFileCategoryId(element).then(function (res) {
              scope.files[element] = _.filter(scope.allFiles, function(file) {
                return file.category_id === res.data.toString() && file.file.id > 0;
              });

              if (index == 2) { // last index
                cfpLoadingBar.complete();

                var modal = $modal({
                  scope: scope,
                  templateUrl: '/partials/modals/display.files.admission.modal.html',
                  prefixEvent: 'displayFilesModal',
                  show: false
                });

                modal.$promise.then(modal.show);
              }
            });
          });
        });
      };

      $scope.$on('displayFilesModal.show', function (e) {
        var scope = e.targetScope;

        scope.allCategories = {};
        fileCategories.forEach(function (element, index, array) {
          AdmissionsService.getFileCategoryId(element).then(function (res) {
            scope.allCategories[element] = res.data.toString();
          });
        });

        var el = document.querySelectorAll('table.applicant-files > tbody');
        for(var x = 0; x < el.length; x++) {
          Sortable.create(el[x], {
            animation: 150,
            group: {
              name: el[x].getAttribute('data-name'),
              put: fileCategories
            },

            onAdd: function (evt) {
              var itemEl = evt.item;
              var id = itemEl.getAttribute('data-id');
              var file_id = itemEl.getAttribute('data-file-id');
              var to_category = scope.allCategories[evt.to.getAttribute('data-name')];
              if (id != '-1') {
                AdmissionsService.deleteAdmissionFile(id).then(function (res) {
                  if (!res.error) {
                    AdmissionsService.postAdmissionFile(scope.item.id, file_id, to_category).then(function () {});
                  }
                });
              }
            }
          });
        }
      });

      $scope.displayImage = function (item) {
        $scope.imageItem = item;
        $scope.displayImageModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/image.modal.html',
          show: false
        });

        $scope.displayImageModal.$promise.then($scope.displayImageModal.show);
      };

      $scope.isImage = function (ext) {
        ext = '|' + ext + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(ext) !== -1;
      };

      //MESSAGES

      $scope.getSentMessages = function (item) {
        item.sentMessageCtr = 0;

        if (item.lastLogin) {
          var data = {
            userId : item.id,
            keyword : 'sent'
          };
          MessageService.getConversations(data).then(function (res) {
            if (res) {
              var conversations = res.data.items;
              conversations.forEach(function (conv){
                conv.Messages.forEach && conv.Messages.forEach(function (message){
                  var created = moment.utc(message.created_at);
                  var dateCreated = created.toDate();

                  if (message.user_id == item.id && dateCreated >= item.lastLogin) {
                    item.sentMessageCtr++;
                  }
                });
              });

            }
          });
        }
      };
    });
})();