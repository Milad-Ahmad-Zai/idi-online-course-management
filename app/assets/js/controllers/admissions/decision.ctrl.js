(function () {
  'use strict';

  angular
    .module('ctrl.admissions.decision', [])

    .controller('AdmissionsDecisionCtrl', function ($scope, $state, $stateParams, $filter, $rootScope, $sce, $q, Notification, user, AdmissionsService, ProfilesService, AuthService, NotesService) {
      $scope.userProfileId = $stateParams.id;
      var id = $stateParams.id || user.id;
      $scope.sheetData = [];
      $scope.currentDecision = {};
      $scope.decisionNote = {};
      var users = [];
      $scope.advisors = [];
      $scope.currentAdvisor = {};

      $scope.configProfile = function (res) {
        $scope.userProfile = angular.copy(res.data);

        if ($scope.userProfile && $scope.userProfile.age) $scope.userProfile.age = +$scope.userProfile.age;
        if ($scope.userProfile && $scope.userProfile.profile_image && $scope.userProfile.profile_image.url) $scope.userProfile.image = $sce.trustAsResourceUrl($scope.userProfile.profile_image.url);
        if ($scope.userProfile && $scope.userProfile.cover_image && $scope.userProfile.cover_image.url) $scope.userProfile.cover = $sce.trustAsResourceUrl($scope.userProfile.cover_image.url);

      };

      $scope.loadProfile = function (userId) {
        var profileId = userId || id;
        return ProfilesService.getProfile(profileId);
      };

      $scope.updateAdvisorParams = function () {
        if ($scope.currentDecision.advisor_id) {
          if (users[$scope.currentDecision.advisor_id]) {
            //$scope.advisor_name = users[$scope.currentDecision.advisor_id].user.name;
            $scope.currentAdvisor = users[$scope.currentDecision.advisor_id];
          }
          $scope.loadUser($scope.currentDecision.advisor_id).then(function (res) {
            users[$scope.currentDecision.advisor_id] = res.data.user;
            //$scope.advisor_name = users[$scope.currentDecision.advisor_id].user.name;
            var filter = $scope.advisors.filter(function (advisor) {
              return advisor.id == $scope.currentDecision.advisor_id;
            });
            users[$scope.currentDecision.advisor_id] = filter[0];
            $scope.currentAdvisor = filter[0];
          });
        } else if (_.isUndefined($scope.currentDecision.advisor_id)) {
          $scope.currentAdvisor = users[$scope.currentDecision.advisor_id];
        } else if (_.isNull($scope.currentDecision.advisor_id)) {
          //$scope.advisor_name = '';
        }
      };
      $scope.updateApplicantParams = function () {
        if ($scope.userProfileId) {
          if ($scope.userProfileId == user.id) {
            $scope.applicant_name = $scope.userProfile.user.name;
          } else {
            $scope.loadProfile().then(function (res) {
              $scope.configProfile(res);
              $scope.applicant_name = $scope.userProfile.user.name;
            });
          }
        } else {
          $scope.applicant_name = $rootScope.user.name;
        }
      };

      $scope.getUserDecisionSheet = function (id) {
        return AdmissionsService.getUserDecisionSheets(id).then(function (res) {
          if (res.error) {
            Notification.error({ message: '', title: 'Error: ' + res.status})
          } else {
            $scope.decisions = res.data;
            if ($scope.decisions.length > 0) {
              $scope.currentDecision = $scope.decisions[0];
              $scope.currentDecisionIndex = 1;
              $scope.loadNote();
            }
            $scope.updateAdvisorParams();
            $scope.updateApplicantParams();
          }
        });
      };

      $scope.saveDecision = function (e) {
        var scope = angular.element(e.target).scope();
        var decisionId = scope.currentDecision.id;
        if (scope.currentAdvisor && scope.currentAdvisor.id) {
          $scope.currentDecision.advisor_id = scope.currentAdvisor.id;
        } else if (scope.currentDecision.advisor_id && users[scope.currentDecision.advisor_id]) {
          $scope.currentDecision.advisor_id = users[scope.currentDecision.advisor_id].id ;
        }
        AdmissionsService.updateUserDecisionSheet(scope.currentDecision, id, decisionId).then(function (res) {
          if ((!(res.error || res.data && res.data.error) && res.data)) {
            $scope.currentDecision = res.data;
            Notification.success({ message: 'Decision saved', title: 'Success'});
            $scope.saveNote();
            //$scope.getUserDecisionSheet(id);
          } else if ((res.error || res.data && res.data.error)) {
            Notification.error({ message: 'Decision was not saved', title: 'Error'});
          }
        })
      };

      $scope.deleteDecision = function () {
        var ids = [];
        ids.push(parseInt($scope.currentDecision.id));
        AdmissionsService.deleteUserDecisionSheet(ids).then(function (res) {
          if (!res.error) {
            Notification.success({title: 'Success'});
            $scope.getUserDecisionSheet(id);
          } else {
            Notification.error({title: 'Error'});
          }
        })
      };
      $scope.changeDecision = function (e) {
        var decisionId = angular.element(e.target).scope().decision.id;
        $scope.currentDecisionIndex = angular.element(e.target).scope().$index + 1;
        if (!(decisionId == $scope.currentDecision && $scope.currentDecision.id)) {
          var filtered = $scope.decisions.filter(function (decision) {
            return decision.id == decisionId;
          });
          $scope.currentDecision = filtered[0];
          $scope.loadNote();
          $scope.updateAdvisorParams();
          $scope.updateApplicantParams();
        }
      };

      $scope.createDecision = function (e) {
        $scope.currentDecision = {};
        $scope.currentDecisionIndex = '';
        $scope.decisionNote = {};
        $scope.updateAdvisorParams();
        $scope.updateApplicantParams();
      };

      $scope.saveNote = function (e) {
        if ($scope.decisionNote && $scope.decisionNote.body == '' && $scope.decisionNote.note_id) {
          $scope.deleteNote();
        } else if ($scope.decisionNote && $scope.decisionNote.note_id) {
          $scope.updateNote();
        } else if ($scope.decisionNote) {
          $scope.createNote();
        }
      };

      $scope.createNote = function (e) {
        var data = {
          source: 'decision',
          source_id: $scope.currentDecision.id,
          body: $scope.decisionNote.body,
          created_by: user.id
        };
        NotesService.createNote(data).then(function (res) {
          if (res.error) {
            Notification.error({ message: 'Note was not created', title: 'Error'});
          } else {
            Notification.success({ message: 'Note was created', title: 'Success'});
            $scope.decisionNote = res.data;
          }
        });
      };

      $scope.updateNote = function (e) {
        var data = {
          source: 'decision',
          source_id: $scope.currentDecision.id,
          body: $scope.decisionNote.body,
          created_by: user.id
        };
        NotesService.updateNote($scope.decisionNote.note_id, data).then(function (res) {
          if (res.error) {
            Notification.error({ message: 'Note was not updated', title: 'Error'});
          } else {
            Notification.success({ message: 'Note was updated', title: 'Success'});
          }
        })
      };

      $scope.deleteNote = function (e) {
        if ($scope.decisionNote.note_id) {
          NotesService.getLatestNoteSourceId($scope.currentDecision.id, 'decision').then(function (res) {
            if (!res.error) {
              var decisionNote = res.data && res.data[0];
              //$scope.decisionNote = {};
              if (res.data.length === 0) {
                Notification.error({message: 'You don\'t have a note', title: 'Error'});
              } else {
                NotesService.deleteNote(decisionNote.note_id).then(function (res) {
                  if (res.error) {
                    Notification.error({message: 'Note was not deleted', title: 'Error'});
                    $scope.decisionNote = decisionNote;
                  } else {
                    Notification.success({message: 'Note was deleted', title: 'Success'});
                    $scope.decisionNote = {};
                  }
                });
              }
            }
          });
        } else {
          $scope.decisionNote.body = '';
        }
      };

      $scope.loadNote = function () {
        NotesService.getLatestNoteSourceId($scope.currentDecision.id, 'decision').then(function (res) {
          if (!res.error) {
            $scope.decisionNote = res.data && res.data[0];
            if (!$scope.decisionNote) {
              $scope.decisionNote = {};
            }
          }
        });
      };

      $scope.loadUser = function (id) {
        return AuthService.getFullUser(id);
      };

      if (!$rootScope.advisorsDeferred) {
        $scope.newAdvisorQuery = true;
        $rootScope.advisorsDeferred = $q.defer();
      }
      if ($scope.newAdvisorQuery) {
        var advisorConfig = {
          start: 0,
          limit: 100,
          filter: 'group=Admissions Advisor',
          sort: 'name'
        };
        AuthService.getUsersList(advisorConfig).then(function (res) {
          if (res.data) {
            $rootScope.advisorsDeferred.resolve(res);
            $scope.advisors = res.data.list;
          }
        });
      } else {
        $rootScope.advisorsDeferred.promise.then(function (res) {
          if (res.data) {
            $scope.advisors = res.data.list;
          }
        });
      }

      if ($scope.checkRights('admissions.decision')) {
        if (id && user) {
          $scope.getUserDecisionSheet(id);
        }
      }
    });
})();