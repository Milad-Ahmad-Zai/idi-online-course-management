(function () {
  angular
  .module('ui.routes')
  .config(config);

  function config($stateProvider) {
    $stateProvider
    // Admissions
      .state('app.admin.admissions', {
        url: '/admissions',
        abstract: true,
        controller: 'AdmissionCtrl',
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {
                name: 'ctrl.admissions', files: ['/assets/js/controllers/admissions/admissions.ctrl.js']
              }
            ])
          }
        },
        data: {
          permissions: {
            only: ['admin', 'auth.user.get', 'auth.profileAny.get'],
            redirectTo: 'app.admin.admissions.student'
          }
        }
      })

      .state('app.admin.admissions.advisor', {
        url: '/advisor',
        views: {
          'content@app': {
            controller: 'AdmissionsAdvisorCtrl',
            templateUrl: '/partials/admissions/advisor.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              { name: 'ctrl.admissions.advisor', files: ['/assets/js/controllers/admissions/advisor.ctrl.js'] },
              { name: 'cfp.hotkeys', files: ['/assets/js/vendor/angular/hotkeys.js'] },
              { name: 'idi.common.profileImage', files: ['/assets/js/directives/common/profile-image/idi.profile-image.js'] },
              {
                files: [
                  '/assets/js/vendor/thirdparty/Sortable.js'
                ]
              }
            ])
          }
        },
        data: {
          permissions: {
            only: ['admissions.admissionAny.get'],
            redirectTo: 'app.admin.admissions.student'
          }
        }
      })

      .state('app.admin.admissions.student', {
        url: '/student/:id',
        views: {
          'content@app': {
            controller: 'AdmissionsStudentCtrl',
            templateUrl: '/partials/admissions/student.html'
          },
          'welcome@app.admin.admissions.student': {
            templateUrl: '/partials/admissions/wizard/welcome.html'
          },
          'personal@app.admin.admissions.student': {
            templateUrl: '/partials/admissions/wizard/personal.html'
          },
          'history@app.admin.admissions.student': {
            templateUrl: '/partials/admissions/wizard/history.html'
          },
          'experience@app.admin.admissions.student': {
            templateUrl: '/partials/admissions/wizard/experience.html'
          },
          'creative@app.admin.admissions.student': {
            templateUrl: '/partials/admissions/wizard/creative.html'
          },
          'review@app.admin.admissions.student': {
            templateUrl: '/partials/admissions/wizard/review.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {
                name: 'ctrl.admissions.student', files: ['/assets/js/controllers/admissions/student.ctrl.js']
              }
            ])
          },
          getCountriesForForm: function (load, $rootScope, AdmissionsService) {
            if (!$rootScope.countriesForForm) {
              AdmissionsService.getCountries().then(function (res) {
                var countries = [];
                _.forEach(res.data, function (n, key) {
                  countries.push({
                    name: n,
                    value: key
                  });
                });
                $rootScope.countriesForForm = countries;
              })
            }
          }
        },
        data: {
          permissions: {
            only: ['admissions.admissionSelf.get', 'admissions.admissionAny.get'],
            redirectTo: 'app.admin.profile.public'
          }
        }
      })

      .state('app.admin.admissions.documents', {
        url: '/documents/:id',
        views: {
          'content@app': {
            controller: 'AdmissionsDocumentsCtrl',
            templateUrl: '/partials/admissions/documents.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {
                name: 'ctrl.admissions.documents', files: ['/assets/js/controllers/admissions/documents.ctrl.js']
              },
              {
                files: [
                  '/assets/js/vendor/thirdparty/Sortable.js'
                ]
              }
            ])
          }
        },
        data: {
          permissions: {
            only: ['admissions.supportDocuments.get'],
            redirectTo: 'app.login'
          }
        }
      })

      .state('app.admin.admissions.uploaded', {
        url: '/uploaded/:id',
        views: {
          'content@app': {
            controller: 'AdmissionsUploadedCtrl',
            templateUrl: '/partials/admissions/uploaded.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {
                name: 'ctrl.admissions.uploaded', files: ['/assets/js/controllers/admissions/uploaded.ctrl.js']
              }
            ])
          }
        },
        data: {
          permissions: {
            only: ['admissions.filesAny.get'],
            redirectTo: 'app.admin.admissions.student'
          }
        }
      })

      .state('app.admin.admissions.summary', {
        url: '/summary/:id',
        views: {
          'content@app': {
            controller: 'AdmissionsSummaryCtrl',
            templateUrl: '/partials/admissions/summary.page.html'
          },
          'summary@app.admin.admissions.summary': {
            templateUrl: '/partials/admissions/summary.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {
                name: 'ctrl.admissions.summary', files: ['/assets/js/controllers/admissions/summary.ctrl.js']
              }
            ])
          }
        },
        data: {
          permissions: {
            only: ['admissions.summarySheet.get'],
            redirectTo: 'app.admin.admissions.student'
          }
        }
      })

      .state('app.admin.admissions.decision', {
        url: '/decision/:id',
        views: {
          'content@app': {
            controller: 'AdmissionsDecisionCtrl',
            templateUrl: '/partials/admissions/decision.page.html'
          },
          'decision@app.admin.admissions.decision': {
            templateUrl: '/partials/admissions/decision.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {
                name: 'ctrl.admissions.decision', files: ['/assets/js/controllers/admissions/decision.ctrl.js']
              }
            ])
          }
        },
        data: {
          permissions: {
            only: ['admissions.decisionSheet.get'],
            redirectTo: 'app.admin.admissions.student'
          }
        }
      })

    // end
  }
})()