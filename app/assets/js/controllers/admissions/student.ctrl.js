(function () {
  'use strict';

  angular
    .module('ctrl.admissions.student', [])

    .controller('AdmissionsStudentCtrl', function ($scope, $state, $stateParams, $rootScope, $filter, $modal, $window, $sce, AuthService, ProfilesService, FilesService, AdmissionsService, Notification, PagesService, $q, FormsService) {
      //$scope.age = getAge($scope.user.dateOfBirth);

      var activities = AdmissionsService.getActivities();
      activities.forEach(function (element, index, array) {
        element.selected = false;
      });

      var fileCategories = [
        'Personal',
        'Academic',
        'Employment',
        'Creative'
      ];

      $scope.formOptions = {
        formState: {
          disabled: false
        }
      };
      $scope.formSummaryOptions = {
        formState: {
          disabled: true
        }
      };

      var user = $rootScope.user;
      user.english_certificate = {};
      var admissionFilesConfig = {
        start: 0,
        limit: 100
      };

      var uploader = {};
      var uploaderParams = {};

      $scope.validURL = function (str) {
        // var pattern = new RegExp('((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\\+\\$,\\w]+@)[A-Za-z0-9.-]+)((?:\\/[\\+~%\\/.\\w-_]*)?\\??(?:[-\\+=&;%@.\\w_]*)#?(?:[\\w]*))?)','i'); // fragment locater
        // return pattern.test(str);
        return true;
      };

      $scope.hasChanged = false;

      $scope.formHasError = false;

      $scope.user = {};
      $scope.id = '';
      $scope.cancelled = true;
      $scope.allFiles = [];
      $scope.files = {};

      $scope.workActivities = angular.copy(activities);
      $scope.otherActivities = angular.copy(activities);
      $scope.modalWorkActivities = angular.copy(activities);
      $scope.modalOtherActivities = angular.copy(activities);
      $scope.workFieldEnabled = false;
      $scope.otherFieldEnabled = false;
      $scope.modalWorkFieldEnabled = false;
      $scope.modalOtherFieldEnabled = false;

      $scope.personalAttachments = [];
      $scope.historyAttachments = [];

      $scope.education_prev_current = {};

      $scope.academic_qualifications = {};

      $scope.highest_level_academic = {};
      $scope.highest_level_math = {};
      $scope.highest_level_english = {};

      $scope.english_certificate = {};

      $scope.portfolio_link = {};


      $scope.work_experience = user.work_experience || {activities: []};

      $scope.other_experience = {
        activities: []
      };

      $scope.certificate_training = {};

      $scope.uploader = [];

      var personalStatementTooltip = '<div class="tooltip-admissions">' +
        '<p>We would also like you to write a Personal Statement.</p>' +
        '<p>Your Personal Statement should be between 250-500 words in length and should answer the following three questions:</p>' +
        '<ol>' +
        '<li>Why have you chosen to apply to the University of Aberdeen to study this subject? </li>' +
        '<li>Which personal qualities do you possess that will help you to successfully complete this programme of study?</li>' +
        '<li>How will studying this programme help you to achieve your aspirations?  </li>' +
        '</ol>' +
        '</div>';

      $scope.personalStatementTooltip = {
        'title': personalStatementTooltip
      };

      $scope.loadCountries = function () {
        return AdmissionsService.getCountries();
      };

      $scope.initializeUploaders = function () {
        //var attachedParams = {};

        function initialize(element, index, array) {
          AdmissionsService.getFileCategoryId(element).then(function (res) {
            //if(!attachedParams[element]) {
            //  attachedParams[element] = {};
            //}

            uploader[element] = $scope.uploader[element] = FilesService.getUploader('admissions', $scope.id, true);
            uploaderParams[element] = {
              categoryId: res.data
            };

            $scope.files[element] = _.filter($scope.allFiles, function (file) {
              return file.category_id == uploaderParams[element].categoryId.toString();
            });

            //$scope.files[element] = _.findWhere($scope.allFiles, {category_id: uploaderParams[element].categoryId.toString()});

            var promises = [];

            uploader[element].onCompleteItem = function (fileItem, response, status, headers) {
              if (response.error || status == 403) {
                Notification.error({message: response.msg, title: 'Error: ' + status});
              }
              //if (!response.error || status == 200) {
              //
              //  attachedParams[element].push(uploaderParams);
              //  attachedParams[element][attachedParams[element].length-1].file_id = angular.copy(response.data.id);
              //}

              if (!response.error && !_.isUndefined(response.error) || status == 200) {
                uploaderParams[element].fileId = angular.copy(response.data.id);

                promises.push(AdmissionsService.postAdmissionFile($scope.id, uploaderParams[element].fileId, uploaderParams[element].categoryId));

                /*AdmissionsService.postAdmissionFile($scope.id, uploaderParams[element].fileId, uploaderParams[element].categoryId).then(function (res) {

                 if (!response.error || status == 200) {
                 //$scope.getFiles();
                 //uploader[element].clearQueue();
                 }
                 });*/
              }
            };

            uploader[element].onCompleteAll = function () {
              uploader[element].clearQueue();
              $q.all(promises).then(function () {

                $scope.getFiles();
              });

            };
            //uploader[element].onCompleteAll = function() {
            //  var dataToSend = {
            //    data: attachedParams[element]
            //  };
            //  CoursesService.attachStudentFile(dataToSend).then(function (res) {
            //    if (!res.error || status == 200) {
            //      attachedParams[element] = [];
            //      $scope.getFiles();
            //      uploader[element].clearQueue();
            //    }
            //  });
            //};
            uploader[element].onBeforeUploadItem = function (item) {
              item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
            };
            switch (element) {
              case 'Creative':
                uploader[element].filters.push({
                  name: 'FileSize', fn: function (item) {
                    return item.size <= 20 * 1024 * 1024; // 20mb
                  }
                })
            }
            $scope.uploader[element].uploaderLoaded = true;
          });
        }

        AdmissionsService.getAdmissionFiles($scope.id, admissionFilesConfig).then(function (res) {
          $scope.allFiles = res.data.items;
          fileCategories.forEach(initialize);
        });
      };

      $scope.getFiles = function () {
        AdmissionsService.getAdmissionFiles($scope.id, admissionFilesConfig).then(function (res) {
          $scope.allFiles = res.data.items;
          fileCategories.forEach(function (element, index, array) {
            $scope.files[element] = _.filter($scope.allFiles, function (file) {
              return file.category_id == uploaderParams[element].categoryId.toString();
            });
          });
        });
      };

      $scope.deleteFile = function (e) {
        var scope = angular.element(e.target).scope();
        AdmissionsService.deleteAdmissionFile(scope.item.id).then(function (res) {
          if (!res.error) {
            Notification.success({message: 'File successfully deleted', title: 'Success'});
            $scope.getFiles();
          } else {
            Notification.error({message: 'File was not deleted', title: 'Error: ' + res.status});
          }
        })
      };

      $scope.setAsProfilePicture = function (e) {
        var scope = angular.element(e.target).scope();

        var file_id = scope.item.file_id;
        var entity_id = scope.item.admission_id;


        ProfilesService.updateProfileImage(entity_id, file_id).then(function (res) {
          if (res.data.status == 500 || res.data.error) {
            Notification.error({message: res.data.msg, title: 'Error: ' + res.data.status});
          } else {
            $scope.submit();
          }
        }, function (res) {
          Notification.error({message: res.data.msg, title: 'Error: ' + res.data.status});
        });
      };


      $scope.setAsProfileCover = function (e) {
        var scope = angular.element(e.target).scope();

        var file_id = scope.item.file_id;
        var entity_id = scope.item.admission_id;


        ProfilesService.updateCoverImage(entity_id, file_id).then(function (res) {
          if (res.data.status == 500 || res.data.error) {
            Notification.error({message: res.data.msg, title: 'Error: ' + res.data.status});
          } else {
            $scope.submit();
          }
        }, function (res) {
          Notification.error({message: res.data.msg, title: 'Error: ' + res.data.status});
        });
      };


      $scope.getAdmission = function (id) {
        AdmissionsService.getAdmission(id).then(function (res) {
          if (res && res.data && res.data.error) {
            Notification.error({message: 'Admission was not loaded', title: 'Error: ' + res.status});
          } else if (res.status === 204) {
            $scope.user = {};
          } else if (res && res.data) {
            $scope.user = res.data;
            $scope.user.state = parseInt($scope.user.state);
            $scope.formOptions.formState.disabled = !!$scope.user.state;
            if (!$rootScope.admissionLoadedNotified) {
              Notification.success({message: 'Admission successfully loaded', title: 'Success'});
              $rootScope.admissionLoadedNotified = true;
            }
            if ($scope.user.portfolio_link) {
              $scope.user.portfolio_link.forEach(function (element, index, array) {
                if (!$scope.validURL(element.link)) {
                  element.link = '';
                } else {
                  if (element.link.indexOf('http:') !== 0) {
                    element.link = 'http://' + element.link;
                  }
                }
              })
            }
          }
        }, function (res) {
          Notification.error({message: 'Admission was not loaded', title: 'Error'});
        });
      };

      $scope.getAge = function (birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
      };

      $scope.attachFiles = function (files, type) {
        for (var i = 0; i < files.length; i++) {
          $scope[type].push(files[i]);
        }
      };

      $scope.addItemToList = function (type) {
        var filtered;
        var valid = true;
        if (!Array.isArray($scope.user[type])) $scope.user[type] = [];

        if (type == 'work_experience') {
          filtered = $scope.workActivities.filter(function (activity) {
            if (activity.value === 'other' && activity.selected === false) $scope[type].other = '';
            return activity.selected === true;
          });
          $scope[type].activities = filtered;
          $scope.workFieldEnabled = false;
          $scope.workActivities = angular.copy(activities);
        }
        if (type == 'other_experience') {
          filtered = $scope.otherActivities.filter(function (activity) {
            if (activity.value === 'other' && activity.selected === false) $scope[type].other = '';
            return activity.selected == true;
          });
          $scope[type].activities = filtered;
          $scope.otherFieldEnabled = false;
          $scope.otherActivities = angular.copy(activities);
        }

        if (type == 'portfolio_link') {
          valid = true;
          if (!/^(https:\/\/|http:\/\/)/.test($scope[type].link)) {
            $scope[type].link = 'http://' + $scope[type].link;
          }
          $scope.linkValid = valid;
        }

        if (valid) {
          $scope.user[type].push(angular.copy($scope[type]));
          $scope[type] = {};
        }
      };

      $scope.removeItemFromList = function (e, type) {
        var scope = angular.element(e.target).scope();
        var indexToDelete = _.findIndex(scope.user[type], scope.node);
        scope.user[type].splice(indexToDelete, 1);
      };

      $scope.editItemInList = function (e, type) {
        var modalActivities = angular.copy(activities);
        $scope.cancelled = true;
        var scope = angular.element(e.target).scope();

        if (scope.node.activities) {
          scope.node.activities.forEach(function (activity) {
            for (var i = 0; i < modalActivities.length; i++) {
              if (activity.value == modalActivities[i].value) {
                modalActivities[i].selected = true;
              }
            }
          });

          var otherItem = scope.node.activities.filter(function (activity) {
            return activity.value == 'other';
          });
        }

        if (otherItem && otherItem.length > 0) {
          if (type == 'work_experience') {
            scope.modalWorkFieldEnabled = true;
          }
          if (type == 'other_experience') {
            scope.modalOtherFieldEnabled = true;
          }
        } else {
          scope.modalWorkFieldEnabled = false;
          scope.modalOtherFieldEnabled = false;
        }

        if (type == 'work_experience') {
          $scope.modalWorkActivities = modalActivities;
        }
        if (type == 'other_experience') {
          $scope.modalOtherActivities = modalActivities;
        }
        scope.type = type;
        $scope.oldNode = angular.copy(scope.node);

        var modal = $modal({
          scope: scope,
          templateUrl: '/partials/modals/admission.item.modal.html',
          prefixEvent: 'admissionItemModal',
          show: false
        });

        modal.$promise.then(modal.show);
      };

      $scope.editAcademicInList = function (e, type) {
        $scope.cancelled = true;
        var scope = angular.element(e.target).scope();

        $scope.oldNode = angular.copy(scope.node);

        var modal = $modal({
          scope: scope,
          templateUrl: '/partials/modals/admission.item.academic.modal.html',
          prefixEvent: 'admissionAcademicModal',
          show: false
        });

        modal.$promise.then(modal.show);
      };

      $scope.saveActivities = function (type, node) {
        var filtered;
        if (type == 'work_experience') {
          filtered = $scope.modalWorkActivities.filter(function (activity) {
            if (activity.value === 'other' && activity.selected === false) $scope[type].other = '';
            return activity.selected === true;
          });
          node.activities = filtered;
          $scope.modalWorkFieldEnabled = false;
          $scope.modalWorkActivities = angular.copy(activities);
        }
        if (type == 'other_experience') {
          filtered = $scope.modalOtherActivities.filter(function (activity) {
            if (activity.value === 'other' && activity.selected === false) $scope[type].other = '';
            return activity.selected == true;
          });
          node.activities = filtered;
          $scope.modalOtherFieldEnabled = false;
          $scope.modalOtherActivities = angular.copy(activities);
        }
      };

      $scope.cancelEditingItem = function (e) {
        var scope = e.targetScope;
        var index = _.findIndex(scope.user[scope.type], scope.node);

        if ($scope.cancelled) {
          scope.user[scope.type][index] = angular.copy($scope.oldNode);
          //scope.node = angular.copy($scope.oldNode);
        }
      };

      $scope.openHelp = function () {
        var modal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/help.modal.admissions.html',
          show: false
        });

        modal.$promise.then(modal.show);
      };

      $scope.openPSModal = function () {
        var modal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/admissions.personalStatement.modal.html',
          show: false
        });

        modal.$promise.then(modal.show);
      }

      $scope.addToSummary = function (e, type) {
        var scope = angular.element(e.target).scope();
        AdmissionsService.getUserSummarySheets(scope.user.id).then(function (res) {
          if (!res.error && res.data) {
            var data;
            var tempData;
            if (_.isArray(res.data) && res.data.length === 0) {
              data = {};
            } else {
              tempData = res.data[0];
              for (var prop in tempData) {
                if (_.isString(tempData[prop]) && prop != 'created' && prop != 'updated') {
                  tempData[prop] = JSON.parse(tempData[prop])
                }
              }
              data = tempData;
            }
            var index = _.findIndex(data[type], scope.node);

            if (index === -1) {
              if (!data[type]) {
                data[type] = [];
              }
              data[type].push(scope.node);
              AdmissionsService.updateUserSummarySheet(scope.user.id, data).then(function (res) {
                if (!res.error && res.data) {
                  Notification.success({message: 'Successfully added to summary page', title: 'Success'})
                }
              })
            } else {
              Notification.warning({message: 'Already on summary page', title: 'Warning'})
            }
          }
        })
      };

      $scope.submit = function (e, state) {
        // if there has been no change & it is not the final submit button then return
        if (!$scope.hasChanged && state != 1) {
          // show save notification regardless of wether there is a change when save button clicked
          if (state == 'save') {
            Notification.success({message: 'Application Saved', title: 'Success', maxCount: 2});
          }
          return
        }
        $scope.cancelled = false;
        $scope.formHasError = false;
        var scope = e ? angular.element(e.target).scope() : $scope;
        var user = scope.user;
        var id = user.id || $scope.id;

        if (state == 1) {
          var age = $filter('ageFilter')(user.date_of_birth);

          var missingFields = [];

          /*if (_.isEmpty(user.title)) missingFields.push('Title');
           if (_.isEmpty(user.first_name)) missingFields.push('First Name');
           if (_.isEmpty(user.last_name)) missingFields.push('Last Name');
           if (age == 0) missingFields.push('Date of Birth');
           if (_.isEmpty(user.address)) missingFields.push('Address');
           if (_.isEmpty(user.city)) missingFields.push('City/Town');
           if (_.isEmpty(user.postcode)) missingFields.push('Postcode');
           if (_.isEmpty(user.landline)) missingFields.push('Landline Telephone');
           if (_.isEmpty(user.mobile)) missingFields.push('Mobile Telephone');
           if (_.isEmpty(user.english_first_lang)) missingFields.push('Is English Your First Language (Y/N)');
           if (_.isEmpty(user.disability)) missingFields.push('Do You Have A Medical Condition Or Disability That May Impact Your Studies');*/

          if (missingFields.length > 0) {
            Notification.error({
              title: 'Error',
              message: 'Please provide input on the following field/s:<ul>' + missingFields.join('<br />') + '</ul>'
            });

            return;
          }
        }

        if (state == 'save' || !state) {
          state = $scope.user.state || 0;
        }
        var dataToSubmit = {
          'title': user.title,
          'first_name': user.first_name,
          'first_name_2': user.first_name_2,
          'last_name': user.last_name,
          'date_of_birth': user.date_of_birth,
          'permanent_domicile': user.permanent_domicile,
          'birth_country': user.birth_country,
          'nationality_personal': user.nationality_personal,
          'residential_category': user.residential_category,
          'gender': user.gender,
          'same_gender_birth': user.same_gender_birth,
          'ethnicity_personal': user.ethnicity_personal,
          'sexual_orientation_personal': user.sexual_orientation_personal,
          'marital_status': user.marital_status,
          'criminal_conviction_personal': user.criminal_conviction_personal,
          'address': user.address,
          'address_2': user.address_2,
          'city': user.city,
          'postcode': user.postcode,
          'landline': user.landline,
          'mobile': user.mobile,
          'permanent_address_different': user.permanent_address_different,
          'pay_your_fees': user.pay_your_fees,
          'pay_your_fees_details': user.pay_your_fees_details,
          'disabled_student_allowance': user.disabled_student_allowance,
          'medical_condition': user.medical_condition,
          'medical_condition_details': user.medical_condition_details,
          'parents_university': user.parents_university,
          'personal_statement': user.personal_statement,
          'academic_qualifications': user.academic_qualifications,
          'work_experience': user.work_experience,
          'prev_higher_education': user.prev_higher_education,
          'prev_studied_abn': user.prev_studied_abn,
          'hesa_id': user.hesa_id,
          'prev_refused_abn': user.prev_refused_abn,
          'english_first_lang': user.english_first_lang,
          'english_certificate': user.english_certificate,
          'state': state
        };

        AdmissionsService.saveAdmission(id, dataToSubmit).then(function (res) {
          if (res.data.error) {
            Notification.error({message: 'Admission was not updated', title: 'Error: ' + res.status});
          } else {
            //$scope.user = res.data;
            if (res.data.state) {
              $scope.user.state = parseInt(res.data.state);
              $scope.formOptions.formState.disabled = !!$scope.user.state;
            }
            $scope.hasChanged = false;
            if(res.data.state && res.data.state==1){
              Notification.success({message: 'Application Submitted', title: 'Success'});
            }else{
              Notification.success({message: 'Application Saved', title: 'Success'});
            }
          }
        }, function (res) {
          Notification.error({message: 'Admission was not updated', title: 'Error'});
        });
      };

      function loadFaq() {
        PagesService.getPagesByTag("admissions-help").then(function (res) {
          if (!res.data.error) {
            $scope.answers = res.data;
            $scope.answers.forEach(function (element) {
              element.trustedText = $sce.trustAsHtml(element.body);
            })
          }

        })
      }

      function loadPageBySlug() {
        PagesService.getPageBySlug("admissions-welcome-page").then(function (res) {
          if (!res.error && res.data) {
            $scope.content = {
              admissionsWelcomePage: $sce.trustAsHtml(res.data.body)
            };
          }
        });
      }

      $scope.userFormFields = {};

      function loadFormFields() {
        var userFormSlugs = ["admissions-personal-tab", "admissions-academic-tab", "admissions-academic-english-tab"];
        var promissesFormFields = [];
        $scope.userSummaryFormFields = [];

        for (var i in userFormSlugs) {
          promissesFormFields.push(FormsService.getFormBySlug(userFormSlugs[i]));
        }

        $q.all(promissesFormFields).then(function (results) {
          for (var i in results) {
            if (!results[i].data.error) {
              $scope.userFormFields[results[i].data.slug] = JSON.parse(results[i].data.body);
              $scope.userSummaryFormFields = $scope.userSummaryFormFields.concat(JSON.parse(results[i].data.body));
            }

          }
        });
      }


      $scope.loadAdmissions = function () {
        if (!$rootScope.countries) {
          $scope.loadCountries().then(function (res) {
            $rootScope.countries = res.data;
          });
        }

        if ($stateParams.id) {
          $scope.id = $stateParams.id;
          $scope.getAdmission($scope.id);
          $scope.initializeUploaders();
        } else if (user.id) {
          $scope.id = user.id;
          $scope.getAdmission($scope.id);
          $scope.initializeUploaders();
        }

        $scope.$on('admissionItemModal.hide', $scope.cancelEditingItem);
        $scope.$on('admissionAcademicModal.hide', $scope.cancelEditingItem);

        $scope.supportDocuments = [];
        $scope.getSupportingDocuments = function () {
          var suppDocConfig = {
            start: 0,
            limit: 99
          };
          AdmissionsService.getSupportingDocuments(suppDocConfig).then(function (res) {
            if (res.status == 200) {
              $scope.supportDocuments = res.data.items;
            }
          });
        };

        $scope.getSupportingDocuments();
      };

      $scope.loadProgrammeList = function () {
        ProfilesService.getProgrammeList().then(function (res) {
          $scope.programmeList = res.data;
        });
      };

      $scope.checkRequiredFields = function () {
        /*$scope.formHasError = false;
         var age = $filter('ageFilter')($scope.user.date_of_birth);
         if (_.isEmpty($scope.user.title) || _.isEmpty($scope.user.first_name) || _.isEmpty($scope.user.last_name) || age == 0
         || _.isEmpty($scope.user.address) || _.isEmpty($scope.user.city) || _.isEmpty($scope.user.postcode) || _.isEmpty($scope.user.landline)
         || _.isEmpty($scope.user.mobile) || _.isEmpty($scope.user.english_first_lang) || _.isEmpty($scope.user.disability) || _.isEmpty($scope.user.personal_statement)) {
         $scope.formHasError = true;
         }*/
        $scope.formHasError = false;


      };

      $scope.openVideo = function () {
        $scope.openVideoModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/admission.welcome.video.modal.html',
          show: false
        });

        $scope.openVideoModal.$promise.then($scope.openVideoModal.show);
      };

      $scope.displayImage = function (item) {
        $scope.imageItem = item;
        $scope.displayImageModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/image.modal.html',
          show: false
        });

        $scope.displayImageModal.$promise.then($scope.displayImageModal.show);
      };

      if (!$stateParams.id && $scope.checkRights('admissions.get') || $stateParams.id && $scope.checkRights('admissions.anyGet')) {
        $scope.admissionsEnabled = true;
        loadFaq();
        loadPageBySlug();
        loadFormFields();
        $scope.loadAdmissions();
        $scope.loadProgrammeList();


      } else {
        $scope.admissionsEnabled = false;
      }

      $scope.getFieldListInfo = function (name) {
        return true;//_.pluck(_.filter($rootScope.profileFieldList, {'name': name})).length > 0 ? true : false;
      };

      $scope.$next = function (e) {
        e.preventDefault();
        window.scrollTo(0, 0);
        $scope.$$childHead.index++;
      };

      $scope.$prev = function (e) {
        e.preventDefault();
        window.scrollTo(0, 0);
        $scope.$$childHead.index--;
      };

      $scope.addAcademicToList = function () {
        $scope.user.academic_qualifications = $scope.user.academic_qualifications || [];
        $scope.user.academic_qualifications.push($scope.academic_qualifications);
        $scope.academic_qualifications = {};
      };

      $scope.addEnglishToList = function () {
        $scope.user.english_certificate = $scope.user.english_certificate || [];
        $scope.user.english_certificate.push($scope.english_certificate);
        $scope.english_certificate = {};
      };

      // Watch all field values that have ng-model=user.xxxxxx for a change.
      // Check newValue and oldValue for empty object so as not to set hasChanged=true when initial data is loaded
      $scope.$watch('user', function (newValue, oldValue) {
        var newCheck = Object.keys(newValue).length === 0 && newValue.constructor === Object;
        var oldCheck = Object.keys(oldValue).length === 0 && oldValue.constructor === Object;
        if (!newCheck && !oldCheck) {
          $scope.hasChanged = true;
        }
      }, true)

    });

})();