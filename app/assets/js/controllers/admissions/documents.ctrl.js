(function () {
  'use strict';

  angular
    .module('ctrl.admissions.documents', ['angularFileUpload'])
    
    .controller('AdmissionsDocumentsCtrl', ['$scope', '$state', '$rootScope', '$window', '$filter', 'AdmissionsService', 'AuthService', 'FilesService', 'FileUploader', 'Notification', 'cfpLoadingBar', 'NgTableParams', function ($scope, $state, $rootScope, $window, $filter, AdmissionsService, AuthService, FilesService, FileUploader, Notification, cfpLoadingBar, NgTableParams) {
      $scope.files = [];
      $scope.documentsUploaderLoaded = false;

      var documentsConfig = {
        start: 0,
        limit: 10
      };

      $scope.sendOrderingArray = function (scope, e) {
        //var difference = e.newIndex - e.oldIndex;
        //var oldOrdering;
        //var items = $scope.files.filter(function (file) {
        //  return file.id == scope.item.id;
        //});
        //
        //var item = items[0];
        //item.ordering = parseInt(item.ordering) + difference;
        //var sortedFiles = _.sortBy(angular.copy($scope.files), 'ordering');
        //var indexOfChangedItem = _.findIndex(sortedFiles, { 'id': item.id});
        //var first = sortedFiles.slice(0, indexOfChangedItem);
        //var second = sortedFiles.slice(indexOfChangedItem + 1);
        //
        //if (difference > 0) {
        //
        //} else if (difference < 0) {
        //
        //}

        //if (difference > 0) {
        //  for (var i = $scope.files.length - 1; i >= 0; i--) {
        //
        //    if (oldOrdering) {
        //      if (oldOrdering == $scope.files[i].ordering && $scope.files[i].id !== item.id) {
        //        $scope.files[i].ordering = parseInt($scope.files[i].ordering) - 1;
        //        oldOrdering = parseInt($scope.files[i].ordering);
        //      }
        //    } else {
        //      oldOrdering = parseInt($scope.files[i].ordering);
        //    }
        //  }
        //} else if (difference < 0) {
        //  for (var j = 0; j < $scope.files.length; j++) {
        //    if (oldOrdering) {
        //      if (oldOrdering == parseInt($scope.files[j].ordering && $scope.files[j].id !== item.id)) {
        //        $scope.files[j].ordering = parseInt($scope.files[j].ordering) + 1;
        //        oldOrdering = parseInt($scope.files[j].ordering);
        //      }
        //    } else {
        //      oldOrdering = parseInt($scope.files[j].ordering);
        //    }
        //  }
        //}
        //var objToSend = [];
        //$scope.files.forEach(function (element, index, array) {
        //  objToSend[index] = {
        //    'id': element.id,
        //    'ordering': parseInt(element.ordering)
        //  }
        //});
        //console.log(objToSend);
      };

      $scope.getFiles = function () {
        cfpLoadingBar.start();
        AdmissionsService.getSupportingDocuments(documentsConfig).then(function (res) {
          if (res.data.error) {
            Notification.error({
              title: 'Error',
              message: res.data.msg
            });
          } else {
            $scope.files = res.data.items;

            $scope.files.forEach(function (item) {
              if (item.created) {
                var created = moment(item.created);
                if (!_.isString(item.file)) {
                  item.file.uploaded = created.toDate();
                }
              }
            });
            var el = document.getElementsByTagName('tbody')[0];
            var sortable = Sortable.create(el, {
              animation: 150,

              // dragging ended
              onEnd: function (evt) {
                var itemScope = angular.element(evt.item).scope();
                $scope.sendOrderingArray(itemScope, evt);
              }
            });

            $scope.filesTable = new NgTableParams({
              page: 1,
              count: 10,
              sorting: {
                id: 'asc'
              }
            }, {
              total: $scope.files.length, // length of data
              getData: function(params) {
                var orderedData = params.sorting() ? $filter('orderBy')($scope.files, params.orderBy()) : $scope.files;
                return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
              }
            });
            cfpLoadingBar.complete();
          }
        });
      };

      $scope.uploadFile = function (data) {
        var fileData = {
          name: data.title,
          file_id: data.id
        };
        AdmissionsService.addSupportingDocument(fileData).then(function (res) {
          uploader.clearQueue();
          if (res.status == 200) {
            Notification.success({
              title: 'Success',
              message: 'File has been uploaded successfully.'
            });
          } else {
            Notification.error({
              title: 'Error',
              message: res.data.msg
            });
          }
          $scope.getFiles();
        });
      };

      $scope.deleteFile = function (id) {
        AdmissionsService.deleteSupportingDocument(id).then(function (res) {
          if (res.status == 200) {
            Notification.success({
              title: 'Success',
              message: 'File has been deleted successfully.'
            });
            $scope.getFiles();
          } else {
            Notification.error({
              title: 'Error',
              message: res.data.msg
            });
          }
        });
      };

      var uploader = $scope.uploader = FilesService.getUploader('admissionsupportdocument', '1', true);

      uploader.onCompleteItem = function(fileItem, response, status, headers) {
        if (!response.error || status == 200) {
          $scope.uploadFile(response.data);
        } else {
          fileItem.msg = response.msg;
        }
      };

      uploader.onBeforeUploadItem = function(item) {
        item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
      };
      $scope.documentsUploaderLoaded = true;

      $scope.getFiles();
    }]);
})();