(function () {
  'use strict';

  angular
    .module('app.module.learningPath', [])
    .controller('ModuleLearningPathCtrl', ModuleLearningPathCtrl);

  function ModuleLearningPathCtrl ($sce, $modal, $scope, $window, $stateParams, $rootScope, spModule, module, MessageService, CoursesService, StudyPath2Service, FilesService, sections, user, Notification, PermissionsService) {
    var vm = this;
    var userId = $stateParams.userId || $rootScope.userId;

    vm.isFlat = PermissionsService.has('courses.learningpath.menu.flat');

    // it's a position of wizzard
    $scope.$parent.module.index = 1;

    vm.sectionOpened = true;
    vm.sections = sections;

    /**
     * Saving learning path's opened sections each time they changed
     */
    $scope.$watch(function () { return vm.sections;}, function (newItem) {
      if (!newItem) return;
      $window.localStorage.learningPathTree =
        angular.toJson(newItem)
    }, true);


    /**
     * Callback wich is called when item was selected
     * in sections tree
     *
     * @param  {Object} content
     * @param  {Object} section
     */
    vm.onItemSelect = function (content, section) {
      vm.content = content;
      vm.content.body = $sce.trustAsHtml(content.body);

      vm.section = section;
      if (content.content_type_name != 'Activities') {
        return;
      }

      showActivityFiles();
      vm.uploader = initUploader(spModule.id, userId, content);
    }

    /**
     * Delete attached file
     *
     * @todo Move to activities directive
     * @param  {Number} id fileId
     */
    vm.deleteFile = function deleteFile (id) {
      var result = $window.confirm('Do you really want to delete this file?');
      if (!result) return;
      StudyPath2Service.deleteStudentFile(id)
        .then(function (res) {
          showActivityFiles();
        })
    }


    vm.openLearningInfoModal = function () {
      var scope = $scope.$new();
      scope.answers = CoursesService.getLearningPathHelp();
      $modal({
        templateUrl: '/partials/modals/studio.learning.tab.info.modal.html',
        scope: scope,
        show: true
      });
    }

    vm.sendError = function() {
      var scope = $scope.$new();
      scope.moduleTitle = module.title;
      scope.sectionTitle = vm.section.title;
      scope.contentTitle = vm.content.title;
      scope.reportError = reportError

      $modal({
        templateUrl: '/partials/modals/studio.send.error.modal.html',
        scope: scope,
        show: true
      })
    }

    function reportError(title, body) {
      var data = {
        source: 'content',
        source_id: vm.content.id,
        title: title,
        body: body,
        created_by: user.id
      }
      CoursesService.sendError(data)
      .then(function() {
        Notification.success({
          title: 'Thank You',
          message: 'Error Successfully Reported'
        })
      })
    }

    /**
     * Loads and displays activity files
     *
     * @todo Move to activities directive
     */
    function showActivityFiles () {
      getActiviyFiles(spModule.id, vm.content.id)
      .then(function (files) {
        vm.files = files;
      })
    }


    /**
     * Loads files with content for activity
     *
     * @todo Move to activities directive
     * @todo Move getting files content to service (?)
     */
    function getActiviyFiles (spModuleId, contentId) {
      var files = [];
      return StudyPath2Service
        .getFilesAttachedToActivity(spModuleId, contentId)
        .then(function (res) {
          files = res.data;
          return res.data.map(function (file) {
            return file.file_id
          });
        })
        .then(FilesService.getBulkFiles)
        .then(function (res) {
          files.map(function (item) {
            item.file_data = res.data.filter(function (file) {
              return file.id === item.file_id;
            })[0];
          });
          return files;
        })
        .then(function (files) {
          return files;
        })
    }

    /**
     * Initilize loader and all callbacks
     * @param  {Number} spModuleId - files will be attached to this spModule
     * @param  {Number} userId     - author of messages and uploader of files
     * @param  {[type]} content    [description]
     * @return {[type]}            [description]
     */
    function initUploader (spModuleId, userId, content) {
      var uploader = FilesService.getUploader('studio', userId, true);
      var fileData = [];
      var params = {
        'sp_module_id': spModuleId,
        'created_by': userId,
        'content_id': content.id
      };

      uploader.onCompleteItem = function (fileItem, res, status, headers) {
        params.file_id = res.data.id;
        fileData[res.data.id] = res.data;
      }

      uploader.onCompleteAll = function onUploadComplete () {
        StudyPath2Service.attachFile(spModuleId, [params])
          .then(function (res) {
            uploader.clearQueue();

            res.data.forEach(function (item, index) {
              item.file_data = fileData[item.file_id];
              item.file_messages = [];
              var data = {
                message: 'Uploaded file: ' + fileData[item.file_id].title + ' to Activity ' + content.title,
                studio_file_id: item.file_id,
                studio_file_data: fileData[item.file_id],
                activityName: content.title,
                activityFile: item
              }

              sendMessageNotificationOnStudioFiles(data, userId, spModule.id);
              showActivityFiles();
            });
          })
      }

      uploader.onBeforeUploadItem = function (item) {
        item.url = item.url + '?access_token=' + angular.fromJson($window.localStorage.token).access_token;
      }


      return uploader;
    }


    function sendMessageNotificationOnStudioFiles (uploadData, userId, spModuleId) {
      var data = {
        user_id: userId,
        text: uploadData.message,
        created_at_local: new Date(),
        deleted: 0,
        file_ids: [],
        attached_to_file_id: uploadData.studio_file_id,
        attached_to_file_data: uploadData.studio_file_data
      }

      getConversation(userId, spModuleId)
        .then(function (res) {
          var conversation = res.data.items[0];

          if (conversation) {
            data.conversation_id = conversation.Conversation.id;
            MessageService.sendMessage(data);
          } else {
            data.conversation_id = null;
            data.topic = module.title,
            data.scope_id = $rootScope.tutorMessageScope.id;
            data.source_id = spModule.id;
            data.to = [spModule.tutor_id];
            MessageService.sendMessage(data);
          }

        });
    }

    function getConversation (userId, spModuleId) {
      var messageData = {
        userId: userId,
        sourceId: spModuleId,
        page: 1,
        scopeId: 13,
        order: 'DESC',
        category: 'none'
      }

      return MessageService.getConversations(messageData)
    }
  }
})();