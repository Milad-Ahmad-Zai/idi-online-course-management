(function () {
  'use strict';

  angular
  .module('app.module', [])
  .controller('ModuleCtrl', ModuleCtrl);

  function ModuleCtrl ($scope, $modal, module, spModule, MessageService, user, conversation) {
    var vm = this;
    vm.index = 0;
    vm.module = module;

    var ATTACHMENT_POPUP_URL = '../../../partials/modals/file.page.modal.html';
    var popupScope = $scope.$new();


    popupScope.tagAttachment = function (form) {
      var params = {
        user_id: user.id,
        text: popupScope.reply.message,
        created_at_local: new Date(),
        attached_to_file_id: popupScope.file.file_data.id,
        conversation_id: conversation.Conversation.id
      }

      MessageService.sendMessage(params)
      .then(function (res) {
        form.$setPristine();
        form.$setUntouched();
        popupScope.reply.message = '';

        $scope.$broadcast('message:added', res.data.id);
        $scope.$broadcast('lp.sections:update-files');

        return popupScope;
      })
      .then(loadAttachmentMessages.bind(null, popupScope.file.file_data.id))
      .then(function (res) {
        popupScope.messages = res.data[popupScope.file.file_data.id];
      });
    }

    popupScope.onMessageDeleted = function (message) {
      $scope.$broadcast('message:deleted', message.id)
    }


    /**
     * Display modal window with attachment and it's
     * messages
     *
     * @param  {Object} file
     */
    vm.displayFile = function (file) {
      popupScope.file = {file_data: file};

      loadAttachmentMessages(file.id)
      .then(function (res) {
        popupScope.messages = res.data[file.id]
        return res;
      })
      .then(function () {
        popupScope.reply = { message: '' };

        $modal({
          template: ATTACHMENT_POPUP_URL,
          templateUrl: ATTACHMENT_POPUP_URL,
          scope: popupScope
        })
      })
    }

    /**
     * Load messages for attachment
     *
     * @param {Number} fileId
     */
    function loadAttachmentMessages (fileId) {
      return MessageService.getFileMessages(fileId)
    }

    $scope.$on('$destroy', function () {
      popupScope.$destroy();
    })

  }
})();