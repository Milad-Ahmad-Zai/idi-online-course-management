(function () {
  'use strict';

  angular
    .module('app.module.forums', [])
    .controller('ModuleForumsCtrl', ModuleForumsCtrl);

  function ModuleForumsCtrl ($modal, $scope, $state, cfpLoadingBar, Notification, NgTableParams, SemestersService, ForumsService, FilesService, FileUploader, user, module) {
    var vm = this;

    // it's a position of wizzard
    $scope.$parent.module.index = 3;

    vm.topic = {};

    var MESSAGES_PER_TOPIC = 5; // TODO:config replace with config

    var forumId = module.forum_id;
    var semesterId = module.semester_id;
    var topicUploader = null;

    var topicFilesAttach = [];
    var tempTopicFileIds = [];

    init();

    function init() {
      getForum(forumId);
      getSemester(semesterId);
      loadTopics();
      initializeAddTopicUploader();
    }


    function initializeAddTopicUploader() {
      topicUploader = vm.topicUploader = FilesService.getUploader('forumattachment', forumId, true);

      topicUploader.onCompleteItem = function(fileItem, response, status, headers) {
        if (!response.error || status == 200) {
          tempTopicFileIds.push(response.data.id);
          topicFilesAttach[response.data.id] = response.data;
        } else {
          fileItem.msg = response.msg;
        }
      };

      topicUploader.onBeforeUploadItem = function(item) {
        item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
      };

      FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
        return false; // true|false
      };
    }

    function getSemester (id) {
      SemestersService.getSemester(id).then(function (res) {
        if (res) {
          vm.currentSemester = res.data.semester;
          vm.topic.semester = vm.currentSemester.name;
        }
      });
    }

    function getForum (id) {
      ForumsService.getForum(id).then(function (res) {
        if (res) {
          vm.currentForum = res.data;
        }
      });
    }

    vm.goToPage = function (page) {
      var params = angular.extend({}, $state.params, {page: page});
      $state.go($state.current.name, params, {
        location: true
      })
    }



    function loadTopics() {
      var LIMIT = 10;

      var params = {
        start: LIMIT * ($state.params.page - 1) || 0,
        limit: LIMIT,
        filter: '',
        id: module.forum_id
      };

      ForumsService.getForumTopics(params).then(function (res) {
        if (!res.data || !res.data.list) {
          vm.topics = [];
          return;
        }

        res.data.list.map(function (item) {
          item.pages = Math.ceil(item.posts_count / MESSAGES_PER_TOPIC);
        })

        vm.topics = res.data.list;
        vm.pagination = {
          limit: res.data.pagination.limit,
          total: res.data.pagination.totalItems,
          page: $state.params.page || 1
        };

      });
    }


    function updateSavePost(post) {
      ForumsService.updateSavePost(post)
      .then(function (resPost) {
        loadTopics();
        vm.addTopicModal.$promise.then(vm.addTopicModal.hide);
        vm.topic = {};

        Notification.success({
          title: 'Success',
          message: 'Topic successfully created.'
        });

        if (tempTopicFileIds.length > 0) {
          tempTopicFileIds.forEach(function (item, index) {
            var data = {
              file_id: item,
              entity_id: resPost.data.id,
              post_id: resPost.data.id,
              type: 'post'
            };
            ForumsService.attachFileToPostTopic(data).then(function (attachRes) {
              if (index == (tempTopicFileIds.length - 1)) {
                tempTopicFileIds = [];
                topicUploader.clearQueue();
              }
            });
          });
        }
      })
      .then(loadTopics);
    }

    vm.addTopic = function () {
      vm.topic.type = 'normal';
      vm.addTopicModal = $modal({
        scope: $scope,
        templateUrl: '/partials/modals/topic.modal.html'
      });
    };

    vm.saveTopic = function () {
      cfpLoadingBar.start();

      var params = {
        created_at: new Date(),
        created_by: user.id,
        subject: vm.topic.subject,
        forum_id: forumId,
        learn_path: vm.topic.type == 'learn_path' ? 1 : 0,
        locked: vm.topic.locked ? 1 : 0,
        message: vm.topic.message,
        semester: vm.topic.semester,
        sticky: vm.topic.type == 'sticky' ? 1 : 0
      };

      // save topic
      ForumsService.updateSaveTopic(params)
      .then(function (res) {
        if (res) {
          // save message
          var postDate = new Date();
          var post = {
            topic_id: res.data.id,
            subject: res.data.subject,
            text: vm.topic.message,
            created_at: postDate,
            updated_at: postDate,
            created_by: user.id
          };
          updateSavePost(post);

        } else {
          vm.addTopicModal.$promise.then(vm.addTopicModal.hide);
          cfpLoadingBar.complete();
          Notification.error({
            title: 'Error',
            message: 'An error occured while processing your request.'
          });
        }
      })
      .then(loadTopics);
    };

    vm.deleteTopic = function (id) {
      ForumsService.deleteTopic(id)
      .then(function (res) {
        if (!res.error) {
          Notification.success({
            title: 'Success',
            message: 'Topic has been deleted successfully.'
          });
        } else {
          Notification.error({
            title: 'Error',
            message: 'Topic hasn\'t been deleted successfully.'
          });
        }
      })
      .then(loadTopics)
    };

    vm.cancelUpload = function (item) {
      var index = item.uploader.queue.indexOf(item);
      var fileId = vm.replyAttachments[index];

      item.isUploading ? item.cancel() : item.remove();

      if (fileId) {
        vm.replyAttachments.splice(index, 1);
        FilesService.deleteFile(fileId);
      }
    };
  }
})();