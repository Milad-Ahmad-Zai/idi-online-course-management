(function () {
  'use strict';
  
  angular
    .module('app.module.welcome', [])
    .controller('ModuleWelcomeCtrl', ModuleWelcomeCtrl);

  function ModuleWelcomeCtrl ($scope, module, student, tutor, supportDocuments) {
    var vm = this;

    // it's a position of wizzard
    $scope.$parent.module.index = 0;

    vm.module = module;
    vm.supportDocuments = supportDocuments.data.list;
    if (student) {
      vm.card = student;
      vm.card.role = 'student';
    } else {
      vm.card = tutor.data;
      vm.card.role = 'tutor';
    }
  }
})();