(function () {
  angular
    .module('ui.routes')
    .config(config);

  function config($stateProvider) {
    $stateProvider
    .state('app.admin.courses.module', {
      url: '/module/:id?userId',
      abstract: true,
      views: {
        'content@app': {
          templateUrl: '/partials/module/module.html',
          controller: 'ModuleCtrl as module'
        }
      },
      resolve: {
        spModule: function ($stateParams, load, StudyPath2Service) {
          var spModuleId = $stateParams.id;
          return StudyPath2Service.getStudypathModuleEntry(spModuleId, true).then(function (res) {
            return res.data;
          })
        },
        module: function ($window, load, spModule, CoursesService) {
          var filter = 'id=' + spModule.module_id + '/get.section=false/get.course=false/get.user=false/files=true'
          // we are checking if user already has opened
          // sections for this module before
          var storedLpSections = angular.fromJson($window.localStorage.learningPathTree || '{}');
          if (spModule.module_id != storedLpSections.moduleId) {
            filter += '/default.content=true'
          }
          return CoursesService.getEntities({
            start: 0,
            limit: 100,
            filter: filter
          }, 'modules')
            .then(function (res) {
              return res.data.list[0];
            });
        },
        conversationParams: function (user, tutorScope, $stateParams, spModule) {
          return params = {
            userId: $stateParams.userId || user.id,
            scopeId: tutorScope.id,
            sourceId: spModule.id,
            page: 1,
            order: 'DESC',
            category: 'none'
          };
        },
        /**
         * TODO: For now conversation returns not only messages
         *       but also attachments and users. We should replace
         *       this method with a new one which load only conversation
         *       info as soon as it will be avaible
         */
        conversation: function (load, MessageService, conversationParams, module, user, spModule, tutorScope, $stateParams) {
          var userIds = [spModule.tutor_id];
          if (spModule.tutor_id != user.id) {
            userIds.push(user.id)
          } else {
            userIds.push($stateParams.userId)
          }

          return MessageService.getOrCreateTutorConversation(spModule.id, {
            user_ids: userIds,
            topic: module.title
          })
          .then(function(res) {
            return res.data
          })
        },
        tutorScope: function (load, MessageService) {
          return MessageService.getUserMessageScope().then(function (res) {
            return res.data.filter(function (item) {
              return item.name_alias == 'tutor';
            })[0];
          })
        },
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'app.module',
              files: ['/assets/js/controllers/module/module.ctrl.js']
            },
            {name: 'idi.messages.message', files: ['/assets/js/directives/messages/message/idi.message.js']},
            {
              name: 'idi.module.learning-path',
              files: ['/assets/js/directives/module/learning-path/idi.learning-path.js']
            },
            {name: 'idi.common.thumbnail', files: ['/assets/js/directives/common/thumbnail/idi.thumbnail.js']},
            // { name: 'unique.filter', files: ['/assets/js/filters/unique.filter.js'] }
            // { name: 'angularAudioRecorder', files: ['/assets/js/vendor/angular/angular-audio-recorder.js'] }
          ])
        }
      }
    })
    .state('app.admin.courses.module.welcome', {
      url: '',
      templateUrl: '/partials/module/module.welcome.html',
      controller: 'ModuleWelcomeCtrl as welcome',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'app.module.welcome', files: ['/assets/js/controllers/module/module.welcome.ctrl.js']},
            {name: 'courses.service', files: ['/assets/js/services/courses.service.js']},
            {name: 'idi.profile.widget', files: ['/assets/js/directives/common/profile/idi.profileWidget.js']}
          ])
        },
        tutor: function (load, spModule, ProfilesService) {
          return ProfilesService.getPublicProfile(spModule.tutor_id);
        },
        student: function (load, module, spModule, user, ProfilesService) {
          if (user.id === spModule.tutor_id) {
            return spModule.student;
          } else {
            return null;
          }
        },
        supportDocuments: function (load, spModule, CoursesService) {
          return CoursesService.getCourseSupportDocuments({
            sort: 'ordering',
            filter: 'source==module/source_id=' + spModule.module_id
          });
        }
      }
    })
    .state('app.admin.courses.module.learning', {
      url: '/learning-path',
      templateUrl: '/partials/module/module.lp.html',
      controller: 'ModuleLearningPathCtrl as lp',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'app.module.learningPath', files: ['/assets/js/controllers/module/module.lp.ctrl.js']}
          ])
        },
        sections: function ($window, load, module) {
          if (module.first_content) {
            return {
              contentId: module.first_content.content_id,
              sectionId: module.first_content.section_id,
              parentId: module.first_content.parent_id,
              moduleId: module.id
            }
          } else {
            return angular.fromJson($window.localStorage.learningPathTree);
          }
        }
      }
    })
    .state('app.admin.courses.module.studio', {
      url: '/studio',
      templateUrl: '/partials/module/module.studio.html',
      controller: 'ModuleStudioCtrl as studio',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'app.module.studio', files: ['/assets/js/controllers/module/module.studio.ctrl.js']},
          ])
        },
        attachments: function (load, conversation, MessageService) {
          return MessageService.getConversationFiles(conversation.Conversation.id)
            .then(function (res) {
              return res.data
            })
            .catch(function() {
              return []
            })
        },
        sections: function ($window, load, module, spModule) {
          if (module.first_content) {
            return {
              contentId: module.first_content.content_id,
              sectionId: module.first_content.section_id,
              parentId: module.first_content.parent_id,
              moduleId: module.id,
              spModuleId: spModule.id
            }
          } else {
            var local = angular.fromJson($window.localStorage.learningPathTree);
            return angular.extend({}, local, {spModuleId: spModule.id});
          }
        }
      }
    })
    .state('app.admin.courses.module.forums', {
      url: '/forums?page',
      templateUrl: '/partials/module/module.forums.html',
      controller: 'ModuleForumsCtrl as forums',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'idi.forums.topics-group', files: ['/assets/js/directives/forums/topics-group/idi.topics-group.js']},
            {name: 'idi.forums.topics-list', files: ['/assets/js/directives/forums/topics-list/idi.topics-list.js']},
            {name: 'bw.paging', files: ['/assets/js/vendor/angular/paging.js']},
            {name: 'app.module.forums', files: ['/assets/js/controllers/module/module.forums.ctrl.js']}
          ])
        }
      }
    })
    .state('app.admin.courses.module.assessment', {
      url: '/assessment/:assessmentId?tab',
      templateUrl: '/partials/module/module.assessment.html',
      controller: 'ModuleAssessmentCtrl as assessments',
      params: {
        tab: 'submission'
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'services.assessments', files: ['/assets/js/services/assessments.service.js']},
            {
              name: 'idi.assessments.assessments',
              files: ['/assets/js/directives/assessments/assessments/idi.assessments.js']
            },
            {
              name: 'idi.assessments.self-evaluation',
              files: ['/assets/js/directives/assessments/self-evaluation/idi.self-evaluation.js']
            },
            {
              name: 'idi.assessments.learning-outcomes',
              files: ['/assets/js/directives/assessments/learning-outcomes/idi.learning-outcomes.js ']
            },
            {name: 'app.module.assessment', files: ['/assets/js/controllers/module/module.assessment.ctrl.js']}
          ])
        },
        enrolmentId: function () {
          // TODO: get actual value
          return 7
        },
        enrolment: function (load, enrolmentId, AssessmentsService) {
          return AssessmentsService.getEnrolment(enrolmentId);
        },
        submissions: function (load, enrolment) {
          return enrolment.data.submissions;
        },
        folders: function (load, AssessmentsService) {
          return AssessmentsService.getFolders();
        }

      }
    })
  }
})()