(function () {
  'use strict';

  angular
    .module('app.module.studio', [])
    .controller('ModuleStudioCtrl', ModuleStudioCtrl);

  function ModuleStudioCtrl ($rootScope, $scope, $sce, $window, $modal, user, NgTableParams, spModule, conversation, conversationParams, attachments, CoursesService, MessageService, FilesService, sections) {
    var vm = this;
    var MESSAGES_COUNT = 5;
    var page = 0;

    // it's a position of wizzard
    $scope.$parent.module.index = 2;

    init();

    vm.replyAttachments = [];
    vm.sections = sections;
    vm.sections.spModuleId = spModule.id;

    vm.messages = [];

    vm.loadNextMessages = loadNextMessages;

    $scope.$on('message:added', addMessage);
    $scope.$on('message:deleted', onMessageDeleted);

    vm.sendReply = function (form) {
      var params = {
        user_id: $rootScope.userId,
        text: vm.reply.message + '',
        created_at_local: new Date(),
        conversation_id: conversation.Conversation.id,
        file_ids: vm.replyAttachments
      };

      MessageService.sendMessage(params)
      .then(function (res) {
        vm.reply.message = '';

        if (vm.replyAttachments.length) {
          MessageService.getConversationFiles(conversation.Conversation.id)
          .then(function (res) {
            vm.attachments = getAttachmentsMap(res.data);
          })
        }

        vm.replyAttachments.splice(0, vm.replyAttachments.length);
        form.$setPristine();
        form.$setUntouched();
        vm.uploader.clearQueue();

        return res.data.id;
      })
      .then(function (id) {
        addMessage({}, id);
      });
    }


    vm.cancelUpload = function (item) {
      var index = item.uploader.queue.indexOf(item);
      var fileId = vm.replyAttachments[index];

      item.isUploading ? item.cancel() : item.remove();

      if (fileId) {
        vm.replyAttachments.splice(index, 1);
        FilesService.deleteFile(fileId);
      }
    }

    /**
     * When someone removes tag message we search for this
     * message in studio messages and mark it as deleted.
     * If user don't have permission to see deleted messages,
     * it will disappear as soon as user updates page.
     *
     * @param  {Number} messageId - id of deleted message
     */
    function onMessageDeleted (event, messageId) {
      vm.messages
      .filter(function (message) {
        return message.id == messageId
      })
      .map(function (message) {
        message.deleted = '1';
      })

    }


    /**
     * Loads one message and adds it to the beginning
     * We use this method to get message that user just added
     *
     * @param {Number} id - id of message to load
     */
    function addMessage (event, id) {
      MessageService.getMessage(id)
        .then(function (res) {
          vm.messages.unshift(res.data);
        })
    }


    vm.openStudioInfoModal = function () {
      $scope.answers = CoursesService.getStudioHelp();

      $modal({
        scope: $scope,
        templateUrl: '/partials/modals/studio.studio.tab.info.modal.html'
      })
    }


    $scope.loadUrl = function (e, fileId) {
      $scope.urlIsLoading = true;
      FilesService.getFileUrl(fileId)
        .then(function (res) {
          $scope.urlIsLoading = false;
          $scope.urls = $scope.urls || [];
          $scope.urls[fileId] = res.data;
        })
    }


    /**
     * Creates map of attachments
     *
     * @param  {Array} attachments - array of attachments
     * @returns {Object} - map of attachments
     * @example
     * {
     *   '124': {<file data>},
     *   '125': {<file data>}
     * }
     */
    function getAttachmentsMap (attachments) {
      return attachments.reduce(function (prev, current) {
        prev[current.id] = current;
        return prev;
      }, {})
    }

    /**
     * Loads next messages in conversation
     */
    function loadNextMessages () {
      page++;
      var offset = page * MESSAGES_COUNT - MESSAGES_COUNT;

      MessageService.getConversationMessages(conversation.Conversation.id, {
        offset: offset,
        count: MESSAGES_COUNT
      })
      .then(function (res) {
        if (!res || !res.data) return
        vm.messages = vm.messages.concat(res.data.list);
        vm.hasMoreMessages = res.data.pagination.currentPage < res.data.pagination.totalPages;
      })
    }


    /**
     * Uploader callbacks
     */
    function onBeforeUpload (item) {
      item.url = item.url + '?access_token=' + angular.fromJson($window.localStorage.token).access_token;
    }

    function onUploadComplete (file, response, status, headers) {
      vm.replyAttachments.push(response.data.id);
    }



    function initializeUploader () {
      var uploader = FilesService.getUploader('message', 'studio', true);

      uploader.onBeforeUploadItem = onBeforeUpload;
      uploader.onCompleteItem = onUploadComplete;

      return uploader;
    }

    /**
     * Initialization of controller
     */
    function init () {
      loadNextMessages();

      vm.attachments = getAttachmentsMap(attachments);
      vm.uploader = initializeUploader();
    }

  }
})();