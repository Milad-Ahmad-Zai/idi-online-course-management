(function () {
  'use strict';

  angular
    .module('app.module.assessment', [])
    .controller('ModuleAssessmentCtrl', ModuleAssessmentCtrl);

  function ModuleAssessmentCtrl($scope, $state, AssessmentsService, FilesService, submissions, folders, $window, $modal) {
    var vm = this;

    init();
    var ATTACHMENT_POPUP_URL = '../../../partials/modals/assessments.file.modal.html';
    var popupScope = $scope.$new();

    function init() {
      // it's a position of wizzard
      $scope.$parent.module.index = 4;

      // all assessments
      vm.assessments = submissions;
      //current assessment
      vm.assessment = vm.assessments[1];
      //Select tab
      vm.tab = $state.params.tab;

      //folders & files
      vm.folders = folders.data.list;
      vm.folder = vm.folders[0];
      vm.files = [];

      //uploader
      vm.uploader = initializeUploader();

      vm.learningOutcomes = [
        {
          title: 'Knowledge and Understadning',
          items: [
            {
              title: '2.1 How to effectively gather, edit and utilise research',
              checked: true
            },
            {
              title: '2.2 Visual languages, materials, techniques',
              checked: false
            },
          ]
        },
        {
          title: 'Skills and Attributes',
          items: [
            {
              title: '2.1 How to effectively gather, edit and utilise research',
              checked: true
            },
            {
              title: '2.2 Visual languages, materials, techniques',
              checked: false
            }
          ]
        }
      ];

      $scope.$watch('assessments.folder', function (folder) {
        if (folder) {
          getFiles(folder);
        }
      });
    }

    function sendFiles(fileId) {
      var params = {
        file_id: fileId,
        folder_id: vm.folder.id,
        submission_id: vm.assessment.id
      };

      AssessmentsService.addFiles(params).then(function (res) {
        vm.uploader.clearQueue();

        return res.data.id;
      }).then(reInitFiles);
    }

    function reInitFiles() {
      getFiles(vm.folder);
    }

    function initializeUploader() {
      var uploader = FilesService.getUploader('assessment', vm.assessment.id, true);
      uploader.onCompleteItem = onUploadComplete;
      uploader.onBeforeUploadItem = onBeforeUpload;

      return uploader;
    }

    function onUploadComplete(file, response) {
      sendFiles(response.data.id);
    }

    function onBeforeUpload(item) {
      item.url = item.url + '?access_token=' + angular.fromJson($window.localStorage.token).access_token;
    }

    function getFiles(folder) {
      var config = {
        start: 0,
        limit: 100,
        filter: 'folder_id=' + folder.id
      };

      AssessmentsService.getFiles(config).then(function (files) {
        vm.files = files.data.list;
      });
    };

    vm.goToTab = function (tabName) {
      $state.go('^.assessment', {tab: tabName}, {'notify': false});
      vm.tab = tabName;
    };

    vm.onSubmitted = function () {
      console.log("onSubmitted");
    };

    /**
     * Display modal window with attachment
     *
     * @param  {Object} file
     */
    vm.displayFile = function (file) {
      popupScope.file = {file_data: file};
      $modal({
        template: ATTACHMENT_POPUP_URL,
        templateUrl: ATTACHMENT_POPUP_URL,
        scope: popupScope
      })
    }

  }
})();