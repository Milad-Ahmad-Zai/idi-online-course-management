(function () {
  'use strict';

  angular
    .module('ctrl.tutor', [])

    .controller('TutorCtrl', function ($scope, $state, $stateParams, $rootScope, $q, $window, CoursesService, NgTableParams, StudyPath2Service, cfpLoadingBar, profile, user) {
      $scope.students = [];

      init();

      function init() {
        $scope.date = new Date();
        $scope.profile = profile;
      }

    });
})();