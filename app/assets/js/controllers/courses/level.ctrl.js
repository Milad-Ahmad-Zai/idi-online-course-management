(function () {
  'use strict';

  angular
    .module('ctrl.level', [])
    .controller('LevelCtrl', LevelCtrl);

    function LevelCtrl($scope, $state, $stateParams, $rootScope, $q, $modal, CoursesService, StudyPath2Service) {

      $scope.levelId = $stateParams.id;
      $scope.level = {};

      $scope.selectModule = function (e, module) {
        var scope = angular.element(e.currentTarget).scope();
        var id = module ? scope.module.CB_content.id : scope.module.id;
        $state.go('app.admin.courses.module.welcome', { id: id });
      };

      $scope.showLevel = function (res) {

        $scope.level = res.data.list[0];
        var cour_id = $scope.level.cour_id;
        StudyPath2Service.getStudentModules($rootScope.userId, cour_id).then(function (res) {
          $scope.modules = res.data;
        });

        $rootScope.studypathDeferred.promise.then(function () {
          $rootScope.studypath.forEach(function (path) {
            $scope.studentModules = path.modules.filter(function (module) {
              return module.cour_id == $scope.level.cour_id && module.semester_id == $scope.level.semester_id;
            });
          });
          $scope.studentModules.forEach(function (studentModule) {
            $scope.level.modules.forEach(function (module) {
              if (module.id == (studentModule.CB_content && studentModule.CB_content.module_id)) {
                studentModule.CB_content = _.merge(studentModule.CB_content, module);
              }
            });
          });
        });

        $scope.supportDocuments = [];

        res.data.list.forEach(function (level) {
          level.files.forEach($scope.supportDocuments.push);
          level.modules.forEach(function (module) {
            module.files.forEach($scope.supportDocuments.push);
          });
        });
      };

      $scope.openVideo = function () {
        $scope.openVideoModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/abs.welcome.video.modal.html',
          show: true
        });
      };

      if ($scope.levelId) {
        var courseConfig = {
          start: 0,
          limit: 100,
          filter: 'id=' + $scope.levelId + '/files=true/files.module=true/get.module=true'
        };
        CoursesService.getEntities(courseConfig, 'courses').then($scope.showLevel);
      }
    }
})();
