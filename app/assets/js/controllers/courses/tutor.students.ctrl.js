(function () {
  'use strict';
  
  angular
    .module('ctrl.tutor.students', [])
    .controller('TutorStudentsCtrl', TutorStudentsCtrl);

    function TutorStudentsCtrl(StudyPath2Service, students) {
      var vm = this;

      vm.list = students.data;
    }
})();