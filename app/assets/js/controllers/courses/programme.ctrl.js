(function () {
  'use strict';

  angular
    .module('ctrl.programme', [])

    .controller('ProgrammeCtrl', function ($scope, $state, $stateParams, $rootScope, $q, $modal, CoursesService, ProfilesService) {

      $scope.programmeId = $stateParams.id;
      $scope.programme = {};

      $scope.selectCourse = function (e) {
        var scope = angular.element(e.currentTarget).scope();
        $state.go('app.admin.courses.level', {id: scope.course.id});
      };

      $scope.showStudents = function (res) {
        if (!(res.error || res.data && res.data.error)) {
          $scope.students = res.data.list;
          $scope.totalStudents = parseInt(res.data.pagination.totalItems);
        }
      };

      $scope.showProgramme = function (programmeResponse) {
        $scope.programmes = programmeResponse.data.list;
        $scope.programme = programmeResponse.data.list[0];

        $scope.filteredModules = [];

        $rootScope.studypathDeferred.promise.then(function () {

          var programmeStudypath = $rootScope.studypath.filter(function (studypath) {
            return $scope.programmeId === studypath.programme.programme_id;
          });
          $scope.currentProgrammeStudypaths = angular.copy(programmeStudypath);
          programmeStudypath = programmeStudypath[0];
          $scope.cutStudypath = programmeStudypath;

          $scope.view = 'programme';
          $scope.termsModal = $modal({
            scope: $scope,
            templateUrl: '/partials/modals/studio.terms.modal.html',
            prefixEvent: 'termsModal',
            show: false
          });
          $scope.registrationFormModal = $modal({
            scope: $scope,
            templateUrl: '/partials/modals/studio.registration.form.html',
            prefixEvent: 'registrationFormModal',
            show: false
          });
          $scope.$on('termsModal.hide', function (res) {
            $scope.declineProgrammeTerms(res, 'closed');
          });
          $scope.$on('registrationFormModal.hide', function (res) {
            $scope.declineUpdate(res, 'closed');
          });

          $scope.showProgrammeData = function () {
            $scope.showSupportDocuments();
            $scope.coursesIds = [];

            $scope.currentProgrammeStudypaths.forEach(function (currentProgrammeStudypath) {
              currentProgrammeStudypath.modules.forEach(function (module) {
                if ($scope.coursesIds.indexOf(module.cour_id) === -1) {
                  $scope.coursesIds.push(module.cour_id);
                }
              });
            });

            $scope.filteredCourses = $scope.programme.courses.filter(function (course) {
              return $scope.coursesIds.indexOf(course.cour_id) > -1;
            });
          };

          if (programmeStudypath.profile_upto_date === '1' && programmeStudypath.terms_and_conditions === '1') {
            $scope.showProgrammeData();
          } else if (programmeStudypath.profile_upto_date !== '1') {
            ProfilesService.getUH($scope.programmeId, $rootScope.userId).then(function (res) {
              if (!(res.error || res.data && res.data.error)) {
                if (res.data == false) {
                  $scope.uh = {};
                } else {
                  $scope.uh = res.data;
                }
                $scope.registrationFormModal.$promise.then($scope.registrationFormModal.show);
              } else {
                Notification.error({ message: 'Data wasn\'t downloaded', title: 'Error: '});
              }
            });
          } else if (programmeStudypath.terms_and_conditions !== '1') {
            $scope.termsModal.$promise.then($scope.termsModal.show);
          } else {
            $scope.termsModal.$promise.then($scope.termsModal.show);
          }
        });

        $scope.showSupportDocuments = function () {
          $scope.supportDocuments = [];
          $scope.programmes.forEach(function (programme) {
            programme.files.forEach($scope.supportDocuments.push);
            programme.courses.forEach(function (level) {
              level.files.forEach($scope.supportDocuments.push);
              level.modules.forEach(function (module) {
                module.files.forEach($scope.supportDocuments.push);
              });
            });
          });
        };
      };

      if ($scope.programmeId) {
        var programmeConfig = {
          start: 0,
          limit: 100,
          filter: 'id=' + $scope.programmeId + '/files=true/files.course=true/files.module=true/get.module=false'
        };

        var studentsConfig = {
          start: 0,
          limit: 6,
          filter: 'programme.programme_id=' + $scope.programmeId
        };

        CoursesService.getListOfStudents(studentsConfig).then($scope.showStudents);
        CoursesService.getEntities(programmeConfig, 'programmes').then($scope.showProgramme);
      }
    });
})();