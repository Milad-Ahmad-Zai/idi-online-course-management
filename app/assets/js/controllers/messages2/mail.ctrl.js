(function () {
  'use strict';

  angular
    .module('ctrl.mail2', [])
    .controller('Mail2Ctrl', Mail2Ctrl);

    function Mail2Ctrl($scope, $state) {
      var vm = this;

      vm.searchBy = 'keyword';

      $scope.category = "Inbox";
      $scope.$state = $state;

    }
})();