(function () {
  'use strict';

  angular
    .module('ctrl.mail2.inbox', [])
    .controller('InboxMail2Ctrl', InboxMail2Ctrl);
    function InboxMail2Ctrl($scope, $rootScope, NgTableParams, $state, Notification, Message2Service, AuthService) {
      var vm = this;
      vm.userId = $rootScope.user.id;
      var usersConfig = {
        id: vm.userId,
        start: 0,
        limit: 20,
        filter: '',
        sort: '-id',
        page: 1
      };

      init();

      function init(){
        $scope.$parent.category = "Messages";
        populateConversationsList();
      }

      vm.cbChecked = function(){
        var unchecked = vm.conversations.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = unchecked.length == 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.conversations, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      function getConversationsList (config) {
        return Message2Service.getUserConversations(config);
      };

      function populateConversationsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'desc'
          }
        }

        vm.conversationsTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return getConversationsList(usersConfig).then(function (res) {
              vm.conversations = res.data;
              console.log(res);
              angular.forEach(vm.conversations, function(value, key){
                AuthService.getUser(value.last_message.sender, {cache: true}).then(function(res){
                  vm.conversations[key].last_message.senderData = res.data.user;
                });
              })

              params.total(parseInt(res.pagination.total_items));
              vm.conversationsTable.total(parseInt(res.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populateConversationsList = populateConversationsList;

      vm.sortConversation = function (e) {
        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            usersConfig.sort = "id";
          } else {
            usersConfig.sort = "-id";
          }
        }

        usersConfig.start = 0;
        vm.populateConversationsList();
      };

      vm.readMail = function(conversation){
        $state.go('app.admin.mail2.conversation', {id: conversation.id, readed: conversation.readed})
      }

      vm.starred = function(conversation){
        var data = {
          conversations_id : [conversation.id]
        }

        var text = conversation.stared ? 'Message added to starred.' : 'Message removed from starred.';
        var method = conversation.stared ? Message2Service.unstareMessage : Message2Service.stareMessage;

        method(data)
        .then(function() {
          conversation.stared = !conversation.stared;
          Notification.success({message: text, title: 'Success'})
        })
        .catch(function(){
          Notification.error({message: text, title: "Error" });
        })
      }

      vm.markMessageAsReadUnread = function(read){

        var text = read ? 'Message marked as readed.' : 'Message marked as unreaded.';
        var method = read ? Message2Service.markConversationReaded : Message2Service.markConversationUnreaded;

        angular.forEach(vm.conversations, function(item){
          if(item.checked && item.readed != read){
            method(item.id)
            .then(function() {
              item.readed = read;
              item.checked = false;
              Notification.success({message: text, title: 'Success'})
            })
            .catch(function(){
              Notification.error({message: text, title: "Error" });
            })
          }
        })
      }

    }
})();