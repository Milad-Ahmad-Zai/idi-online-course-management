(function () {
  angular
    .module('ui.routes')
    .config(config);

  function config($stateProvider) {
    $stateProvider
    // mail
      .state('app.admin.mail2', {
        url: '/messages2',
        abstract: true,
        views: {
          'content@app': {
            controller: 'Mail2Ctrl as mail',
            templateUrl: '/partials/messages2/index.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2', files: ['/assets/js/controllers/messages2/mail.ctrl.js']},
              {name: 'message2.service', files: ['/assets/js/services/message2.service.js']}
            ]);
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        }
      })
      .state('app.admin.mail2.new', {
        url: '/new',
        params: {
          to: null,
          draft: null
        },
        views: {
          'subcontent': {
            controller: 'SingleMail2Ctrl as compose',
            templateUrl: '/partials/messages2/new.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.messageAnyone.create', 'messages.messageAnyGroup.create'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.single', files: ['/assets/js/controllers/messages2/singlemail.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail2.inbox', {
        url: '/inbox',
        views: {
          'subcontent': {
            controller: 'InboxMail2Ctrl as inbox',
            templateUrl: '/partials/messages2/inbox.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.inbox', files: ['/assets/js/controllers/messages2/inbox.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail2.conversation', {
        url: '/conversation/:id',
        params: {
          readed: null,
        },
        views: {
          'subcontent': {
            controller: 'ReadMail2Ctrl as conversation',
            templateUrl: '/partials/messages2/read.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.read', files: ['/assets/js/controllers/messages2/read.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail2.drafts', {
        url: '/drafts',
        views: {
          'subcontent': {
            controller: 'DraftsMail2Ctrl as drafts',
            templateUrl: '/partials/messages2/drafts.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.drafts', files: ['/assets/js/controllers/messages2/drafts.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail2.sent', {
        url: '/sent',
        views: {
          'subcontent': {
            controller: 'SentMail2Ctrl as sent',
            templateUrl: '/partials/messages2/sent.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.sent', files: ['/assets/js/controllers/messages2/sent.ctrl.js']}
            ]);
          }
        }
      })
    // end
  }
})()