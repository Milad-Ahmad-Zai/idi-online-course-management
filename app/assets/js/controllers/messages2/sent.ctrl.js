(function () {
  'use strict';

  angular
    .module('ctrl.mail2.sent', [])
    .controller('SentMail2Ctrl', SentMail2Ctrl);
    function SentMail2Ctrl($scope, $rootScope, $modal, NgTableParams, Message2Service, FilesService) {
      var vm = this;
      vm.filter = {};
      vm.userId = $rootScope.user.id;
      var usersConfig = {
        id: vm.userId,
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id',
        page: 1
      };

      init();

      function init(){
        $scope.$parent.category = "Sent Message";
        populateConversationsList();
      }

      function getConversationsList (config) {
        return Message2Service.getUserSentMessages(config);
      };

      function getData (config, $defer, params) {
        config.limit = params.count();
        config.start = ((params.page() || 1) - 1) * params.count();
        config.page = params.page();

        usersConfig.sort = Object.keys(params.sorting())[0];
        if (params.sorting()[usersConfig.sort] == 'desc') {
          usersConfig.sort = '-' + usersConfig.sort;
        }

        return getConversationsList(config).then(function (res) {
          vm.conversations = res.data;
          console.log(res);
          
          params.total(parseInt(res.pagination.total_items));
          vm.conversationsTable.total(parseInt(res.pagination.total_items));
          return res.data;
        })
      }

      function populateConversationsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        vm.conversationsTable = new NgTableParams({
          page: usersConfig.page,
          count: usersConfig.limit,
          sorting: sort
        }, {
          getData: getData.bind(null, usersConfig)
        });
      };

      vm.populateConversationsList = populateConversationsList;

      vm.isImage = function (ext) {
        ext = '|' + ext + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(ext) !== -1;
      };

      vm.displayImage = function (item) {
        $scope.imageItem = {file: item};
        
        vm.displayImageModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/image.modal.html',
          show: false
        });

        vm.displayImageModal.$promise.then(vm.displayImageModal.show);
      };
      
    }
})();