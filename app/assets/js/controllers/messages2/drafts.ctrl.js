(function () {
  'use strict';

  angular
    .module('ctrl.mail2.drafts', [])
    .controller('DraftsMail2Ctrl', DraftsMail2Ctrl);
    function DraftsMail2Ctrl($scope, $rootScope, $modal, NgTableParams, Message2Service, FilesService, Notification) {
      var vm = this;
      
      vm.filter = {};
      vm.userId = $rootScope.user.id;
      var usersConfig = {
        id: vm.userId,
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id',
        page: 1
      };

      init();

      function init(){
        $scope.$parent.category = "Draft Messages";
        populateConversationsList();
      }

      vm.cbChecked = function(){
        var unchecked = vm.conversations.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = unchecked.length == 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.conversations, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      function getConversationsList (config) {
        return Message2Service.getUserDraftMessages(config);
      };

      function getData (config, $defer, params) {
        config.limit = params.count();
        config.start = ((params.page() || 1) - 1) * params.count();
        config.page = params.page();

        usersConfig.sort = Object.keys(params.sorting())[0];
        if (params.sorting()[usersConfig.sort] == 'desc') {
          usersConfig.sort = '-' + usersConfig.sort;
        }

        return getConversationsList(config).then(function (res) {
          vm.conversations = res.data;
          console.log(res);

          //load attachments
          angular.forEach(vm.conversations, function(message){
            if(message.files.length){
              FilesService.getBulkFiles(message.files).then(function(response){
                message.filesData = response.data;
              });
            }
          });

          params.total(parseInt(res.pagination.total_items));
          vm.conversationsTable.total(parseInt(res.pagination.total_items));
          return res.data;
        })
      }

      function populateConversationsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        vm.conversationsTable = new NgTableParams({
          page: usersConfig.page,
          count: usersConfig.limit,
          sorting: sort
        }, {
          getData: getData.bind(null, usersConfig)
        });
      };

      vm.populateConversationsList = populateConversationsList;

      vm.isImage = function (ext) {
        ext = '|' + ext + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(ext) !== -1;
      };

      vm.displayImage = function (item) {
        $scope.imageItem = {file: item};
        
        vm.displayImageModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/image.modal.html',
          show: false
        });

        vm.displayImageModal.$promise.then(vm.displayImageModal.show);
      };

      vm.deleteDraft = function(){
        angular.forEach(vm.conversations, function(v, k) {
          if(v.checked){
            Message2Service.deleteDraft(v.id).then(function(response){
                var index = vm.conversations.indexOf(v);
                vm.conversations.splice(index, 1);
                Notification.success({ message: 'Draft deleted.', title: 'Success'});                
            })
            .catch(function(){
              Notification.error({ message: 'Failed to process request.', title: 'Error' });
            });
          }
        });
      }

    }
})();