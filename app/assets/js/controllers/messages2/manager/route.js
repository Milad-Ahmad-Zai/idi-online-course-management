(function () {
  angular
    .module('ui.routes')
    .config(config);

  function config($stateProvider) {
    $stateProvider
    // mail
      .state('app.admin.mail2.manager', {
        url: '/manager',
        views: {
          'content@app': {
            controller: 'Mail2ManagerCtrl as mailManager',
            templateUrl: '/partials/messages2/manager/index.html'
          }
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.manager', files: ['/assets/js/controllers/messages2/manager/mail.manager.ctrl.js']},
              {name: 'message2.service', files: ['/assets/js/services/message2.service.js']},
              {name: 'ui.readmore', files: ['/assets/js/directives/ui.readmore.js']}
            ]);
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        }
      })
      .state('app.admin.mail2.manager.scopes', {
        url: '/scopes',
        views: {
          'subcontent': {
            controller: 'ScopesMail2Ctrl as scopes',
            templateUrl: '/partials/messages2/manager/scopes.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.manager.scopes', files: ['/assets/js/controllers/messages2/manager/scopes.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail2.manager.conversations', {
        url: '/conversations',
        views: {
          'subcontent': {
            controller: 'ConversationsMail2Ctrl as conversations',
            templateUrl: '/partials/messages2/manager/conversations.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.manager.conversations', files: ['/assets/js/controllers/messages2/manager/conversations.ctrl.js']}
            ]);
          }
        }
      })
      .state('app.admin.mail2.manager.messages', {
        url: '/conversation/:id',
        views: {
          'subcontent': {
            controller: 'MessagesManagerMail2Ctrl as messages',
            templateUrl: '/partials/messages2/manager/messages.html'
          }
        },
        data: {
          /*permissions: {
            only: ['messages.conversationOwn.get', 'messages.conversationAny.search', 'messages.messages.sent'],
            redirectTo: 'app.login'
          }*/
        },
        resolve: {
          load: function ($ocLazyLoad) {
            return $ocLazyLoad.load([
              {name: 'ctrl.mail2.manager.messages', files: ['/assets/js/controllers/messages2/manager/messages.ctrl.js']}
            ]);
          }
        }
      })
    // end
  }
})()