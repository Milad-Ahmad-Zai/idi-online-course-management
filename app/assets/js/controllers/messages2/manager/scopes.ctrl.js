(function () {
  'use strict';

  angular
    .module('ctrl.mail2.manager.scopes', [])
    .controller('ScopesMail2Ctrl', ScopesMail2Ctrl);
    function ScopesMail2Ctrl($scope, $rootScope, NgTableParams, $state, $modal, Notification, Message2Service, AuthService) {
      var vm = this;
      vm.filter = {};
      vm.newScope = {};
      var scopeId = null;
      var usersConfig = {
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id',
        page: 1
      };

      init();

      function init(){
        populateScopesList();
      }

      function getScopesList (config) {
        return Message2Service.getScopesList(config);
      };

      function getData (config, $defer, params) {
        config.limit = params.count();
        config.start = ((params.page() || 1) - 1) * params.count();
        config.page = params.page();

        usersConfig.sort = Object.keys(params.sorting())[0];
        if (params.sorting()[usersConfig.sort] == 'desc') {
          usersConfig.sort = '-' + usersConfig.sort;
        }

        return getScopesList(config).then(function (res) {
          vm.scopesList = res.data;
          params.total(parseInt(res.pagination.total_items));
          vm.scopesTable.total(parseInt(res.pagination.total_items));
          return res.data;
        })
      }

      function populateScopesList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        vm.scopesTable = new NgTableParams({
          page: usersConfig.page,
          count: usersConfig.limit,
          sorting: sort
        }, {
          getData: getData.bind(null, usersConfig)
        });
      };

      vm.populateScopesList = populateScopesList;

      vm.cbChecked = function(){
        var unchecked = vm.scopesList.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.scopesList, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      vm.filterByKeyword = function (e) {
        if (e.which === 13) {
          if (vm.filter.name.length > 0) {
            usersConfig.filter = 'search=' + vm.filter.name;
          } else {
            usersConfig.filter = '';
          }         

          usersConfig.start = 0;
          vm.scopesTable.reload();
        }
      };

      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/messages2/manager/scopes.modal.html', show: false});
      vm.showAddNewModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };

      vm.initNewScope = function(){
        if(vm.method !== 'save'){
          vm.method='save'; 
          vm.newScope = {};
          vm.newScope.enable = true;
        }
        vm.showAddNewModal();
      }

      vm.addNew = function(){
        if(vm.newScope.name){
          if(vm.method === 'save'){
            Message2Service.addScope(vm.newScope).then(function(response){
              vm.scopesList.push(response.data);
              vm.newScope = {};
              addNewModal.hide();
              Notification.success({ message: 'New scope created.', title: 'Success'});
            })
            .catch(function(){
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            });
          }else{
            Message2Service.updateScope(vm.newScope, scopeId).then(function(response){
              vm.scopesTable.reload();
              vm.newScope = {};
              addNewModal.hide();
              Notification.success({ message: 'Scope updated.', title: 'Success'});
            })
            .catch(function(){
              Notification.error({ message: 'Form submission failed.', title: 'Error' });
            });
          }
        }
      }

      vm.update = function(id){
        angular.forEach(vm.scopesList, function(item){
          if(item.id === id){
            vm.method = 'update';
            scopeId = item.id;
            vm.newScope.name = item.name;
            vm.newScope.enable = item.enable;
            vm.showAddNewModal();
          }
        });
      }

      vm.delete = function(){
        angular.forEach(vm.scopesList, function(v, k) {
          if(v.checked){
            Message2Service.deleteScope(v.id).then(function(response){
              var index = vm.scopesList.indexOf(v);
              vm.scopesList.splice(index, 1);
              Notification.success({ message: 'Scope deleted.', title: 'Success'});
            })
            .catch(function(){
              Notification.error({ message: 'Scope deletion failed.', title: 'Error' });
            });
          }
        });
      }


    }
})();