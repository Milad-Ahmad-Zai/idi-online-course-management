(function () {
  'use strict';

  angular
    .module('ctrl.mail2.manager.conversations', [])
    .controller('ConversationsMail2Ctrl', ConversationsMail2Ctrl);
    function ConversationsMail2Ctrl($scope, $rootScope, NgTableParams, $state, $modal, Notification, Message2Service, AuthService) {
      var vm = this;
      vm.filter = {};
      var usersConfig = {
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id',
        page: 1
      };
      var scopesConfig = {
        start: 0,
        limit: 100
      };
      
      init();

      function init(){
        populateConversationsList();
        Message2Service.getScopesList(scopesConfig).then(function (res) {
          vm.messageScopes = res.data;
        });
        AuthService.getUsersList({limit: 10}).then(function(response){
          if(response.error === false){
            vm.users = response.data.list;
          }
        });
      }

      function getConversationsList (config) {
        return Message2Service.getConversations(config);
      };

      function getData (config, $defer, params) {
        config.limit = params.count();
        config.start = ((params.page() || 1) - 1) * params.count();
        config.page = params.page();

        usersConfig.sort = Object.keys(params.sorting())[0];
        if (params.sorting()[usersConfig.sort] == 'desc') {
          usersConfig.sort = '-' + usersConfig.sort;
        }

        return getConversationsList(config).then(function (res) {
          console.log(res.data);
          vm.conversationsList = res.data;
          params.total(parseInt(res.pagination.total_items));
          vm.conversationsTable.total(parseInt(res.pagination.total_items));
          return res.data;
        })
      }

      function populateConversationsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        vm.conversationsTable = new NgTableParams({
          page: usersConfig.page,
          count: usersConfig.limit,
          sorting: sort
        }, {
          getData: getData.bind(null, usersConfig)
        });
      };

      vm.populateConversationsList = populateConversationsList;

      vm.cbChecked = function(){
        var unchecked = vm.conversationsList.filter(function (item) {
          return !item.checked;
        });
        vm.allSelected = !unchecked.length > 0;
      }
      
      vm.toggleAll = function() {
        var isAllChecked = true;
        if (vm.allSelected) {
          isAllChecked = false;
        }
        angular.forEach(vm.conversationsList, function(v, k) {
          v.checked = !isAllChecked;
          vm.allSelected = !isAllChecked;
        });
      }

      vm.filterByKeyword = function (e) {
        if (e && e.which !== 13) return;

        var newFilter = "";
        
        angular.forEach(vm.filter, function(v,k){
          if(v){
            switch(k){
              //topic filter
              case 'topic':
                newFilter = newFilter + '&filter[]=topic==' + v;
                break;

              //conversation id filter
              case 'id':
                newFilter = newFilter + '&filter[]=id=IN=' + v;
                break;
              
              //scope filter
              case 'scope':
                newFilter = newFilter + '&filter[]=scope.name==' + v;
                break;
              
              //user filter
              case 'user_id':
                newFilter = newFilter + '&filter[]=users=IN=' + v;
                break;
              
              //date from filter
              case 'dateFrom':
                newFilter = newFilter + '&filter[]=from==' + v;
                break;
              
              //date to filter
              case 'dateTo':
                newFilter = newFilter + '&filter[]=to==' + v;
                break;
            }
          }
        });

        usersConfig.filter = newFilter;
        usersConfig.start = 0;
        vm.conversationsTable.reload();
      };

      vm.clearFilters = function(){
        vm.filter = {};
        usersConfig.filter = '';
        usersConfig.start = 0;
        vm.conversationsTable.reload();
      }

      vm.loadUsers = function(str){
        Message2Service.getUsersList(str).then(function(response){
          if(response.error === false){
            vm.users = response.data.list;
          }
        });
      }

      vm.delete = function(){
        angular.forEach(vm.conversationsList, function(v, k) {
          if(v.checked){
            Message2Service.deleteConversation(v.id).then(function(response){
                var index = vm.conversationsList.indexOf(v);
                vm.conversationsList.splice(index, 1);
                Notification.success({ message: 'Conversation deleted.', title: 'Success'});                
            })
            .catch(function(){
              Notification.error({ message: 'Failed to process request.', title: 'Error' });
            });
          }
        });
      }


    }
})();