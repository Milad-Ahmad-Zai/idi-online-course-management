(function () {
  'use strict';

  angular
    .module('ctrl.mail2.manager.messages', [])
    .controller('MessagesManagerMail2Ctrl', MessagesManagerMail2Ctrl);
    function MessagesManagerMail2Ctrl($scope, $rootScope, NgTableParams, $state, $stateParams, $modal, Notification, Message2Service, ProfilesService, AuthService) {
      var vm = this;
      vm.filter = {};
      var conversation_id = parseInt($stateParams.id);
      vm.subscription = {};
      var usersConfig = {
        id: conversation_id,
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id',
        page: 1
      };

      init();

      function init(){
        populateConversationsList();
      }

      function getConversationsList (config) {
        return Message2Service.getConversationById(config);
      };

      function getData (config, $defer, params) {
        config.limit = params.count();
        config.start = ((params.page() || 1) - 1) * params.count();
        config.page = params.page();

        usersConfig.sort = Object.keys(params.sorting())[0];
        if (params.sorting()[usersConfig.sort] == 'desc') {
          usersConfig.sort = '-' + usersConfig.sort;
        }

        return getConversationsList(config).then(function (res) {
          vm.conversations = res.data;

          angular.forEach(vm.conversations.users, function(value, key){
            var senderId = value.user_id;

            ProfilesService.getProfile(senderId, {cache: true}).then(function(res){
              vm.conversations.users[key].userData = res.data;

              //last_message user data
              if(senderId === vm.conversations.last_message.sender){
                vm.conversations.last_message.senderData = res.data;
              }

              angular.forEach(vm.conversations.messages.data, function(value, key){
                if(value.sender === senderId){
                  vm.conversations.messages.data[key].senderData = res.data;
                }
              })

            });
          });

          params.total(parseInt(res.data.messages.pagination.total_items));
          vm.conversationsTable.total(parseInt(res.data.messages.pagination.total_items));
          return res.data;
        })
      }

      function populateConversationsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        vm.conversationsTable = new NgTableParams({
          page: usersConfig.page,
          count: usersConfig.limit,
          sorting: sort
        }, {
          getData: getData.bind(null, usersConfig)
        });
      };

      vm.populateConversationsList = populateConversationsList;   

      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/messages2/manager/users.modal.html', show: false});
      vm.showAddNewModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };

      vm.loadUsers = function(str){
        Message2Service.getUsersList(str).then(function(response){
          if(response.error === false){
            vm.users = response.data.list;
          }
        });
      }
      
      vm.sortConversation = function (e) {
        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            usersConfig.sort = "id";
          } else {
            usersConfig.sort = "-id";
          }
        }

        usersConfig.start = 0;
        vm.populateConversationsList();
      };

      vm.subscribe = function(){
        if(vm.subscription.users_id){
          var data = {};
          data.users_id = [];
          data.users_id.push(parseInt(vm.subscription.users_id));
          Message2Service.subscribeUser(data, conversation_id).then(function(response){
            addNewModal.hide();
            vm.populateConversationsList();
            Notification.success({ message: 'User subscription successful.', title: 'Success'});
          })
          .catch(function(){
            Notification.success({ message: 'There was an error processing your request.', title: 'Error'});
          });
        }
      }

      vm.unsubscribe = function(id){
        var data = {};
        data.users_id = [];
        data.users_id.push(parseInt(id));
        Message2Service.unsubscribeUser(data, conversation_id).then(function(response){
          addNewModal.hide();
          vm.populateConversationsList();
          Notification.success({ message: 'User subscription successful.', title: 'Success'});
        })
        .catch(function(){
          Notification.success({ message: 'There was an error processing your request.', title: 'Error'});
        });
      }

    }
})();