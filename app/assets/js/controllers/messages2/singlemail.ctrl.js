(function () {
  'use strict';

  angular
    .module('ctrl.mail2.single', ['cfp.loadingBar', 'angularFileUpload'])
    .controller('SingleMail2Ctrl', SingleMail2Ctrl);

    function SingleMail2Ctrl($scope, $state, $stateParams, $rootScope, $interval, $timeout, $sce, $window, $modal, $filter, cfpLoadingBar, Notification, user, Message2Service, FilesService, FileUploader, AuthService, ProfilesService) {
      var vm = this;

      vm.files = [];
      vm.tempFileIds = [];
      vm.userGroupList = [];
      vm.userList = [];
      vm.reply = {};
      vm.draft = false;
      vm.recipients = {
        to: []
      };
      var scopesConfig = {
        start: 0,
        limit: 100
      };
      var usersConfig = {
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id'
      };

      var uploader = vm.uploader = FilesService.getUploader('message', 'message', true);
      init();

      function init(){
        $scope.$parent.category = "New Message";
        

        if($stateParams.draft){
          vm.reply.subject = $stateParams.draft.topic;
          vm.reply.message = $stateParams.draft.body;
          vm.reply.messageScope = $stateParams.draft.scope;
          
          //Files
          vm.tempFileIds = $stateParams.draft.files;
          if($stateParams.draft.recipients.length){
            angular.forEach($stateParams.draft.filesData, function(item){
              vm.files[item.id] = item;
              vm.uploader.queue.push({
                  id: item.id,
                  isUploaded: true,
                  isUploading: false,
                  isError: false,
                  draft: true,
                  file: {name: item.title},
                  progress: 100
                });
            });
          }
          
          //Recipients
          if($stateParams.draft.recipients.length){
            ProfilesService.getBulkProfiles($stateParams.draft.recipients).then(function(res){
              angular.forEach(res.data, function(item){
                vm.userGroupList.push({
                  id: item.user.id,
                  name: item.user.name,
                  type: 'user'
                });
              })
              vm.recipients.to = vm.userGroupList;
            })
          }
        }

        Message2Service.getScopesList(scopesConfig).then(function (res) {
          vm.newMessageScope = res.data;
        });
        
      }

      vm.refreshUsers = function (userText) {
        usersConfig.filter = 'name==' + userText;
        usersConfig.sort = 'name';
        vm.updateUserList();
      };

      vm.updateUserList = function () {
        AuthService.getUsersList(usersConfig).then(function (res) {
          var userList = res.data.list;
          userList.forEach(function (user) {
            ProfilesService.getBasicProfile(user.id).then(function (userRes) {
              if (!userRes.error && userRes.data) {
                if (userRes.data.first_name || userRes.data.last_name) {
                  userRes.data.first_name = userRes.data.first_name || '';
                  userRes.data.last_name = userRes.data.last_name || '';
                  var isUserGroupExist = false;
                  vm.userGroupList.forEach(function (userGroup) {
                    if (userGroup.id == userRes.data.id) {
                      isUserGroupExist = true;
                    }
                  });

                  if (!isUserGroupExist) {
                    vm.userGroupList.push({
                      id: userRes.data.id,
                      name: userRes.data.first_name + ' ' + userRes.data.last_name,
                      type: 'user'
                    });
                  }

                  var isUserExist = false;
                  vm.userList.forEach(function (user) {
                    if (user.id == userRes.data.id) {
                      isUserExist = true;
                    }
                  });

                  if (!isUserExist) {
                    vm.userList.push({
                      id: userRes.data.id,
                      name: userRes.data.first_name + ' ' + userRes.data.last_name
                    });
                  }
                }
              }
            });
          });
        });
      };

      vm.createMessage = function (form) {
        if (form.$invalid) {
          return;
        }

        if (vm.recipients.to.length == 0) {
          return;
        }

        cfpLoadingBar.start();
        vm.createFormSubmitted = true;

        var toArray = vm.recipients.to.map(function(item) {
          return parseInt(item.id)
        })
        
        // send data to api
        var data = {
          'topic': vm.reply.subject,
          'body': vm.reply.message,
          'recipients': toArray,
          'files_id': vm.tempFileIds
        };

        var successMessage;
        vm.draft ? successMessage = 'Your message has been saved.' : successMessage = 'Your message has been successfully sent.';
        var service = null;
        var draftId = null;
        if(vm.draft){
          if($stateParams.draft){ 
            draftId = $stateParams.draft.id;
            service = Message2Service.updateDraft;
          }else{
            data.scope_id = vm.reply.messageScope;
            service = Message2Service.saveDraft;
          }
        }else{
          service = Message2Service.sendMessage;
        }

        service(data, draftId).then(function (res) {
          
          // reset reply inputs
          form.$setPristine();
          vm.reply = {};
          vm.recipients = {};
          
          $state.go('app.admin.mail2.inbox');

          Notification.success({
            message: successMessage,
            title: "Success"
          });
          
          cfpLoadingBar.complete();
          vm.createFormSubmitted = false;
        })
        .catch(function(){
          Notification.error({
            message: "There's an error processing your request.",
            title: "Error"
          });
        });
      };

      // add attachments
      uploader.onCompleteItem = function(fileItem, response, status, headers) {
        if (!response.error || status == 200) {
          console.log(response.data);
          vm.tempFileIds.push(parseInt(response.data.id));
          vm.files[response.data.id] = response.data;
        } else {
          fileItem.msg = response.msg;
        }
      };

      uploader.onBeforeUploadItem = function(item) {
        item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
      };

      FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
        return false; // true|false
      };

      vm.removeFileId = function (item) {
        var index = uploader.queue.indexOf(item);
        if(item.draft == true) uploader.queue.splice(index, 1);;
        var fileId = vm.tempFileIds[index];
        if (fileId) {          
          vm.tempFileIds.splice(index, 1);
          FilesService.deleteFile(fileId).then(function(res) {});
        }
      };

    }
})();