(function () {
  'use strict';

  angular
    .module('ctrl.mail2.read', [])
    .controller('ReadMail2Ctrl', ReadMail2Ctrl);
    function ReadMail2Ctrl($scope, $rootScope, $stateParams, NgTableParams, cfpLoadingBar, Notification, $modal, FilesService, FileUploader, Message2Service, ProfilesService) {
      var vm = this;

      vm.conversations = {};
      var topic = "";
      vm.files = [];
      vm.tempFileIds = [];
      vm.conversation_id = parseInt($stateParams.id);
      vm.reply = {};
      var usersConfig = {
        id: vm.conversation_id,
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id',
        page: 1
      };

      init();

      function init(){
        $scope.$parent.category = "Conversation: " + topic;
        if($stateParams.readed == false){
          Message2Service.markConversationReaded(vm.conversation_id);
        }
        populateConversationsList();
      }

      function getConversationsList (config) {
        return Message2Service.getUserConversation(config);
      };

      function populateConversationsList() {

        var sort = {};
        var splittedSort;

        if (usersConfig.sort) {
          if (usersConfig.sort.indexOf('-') > -1) {
            splittedSort = usersConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[usersConfig.sort] = 'asc';
          }
        } else {
          sort = {
            id: 'asc'
          }
        }

        vm.conversationsTable = new NgTableParams({
          page: usersConfig.page || 1,            // show first page
          count: usersConfig.limit,          // count per page
          sorting: sort
        }, {
          getData: function($defer, params) {
            usersConfig.limit = params.count();
            usersConfig.start = (params.page() - 1) * params.count();

            usersConfig.sort = Object.keys(params.sorting())[0];
            if (params.sorting()[usersConfig.sort] == 'desc') {
              usersConfig.sort = '-' + usersConfig.sort;
            }

            return getConversationsList(usersConfig).then(function (res) {
              vm.conversations = res.data;
              if(!topic) {
                topic = vm.conversations.topic || "";
                $scope.$parent.category = "Conversation: " + topic;
              }
              console.log(vm.conversations);

              //load attachments
              angular.forEach(vm.conversations.messages.data, function(msg){
                angular.forEach(msg.files, function(file){
                  vm.files[file.id] = file;
                });
              });

              //load users profile
              angular.forEach(vm.conversations.users, function(value, key){
                var senderId = value.user_id;

                ProfilesService.getProfile(senderId, {cache: true}).then(function(res){
                  vm.conversations.users[key].userData = res.data;

                  //last_message user data
                  if(senderId === vm.conversations.last_message.sender){
                    vm.conversations.last_message.senderData = res.data;
                  }

                  angular.forEach(vm.conversations.messages.data, function(value, key){
                    if(value.sender === senderId){
                      vm.conversations.messages.data[key].senderData = res.data;
                    }
                  })

                });
              });
              
              params.total(parseInt(res.data.messages.pagination.total_items));
              vm.conversationsTable.total(parseInt(res.data.messages.pagination.total_items));
              return res.data;
            });
          }
        });
      };

      vm.populateConversationsList = populateConversationsList;

      // submit the reply message form
      vm.replyFormSubmitted = false;
      vm.addReplyMessage = function(form) {
        if (form.$invalid) {
          return;
        }

        cfpLoadingBar.start();
        vm.replyFormSubmitted = true;

        // send data to api
        var data = {
          'conversation_id': vm.conversation_id,
          'body': vm.reply.message + '',
          'files_id': vm.tempFileIds
        };

        if (vm.conversation_id) {
          vm.replyMessage(data, form);
        } 
      };

      vm.replyMessage = function (data, form) {
        Message2Service.sendMessage(data).then(function (res) {
          
            populateConversationsList();
            

            // send notification
            /*var notificationData = {
              conversation_id: vm.conversation_id,
              user_id: $scope.$parent.userId,
              msg_text: vm.reply.message + ''
            };
            vm.sendNotification(notificationData);*/

            // reset reply inputs
            form.$setPristine();
            vm.reply.message = '';
            

            Notification.success({
              message: 'Your message has been successfully sent.',
              title: "Success"
            });

          cfpLoadingBar.complete();
          vm.replyFormSubmitted = false;
        })
        .catch(function(){
          Notification.error({
            message: "There's an error processing your request.",
            title: "Error"
          });
        });
      }

      vm.isImage = function (ext) {
        ext = '|' + ext + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(ext) !== -1;
      };

      vm.displayImage = function (item) {
        $scope.imageItem = {file: item};
        
        vm.displayImageModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/image.modal.html',
          show: false
        });

        vm.displayImageModal.$promise.then(vm.displayImageModal.show);
      };

      // add attachments
      var uploader = vm.uploader = FilesService.getUploader('message', 'message2', true);

      uploader.onCompleteItem = function(fileItem, response, status, headers) {
        if (!response.error || status == 200) {
          vm.tempFileIds.push(parseInt(response.data.id));
          vm.files[response.data.id] = response.data;
        } else {
          fileItem.msg = response.msg;
        }
      };

      uploader.onBeforeUploadItem = function(item) {
        item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
      };

      FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
        return false; // true|false
      };

      vm.removeFileId = function (item) {
        var index = uploader.queue.indexOf(item);
        var fileId = vm.tempFileIds[index];
        if (fileId) {
          vm.tempFileIds.splice(index, 1);
          FilesService.deleteFile(fileId).then(function(res) {});
        }
      };

      vm.deleteMessage = function(message){
        Message2Service.deleteMessage(message.id).then(function(response){
          populateConversationsList();
          Notification.success({ message: 'Message deleted.', title: 'Success'});                
        })
        .catch(function(){
          Notification.error({ message: 'Failed to delete message.', title: 'Error' });
        });
      }

    }
})();