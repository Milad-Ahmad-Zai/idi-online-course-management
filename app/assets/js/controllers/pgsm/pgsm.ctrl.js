(function () {
  'use strict';

  angular
    .module('ctrl.pgsm', [])
    .controller('PgsmCtrl', PgsmCtrl);

    function PgsmCtrl ($scope, PgsmService, $modal) {
      var vm = this;

      vm.page = {};
      vm.page.tags = [];
      vm.isSaving = false;

      PgsmService.getPagesList().then(function(response){
        vm.pages = response;
      });

      var addNewModal = $modal({scope: $scope, templateUrl: '../../../partials/pgsm/pgsm_modal.html', show: false});
      vm.showModal = function() {
        addNewModal.$promise.then(addNewModal.show);
      };

      vm.setPage = function(page){
        vm.page = page;
        vm.showModal();
      }

      vm.savePage = function(){
          vm.isSaving = true;
          var array = vm.page.tags.toString().split(",");
          vm.page.tags = array;

          PgsmService.savePage(vm.page)
          .then(function (response) {
            vm.isSaving = false
            addNewModal.hide();
          })
          .then(PgsmService.getPagesList)
          .then(function (response) {
            vm.pages = response
          });
      }

      vm.deletePage = function(id){
        if (confirm("Are you sure?")) {

          PgsmService.deletePage(id)
          .then(PgsmService.getPagesList)
          .then(function (response) {
            vm.pages = response;
          });

        }
      }

      vm.addNewPage = function(parentId){
        vm.page = {};
        vm.page.parent_id = parentId;
        vm.page.tags = [];
        vm.showModal();
      }

      vm.addNewParent = function(){
        vm.page = {};
        vm.page.tags = [];
        vm.showModal();
      }

    }
})();
