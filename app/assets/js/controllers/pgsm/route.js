(function () {
  'use strict';

  angular
    .module('ui.routes')
    .config(config)

    function config($stateProvider) {
      $stateProvider
        .state('app.admin.pages', {
          url: '/pages/management',
          views: {
            'content@app': {
              controller: 'PgsmCtrl',
              templateUrl: '/partials/pgsm/index.html'
            }
          },
          resolve: {
            load: function ($ocLazyLoad) {
              return $ocLazyLoad.load([
                {name: 'ctrl.pgsm', files: ['/assets/js/controllers/pgsm/pgsm.ctrl.js']},
                {name: 'pgsm.service', files: ['/assets/js/services/pgsm.service.js']},
                {name: 'idi.pgsm.tree', files: ['/assets/js/directives/pgsm/idi.pgsm.tree.js']}
              ]);
            }
          },
          data: {
            permissions: {
              only: ['pages.page.get', 'pages.page.create', 'pages.page.update'],
              redirectTo: 'app.login'
            }
          }
        });
    }

})();
