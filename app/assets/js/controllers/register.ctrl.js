(function () {
  'use strict';

  angular
    .module('register.ctrl', [])
    .controller('RegisterCtrl', RegisterCtrl);

function RegisterCtrl($state, $rootScope, CONFIG, AuthService, Notification, AdmissionsService) {
    var vm = this;
    vm.registerData = {};

    /*vm.loadCountries = function () {
      return AdmissionsService.getCountries();
    };*/

    AuthService.getCountryLocal().then(function (countries) {
      vm.countries = countries;
    });

    vm.disableButton = false;

    vm.register = function () {
      vm.disableButton = true;
      AuthService.generateRegistrationKey({ client_id: CONFIG.client.client_id })
      .then(function (responseWithKey) {
        var params = {
          name: vm.registerData.firstName + ' ' + vm.registerData.lastName,
          username: vm.registerData.username,
          password: vm.registerData.password,
          email: vm.registerData.email,
          mobile: vm.registerData.mobile,
          key: responseWithKey.data,
          client_id: CONFIG.client.client_id,
          country: vm.registerData.country,
          preferred_programme: vm.registerData.preferred_programme,
          preferred_start_date: vm.registerData.preferred_start_date,
          preferred_mode: vm.registerData.preferred_mode
        };

        return params;
      })
      .then(AuthService.registerUser)
      .then(function (res) {
        vm.disableButton = false;
        Notification.success({
          message: 'User created',
          title: 'Success'
        });
        $state.go('app.login', { username: vm.registerData.username, password: vm.registerData.password });
      })
      .catch(function (res) {
        vm.disableButton = false;
        Notification.error({
          message: res.data.msg,
          title: 'Error'
        });
      })
    }
  }
})();