(function () {
  'use strict';

  angular
  .module('ctrl.galleries.albums.list', [])

  .controller('AlbumsListCtrl', ['$scope', '$state', 'GalleriesService', 'FilesService', Controller])

  function Controller ($scope, $state, GalleriesService, FilesService) {
    var albumsConfig = {
      id: $state.params.galleryId,
      start: 0,
      limit: 999,
      page: 1,
      sort: 'id'
    };

    function getAlbums() {
      GalleriesService.getGalleryAlbums(albumsConfig)
      // getting list of albums
      .then(function (res) {
        return res.data.list;
      })
      // getting images for albums
      .then(function (albums) {
        var imagesIds = albums
          .filter(function (album) { return !!album.thumb_file_id; })
          .map(function (album) { return album.thumb_file_id; });

        // loading images for album covers
        return FilesService.getBulkFiles(imagesIds).then(function (res) {
          var images = res.data.reduce(function (prev, current) {
            prev[current.id] = current;
            return prev;
          }, {});

          // assigning image files to albums
          albums.forEach(function (album) {
            if (album.thumb_file_id) {
              album.thumb_file = images[album.thumb_file_id];
            }
          })

          return albums;
        })
      })
      // assigning albums to scope
      .then(function (albums) {
        $scope.albums = albums;
      })
    }

    getAlbums();
  }
})();