(function () {
  'use strict';

  angular
  .module('ctrl.galleries.list', [])
  .controller('GalleriesListCtrl', ['$scope', '$state', '$filter', 'NgTableParams', 'GalleriesService', 'cfpLoadingBar', Controller])

  function Controller ($scope, $state, $filter, NgTableParams, GalleriesService, cfpLoadingBar) {
    var listLimit = 25;

    var commonConfig = {
      id: $state.params.studioId,
      start: 0,
      limit: listLimit,
      page: 1
    };
    
    var stickyConfig = angular.extend({}, commonConfig, {
      filter: 'sticky=1/learn_path=0'
    });
    var learnPathConfig = angular.extend({}, commonConfig, {
      filter: 'sticky=1/learn_path=0'
    });
    var normalConfig = angular.extend({}, commonConfig, {
      filter: 'sticky!=1/sticky!=1'
    });


    function getStudioName () {
      GalleriesService.getStudio(commonConfig.id).then(function (res) {
        $scope.studio = res.data;
      })
    }

    function getGalleriesList(config) {
      return GalleriesService.getStudioGalleries(config);
    }

    /**
     * Getting data for NgTableParams
     * @param  {Object} config - config for API call,
     *                           should be binded (see @example)
     * @example
     * new NgTableParams(tableConfig, {
     *   getData: getData.bind(null, config)
     * })
     */
    function getData (config, $defer, params) {
      cfpLoadingBar.start();
      config.limit = params.count();
      config.start = ((params.page() || 1) - 1) * params.count();
      config.page = params.page();

      return getGalleriesList(config).then(function (res) {
        params.total(parseInt(res.data.pagination.totalItems));
        var list = $filter('orderBy')(res.data.list, 'ordering');

        cfpLoadingBar.complete();

        return list;
      })
    }

    (function init () {
      getStudioName();

      $scope.stickyTable = new NgTableParams({
        page: stickyConfig.page,
        count: stickyConfig.limit,
        sorting: {id: 'asc'}
      }, {
        getData: getData.bind(null, stickyConfig)
      });

      $scope.learnPathTable = new NgTableParams({
        page: learnPathConfig.page,
        count: learnPathConfig.limit,
        sorting: {id: 'asc'}
      }, {
        getData: getData.bind(null, learnPathConfig)
      });

      $scope.normalTable = new NgTableParams({
        page: normalConfig.page,
        count: normalConfig.limit,
        sorting: {id: 'asc'}
      }, {
        getData: getData.bind(null, normalConfig)
      })
    })()

  }
})()