(function () {
  'use strict';

  angular
    .module('ctrl.studios.list', [])
    .controller('StudiosListCtrl', Controller)


  function Controller ($scope, GalleriesService) {
    var galleriesConfig = {
      start: 0,
      limit: 999,
      page: 1,
      filter: '',
      sort: 'id'
    }


    function getStudios () {
      GalleriesService.getStudiosList(galleriesConfig).then(function (res) {
        $scope.studios = res.data.list;
      })
    }

    getStudios();
  }
})()