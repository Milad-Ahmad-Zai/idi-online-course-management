(function () {
  'use strict';

  angular
    .module('ctrl.forums.list', [])

    .controller('ForumsListCtrl', function ($scope, $state, $rootScope, $sce, $window, $filter, cfpLoadingBar, Notification, user, permissions, NgTableParams, AuthService, ForumsService, SemestersService) {

      var forumsConfig = {
        start: 0,
        limit: 999,
        page: 1,
        filter: 'state=1/subforums=1',
        sort: 'id'
      };
        
    $scope.treeOptions = {
      dragStop: function(event) {
        var forumReq = [];
          
        $scope.userForumsList.forEach(function (item, key) {
            var current = {
                id: item.id,
                ordering: key
            };
            forumReq.push(current);
        });
          
        ForumsService.postForumsOrder(forumReq);
          
       }
    };
      
      $scope.getUserForums = function () {
        $scope.userForumsList = [];
        cfpLoadingBar.start();
        ForumsService.getUserForums(forumsConfig).then(function (res) {
          if (res) {
            $scope.userForums = res.data.list;
            $scope.userForumsList = $filter('orderBy')($scope.userForums, 'ordering');  
            $scope.userForumsList.forEach(function (item) {
              if (item.created !== '0000-00-00 00:00:00') {
                item.created_date = moment.utc(item.created).toDate();
              }
              item.state = parseInt(item.state);
            });
            cfpLoadingBar.complete();
          }
        });
      };
      
      $scope.getForums = function () {
        return ForumsService.getUserForums(forumsConfig);
      };
      
      $scope.updateForumsData = function (items) {
        items.forEach(function (item) {
          if (item.created !== '0000-00-00 00:00:00') {
            item.created_date = moment.utc(item.created).toDate();
          }
          
          item.state = parseInt(item.state);
        });
        
        $scope.$parent.forumsList = items;
        
        return items;
      };
      
      $scope.populateForumsData = function () {
        $scope.forumsList = new NgTableParams({
          page: forumsConfig.page,
          count: forumsConfig.limit,
          sorting: {id: 'asc'}
        }, {
          getData: function ($defer, params) {
            forumsConfig.limit = params.count();
            forumsConfig.start = ((params.page() || 1) - 1) * params.count();
            forumsConfig.page = params.page();
            
            cfpLoadingBar.start();
            
            return $scope.getForums().then(function (res) {
              params.total(parseInt(res.data.pagination.totalItems));
              $scope.forumsList.total(parseInt(res.data.pagination.totalItems));
              
              cfpLoadingBar.complete();
              
              return $scope.updateForumsData(res.data.list);
            });
          }
        });
      };
      
      var semesterConfig = {
        start: 0,
        limit: 9999,
        sort: 'id'
      };
      
      $scope.getSemesterList = function () {
        if ($rootScope.semesterList.length > 0) {
          if ($rootScope.filter.semester) forumsConfig.filter = 'state=1/subforums=1/semester_id=' + $rootScope.filter.semester.id;
          $scope.getUserForums();
        } else {
          SemestersService.getSemestersList(semesterConfig).then(function (res) {
            if (res) {
              $rootScope.semesterList = res.data.list;
              if (!$rootScope.filter.semester) {
                $rootScope.filter.semester = $rootScope.semesterList[0];
              }
              forumsConfig.filter = 'state=1/subforums=1/semester_id=' + $rootScope.filter.semester.id;
              $scope.getUserForums();
            }
          });
        }
      };
      
      $scope.filterForumsByEnter = function (e) {
        if (e.which === 13) {
          $scope.filterForums();
        }
      };
      
      $scope.filterForums = function () {
        forumsConfig = {
          start: 0,
          limit: 9999,
          sort: 'id',
          userId: user.id,
          filter: 'state=1/subforums=1/'
        };
        
        if ($scope.filter.text) forumsConfig.filter += 'title==' + $scope.filter.text + '/';
        if ($scope.filter.semester) forumsConfig.filter += 'semester_id=' + $scope.filter.semester.id + '/';
        forumsConfig.filter = forumsConfig.filter.slice(0, -1);
        $scope.getUserForums();
      };
      
      if (user.id) {
        forumsConfig.userId = user.id;
        if ($scope.checkRights('forums.editTopicState')) {
          $scope.getSemesterList();
        } else {
          $scope.getUserForums();
        }
      }
    });
})();