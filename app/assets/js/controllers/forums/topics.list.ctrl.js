(function () {
  'use strict';

  angular
    .module('ctrl.topics.list', [])

    .controller('TopicsListCtrl', ['$scope', '$state', '$rootScope', '$sce', '$window', '$modal', '$filter', 'cfpLoadingBar', 'Notification', 'NgTableParams', 'AuthService', 'ForumsService', 'FilesService', 'FileUploader', 'SemestersService', function ($scope, $state, $rootScope, $sce, $window, $modal, $filter, cfpLoadingBar, Notification, NgTableParams, AuthService, ForumsService, FilesService, FileUploader, SemestersService) {

      $scope.topic = {
        type: 'normal'
      };
      $scope.limit = 10;
      var LIMIT = 25;
      $scope.countArr = [10, 25, 50, 100];
      $scope.tempFileIds = [];
      $scope.showTable = {
        learnPathSticky: true,
        sticky: true,
        learnPath: true,
        topic: true
      };

      $rootScope.topicsList = [];

      function getTopicsList (config) {
        return ForumsService.getForumTopics(config);
      }

      $scope.getForumName = function () {
        if (!$scope.forumsList) {
          ForumsService.getForum($state.params.forumId).then(function (res) {
            if (res) {
              $rootScope.forum = res.data;
              SemestersService.getSemester($rootScope.forum.semester_id).then(function (res) {
                if (res) {
                  $rootScope.forum.semester = $rootScope.filter.semester = res.data.semester;
                  $scope.topic.semester = $rootScope.forum.semester.name;
                }
              });
            }
          });
        } else {
          $scope.forumsList.forEach(function (item) {
            if (item.id == $state.params.forumId) {
              $rootScope.forum = item;
              SemestersService.getSemester($rootScope.forum.semester_id).then(function (res) {
                if (res) {
                  $rootScope.forum.semester = $rootScope.filter.semester = res.data.semester;
                  $scope.topic.semester = $rootScope.forum.semester.name;
                }
              });
            }
          });
        }
      };

      var uploader = $scope.uploader = $scope.topicUploader = FilesService.getUploader('forumattachment', $state.params.forumId, true);

      uploader.onBeforeUploadItem = function(item) {
        item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
      };

      FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
        return false;
      };

      uploader.onCompleteItem = function(fileItem, response, status, headers) {
        if (!response.error || status == 200) {
          $scope.tempFileIds.push(response.data.id);
        } else {
          fileItem.msg = response.msg;
        }
      };

      $scope.removeFileId = function (item) {
        var index = uploader.queue.indexOf(item);
        var fileId = $scope.tempFileIds[index];
        if (fileId) {
          $scope.tempFileIds.splice(index, 1);
          FilesService.deleteFile(fileId).then(function(res) {});
        }
      };

      $scope.addTopic = function () {
        $scope.addTopicModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/topic.modal.html',
          show: false
        });

        $scope.addTopicModal.$promise.then($scope.addTopicModal.show);
      };

      $scope.saveTopic = function () {
        cfpLoadingBar.start();
        $scope.topic.forum_id = $rootScope.forum.id;

        $scope.topic.sticky = $scope.topic.type == 'sticky' ? 1 : 0;
        $scope.topic.learn_path = $scope.topic.type == 'learn_path' ? 1 : 0;
        $scope.topic.locked = $scope.topic.locked ? 1 : 0;

        delete $scope.topic.type;

        $scope.topic.created_by = $rootScope.userId;
        $scope.topic.created_at = new Date();

        // save topic
        ForumsService.updateSaveTopic($scope.topic)
        .then(function (res) {
          if (res) {
            // save message
            var postDate = new Date();
            var post = {
              topic_id: res.data.id,
              subject: res.data.subject,
              text: $scope.topic.message,
              created_at: postDate,
              updated_at: postDate,
              created_by: $rootScope.userId
            };
            ForumsService.updateSavePost(post).then(function (resPost) {
              populateTables(false, $scope.topic.sticky, $scope.topic.learn_path);
              $scope.addTopicModal.$promise.then($scope.addTopicModal.hide);
              $scope.topic = {};

              Notification.success({
                title: 'Success',
                message: 'Topic successfully created.'
              });

              if ($scope.tempFileIds.length > 0) {
                $scope.tempFileIds.forEach(function (item, index) {
                  var data = {
                    file_id: item,
                    entity_id: resPost.data.id,
                    post_id: resPost.data.id,
                    type: 'post'
                  };
                  ForumsService.attachFileToPostTopic(data).then(function (attachRes) {
                    if (index == ($scope.tempFileIds.length - 1)) {
                      $scope.tempFileIds = [];
                      uploader.clearQueue();
                    }
                  });
                });
              }
            });
          } else {
            $scope.addTopicModal.$promise.then($scope.addTopicModal.hide);
            cfpLoadingBar.complete();
            Notification.error({
              title: 'Error',
              message: 'An error occured while processing your request.'
            });
          }
        })
        .then(populateTables)
      };

      $scope.getNumber = function (num) {
        return new Array(num);
      };

      function populateTables () {
        var params = {
          start: LIMIT * ($state.params.page - 1) || 0,
          limit: LIMIT,
          page: $state.params.page || 1,
          id: $state.params.forumId,
          sort: '-last_replied_date'
        }

        getTopicsList(params).then(function (res) {
          if (!res.data || !res.data.list) {
            $scope.topics = [];
            return;
          }

          res.data.list.map(function (item) {
            item.pages = Math.ceil(item.posts_count / 10);
          })

          $scope.topics = res.data.list;
          $scope.pagination = {
            limit: res.data.pagination.limit,
            total: res.data.pagination.totalItems,
            page: $state.params.page || 1
          };
        })

      };


      populateTables();
      $scope.getForumName();
    }]);
})();