(function () {
  'use strict';

  angular
    .module('ctrl.forums', [])

    .controller('ForumsCtrl', ['$scope', '$state', '$rootScope', 'AuthService', 'ForumsService', function ($scope, $state, $rootScope, AuthService, ForumsService) {

      $rootScope.userList = [];
      $rootScope.semesterList = [];
      $rootScope.filter = {};
      $rootScope.managerFilter = {};
      
      $rootScope.managerForumsConfig = {
        start: 0,
        limit: 10,
        page: 1,
        filter: '',
        sort: {
          id: 'asc'
        }
      };
      
      $scope.updateTitle = function () {
        if ($state.current.name.indexOf('manager') > -1) {
          $scope.title = 'Forums Manager';
        } else {
          $scope.title = 'Forums';
        }
      }
      
      $scope.updateTitle();
      
      $rootScope.$on('$stateChangeSuccess', function (event, data) {
        $scope.updateTitle();
      });
      
    }]);
})();