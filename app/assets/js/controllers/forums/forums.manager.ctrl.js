(function () {
  'use strict';

  angular
    .module('ctrl.forums.manager', [])

    .controller('ForumsManagerCtrl', ['$scope', '$state', '$rootScope', '$sce', '$window', '$modal', 'cfpLoadingBar', 'Notification', 'NgTableParams', 'AuthService', 'ForumsService', 'ProfilesService', 'SemestersService', function ($scope, $state, $rootScope, $sce, $window, $modal, cfpLoadingBar, Notification, NgTableParams, AuthService, ForumsService, ProfilesService, SemestersService) {

      /*var forumsConfig = {
        start: 0,
        limit: 10,
        page: 1,
        filter: '',
        sort: {
          id: 'asc'
        }
      };*/

      var forumUsersConfig = {
        start: 0,
        limit: 10,
        page: 1
      };

      var forumHiddenUsersConfig = {
        start: 0,
        limit: 10,
        page: 1
      };

      var forumBannedUsersConfig = {
        start: 0,
        limit: 10,
        page: 1
      };

      var semesterConfig = {
        start: 0,
        limit: 9999,
        sort: 'id'
      };

      function convertSortFromTable () {
        var data = {};
        if (!_.isObject($rootScope.managerForumsConfig.sort)) {
          return $rootScope.managerForumsConfig.sort;
        }
        if ($rootScope.managerForumsConfig.sort) {
          data.sort = Object.keys($rootScope.managerForumsConfig.sort)[0];
          if ($rootScope.managerForumsConfig.sort[Object.keys($rootScope.managerForumsConfig.sort)[0]] == 'desc') {
            data.sort = '-' + data.sort;
          }
        }
        if (!data.sort) {
          data.sort = ''
        }
        return data.sort;
      }

      $scope.getForums = function () {
        return ForumsService.getForumsList($rootScope.managerForumsConfig);
      };

      $scope.updateData = function (items) {
        items.forEach(function (item) {
          if (item.created_at) {
            item.created_date = moment.utc(item.created_at).toDate();
          }

          if (item.semester_id) {
            SemestersService.getSemester(item.semester_id).then(function (res) {
              if (res) {
                item.semester = res.data.semester;
              }
            });
          }

          item.state = parseInt(item.state);
        });
        return items;
      };

      $scope.populateData = function () {
        var sort = {};
        var splittedSort;

        if ($rootScope.managerForumsConfig.sort && !_.isObject($rootScope.managerForumsConfig.sort)) {
          if ($rootScope.managerForumsConfig.sort.indexOf('-') > -1) {
            splittedSort = $rootScope.managerForumsConfig.sort.split('-')[1];
            sort[splittedSort] = 'desc';
          } else {
            sort[$rootScope.managerForumsConfig.sort] = 'asc';
          }
        } else if ($rootScope.managerForumsConfig.sort && _.isObject($rootScope.managerForumsConfig.sort)) {
          $rootScope.managerForumsConfig.sort = convertSortFromTable();
          sort = $rootScope.managerForumsConfig.sort;
        } else {
          sort = {
            id: 'asc'
          }
        }

        $scope.forumsList = new NgTableParams({
          page: $rootScope.managerForumsConfig.page,
          count: $rootScope.managerForumsConfig.limit,
          sorting: sort
        }, {
          getData: function ($defer, params) {
            $rootScope.managerForumsConfig.limit = params.count();
            $rootScope.managerForumsConfig.start = ((params.page() || 1) - 1) * params.count();
            $rootScope.managerForumsConfig.page = params.page();
            if (params.sorting() && !_.isEmpty(params.sorting())) {
              $rootScope.managerForumsConfig.sort = params.sorting();
              $rootScope.managerForumsConfig.sort = convertSortFromTable();
            } else {
              delete $rootScope.managerForumsConfig.sort;
            }

            cfpLoadingBar.start();

            return $scope.getForums().then(function (res) {
              params.total(parseInt(res.data.pagination.totalItems));
              $scope.forumsList.total(parseInt(res.data.pagination.totalItems));

              cfpLoadingBar.complete();

              return $scope.updateData(res.data.list);
            });
          }
        });
      };

      $scope.filterForumsByEnter = function (e) {
        if (e.which === 13) {
          $scope.filterForums();
        }
      };

      $scope.filterForums = function () {
        var sort = $rootScope.managerForumsConfig.sort;
        $rootScope.managerForumsConfig = {
          start: 0,
          limit: 10,
          page: 1,
          sort: sort,
          filter: ''
        };

        if ($scope.managerFilter.text) $rootScope.managerForumsConfig.filter += 'title==' + $scope.managerFilter.text + '/';
        if ($scope.managerFilter.scope) $rootScope.managerForumsConfig.filter += 'scope==' + $scope.managerFilter.scope + '/';
        if ($scope.managerFilter.state) $rootScope.managerForumsConfig.filter += 'state==' + $scope.managerFilter.state + '/';
        if ($scope.managerFilter.semester) $rootScope.managerForumsConfig.filter += 'semester_id=' + $scope.managerFilter.semester.id + '/';

        $rootScope.managerForumsConfig.filter = $rootScope.managerForumsConfig.filter.slice(0, -1);

        $scope.populateData();
      };

      $scope.publishItem = function (item) {
        if (!item.state) {
          ForumsService.publishForum(item.id).then(function (res) {
            if (res) {
              Notification.success({
                title: 'Success',
                message: '\"' + item.title + '\" published successfully.'
              });
              item.state = 1;
            }
          });
        } else {
          ForumsService.unpublishForum(item.id).then(function (res) {
            if (res) {
              Notification.success({
                title: 'Success',
                message: '\"' + item.title + '\" unpublished successfully.'
              });

              item.state = 0;
            }
          });
        }
      };

      $scope.addForum = function () {
        $scope.addForumModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/forum.modal.html',
          show: false
        });

        $scope.addForumModal.$promise.then($scope.addForumModal.show);
      };

      $scope.saveForum = function (form) {
        if (!$scope.forum.id && !$scope.forum.semester_ids) {
          $scope.semesterError = true;
          return;
        } else {
          $scope.semesterError = false;
        }

        $scope.saveForumSubmitted = true;
        var forumReq = {
          title: $scope.forum.title,
          description: $scope.forum.description
        };
        if ($scope.forum.state) {
          forumReq.state = 1;
        } else {
          forumReq.state = 0;
        }

        if (!$scope.forum.id) {
          forumReq.created_by = $rootScope.userId;
          forumReq.created_at = new Date();

          forumReq.semester_id = $scope.forum.semester_ids[0];
        }

        var forumId = $scope.forum.id ? $scope.forum.id : false;

        ForumsService.updateSaveForum(forumId, forumReq).then(function (res) {
          if (res) {
            $scope.saveForumSubmitted = false;
            $scope.isSaved = true;
            // save multiple semesters
            if (!$scope.forum.id) {
              $scope.scopeForums.push(res.data.id);
              $scope.forum.semester_ids.forEach(function (sem_id, index) {
                if (index > 0) {
                  var forum = {
                    title: $scope.forum.title,
                    description: $scope.forum.description,
                    created_by: $rootScope.userId,
                    created_at: new Date(),
                    semester_id: sem_id,
                    scope: 'custom',
                    scope_id: res.data.id
                  };
                  ForumsService.updateSaveForum(false, forum).then(function (forumRes) {
                    $scope.scopeForums.push(forumRes.data.id);
                  });
                }
              });
            }

            if ($scope.forum.state == 1) $scope.forum.state = true;

            form.$setPristine();
            form.$setUntouched();
            $scope.forum.id = res.data.id;

            Notification.success({
              title: 'Success',
              message: 'Forum successfully created.'
            });
          }
        });
      };

      $scope.deleteForum = function (id) {
        ForumsService.deleteForum(id).then(function (res) {
          $scope.populateData();
          Notification.success({
            title: 'Success',
            message: 'Forum successfully deleted.'
          });
        }).catch(function (res) {
          switch (res.status) {
            case 403:
              Notification.error({
                title: 'Error',
                message: 'You don\'t have permissions to delete forums.'
              });
              break;
            default:
              Notification.error({
                title: 'Error',
                message: 'Something went wrong.'
              });
          }
        })
      };

      $scope.editForum = function (item) {
        $rootScope.forum = item;
        if ($rootScope.forum.state == 1) $rootScope.forum.state = true;

        $state.go('app.admin.forums.manager.edit', {id: $rootScope.forum.id});
      };

      var usersConfig = {
        start: 0,
        limit: 20,
        filter: '',
        sort: 'id'
      };

      $scope.refreshUsers = function (userText) {
        usersConfig.filter = 'name==' + userText;
        usersConfig.sort = 'name';
        $scope.updateUserList();
      };

      $scope.updateUserList = function () {
        AuthService.getUsersList(usersConfig).then(function (res) {
          var userList = res.data.list;
          userList.forEach(function (user) {
            ProfilesService.getBasicProfile(user.id).then(function (userRes) {
              if (userRes && userRes.data) {
                if (userRes.data.first_name && userRes.data.last_name) {
                  var isUserExist = false;
                  $scope.userList.forEach(function (user) {
                    if (user.id == userRes.data.id) {
                      isUserExist = true;
                    }
                  });

                  if (!isUserExist) {
                    $scope.userList.push({
                      id: userRes.data.id,
                      name: userRes.data.first_name + ' ' + userRes.data.last_name
                    });
                  }
                }
              }
            });
          });
        });
      };

      $scope.addUser = function (state) {
        cfpLoadingBar.start();
        var access = [];
        if (state == 1) access = $scope.forum.userAccess;
        else if (state == 2) access = $scope.forum.userBannedAccess;
        else if (state == 3) access = $scope.forum.userHiddenAccess;

        $scope.scopeForums.forEach(function (forumId, forumIndex) {
          access.forEach(function (item, index) {
            var config = {
              forumId: forumId,
              profileId: item
            };
            var data = {
              state: state,
              created_by: $rootScope.userId,
              created_at: new Date()
            };
            ForumsService.assignUserToForum(config, data).then(function (res) {
              if (index == (access.length -1) && forumIndex == ($scope.scopeForums.length - 1)) {
                $scope.forum.userAccess = [];
                $scope.forum.userBannedAccess = [];
                $scope.forum.userHiddenAccess = [];
                $scope.populateUserAccessTables();

                Notification.success({
                  title: 'Success',
                  message: 'Users added access to forum successfully.'
                });
                cfpLoadingBar.complete();
              }
            });
          });
        });
      };

      $scope.deleteForumUser = function (id) {
        cfpLoadingBar.start();
        var config = {
          forumId: $scope.forum.id,
          userId: id
        };
        ForumsService.removeUserAccessToForum(config).then(function (res) {
          if (res) {
            $scope.populateUserAccessTables();
            Notification.success({
              title: 'Success',
              message: 'Successfully removed user from accessing to forum.'
            });
            cfpLoadingBar.complete();
          }
        });
      };

      $scope.getForumUsers = function (config) {
        return ForumsService.getForumUsers(config);
      };

      $scope.updateForumUsersData = function (items) {
        return items;
      };

      $scope.populateForumUsersData = function () {
        $scope.forumUsersList = new NgTableParams({
          page: forumUsersConfig.page,
          count: forumUsersConfig.limit,
          sorting: {id: 'asc'}
        }, {
          getData: function ($defer, params) {
            forumUsersConfig.limit = params.count();
            forumUsersConfig.start = ((params.page() || 1) - 1) * params.count();
            forumUsersConfig.page = params.page();

            cfpLoadingBar.start();

            return $scope.getForumUsers(forumUsersConfig).then(function (res) {
              params.total(parseInt(res.data.pagination.totalItems));
              $scope.forumUsersList.total(parseInt(res.data.pagination.totalItems));

              cfpLoadingBar.complete();

              return $scope.updateForumUsersData(res.data.list);
            });
          }
        });
      };

      $scope.populateForumHiddenUsersData = function () {
        $scope.forumHiddenUsersList = new NgTableParams({
          page: forumHiddenUsersConfig.page,
          count: forumHiddenUsersConfig.limit,
          sorting: {id: 'asc'}
        }, {
          getData: function ($defer, params) {
            forumHiddenUsersConfig.limit = params.count();
            forumHiddenUsersConfig.start = ((params.page() || 1) - 1) * params.count();
            forumHiddenUsersConfig.page = params.page();

            cfpLoadingBar.start();

            return $scope.getForumUsers(forumHiddenUsersConfig).then(function (res) {
              params.total(parseInt(res.data.pagination.totalItems));
              $scope.forumHiddenUsersList.total(parseInt(res.data.pagination.totalItems));

              cfpLoadingBar.complete();

              return $scope.updateForumUsersData(res.data.list);
            });
          }
        });
      };

      $scope.populateForumBannedUsersData = function () {
        $scope.forumBannedUsersList = new NgTableParams({
          page: forumBannedUsersConfig.page,
          count: forumBannedUsersConfig.limit,
          sorting: {id: 'asc'}
        }, {
          getData: function ($defer, params) {
            forumBannedUsersConfig.limit = params.count();
            forumBannedUsersConfig.start = ((params.page() || 1) - 1) * params.count();
            forumBannedUsersConfig.page = params.page();

            cfpLoadingBar.start();

            return $scope.getForumUsers(forumBannedUsersConfig).then(function (res) {
              params.total(parseInt(res.data.pagination.totalItems));
              $scope.forumBannedUsersList.total(parseInt(res.data.pagination.totalItems));

              cfpLoadingBar.complete();

              return $scope.updateForumUsersData(res.data.list);
            });
          }
        });
      };

      $scope.populateUserAccessTables = function () {
        forumUsersConfig.filter = 'forum_id=' + $scope.forum.id + '/state=1';
        forumHiddenUsersConfig.filter = 'forum_id=' + $scope.forum.id + '/state=3';
        forumBannedUsersConfig.filter = 'forum_id=' + $scope.forum.id + '/state=2';

        $scope.populateForumUsersData();
        $scope.populateForumHiddenUsersData();
        $scope.populateForumBannedUsersData();
      };

      $scope.filterByUserKeyword = function (e) {
        if (e.which === 13) {
          forumUsersConfig = {
            start: 0,
            limit: 10,
            page: 1,
            filter: 'forum_id=' + $scope.forum.id + '/name='
          };

          $scope.populateForumUsersData();
        }
      };

      if ($rootScope.semesterList.length == 0) {
        SemestersService.getSemestersList(semesterConfig).then(function (res) {
          if (res) {
            $rootScope.semesterList = res.data.list;
          }
        });
      }

      if ($state.is('app.admin.forums.manager')) {
        $scope.forum = {};

        if ($scope.managerFilter.text) $rootScope.managerForumsConfig.filter += 'title==' + $scope.managerFilter.text + '/';
        if ($scope.managerFilter.scope) $rootScope.managerForumsConfig.filter += 'scope==' + $scope.managerFilter.scope + '/';
        if ($scope.managerFilter.state) $rootScope.managerForumsConfig.filter += 'state==' + $scope.managerFilter.state + '/';
        if ($scope.managerFilter.semester) $rootScope.managerForumsConfig.filter += 'semester_id=' + $scope.managerFilter.semester.id + '/';

        $rootScope.managerForumsConfig.filter = $rootScope.managerForumsConfig.filter.slice(0, -1);

        $scope.populateData();
      } else if ($state.is('app.admin.forums.manager.edit')) {
        $scope.userList = [];
        $scope.forum = {};
        $scope.isNew = true;
        $scope.scopeForums = [];
        if ($state.params.id) {
          $scope.isNew = false;
          $scope.scopeForums.push($state.params.id);
          ForumsService.getForum($state.params.id).then(function (res) {
            if (res) {
              $scope.forum = res.data;
              if ($scope.forum.state == "1") $scope.forum.state = true;
              $scope.forum.userAccess = [];

              $scope.populateUserAccessTables();
            }
          });
        }
      }

    }]);
})();