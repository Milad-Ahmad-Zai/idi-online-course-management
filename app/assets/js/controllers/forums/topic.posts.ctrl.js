(function () {
  'use strict';

  angular
    .module('ctrl.topic.posts', ['angularFileUpload'])

    .controller('TopicPostsCtrl', function ($scope, $state, $stateParams, $rootScope, $sce, $window, $modal, cfpLoadingBar, Notification, AuthService, ForumsService, FilesService, FileUploader, SemestersService, PermissionsService, $timeout) {

      $scope.fromStudio = $stateParams.fromStudio;

      $scope.tempFileIds = [];
      $scope.isDesc = true;
      $scope.reply = {};
      $scope.order = 99999999;
      var postsConfig = {
        start: 0,
        limit: 10,
        page: 1,
        sort: '-id',
        all: true,
        id: $state.params.topicId
      };

      if (PermissionsService.has('forums.deletedPosts.get')) {
        postsConfig.filter = '/deleted=IN=0,1';
      }

      if ($state.params.pageNo) {
        postsConfig.page = parseInt($state.params.pageNo);
        postsConfig.start = ((postsConfig.page || 1) - 1) * postsConfig.limit;
      }

      $scope.pagination = {};

      $scope.getTopicName = function () {
        postsConfig.id = $state.params.topicId;
        if (!$rootScope.topicsList || !$rootScope.topicsList.length) {
          ForumsService.getPostById(postsConfig.id).then(function (res) {
            if (res) {
              $scope.topic = res.data;

              $scope.topic.sticky = $scope.topic.sticky == '1' ? true : false;
              $scope.topic.learn_path = $scope.topic.learn_path == '1' ? true : false;
              $scope.topic.locked = $scope.topic.locked == '1' ? true : false;

              // get forum name
              ForumsService.getForum($scope.topic.forum_id).then(function (res) {
                if (res) {
                  $rootScope.forum = res.data;
                  SemestersService.getSemester($rootScope.forum.semester_id).then(function (res) {
                    if (res) {
                      $rootScope.forum.semester = $rootScope.filter.semester = res.data.semester;
                      $scope.topic.semester = $rootScope.forum.semester.name;
                    }
                  });
                }
              });
            }
          });
        } else {
          $rootScope.topicsList.forEach(function (item) {
            if (item.id == postsConfig.id) {
              $scope.topic = item;

              $scope.topic.sticky = $scope.topic.sticky == '1' ? true : false;
              $scope.topic.learn_path = $scope.topic.learn_path == '1' ? true : false;
              $scope.topic.semester = $rootScope.filter.semester.name;
              $scope.topic.locked = $scope.topic.locked == '1' ? true : false;

              ForumsService.getPostById($scope.topic.id).then(function (res) {
                if (res) {
                  $scope.topic.is_current_user_subscribed = res.data.is_current_user_subscribed;
                }
              });
            }
          });
        }
      };

      $scope.getPosts = function (hasMessage, msg) {
        cfpLoadingBar.start();
        ForumsService.getPostsByTopic(postsConfig).then(function (res) {
          if (res) {
            $scope.pagination = {
              currentPage: parseInt(res.data.pagination.currentPage),
              totalPages: parseInt(res.data.pagination.totalPages),
            };
            $scope.posts = res.data.list;
            $scope.posts.forEach(function (item) {
              // get reply to
              if (item.reply_to) {
                ForumsService.getPost(item.reply_to).then(function (quoteRes) {
                  if (quoteRes && !quoteRes.data.deleted_at) {
                    item.reply_to_data = quoteRes.data;
                    item.reply_to_data.textHtml = item.reply_to_data.text.replace(/\r?\n/g, '<br />');

                    if (item.reply_to_data.created_at) {
                      item.reply_to_data.created_date = moment.utc(item.reply_to_data.created_at).toDate();
                    }
                  } else {
                      AuthService.getFullUser(quoteRes.data.deleted_by).then(function (res) {
                        item.reply_to_data = { isDeleted: true, deleted_by: res.data.user.name, deleted_at: quoteRes.data.deleted_at};
                    });
                  }
                });
              }
            });

            if (hasMessage) {
              Notification.success({
                title: 'Success',
                message: msg
              });
            }

            cfpLoadingBar.complete();
          }
        });
      };

      $scope.increaseTopicHits = function (id) {
        ForumsService.increaseTopicHitsNumber(id).then(function (res) {});
      };

      $scope.displayNotification = function (isSuccess, msg) {
        if (isSuccess) {
          Notification.success({
            title: 'Success',
            message: msg
          });
        } else {
          Notification.error({
            title: 'Error',
            message: msg
          });
        }
      };

      $scope.replyPost = function (form) {
        cfpLoadingBar.start();

        var postDate = new Date();
        var replyObj = {
          topic_id: $scope.topic.id,
          reply_to: $scope.reply.replyTo,
          subject: $scope.topic.subject,
          text: $scope.reply.message,
          created_at: postDate,
          updated_at: postDate,
          created_by: $rootScope.userId
        };
        ForumsService.updateSavePost(replyObj).then(function (resPost) {
          if (resPost) {
            if (!resPost.error) {
              $scope.reply = {};
              $scope.replyTo = {};
              form.$setPristine();
              form.$setUntouched();

              // post attachments if any
              if ($scope.tempFileIds.length > 0) {
                $scope.tempFileIds.forEach(function (item, index) {
                  var data = {
                    file_id: item,
                    entity_id: resPost.data.id,
                    post_id: resPost.data.id,
                    type: 'post'
                  };
                  ForumsService.attachFileToPostTopic(data).then(function (attachRes) {
                    if (index == ($scope.tempFileIds.length - 1)) {
                      $scope.files = [];
                      $scope.tempFileIds = [];
                      uploader.clearQueue();

                      $scope.getPosts(false);
                      $scope.displayNotification(true, 'Post successfully saved.');
                    }
                  });
                });
              } else {
                $scope.getPosts(false);
                $scope.displayNotification(true, 'Post successfully saved.');
              }
            } else {
              $scope.displayNotification(true, restPost.msg);
            }
          } else {
            $scope.displayNotification(true, 'There was a problem processing your request.');
          }

          cfpLoadingBar.complete();
        });
      };

      var uploader = $scope.uploader = FilesService.getUploader('forumattachment', postsConfig.id, true);

      uploader.onBeforeUploadItem = function(item) {
        item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
      };

      FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
        return false;
      };

      uploader.onCompleteItem = function(fileItem, response, status, headers) {
        if (!response.error || status == 200) {
          $scope.tempFileIds.push(response.data.id);
        } else {
          fileItem.msg = response.msg;
        }
      };

      $scope.removeFileId = function (item) {
        var index = uploader.queue.indexOf(item);
        var fileId = $scope.tempFileIds[index];
        if (fileId) {
          $scope.tempFileIds.splice(index, 1);
          FilesService.deleteFile(fileId).then(function(res) {});
        }
      };

      $scope.setReply = function () {
        window.scrollTo(0, document.body.scrollHeight);
        var textArea = document.getElementById('replyMessage');
        textArea.focus();
      };

      $scope.showOptions = function(item) {
        var flag = false;

        if ($rootScope.currentPermissions.indexOf('forums.postAny.delete') > -1 || $rootScope.currentPermissions.indexOf('forums.postAny.edit') > -1) {
          flag = true;
        } else if (item.profile) {
          if ($rootScope.userId == item.profile.id) {
            var diffMs = new Date() - item.created_date;
            var diffMins = diffMs / 60000; // minutes

            if (diffMins < 10) {
              flag = true;
            }
          }
        }

        return flag;
      };

      $scope.deletePost = function (id) {
        cfpLoadingBar.start();
        ForumsService.deletePost(id).then(function (res) {
          if (res) {
            cfpLoadingBar.complete();
            $scope.getPosts(true, 'Post successfully deleted.');
          } else {
            cfpLoadingBar.complete();
            $scope.displayNotification(true, 'Post successfully deleted.');
          }
        });
      };

      $scope.editPost = function (item) {
        item.isEdit = true;
            $scope.topic = {
                subject: $scope.topic.subject,
                id: $scope.topic.id
            };
      };

      $scope.sortPost = function () {
        $scope.isDesc = !$scope.isDesc;
        postsConfig.sort = $scope.isDesc ? '-id' : 'id';
        $scope.getPosts(false);
      };

      $scope.quoteReply = function (item, order) {
        $scope.reply.replyTo = item.id;
        $scope.replyTo = item;
        $scope.order = order;
        var textArea = document.getElementById('replyMessage');
        textArea.focus();

        $timeout(function () {
          document.getElementById('quoteform').scrollIntoView();
        }, 100)
      };

      $scope.cancelPost = function (form) {
        $scope.files = [];
        $scope.tempFileIds = [];
        uploader.clearQueue();
        $scope.reply = {};
        $scope.replyTo = {};
        form.$setPristine();
        form.$setUntouched();
        $scope.order=99999999;
      };

      $scope.editTopic = function () {
        $scope.editTopicModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/topic.modal.html',
          show: false
        });

      var forumsConfig = {
        userId: $rootScope.userId,
        start: 0,
        limit: 999,
        filter: 'state=1/scope=course||scope=custom/subforums=1/export=1',
        sort: 'id'
      }
      var currentForum = $rootScope.forum.id;
      $scope.forums = [];

      $scope.getUserForums = function () {
        ForumsService.getUserForums(forumsConfig).then(function (res) {
          if (res) {
            $scope.userForumsList = res.data.list;

              angular.forEach($scope.userForumsList, function(item, key) {
                  var forum = {
                      title: item.title,
                      id: item.id
                  }
                  $scope.forums.push(forum);
            if (item.id == currentForum) {
               $scope.topic.selected = {title: item.title, id: item.id};
            }

            });

          } else {
              return false;
          }

        });
      };



        $scope.getUserForums();
        $scope.editTopicModal.$promise.then($scope.editTopicModal.show);
      };

      $scope.saveTopic = function () {
        cfpLoadingBar.start();

        if ($scope.topic.selected.id) {
            $scope.topic.forum_id = $scope.topic.selected.id;
            $scope.forum.id = $scope.topic.selected.id;
            $scope.forum.title = $scope.topic.selected.title;

        } else {
            $scope.topic.forum_id = $rootScope.forum.id;
        }

        if ($scope.topic.sticky) {
          $scope.topic.sticky = 1;
        } else {
          $scope.topic.sticky = 0;
        }

        if ($scope.topic.learn_path) {
          $scope.topic.learn_path = 1;
        } else {
          $scope.topic.learn_path = 0;
        }

        if ($scope.topic.locked) {
          $scope.topic.locked = 1;
        } else {
          $scope.topic.locked = 0;
        }

        $scope.topic.created_by = $rootScope.userId;
        $scope.topic.created_at = new Date();

        // save topic
        ForumsService.updateSaveTopic($scope.topic, $scope.topic.id).then(function (res) {
          if (res) {
            $scope.editTopicModal.$promise.then($scope.editTopicModal.hide);
            cfpLoadingBar.complete();
            Notification.success({
              title: 'Success',
              message: 'Topic successfully updated.'
            });
          } else {
            $scope.editTopicModal.$promise.then($scope.editTopicModal.hide);
            cfpLoadingBar.complete();
            Notification.error({
              title: 'Error',
              message: 'An error occured while processing your request.'
            });
          }

          $scope.topic.sticky = $scope.topic.sticky == '1' ? true : false;
          $scope.topic.learn_path = $scope.topic.learn_path == '1' ? true : false;
          $scope.topic.locked = $scope.topic.locked == '1' ? true : false;
        });
      };

      $scope.deleteTopic = function () {
        ForumsService.deleteTopic($scope.topic.id).then(function (res) {
          if (res) {
            $scope.editTopicModal.$promise.then($scope.editTopicModal.hide);
            $state.go('app.admin.forums.view', {forumId: $rootScope.forum.id});
          }
        });
      };

      $scope.displayImage = function (item) {
        $scope.imageItem = item;
        $scope.displayImageModal = $modal({
          scope: $scope,
          templateUrl: '/partials/modals/image.modal.html',
          show: false
        });

        $scope.displayImageModal.$promise.then($scope.displayImageModal.show);
      };

      $scope.getNumber = function (num) {
        return new Array(num);
      };

      $scope.subscribeUser = function () {
        cfpLoadingBar.start();
        var config = { id: $scope.topic.id, profileId: $rootScope.userId };
        if ($scope.topic.is_current_user_subscribed) {
          ForumsService.unsubscribeUserFromTopic(config).then(function (res) {
            if (res) {
              $scope.topic.is_current_user_subscribed = !$scope.topic.is_current_user_subscribed;
              cfpLoadingBar.complete();
              Notification.success({
                title: 'Success',
                message: 'Successfully unsubscribed from topic.'
              });
            }
          });
        } else {
          ForumsService.subscribeUserToTopic(config).then(function (res) {
            if (res) {
              $scope.topic.is_current_user_subscribed = !$scope.topic.is_current_user_subscribed;
              cfpLoadingBar.complete();
              Notification.success({
                title: 'Success',
                message: 'Successfully subscribed to topic.'
              });
            }
          });
        }
      };

      $scope.getTopicName();
      $scope.getPosts(false);
      $scope.increaseTopicHits(postsConfig.id);

    });
})();