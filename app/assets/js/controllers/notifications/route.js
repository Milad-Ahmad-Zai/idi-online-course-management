(function () {
  angular
  .module('ui.routes')
  .config(config);

  function config($stateProvider) {
    $stateProvider
    .state('app.admin.notifications', {
      url: '/notifications',
      abstract: true,
      views: {
        'content@app': {
          controller: 'NotificationsCtrl',
          template: '<ui-view=""></ui-view>'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            { name: 'ctrl.notifications', files: ['/assets/js/controllers/notifications/notifications.ctrl.js']}
          ])
        }
      }
    })
    .state('app.admin.notifications.view', {
      url: '',
      views: {
        'content@app': {
          controller: 'NotificationsCtrl as notifications',
          templateUrl: '/partials/notifications/notifications.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            { name: 'ctrl.notifications', files: ['/assets/js/controllers/notifications/notifications.ctrl.js']},
            { name: 'idi.notifications.widget', files: ['/assets/js/directives/common/notifications/idi.notificationsWidget.js']},
            { name: 'idi.bioCard', files: ['/assets/js/directives/common/bio-card/idi.bio-card.js']}
          ])
        }
      }
    })
  }
})()