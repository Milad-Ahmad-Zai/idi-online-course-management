(function () {
  'use strict';

  angular
    .module('ctrl.notifications', [])

    .controller('NotificationsCtrl', function ($scope, $state, $rootScope) {
      var vm = this;

      $scope.formData = {
        "radioFilter": "all",
        "searchText": ""
      }

      function init() {
        vm.notifications = [
          {"id":"001", "type":"message", "date":"2016-12-14 09:38:27", "message":{ "id":"25", "name":"Vitalij", "content":"Do you like the new notifications widget"}},
          {"id":"002", "type":"alert", "date":"2016-12-14 07:38:27", "content":"Site will be down for maintenance on 25/12/2016" },
          {"id":"003", "type":"calendar", "date":"2016-12-12 09:38:27", "calendar":{ "title":"Very important event", "start":"11:00", "end":"13:30"}},
          {"id":"004", "type":"upload", "date":"2016-12-7 09:38:27", "upload":{ "title":"eps1.7_wh1ter0se.m4v", "url":"http://www.google.com"}},
          {"id":"005", "type":"forum", "date":"2016-11-14 09:38:27", "topic":{ "name":"Important module discussion", "url":"http://www.google.com"}},
        ];
        vm.radioData = ["All","Message","Forum","Calendar","Alert","Upload"];
        vm.checkData = [
          {"checked":true, "value":"Message"},
          {"checked":true, "value":"Forum"},
          {"checked":true, "value":"Calendar"},
          {"checked":true, "value":"Alert"},
          {"checked":true, "value":"Upload"}
        ];
      }

      init();
    })
})()