(function () {

  'use strict';

  angular
    .module('ctrl.main', ['ui-notification', 'angular.filter'])
    .controller('MainCtrl', MainCtrl);

  function MainCtrl ($scope, $rootScope, $state, $q, user, profile, Notification, ProfilesService, StudyPath2Service, AuthService) {

    $rootScope.admissionLoadedNotified = false;

    $rootScope.totalUnreadCtr = 0;
    $rootScope.messageScopeObj = {};
    $rootScope.oldMessageScopeName = '';
    $rootScope.currentScope = '';
    $rootScope.fiveMessages = [];
    $rootScope.semesterList = [];

    if (!$rootScope.studypathDeferred) {
      $scope.newStudypathQuery = true;
      $rootScope.studypathDeferred = $q.defer();
    }

    $scope.loadStudypath = function () {
      $scope.newStudypathQuery = true;
      $rootScope.studypathDeferred = $q.defer();

      if (user) {
        var config = {
          start: 0,
          limit: 100,
          filter: "user_id=" + user.Id
        };
        $scope.applyStudypath = function (res) {
          if (res.data && res.data.list) {
            $rootScope.studypath = res.data.list;
          } else {
            $rootScope.studypath = [];
          }
          $rootScope.studypathDeferred.resolve(res);
        };

        if ($scope.checkRights('studypath.any')) {
          StudyPath2Service.getAllStudyPath(config).then($scope.applyStudypath);
        }
      }
    };

    $scope.scrollbarConfigInner = {
      autoResize: true,
      scrollbar: {
        width: 4,
        hoverWidth: 2,
        color: '#65cac0',
        show: false
      },
      scrollbarContainer: {
        width: 8,
        color: '#e9f0f5'
      },
      scrollTo: null
    };

    // studypath terms and profile

    $scope.declineUpdate = function (res, closed) {
      var scope = this;
      if (closed !== 'closed') {
        scope.registrationFormModal.$promise.then(scope.registrationFormModal.hide);
      }
    };
    $scope.updateStudioProfile = function () {
      var scope = this;
      var id = (scope.item && scope.item.programme_id) || scope.programmeId;
      var config = {
        studypath: {
          profile_upto_date : 1
        }
      };
      ProfilesService.saveUH(scope.programmeId, $rootScope.userId, scope.uh).then(function (res) {
        if (!(res.error || res.data && res.data.error)) {
          StudyPath2Service.updateStudyPath(scope.cutStudypath.id, config).then(function (res) {
            if (!(res.error || res.data && res.data.error)) {
              $scope.registrationModalHide = true;
              scope.registrationFormModal.$promise.then(scope.registrationFormModal.hide);
              if (scope.cutStudypath.terms_and_conditions == "1") {
                console.info('here was redirect to level')
              } else {
                scope.termsModal.$promise.then(scope.termsModal.show);
              }
            } else {
              Notification.error({ message: 'Studypath wasn\'t updated', title: 'Error: '})
            }
          })
        } else {
          Notification.error({ message: 'Data wasn\'t sended', title: 'Error: '})
        }
      });
    };

    $scope.declineProgrammeTerms = function (res, closed) {
      var scope = this;
      if (closed !== 'closed') {
        scope.termsModal.$promise.then(scope.termsModal.hide);
      }
      if (scope.termsModalHide) {
        if (scope.view == 'studio') {
          scope.isModuleShown = true;
        } else if (scope.view == 'programme') {
          scope.showProgrammeData();
        }
      }
    };

    $scope.acceptProgrammeTerms = function () {
      var scope = this;
      var id = (scope.item && scope.item.programme_id) || scope.programmeId;
      var config = {
        studypath: {
          terms_and_conditions: 1,
          profile_upto_date : 1
        }
      };
      StudyPath2Service.updateStudyPath(scope.cutStudypath.id, config).then(function (res) {
        if (!(res.error || res.data && res.data.error)) {
          $scope.termsModalHide = true;
          scope.termsModal.$promise.then(scope.termsModal.hide);
          $scope.loadStudypath();
          if (scope.view == 'selectProgramme') {
            console.log('here was redirect to programme')
          } else if (scope.view == 'studio') {
            scope.isModuleShown = true;
          }
        }
      });
    };


    if (user) {
      $rootScope.getMessageScopes();
      var config = {
        start: 0,
        limit: 100,
        filter: "user_id=" + user.id
      };

      $scope.applyStudypath = function (res) {
        if (res.data && res.data.list) {
          $rootScope.studypath = res.data.list;
        } else {
          $rootScope.studypath = [];
        }
        $rootScope.studypathDeferred.resolve(res);
      };

      if ($scope.checkRights('studypath.any')) {
        StudyPath2Service.getAllStudyPath(config).then($scope.applyStudypath);
      }
      if (profile) {
        $rootScope.firstInitial = profile.first_name.substr(0,1);
        $rootScope.oldProfile = angular.copy(profile);
        $rootScope.profile = angular.copy(profile);
        $scope.userProfile = angular.copy(profile);

        var firstName = profile.first_name || '';
        var lastName = profile.last_name || '';
        $rootScope.fullName = firstName + ' ' + lastName;

        if (profile.profile_image && profile.profile_image.type == 'profileimage') {
          $rootScope.profileImage = profile.profile_image.url;
        }

        if (profile.profile_image.ext=='css') {
          $rootScope.noImage = true;
        } else {
          $rootScope.noImage = false;
        }

        $scope.getLastFiveMessages();
      }
    }
  }
})();