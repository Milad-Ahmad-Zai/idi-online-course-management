(function () {
  'use strict';

  angular
    .module('ctrl.profiles.notes_history', ['angularFileUpload'])

    .controller('NotesHistoryCtrl', function ($scope, $state, $stateParams, $rootScope, $filter, Notification, user, AuthService, ProfilesService) {
      var id = $stateParams.id || user.id;
      $scope.currentProfile = {};
      $scope.profiles = [];
      var skip = 0,
          take = 100;

      $scope.getProfileVersions = function () {
        ProfilesService.getProfileVersions(id, skip, take).then(function (res) {
          if (res.error || res.data.error) {
            Notification.error({ message: '', title: 'Error: ' + res.status})
          } else {
            if (res && res.data) {
              $scope.profiles = res.data.items;
              $scope.pagination = res.data.pagination;
              if ($scope.profiles.length > 0) {
                $scope.currentProfile = $scope.profiles[0];
                $scope.currentProfileVersion = $filter('date')($scope.currentProfile.created, 'MM/dd/yyyy "at" h:mma');
              }
            }
          }
        });
      };

      $scope.moreVersions = function () {
        skip += take;
        var takeForMore = take;
        if ($scope.pagination.total_items - skip < takeForMore) {
          takeForMore = $scope.pagination.total_items - skip;
        }
        ProfilesService.getProfileVersions(id, skip, takeForMore).then(function (res) {
          if (res.error) {
            Notification.error({ message: '', title: 'Error: ' + res.status})
          } else {
            res.data.items.forEach(function (element, index, array) {
              $scope.profiles.push(element);
            });

            $scope.pagination = res.data.pagination;
            if ($scope.profiles.length > 0) {
              $scope.currentProfile = $scope.profiles[skip];
              $scope.currentProfileVersion = $filter('date')($scope.currentProfile.created, 'MM/dd/yyyy "at" h:mma');
            }
          }
        });
      };

      $scope.changeProfile = function (e) {
        var scope = angular.element(e.target).scope();
        var profileVersionId = scope.profile.id;
        if (!(profileVersionId == $scope.currentProfile && $scope.currentProfile.id)) {
          var filtered = $scope.profiles.filter(function (profile) {
            return profile.id == profileVersionId;
          });
          $scope.currentProfile = filtered[0];
          $scope.currentProfileVersion = $filter('date')($scope.currentProfile.created, 'MM/dd/yyyy "at" h:mma');
        }
      };

      $scope.restoreProfile = function (e) {
        ProfilesService.restoreProfileVersion($scope.currentProfile.id).then(function (res) {
          if (!res.error) {
          }
        })
      };

      if ($scope.checkRights('profiles.versions')) {
        if (id && user) {
          $scope.getProfileVersions();
        }
      }
    });
})();
