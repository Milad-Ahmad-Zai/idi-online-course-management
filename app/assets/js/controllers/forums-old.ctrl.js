(function () {
  'use strict';

  angular
    .module('ctrl.forums', [])

    .controller('ForumsOldCtrl', function ($scope, $state, $rootScope, $sce, $window, user) {
      $scope.setProject = function (id) {
        var url = 'https://idi-study.com/forum-10139?tmpl=component&accessFromApp=allowed&user=' + id + '&access_token=' + JSON.parse($window.localStorage.token).access_token;
        $scope.frameUrl = $sce.trustAsResourceUrl(url);
      };

      if (user.id) {
        $scope.setProject(user.id);
      }
    });
})();