angular.element(document).ready(function () {
  angular
    .bootstrap(document, [
      'app.config',
      'LocalStorageModule',
      'permission',
      'ui.routes',
      'auth.service',
      'app.run',
      'interceptor.service',
      'ngTouch',
      'ngSanitize',
      'ngAnimate',
      'formly',
      'formlyBootstrap',
      'mgcrea.ngStrap'
    ]);
});