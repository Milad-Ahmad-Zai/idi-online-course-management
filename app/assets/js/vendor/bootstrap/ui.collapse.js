(function () {
  'use strict';

  angular
    .module('ui.collapse', [])
    .directive('uiCollapse', ['$timeout', '$window', function ($timeout, $window) {
    return {
      restrict: 'AC',
      link: function ($scope, $el, $attr) {
        var items = $el[0].querySelectorAll('.panel [data-toggle="collapse"]');
        var programmeItems;
        var courseItems;

        if ($attr.uiCollapse == 'studio' || $attr.uiCollapse == 'studio-message' || $attr.uiCollapse == 'forum') {
          $timeout(function() {
            items = $el[0].querySelectorAll('.panel [data-toggle="collapse"]');
            addLogicToItems();
          }, 500);
        } else if ($attr.uiCollapse == 'programme') {
          $timeout(function() {
            programmeItems = $el[0].querySelectorAll('.panel.student-programme-list [data-toggle="collapse"]');
            addLogicToProgrammeItems();
            
            courseItems = $el[0].querySelectorAll('.panel.course-list [data-toggle="collapse"]');
            addLogicToCourseItems();
          }, 21000);
        } else {
          addLogicToItems()
        }
        function addLogicToItems () {
          angular
            .element( items )
            .on('click', function (e) {
              e.preventDefault();
              var hash = e.target.hash;
              var target = $el[0].querySelector(hash);
              var panels = $el[0].querySelectorAll('.panel-collapse.collapse.in');

              [].forEach.call(panels, function (item) {
                if ( target === item ) return;
                item.classList.remove('in');
              });

              target.classList.toggle('in');
            });

            if ($attr.uiCollapse == 'studio-message' && $window.innerWidth < 992) {
              var panels = $el[0].querySelectorAll('.panel-collapse.collapse.in');

              [].forEach.call(panels, function (item) {
                item.classList.remove('in');
              });
            }
        }

        function addLogicToProgrammeItems () {
          angular
            .element( programmeItems )
            .on('click', function (e) {
              e.preventDefault();
              var hash = e.target.hash;
              var target = $el[0].querySelector(hash);
              var panels = $el[0].querySelectorAll('.student-programme.panel-collapse.collapse.in');

              if (target.className.indexOf('student-programme') > -1) {
                [].forEach.call(panels, function (item) {
                  if ( target === item ) return;
                  item.classList.remove('in');
                });

                target.classList.toggle('in');
              }
            });
        }

        function addLogicToCourseItems () {
          angular
            .element( courseItems )
            .on('click', function (e) {
              e.preventDefault();
              var hash = e.target.hash;
              var target = $el[0].querySelector(hash);
              var panels = $el[0].querySelectorAll('.student-course.panel-collapse.collapse.in');

              [].forEach.call(panels, function (item) {
                if ( target === item ) return;
                item.classList.remove('in');
              });

              target.classList.toggle('in');
            });
        }

      }
    };
  }]);

})();