// Message Service for calling REST API
(function () {
    'use strict';

    angular
    .module('message2.service', [])
    .service('Message2Service', Message2Service)

    function Message2Service($http, $interpolate, CONFIG) {
      var BASE_URL = CONFIG.url + 'messages/v2/';
      var AUTH_URL = CONFIG.url + 'auth/users/';

      var URLS = {
        getScopesList: BASE_URL + 'scope',
        sendMessage: BASE_URL + 'message',
        getUserConversations: BASE_URL + 'user/{{ id }}/conversation' +
        '?start={{start}}' +
        '{{limit ? "&limit=" + limit : "" }}' +
        '{{ sort ? "&sort[]=" + sort : "" }}' +
        '{{ filter ? "&filter[]=" + filter : "" }}',
        getUserConversation: BASE_URL + 'conversation/{{ id }}',
        getConversations: BASE_URL + 'conversation' +
        '?start={{start}}' +
        '{{limit ? "&limit=" + limit : "" }}' +
        '{{ sort ? "&sort[]=" + sort : "" }}' +
        '{{ filter ? "&filter[]=" + filter : "" }}',
        getConversationById: BASE_URL + 'conversation/{{ id }}' +
        '?start={{start}}' +
        '{{limit ? "&limit=" + limit : "" }}' +
        '{{ sort ? "&sort[]=" + sort : "" }}' +
        '{{ filter ? "&filter[]=" + filter : "" }}',
        addScope: BASE_URL + 'scope',
        updateScope: BASE_URL + 'scope/{{ id }}',
        deleteScope: BASE_URL + 'scope/{{ id }}',
        getUsersList: AUTH_URL + 'list',
        stareMessage: BASE_URL + 'conversation/stared',
        unstareMessage: BASE_URL + 'conversation/unstared',
        markConversationReaded: BASE_URL + 'conversation/{{ id }}/readed',
        markConversationUnreaded: BASE_URL + 'conversation/{{ id }}/unreaded',
        userSubscription: BASE_URL + 'conversation/{{ id }}/user',
        getUserSentMessages: BASE_URL + 'user/{{ id }}/message/sended' +
        '?start={{start}}' +
        '{{limit ? "&limit=" + limit : "" }}' +
        '{{ sort ? "&sort[]=" + sort : "" }}' +
        '{{ filter ? "&filter[]=" + filter : "" }}',
        getUserDraftMessages: BASE_URL + 'user/{{ id }}/draft' +
        '?start={{start}}' +
        '{{limit ? "&limit=" + limit : "" }}' +
        '{{ sort ? "&sort[]=" + sort : "" }}' +
        '{{ filter ? "&filter[]=" + filter : "" }}',
        saveDraft: BASE_URL + 'draft',
        updateDraft: BASE_URL + 'draft/{{ id }}',
        deleteDraft: BASE_URL + 'draft/{{ id }}',
        deleteConversation: BASE_URL + 'conversation/{{ id }}',
        deleteMessage: BASE_URL + 'message/{{ id }}',
      };

      function handleSuccess (response) {
        return response.data;
      }

      return {
        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Scope-ScopeList
       */
        getScopesList: function (config) {
          return $http.get(URLS.getScopesList, { params: config }).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Scope-CreateNewScope
       */
        addScope: function (data) {
          return $http.post(URLS.addScope, data).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Scope-UpdateScope
       */
        updateScope: function (data, id) {
          var url = $interpolate(URLS.updateScope)({id: id});
          return $http.put(url, data).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Scope-DeleteScope
       */
        deleteScope: function (id) {
          var url = $interpolate(URLS.deleteScope)({id: id});
          return $http.delete(url).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Message-CreateNewMessage
       */
        sendMessage: function(data) {
          return $http.post(URLS.sendMessage, data).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-ConversationList
       */
        getConversations: function(config) {
          var url = $interpolate(URLS.getConversations)(config);
          return $http.get(url).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-GetConversationById
       */
        getConversationById: function(config) {
          var url = $interpolate(URLS.getConversationById)({id: config.id});
          return $http.get(url, { params: config }).then(handleSuccess);
        },

        /**
       * @see https://api.idi-study.com/messages2/user/:id/conversation
       */
        getUserConversations: function(config) {
          var url = $interpolate(URLS.getUserConversations)(config);
          return $http.get(url).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-GetConversationById
       */
        getUserConversation: function (config) {
          var url = $interpolate(URLS.getUserConversation)({id: config.id});
          return $http.get(url, { params: config }).then(handleSuccess);
        },

        /**
         * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
         */
        getUsersList: function (filter) {
          var url = URLS.getUsersList + "/filter/search=" + filter;
          return $http.get(url).then(handleSuccess);

        },

         /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-ConversationMakrStared
       */
        stareMessage: function(data) {
          return $http.post(URLS.stareMessage, data).then(handleSuccess);
        },

         /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-ConversationUnmarkStared
       */
        unstareMessage: function(data) {
          return $http.post(URLS.unstareMessage, data).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-ConversationMarkAsReaded
       */
        markConversationReaded: function (id) {
          var url = $interpolate(URLS.markConversationReaded)({id: id});
          return $http.post(url).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-ConversationMarkAsUnreaded
       */
        markConversationUnreaded: function (id) {
          var url = $interpolate(URLS.markConversationUnreaded)({id: id});
          return $http.post(url).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-ConversationAddUser
       */
        subscribeUser: function (config, id) {
          var url = $interpolate(URLS.userSubscription)({id: id});
          return $http.post(url, config).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-ConversationRemoveUser
       */
        unsubscribeUser: function (config, id) {
          var url = $interpolate(URLS.userSubscription)({id: id});
          return $http({
              method: 'DELETE',
              url: url,
              data: config,
              headers: {
                  'Content-type': 'application/json;charset=utf-8'
              }
          })
          .then(handleSuccess);
        },

        /**
         * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-User-UserSendedMessage
         */
        getUserSentMessages: function (config) {
          var url = $interpolate(URLS.getUserSentMessages)(config);
          return $http.get(url).then(handleSuccess);

        },

        /**
         * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-User-UserSendedMessage
         */
        getUserDraftMessages: function (config) {
          var url = $interpolate(URLS.getUserDraftMessages)(config);
          return $http.get(url).then(handleSuccess);

        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Draft-CreateDraft
       */
        saveDraft: function (data) {
          return $http.post(URLS.saveDraft, data).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Draft-UpdateDraft
       */
        updateDraft: function (data, id) {
          var url = $interpolate(URLS.updateDraft)({id: id});
          return $http.put(url, data).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Draft-DeleteDraft
       */
        deleteDraft: function (id) {
          var url = $interpolate(URLS.deleteDraft)({id: id});
          return $http.delete(url).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Conversation-DeleteConversation
       */
        deleteConversation: function (id) {
          var url = $interpolate(URLS.deleteConversation)({id: id});
          return $http.delete(url).then(handleSuccess);
        },

        /**
       * @see http://docs.idi-demo.com/apidocs/ms-messages2/docs/#api-Message-DeleteMessage
       */
        deleteMessage: function (id) {
          var url = $interpolate(URLS.deleteMessage)({id: id});
          return $http.delete(url).then(handleSuccess);
        },

      };

    }
})();