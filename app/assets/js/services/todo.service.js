// Todo Service for calling REST API
(function () {
  'use strict';

  angular
    .module('todo.service', [])
    .service('TodoService', TodoService)
    .factory('TodoScope', TodoScope);

  function TodoService($http, $interpolate, CONFIG) {

    var BASE_URL = CONFIG.url + 'todo/';

    var URLS = {
      getTodoAPIDetails: BASE_URL + 'api',
      getTodoPermissions: BASE_URL + 'permissions',
      getTodoTaskList: BASE_URL + 'task/list',
      getTodoUserTaskList: BASE_URL + 'user/{{ userID }}/task/list',
      getTodoTaskByID: BASE_URL + 'task/{{ taskID }}',
      addTodoTask: BASE_URL + 'task/',
      setTodoTaskComplete: BASE_URL + 'task/{{ taskID }}/complete/',
      updateTodoTask: BASE_URL + 'task/{{ taskID }}',
      deleteTodoTask: BASE_URL + 'task/{{ taskID }}'
    }

    var getTodoAPIDetails = function() {
      var req = $http.get(URLS.getTodoAPIDetails);
      return req.then(handleSuccess,handleError);
    };

    var getTodoPermissions = function() {
      var req = $http.get(URLS.getTodoPermissions);
      return req.then(handleSuccess,handleError);
    }

    var getTodoTaskList = function() {
      var req = $http.get(URLS.getTodoTaskList);
      return req.then(handleSuccess, handleError);
    }

    var getTodoUserTaskList = function(data) {
      var url = $interpolate(URLS.getTodoUserTaskList)({
        userID: data
      });
      var req = $http.get(url);
      return req.then( handleSuccess, handleError );
    }

    var getTodoTaskByID = function(data) {
      var url = $interpolate(URLS.getTodoTaskByID)({
        taskID: data
      });
      var req = $http.get(url);
      return req.then( handleSuccess, handleError);
    }

    var addTodoTask = function(data) {
      return $http({
          method: 'POST',
          url: URLS.addTodoTask,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
    }

    var setTodoTaskComplete = function(data) {
      var url = $interpolate(URLS.setTodoTaskComplete)({
        taskID: data
      });
      return $http({
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then( handleSuccess, handleError);
    }

    var updateTodoTask = function(id, data) {
      var url = $interpolate(URLS.updateTodoTask)({
        taskID: id
      });
      return $http({
        method: 'PUT',
        url: url,
        data: data,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then( handleSuccess, handleError);
    }

    var deleteTodoTask = function(data) {
      var url = $interpolate(URLS.deleteTodoTask)({
        taskID: data
      });
      return $http({
        method: 'DELETE',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then( handleSuccess, handleError);
    }

    var handleSuccess = function (response) {
      if (response.status === 200) {
        return response.data;
      } else {
        return false;
      }
    };

    var handleError = function (response) {

    };

    return {
      // api calls
      getTodoAPIDetails : getTodoAPIDetails,
      getTodoPermissions : getTodoPermissions,
      getTodoTaskList : getTodoTaskList,
      getTodoUserTaskList : getTodoUserTaskList,
      getTodoTaskByID : getTodoTaskByID,
      addTodoTask : addTodoTask,
      setTodoTaskComplete: setTodoTaskComplete,
      updateTodoTask: updateTodoTask,
      deleteTodoTask: deleteTodoTask,
    }
  }

  function TodoScope () {
    var obj = {};

    return {
      store: function (key, value) {
        obj[key] = value;
      },
      get: function (key) {
        return obj[key];
      }
    }
  }

})()