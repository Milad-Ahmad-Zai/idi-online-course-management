(function () {
  'use strict';

  angular
  .module('forums.service', [])
  .service('ForumsService', ForumsService);

  function ForumsService($http, $interpolate, $q, CONFIG) {
    var BASE_URL = CONFIG.url + 'forums/';

    var URLS = {
      assignSemesterToForum: BASE_URL + 'forum/{{ forumId }}/semester/{{ semesterId }}',
      assignUserToForum: BASE_URL + 'forum/{{ forumId }}/assignUser/{{ profileId }}',
      createForumRelation: BASE_URL + 'forum/relation/{{ parentId }}/{{ childrenId }}/',
      deleteForum: BASE_URL + 'forum/{{ id }}',
      getForumTopics: BASE_URL + 'forum/{{ id }}/topics/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getForumUsers: BASE_URL + 'forum/users/relation/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getForum: BASE_URL + 'forum/{{ id }}',
      getForumsList: BASE_URL + 'forums/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      postForumsOrder: BASE_URL + 'forum/reorder/',
      getUserForums: BASE_URL + 'forums/user/{{ userId }}/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      hardDeleteForum: BASE_URL + 'forum/hard/{{ id }}',
      publishForum: BASE_URL + 'forum/{{ id }}/publish',
      removeForumRelation: BASE_URL + 'forum/relation/{{ parentId }}/{{ childrenId }}',
      removeSemesterFromForum: BASE_URL + 'forum/{{ forumId }}/semester/{{ semesterId }}',
      removeUserAccessToForum: BASE_URL + 'forum/{{ forumId }}/removeUser/{{ userId }}',
      unpublishForum: BASE_URL + 'forum/{{ id }}/unpublish',
      updateForumRelation: BASE_URL + 'forum/updateRelations/{{ parentId }}/',
      updateSaveForum: BASE_URL + 'forum',

      // topic

      deleteTopic: BASE_URL + 'topic/{{ id }}',
      getAttachmentsByTopic: BASE_URL + 'topic/{{ id }}/attachments',
      getPostsByTopic: BASE_URL + 'topic/{{ id }}/posts/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      postTopicsOrder: BASE_URL + 'topic/reorder/',
      getPostById: BASE_URL + 'topic/{{ id }}',
      increaseTopicHitsNumber: BASE_URL + 'topic/{{ id }}/hit',
      moveTopicToForum: BASE_URL + 'topic/{{ id }}/move/{{ forumId }}',
      subscribeUserToTopic: BASE_URL + 'topic/{{ id }}/subscribe/{{ profileId }}',
      unsubscribeUserFromTopic: BASE_URL + 'topic/{{ id }}/unsubscribe/{{ profileId }}',
      updateSaveTopic: BASE_URL + 'topic{{ id ? "/" + id : "" }}',

      // post

      deletePost: BASE_URL + 'post/{{ id }}',
      getAttachmentsByPost: BASE_URL + 'post/{{ id }}/attachments',
      getPost: BASE_URL + 'post/{{ id }}',
      markPostForModeration: BASE_URL + 'post/moderation/{{ id }}',
      movePostToTopic: BASE_URL + 'post/{{ id }}/move/{{ topicId }}',
      updateSavePost: BASE_URL + 'post{{ id ? "/" + id : "" }}',

      // attachment

      attachFileToPostTopic: BASE_URL + 'attachment',
      deleteAttachmentComment: BASE_URL + 'attachment/comment/{{ commentId }}',
      deleteAttachment: BASE_URL + 'attachment/{{ id }}',
      saveUpdateComment: BASE_URL + 'attachment/{{ id }}/comment',

      // album

      commentOnAlbum: BASE_URL + 'album/{{ id }}/comment',
      deleteAlbumComment: BASE_URL + 'album/comment/{{ commentId }}',
      deleteAlbum: BASE_URL + 'album.{{ id }}',
      getFilesByAlbum: BASE_URL + 'album/{{ id }}/files/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      increaseAlbumHitsNumber: BASE_URL + 'album/{{ id }}/hit',
      moveAlbumToGallery: BASE_URL + 'album/{{ id }}/move/{{ galleryId }}',
      updateSaveAlbum: BASE_URL + 'album',

      // file

      addFileToAlbum: BASE_URL + 'file',
      commentOnFile: BASE_URL + 'file/{{ id }}/comment',
      deleteFileComment: BASE_URL + 'files/comment/{{ commentId }}',
      deleteFile: BASE_URL + 'file/{{ id }}',
      getFile: BASE_URL + 'file/{{ id }}',
      moveFileToAlbum: BASE_URL + 'file/{{ id }}/move/{{ albumId }}',
    };

    var handleSuccess = function (response) {
      return response.data;
    };

    var handleError = function (response) {
      return $q.reject(response.data);
    };


    /**
     * Transforms post's data to more comfortable format
     * @param  {Object} post - backend's data about one post
     * @return {Object} - transformed data
     */
    function transfromPost(post) {
      post.created_at = post.created_at && moment.utc(post.created_at).toDate();
      post.updated_at = post.updated_at && moment.utc(post.updated_at).toDate();
      post.deleted_at = post.deleted_at && moment.utc(post.deleted_at).toDate();
      post.highlighted = post.highlighted == "1" ? true : false;
      post.deleted = post.deleted == "1" ? true : false;
      post.text = post.text && post.text.replace(/\r?\n/g, '<br />')
    }


    return {

      // forums

      assignSemesterToForum: function (config) {
        var url = $interpolate(URLS.assignSemesterToForum)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      assignUserToForum: function (config, data) {
        var url = $interpolate(URLS.assignUserToForum)(config);
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      createForumRelation: function (config) {
        var url = $interpolate(URLS.createForumRelation)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      deleteForum: function (id) {
        var url = $interpolate(URLS.deleteForum)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getForumTopics: function (config) {
        var url = $interpolate(URLS.getForumTopics)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      getForumUsers: function (config) {
        var url = $interpolate(URLS.getForumUsers)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      getForum: function (id) {
        var url = $interpolate(URLS.getForum)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      getForumsList: function (config) {
        var url = $interpolate(URLS.getForumsList)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      postForumsOrder: function (data) {
        var url = URLS.postForumsOrder;
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      getUserForums: function (config) {
        var url = $interpolate(URLS.getUserForums)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      hardDeleteForum: function (id) {
        var url = $interpolate(URLS.hardDeleteForum)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      publishForum: function (id) {
        var url = $interpolate(URLS.publishForum)({ id : id });
        return $http.post(url).then(handleSuccess, handleError);
      },
      removeForumRelation: function (config) {
        var url = $interpolate(URLS.removeForumRelation)(config);
        return $http.delete(url).then(handleSuccess, handleError);
      },
      removeSemesterFromForum: function (config) {
        var url = $interpolate(URLS.removeSemesterFromForum)(config);
        return $http.delete(url).then(handleSuccess, handleError);
      },
      removeUserAccessToForum: function (config) {
        var url = $interpolate(URLS.removeUserAccessToForum)(config);
        return $http.delete(url).then(handleSuccess, handleError);
      },
      unpublishForum: function (id) {
        var url = $interpolate(URLS.unpublishForum)({ id : id });
        return $http.post(url).then(handleSuccess, handleError);
      },
      updateSaveForum: function (id, data) {
        var url = URLS.updateSaveForum;
        if (id) url += '/' + id;
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // topic

      deleteTopic: function (id) {
        var url = $interpolate(URLS.deleteTopic)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getAttachmentsByTopic: function (id) {
        var url = $interpolate(URLS.getAttachmentsByTopic)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      getPostsByTopic: function (config) {
        var url = $interpolate(URLS.getPostsByTopic)(config);
        return $http.get(url, {
          transformResponse: function (data, headers) {
            var result = angular.fromJson(data);
            result.data.list.forEach(transfromPost);

            return result;
          }
        }).then(handleSuccess, handleError);
      },
      getPostById: function (id) {
        var url = $interpolate(URLS.getPostById)({ id : id });
        return $http.get(url, {
          transformResponse: function (data, headers) {
            var result = angular.fromJson(data)
            transfromPost(result.data);
            return result;
          }
        }).then(handleSuccess, handleError);
      },
      increaseTopicHitsNumber: function (id) {
        var url = $interpolate(URLS.increaseTopicHitsNumber)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      moveTopicToForum: function (config) {
        var url = $interpolate(URLS.moveTopicToForum)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      subscribeUserToTopic: function (config) {
        var url = $interpolate(URLS.subscribeUserToTopic)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      unsubscribeUserFromTopic: function (config) {
        var url = $interpolate(URLS.unsubscribeUserFromTopic)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      updateSaveTopic: function (data, id) {
        var url = $interpolate(URLS.updateSaveTopic)({id: id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      postTopicsOrder: function (data) {
        var url = URLS.postTopicsOrder;
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // post

      deletePost: function (id) {
        var url = $interpolate(URLS.deletePost)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getAttachmentsByPost: function (id) {
        var url = $interpolate(URLS.getAttachmentsByPost)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      getPost: function (id) {
        var url = $interpolate(URLS.getPost)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      markPostForModeration: function (id) {
        var url = $interpolate(URLS.markPostForModeration)({ id : id });
        return $http.post(url).then(handleSuccess, handleError);
      },
      movePostToTopic: function (config) {
        var url = $interpolate(URLS.movePostToTopic)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      updateSavePost: function (data, id) {
        var url = $interpolate(URLS.updateSavePost)({ id : id });
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // attachment

      attachFileToPostTopic: function (data) {
        return $http.post(URLS.attachFileToPostTopic, data).then(handleSuccess, handleError);
      },
      deleteAttachmentComment: function (commentId) {
        var url = $interpolate(URLS.deleteAttachmentComment)({ commentId : commentId });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      deleteAttachment: function (id) {
        var url = $interpolate(URLS.deleteAttachment)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      saveUpdateComment: function (id, data) {
        var url = URLS.saveUpdateComment;
        if (id) url += '/' + id;
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // gallery

      assignUserToGallery: function (config) {
        var url = $interpolate(URLS.assignUserToGallery)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      deleteGallery: function (id) {
        var url = $interpolate(URLS.deleteGallery)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getGalleryAlbums: function (id) {
        var url = $interpolate(URLS.getGalleryAlbums)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      getGallery: function (id) {
        var url = $interpolate(URLS.getGallery)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      getUserGalleries: function (config) {
        var url = $interpolate(URLS.getUserGalleries)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      removeUserAccessToGallery: function (config) {
        var url = $interpolate(URLS.removeUserAccessToGallery)(config);
        return $http.delete(url).then(handleSuccess, handleError);
      },
      updateSaveGallery: function (id, data) {
        var url = URLS.updateSaveGallery;
        if (id) url += '/' + id;
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // album

      commentOnAlbum: function (id, data) {
        var url = $interpolate(URLS.commentOnAlbum)({ id : id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      deleteAlbumComment: function (commentId) {
        var url = $interpolate(URLS.deleteAlbumComment)({ commentId : commentId });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      deleteAlbum: function (id) {
        var url = $interpolate(URLS.deleteAlbum)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getFilesByAlbum: function (config) {
        var url = $interpolate(URLS.getFilesByAlbum)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      increaseAlbumHitsNumber: function (id) {
        var url = $interpolate(URLS.increaseAlbumHitsNumber)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      moveAlbumToGallery: function (config) {
        var url = $interpolate(URLS.moveAlbumToGallery)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
      updateSaveAlbum: function (id, data) {
        var url = URLS.updateSaveAlbum;
        if (id) url += '/' + id;
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // file


      addFileToAlbum: function (data) {
        return $http.post(URLS.addFileToAlbum, data).then(handleSuccess, handleError);
      },
      commentOnFile: function (id, data) {
        var url = $interpolate(URLS.commentOnFile)({ id : id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      deleteFileComment: function (commentId) {
        var url = $interpolate(URLS.deleteFileComment)({ commentId : commentId });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      deleteFile: function (id) {
        var url = $interpolate(URLS.deleteFile)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getFile: function (id) {
        var url = $interpolate(URLS.getFile)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      moveFileToAlbum: function (config) {
        var url = $interpolate(URLS.moveFileToAlbum)(config);
        return $http.post(url).then(handleSuccess, handleError);
      },
    };

  }
})();