// Message Service for calling REST API
(function () {
    'use strict';

    angular
    .module('message.service', [])
    .service('MessageService', MessageService)
    .factory('MessageScope', MessageScope);

    function MessageService($http, $interpolate, CONFIG) {
      var BASE_URL = CONFIG.url + 'messages/';

      var URLS = {
        // messages
        getAllUserConversation: BASE_URL + 'messages/conversations/{{ user_id }}/scope/{{ scope_id }}',
        getConversation: BASE_URL + 'messages/conversations/{{ conversation_id }}',
        getConversationByScopeAndSourceIds: BASE_URL + 'messages/conversations/scope/{{ scopeId }}/source/{{ sourceId }}',
        getConversationFromUser: BASE_URL + 'messages/conversations/{{ user_id }}/from/{{ from_users }}',
        getOrCreateTutorConversation: BASE_URL + 'conversation/tutor/{{ source_id }}',
        deleteConversation: BASE_URL + 'messages/conversation/{{ id }}',
        filterConversations: BASE_URL + 'messages/{{ ids }}/filter/',
        deleteConversationUser: BASE_URL + 'messages/conversation-user/{{ id }}',
        message: BASE_URL + 'messages',
        optionMessage: BASE_URL + 'messages/{{ user_id }}/flag/{{ conversation_id }}/{{ param }}',
        getTemplateList: BASE_URL + 'messages/templates/scope/{{ scope_id }}',
        addTemplate: BASE_URL + 'messages/templates',
        modifyTemplate: BASE_URL + 'messages/templates/{{ template_id }}',
        messageGroup: BASE_URL + 'messages/groups/all',
        createMessageGroup: BASE_URL + 'messages/group',
        optionMessageGroup: BASE_URL + 'messages/group/{{ group_id }}',
        messageGroupUsers: BASE_URL + 'messages/group-users',
        countUnread: BASE_URL + 'count-unread/{{ user_id }}',
        countScopeUnread: BASE_URL + 'count-unread/scope/{{ scope_id }}',
        getAllScope: BASE_URL + 'messages/scopes/all',
        getUserMessageScope: BASE_URL + 'messages/scopes/user',
        subscribeConversation: BASE_URL + 'conversation/{{ user_id }}/subscribe/{{ conversation_id }}',
        getStudentManagers: BASE_URL + 'manager/student/{{ id }}',
        createStudentManager: BASE_URL + 'manager',
        deleteStudentManager: BASE_URL + 'manager/{{ id }}',
        updateStudentManager: BASE_URL + 'manager/{{ id }}',
        sendNotification: BASE_URL + 'notify',
        lastFiveMessages: BASE_URL + 'last-5-messages',
        getFileMessages: BASE_URL + 'file/{{ fileId }}/messages',
        getConversationMessages: BASE_URL + 'message/list/page/{{offset}}/{{count}}/filter/conversation_id={{id}}/files.get=true/users.get=true',
        getConversationFiles: BASE_URL + 'conversation/{{id}}/files',
        createConversation: BASE_URL + 'conversation/',
      };


      var getUserConversationsByScope = function (data) {
        var url = $interpolate(URLS.getAllUserConversation)({
                    user_id: data.userId,
                    scope_id: data.scopeId
                  });
        if (data.page) {
          url = url + '/page/' + data.page;
        }
        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      var getConversations = function (data) {
        var url = URLS.message;

        if (data.userId) {
          url += '/' + data.userId;
        }

        if (data.scopeId) {
          url += '/' + data.scopeId;
        } else if (data.sourceId) {
          url += '/0';
        }

        if (data.sourceId) {
          url += '/' + data.sourceId;
        }

        url += '/filter';
        if (data.keyword) {
          url +=  '/' + data.keyword;
        }

        if (!data.category) {
          url += '/my_scope=true';
        } else if (data.category == 'unreplied') {
          url += '/unreplied=true';
        } else if (data.category == 'other') {
          url += '/other=true';
        }

        if (data.page) {
          url +=  '/page/' + data.page;
        }

        if (data.limit) {
          url += '/' + data.limit;
        }

        if (data.order) {
          url += '/' + data.order;
        }

        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      var getConversationByScopeAndSourceIds = function (data) {
        var url = $interpolate(URLS.getConversationByScopeAndSourceIds)(data);
        return $http.get(url).then(handleSuccess, handleError);
      };

      var getConversation = function (id) {
        var url = $interpolate(URLS.getConversation)({
                    conversation_id: id
                  });
        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      /**
       * Gets an existing conversation with tutor if it
       * exists and creates a new one otherwise
       *
       * @see http://docs.idi-demo.com/apidocs/ms-messages/docs/#api-Conversation-GetOrCreateConversation
       *
       * @param {Number} source_id - spModule id
       * @param {Object} data - additional data
       * @param {String} data.topic - conversation's topic
       * @param {Array<String>} data.user_ids - array of conversation members ids
       */
      var getOrCreateTutorConversation = function(sourceId, data) {
        var url = $interpolate(URLS.getOrCreateTutorConversation)({
          source_id: sourceId
        })
        return $http.post(url, data)
          .then( handleSuccess, handleError)
      }

      var createConversation = function (data) {
        return $http.post(URLS.createConversation, data).then( handleSuccess, handleError );
      }

      var getConversationFromUser = function (id, fromUser) {
        var url = $interpolate(URLS.getConversationFromUser)({
                    user_id: id,
                    from_users: fromUser
                  });
        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      var deleteConversation = function (id) {
        var url = $interpolate(URLS.deleteConversation)({ id: id });
        return $http({
          method: 'DELETE',
          url: url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var getMessage = function (id) {
        var url = URLS.message + '/' + id;
        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      var sendMessage = function (data) {
        return $http({
          method: 'POST',
          url: URLS.message,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var deleteMessage = function (id) {
        return $http({
          method: 'DELETE',
          url: URLS.message + '/' + id,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var updateMessage = function (id, data) {
        return $http({
          method: 'PUT',
          url: URLS.message + '/' + id,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var optionMessage = function (data) {
        var url = $interpolate(URLS.optionMessage)({
                    user_id: data.userId,
                    conversation_id: data.conversationId,
                    param: data.param
                  });
        return $http({
          method: 'PUT',
          url: url
        }).then( handleSuccess, handleError );
      };

      var deleteConversationUser = function (id) {
        var url = $interpolate(URLS.deleteConversationUser)({id : id});
        return $http({
          method: 'DELETE',
          url: url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      /**
       * Get messages of specific conversation
       *
       * @param  {Number} conversationId
       * @param  {Object} params
       * @param  {Number} params.offset - how much messages to skip
       * @param  {Number} params.count  - how much messages to return
       *
       * @return {Promise}
       */
      var getConversationMessages = function (conversationId, params) {
        var url = $interpolate(URLS.getConversationMessages)({
          id: conversationId,
          offset: params.offset || 0,
          count: params.count || 5
        })
        return $http.get(url).then( handleSuccess, handleError );
      }

      var getConversationFiles = function (conversationId) {
        var url = $interpolate(URLS.getConversationFiles)({id: conversationId});

        return $http.get(url).then( handleSuccess );
      }

      var getTemplates = function (id) {
        var url = $interpolate(URLS.getTemplateList)({scope_id : id});
        var req = $http.get(url + '/page/1/9999');
        return req.then( handleSuccess, handleError );
      };

      var addTemplate = function (data) {
        return $http({
          method: 'POST',
          url: URLS.addTemplate,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var updateTemplate = function (data) {
        var url = $interpolate(URLS.modifyTemplate)({template_id : data.id});
        return $http({
          method: 'PUT',
          url: url,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var deleteTemplate = function (id) {
        var url = $interpolate(URLS.modifyTemplate)({template_id : id});
        return $http({
          method: 'DELETE',
          url: url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var getAllGroup = function () {
        var req = $http.get(URLS.messageGroup);
        return req.then( handleSuccess, handleError );
      };

      var getGroup = function (id) {
        var url = $interpolate(URLS.optionMessageGroup)({group_id : id});
        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      var createGroup = function (data) {
        return $http({
          method: 'POST',
          url: URLS.createMessageGroup,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var updateGroup = function (id, data) {
        var url = $interpolate(URLS.optionMessageGroup)({group_id : id});
        return $http({
          method: 'PUT',
          url: url,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var deleteGroup = function (id) {
        var url = $interpolate(URLS.optionMessageGroup)({group_id : id});
        return $http({
          method: 'DELETE',
          url: url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var getGroupUsers = function (id) {
        var url = URLS.messageGroupUsers;
        if (id) {
          url += '/' + id;
        }

        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      var addGroupUser = function (data) {
        return $http({
          method: 'POST',
          url: URLS.messageGroupUsers,
          data: data,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var deleteGroupUser = function (groupId, userId) {
        var url = URLS.messageGroupUsers + '/' + groupId + '/' + userId;
        return $http({
          method: 'DELETE',
          url: url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).then( handleSuccess, handleError );
      };

      var countUnread = function (data) {
        var url = $interpolate(URLS.countUnread)({user_id : data.user_id});

        if (data.scope_id) {
          url += '/scope/' + data.scope_id;
        }

        if (data.source_id) {
          url += '/source/' + data.source_id;
        }

        var req = $http.get(url);
        return req.then( handleSuccess, handleError );
      };

      var countScopeUnread = function (scopeId) {
        var url = $interpolate(URLS.countUnread)({scope_id : scopeId});
        return $http.get(url).then( handleSuccess, handleError );
      };

      var getAllScope = function () {
        var req = $http.get(URLS.getAllScope);
        return req.then( handleSuccess, handleError );
      };

      var getUserMessageScope = function () {
        return $http.get(URLS.getUserMessageScope).then( handleSuccess, handleError );
      };

      var subscribeConversation = function (userId, conversationId) {
        var url = $interpolate(URLS.subscribeConversation)({
          user_id : userId,
          conversation_id: conversationId
        });

        return $http.get(url).then( handleSuccess, handleError );
      };

      var getStudentManagers = function (id) {
        var url = $interpolate(URLS.getStudentManagers)({id : id});
        return $http.get(url).then( handleSuccess, handleError );
      };

      var createStudentManager = function (data) {
        return $http.post(URLS.createStudentManager, data).then( handleSuccess, handleError );
      };

      var deleteStudentManager = function (id) {
        var url = $interpolate(URLS.deleteStudentManager)({id : id});
        return $http.delete(url).then( handleSuccess, handleError );
      };

      var updateStudentManager = function (id, data) {
        var url = $interpolate(URLS.updateStudentManager)({id : id});
        return $http.put(url, data).then( handleSuccess, handleError );
      };

      var sendNotification = function (data) {
        return $http.post(URLS.sendNotification, data).then( handleSuccess, handleError );
      };

      var lastFiveMessages = function () {
        return $http.get(URLS.lastFiveMessages).then( handleSuccess, handleError );
      };

      var getFileMessages = function (fileId) {
        var url = $interpolate(URLS.getFileMessages)({fileId: fileId});
        return $http.get(url).then(handleSuccess, handleError);
      }

      var handleSuccess = function (response) {
        if (response.status === 200) {
          return response.data;
        } else {
          return false;
        }
      };

      var handleError = function (response) {

      };

      var getCategoryScope = function(userId, permissions, groups) {
        var data = [];

        var isTutor = false;
        var isStudent = false;
        var isApplicant = false;

        groups.forEach(function (group) {
          if (group.toLowerCase().indexOf('tutor') > -1) {
            isTutor = true;
          }

          if (group.toLowerCase().indexOf('student') > -1) {
            isStudent = true;
          }

          if (group.toLowerCase().indexOf('applicant') > -1) {
            isApplicant = true;
          }
        });

        data = [
          {scope_id: 1, source_id: 1, expanded: false, topic: 'tutor', show: isStudent},
          {scope_id: 1, source_id: 2, expanded: true, topic: 'finance', show: isStudent},
          {scope_id: 1, source_id: 3, expanded: true, topic: 'course-coordinator', show: isStudent},
          {scope_id: 1, source_id: 4, expanded: true, topic: 'student-support', show: isStudent},
          {scope_id: 1, source_id: 5, expanded: true, topic: 'technical-support', show: isStudent},
          {scope_id: 1, source_id: 6, expanded: true, topic: 'academic-administrator', show: isStudent},
          {scope_id: 1, source_id: 1, expanded: false, topic: 'student', show: (permissions.indexOf('admin') > -1 || isTutor)},
          {scope_id: 2, expanded: false, topic: 'private-messages', show: (permissions.indexOf('messages.messageAnyone.create') > -1 && permissions.indexOf('messages.messageAnyGroup.create') > -1)},
          {scope_id: 3, expanded: false, topic: 'announcements', show: true},
          {expanded: false, topic: 'other', show: permissions.indexOf('admin') > -1},
          {scope_id: 4, expanded: false, topic: 'studio', show: true},
          {expanded: false, topic: 'sent-box', show: permissions.indexOf('messages.messages.sent') > -1}
        ];

        return data;
      };

      return {
        // hard coded json
        getCategoryScope : getCategoryScope,

        // api calls
        getUserConversationsByScope : getUserConversationsByScope,
        getConversations : getConversations,
        getConversation : getConversation,
        getConversationByScopeAndSourceIds: getConversationByScopeAndSourceIds,
        getConversationFromUser : getConversationFromUser,
        getOrCreateTutorConversation: getOrCreateTutorConversation,
        deleteConversation : deleteConversation,
        getMessage: getMessage,
        sendMessage: sendMessage,
        deleteMessage: deleteMessage,
        updateMessage: updateMessage,
        optionMessage: optionMessage,
        deleteConversationUser: deleteConversationUser,
        getTemplates: getTemplates,
        addTemplate: addTemplate,
        updateTemplate: updateTemplate,
        deleteTemplate: deleteTemplate,
        getAllGroup: getAllGroup,
        getGroup: getGroup,
        createGroup: createGroup,
        updateGroup: updateGroup,
        deleteGroup: deleteGroup,
        getGroupUsers: getGroupUsers,
        addGroupUser: addGroupUser,
        deleteGroupUser: deleteGroupUser,
        countUnread: countUnread,
        countScopeUnread: countScopeUnread,
        getAllScope: getAllScope,
        getUserMessageScope: getUserMessageScope,
        subscribeConversation: subscribeConversation,
        getStudentManagers: getStudentManagers,
        createStudentManager: createStudentManager,
        deleteStudentManager: deleteStudentManager,
        updateStudentManager: updateStudentManager,
        sendNotification: sendNotification,
        lastFiveMessages: lastFiveMessages,
        getFileMessages: getFileMessages,
        getConversationMessages: getConversationMessages,
        getConversationFiles: getConversationFiles,
        createConversation: createConversation
      };
    }

    function MessageScope () {
      var obj = {};

      return {
        store: function (key, value) {
          obj[key] = value;
        },
        get: function (key) {
          return obj[key];
        }
      }
    }
})();