(function () {
  'use strict';

  angular
  .module('courses.service', [])
  .service('CoursesService', CoursesService);

  function CoursesService($http, $interpolate, CONFIG) {
    var COURSES_URL = CONFIG.url + 'courses/';
    var STUDYPATH_URL = CONFIG.url + 'studypath/';

    var URLS = {
      getEntities: COURSES_URL +
      '{{entity}}/list' +
      '{{ start >= 0 ? "/page/" + start : "" }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',

      getUniqueProgrammes: COURSES_URL + 'programmes/unique',

      // studypath in courses

      getSemesterProgrammes: COURSES_URL + 'semester/{{id}}/programmes',

      getAllStudypathBySearch: STUDYPATH_URL +
      'list/page/{{start}}' +
      '{{limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',

      attachStudentFile: STUDYPATH_URL + 'module/files',
      deleteStudentFile: STUDYPATH_URL + 'module/files/{{ id }}',

      attachMessageToFile: STUDYPATH_URL + 'module/files/message',
      deleteStudentFileMessage: STUDYPATH_URL + 'module/files/message/{{ id }}',

      getTutorModules: STUDYPATH_URL + 'tutor/modules/{{ id }}',

      statusList: STUDYPATH_URL +
      'status/list' +
      '{{ start ? "/page/" + start : "" }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      updateStudyPathStatus: STUDYPATH_URL + 'status',

      getContentFlagsByStudyPath: COURSES_URL + 'content-flag/section/{{section_id}}/study_path/{{study_path_id}}',
      flagContent: COURSES_URL + 'content-flag{{ id ? "/" + id : "" }}',

      // learning path in courses
      sectionsList: COURSES_URL + 'sections/list/filter/get.module=false/get.user=false' +
      '/get.content={{ !!content }}' +
      '/get.contentCount={{ !!contentCount }}' +
      '{{ id ? "/id=" + id : "" }}' +
      '{{ parentId >= 0 ? "/parent_id=" + parentId : "" }}' +
      '{{ moduleId ? "/module.id=" + moduleId : "" }}',

      courseSupportDocuments: COURSES_URL + 'files/list' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',

      // Errors in courses

      sendError: COURSES_URL + 'error{{ id ? "/" + id : "" }}',

      // Assessment example files in courses

      contentExampleFile: COURSES_URL + 'content-example-file{{ id ? "/" + id : "" }}',

      // Assessment Examples in courses

      getAssessmentExamples: COURSES_URL +
      'assessment-example/list/page/{{start}}' +
      '{{limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',

      assessmentExampleFile: COURSES_URL + 'assessment-example-file{{ id ? "/" + id : "" }}',

      moduleAssessmentExample: COURSES_URL + 'module-assessment-example{{ id ? "/" + id : "" }}',

      assessmentExample: COURSES_URL + 'assessment-example{{ id ? "/" + id : "" }}',

      getCourseTypes: COURSES_URL + 'course-typesCRUD',
    };

    var toParams = function (obj) {
      var p = [];
      for (var key in obj) {
        p.push(key + '=' + encodeURIComponent(obj[key]));
      }
      return p.join('&');
    };

    var handleSuccess = function (res) {
      if (res.status == 204) {
        return res;
      }
      return res.data;
    };

    var handleError = function (res) {
      return res;
    };
    var studioFaq = [
      {
        title: 'How do I send messages to my tutor?',
        trustedText: 'You can message your tutor via the message box at the top of the Lab page. Simply type your message in the text box and click the “Send” button.'
      },
      {
        title: 'Is it possible to edit or delete a message once it has been sent?',
        trustedText: 'Messages can be deleted or edited within a ten minute period starting immediately after posting. After this time you will not be able to delete or edit a message.'
      },
      {
        title: 'I would like to personalise and emphasise parts of my messages, are there any options to customise text?',
        trustedText: 'Yes. There is a toolbar above the text input box that allows you to customise your text and message layout.'
      },
      {
        title: 'How do I view files that were uploaded in previous activities?',
        trustedText: 'Using the Learning Path on the left hand side of the page you can view files from previous activities. They are categorised by activity title.'
      }
    ];

    var learningPathFaq = [
      {
        title: 'How do I send a message to my tutor?',
        trustedText: 'You can message your tutor via the message box at the top of the Lab page. Simply type your message in the text box and click the “Send” button.'
      },
      {
        title: 'Is the recommended duration for each activity flexible?',
        trustedText: 'Each activity has a suggested duration. This is a guide to help you manage your time; you may spend more or less time on each activity if you wish.'
      },
      {
        title: 'How do I upload a file for an activity?',
        trustedText: 'To upload a file for an activity you must scroll to the bottom of an activity page. Here you will find the uploader. You can either drag and drop files into this area or click the uploader and browse for files available on your computer.'
      },
      {
        title: 'An error message appears when I attempt to upload a file, what should I do?',
        trustedText: 'If this occurs then please attempt to clear your cookies and cache (this can be done by accessing your browsers history options or by pressing Ctrl + Shift + Delete on your keyboard) and also check that your file is within the file size limit. If the issue continues then please contact Technical Support for assistance.'
      },
      {
        title: 'What is the maximum file size limit when uploading work for an activity?',
        trustedText: 'Currently the file size limit is 20mb per file.'
      },
      {
        title: 'Can I delete files after they have been uploaded?',
        trustedText: 'Yes it is possible to delete files after they have been uploaded. This can be done by clicking the delete button next to the file you wish to remove.'
      }
    ];

    return {
      // okay
      getLearningPathHelp: function() {
        return learningPathFaq;
      },
      // okay
      getStudioHelp: function() {
        return studioFaq;
      },
      // probably okay, uses courses API
      getSemesterProgrammes: function (config) {
        var url = $interpolate(URLS.getSemesterProgrammes)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      // probably okay, replaced with a new one
      getAllStudypathBySearch: function (config) {
        var url = $interpolate(URLS.getAllStudypathBySearch)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      // okay, uses courses API
      getEntities: function (config, entity) {
        var data = config;
        data.entity = entity;
        if (!_.isUndefined(config.start) && !_.isNaN(config.start) && !_.isNull(config.start)) {
          config.start = parseInt(config.start);
        }
        var url = $interpolate(URLS.getEntities)(data);
        return $http.get(url).then(handleSuccess, handleError);
      },
      // okay, uses courses API
      getUniqueProgrammes: function () {
        return $http.get(URLS.getUniqueProgrammes).then(handleSuccess, handleError);
      },
      // ❌ not okay, waiting for backend impl
      attachStudentFile: function (config) {
        return $http.post(URLS.attachStudentFile, config).then(handleSuccess, handleError);
      },
      // ❌ not okay, waiting for backend impl
      deleteStudentFile: function (id) {
        var url = $interpolate(URLS.deleteStudentFile)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      // ❌ not okay, waiting for backend impl
      attachMessageToFile: function (config) {
        return $http.post(URLS.attachMessageToFile, config).then(handleSuccess, handleError);
      },
      // ❌ not okay, waiting for backend impl
      deleteStudentFileMessage: function (id) {
        var url = $interpolate(URLS.deleteStudentFileMessage)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      // okay
      getTutorModules: function (id) {
        var url = $interpolate(URLS.getTutorModules)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      // okay
      sendError: function (config, id) {
        var url = $interpolate(URLS.sendError)({id: id});
        return $http.post(url, config).then(handleSuccess, handleError);
      },
      // okay
      flagContent: function (data, id) {
        var url = $interpolate(URLS.flagContent)({id: id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      // okay
      getContentFlagsByStudyPath: function (section_id, study_path_id) {
        var url = $interpolate(URLS.getContentFlagsByStudyPath)({section_id: section_id, study_path_id: study_path_id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      // okay
      postContentExampleFile: function (data, id) {
        var url = $interpolate(URLS.contentExampleFile)({id: id || ''});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      // okay
      deleteContentExampleFile: function (id) {
        var url = $interpolate(URLS.contentExampleFile)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      // ❌ we don't have this // ASKED VITALIJ
      statusList: function (config) {
        var url = $interpolate(URLS.statusList)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      // ❌ we don't have this
      updateStudyPathStatus: function (data) {
        return $http.post(URLS.updateStudyPathStatus, data).then(handleSuccess, handleError);
      },

      // Assessment Examples
      // probably okay
      getAssessmentExamples: function (config) {
        var url = $interpolate(URLS.getAssessmentExamples)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      // probably okay
      postAssessmentExampleFile: function (data, id) {
        var url = $interpolate(URLS.assessmentExampleFile)({id: id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      // probably okay
      deleteAssessmentExampleFile: function (data, id) {
        var url = $interpolate(URLS.assessmentExampleFile)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      // probably okay
      postModuleAssessmentExample: function (data, id) {
        var url = $interpolate(URLS.moduleAssessmentExample)({id: id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      // probably okay
      deleteModuleAssessmentExample: function (data, id) {
        var url = $interpolate(URLS.moduleAssessmentExample)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      // probably okay
      postAssessmentExample: function (data, id) {
        var url = $interpolate(URLS.assessmentExample)({id: id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      // probably okay
      deleteAssessmentExample: function (data, id) {
        var url = $interpolate(URLS.assessmentExample)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      // probably okay
      getCourseTypes: function () {
        return $http.get(URLS.getCourseTypes).then(handleSuccess, handleError);
      },

      getSectionsList: function (moduleId, parentId, params) {
        parentId = parentId || 0;
        var url = $interpolate(URLS.sectionsList)(angular.extend({
          moduleId: moduleId,
          parentId: parentId,
          content: false,
          contentCount: false
        }, params))

        return $http.get(url).then(handleSuccess, handleError);
      },

      getSectionContent: function (id) {
        var url = $interpolate(URLS.sectionsList)({
          id: id,
          content: true
        })
        return $http.get(url).then(handleSuccess, handleError);
      },
      getCourseSupportDocuments: function (config) {
        var url = $interpolate(URLS.courseSupportDocuments)(config);
        return $http.get(url).then(handleSuccess, handleError);
      }
    };
  }
})();