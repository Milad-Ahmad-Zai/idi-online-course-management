(function () {
  angular
  .module('services.studypath2', [])
  .factory('StudyPath2Service', studypath2Service)

  function studypath2Service ($interpolate, $http, CONFIG) {
    var BASE_URL = CONFIG.url + 'studypath2/';

    var URLS = {
      allStudyPath: BASE_URL + 'studypath/list/page/{{start}}/{{limit}}'
        + '{{ filter ? "/filter/" + filter : "" }}'
        + '{{ sort ? "/sort/" + sort : "" }}',
      studypath: BASE_URL + 'studypath{{ id ? "/" + id : "" }}',
      getStudypathModuleEntry: BASE_URL + 'spmodule/{{ id }}{{ withStudent ? "/withStudent" : ""}}',
      attachFile: BASE_URL + 'file/spmodule/{{ spModuleId }}',
      deleteStudentFile: BASE_URL + 'file/{{ fileId }}',
      attachedFiles: BASE_URL + 'file/spmodule/{{ id }}',
      studentProgrammes: BASE_URL + 'student/{{id}}/programmes',
      getFilesAttachedToActivity: BASE_URL + 'file/spmodule/{{ spModuleId }}/content/{{ contentId }}',
      studentModules: BASE_URL + 'student/{{student_id}}/course/{{course_name_id}}/module',
      tutorModules: BASE_URL + 'tutor/{{id}}/modules',
      otherTutorModules: BASE_URL + 'not_tutor/{{id}}/modules',
      getListOfStudentsForTutor: BASE_URL + 'tutor/{{ tutorId }}/modules/{{ moduleId }}/spmodule/withStudent',
      getListOfStudentsForOtherTutors: BASE_URL + 'not_tutor/{{ tutorId }}/modules/{{ moduleId }}/spmodule/withStudent',
      programmeStatuses: BASE_URL + 'statuses/programme',
      levelStatuses: BASE_URL + 'statuses/level',
      moduleStatuses: BASE_URL + 'statuses/module',
      updateProgrammeStatus: BASE_URL + 'studypath/{{ id }}/status',
      updateLevelStatus: BASE_URL + 'studypath/{{ courseId }}/level/{{ studypathId }}/status',
      updateModuleStatus: BASE_URL + 'spmodule/{{ id }}/status',
      updateStudyPathModule: BASE_URL + 'spmodule/{{ id }}',
      getStudentProgrammesWithModules: BASE_URL + 'student/{{ studentId }}/programmes/withModules'
    };

    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }

    function lastLoginTransform (res) {
      res.data.forEach(function (item) {
        var m = moment(item.last_login).utc();
        if (m.isValid()) {
          item.lastLogin = m.toDate();
          item.lastLoginDuration = m.fromNow(true)
        } else {
          item.lastLoginDuration = '-'
        }
      });

      return res;
    }

    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Study_Path-SearchStudyPaths
       * @param {Object} params
       * @param {String} params.filter - filter query
       * @param {String} params.sort - sort query
       *
       * @example
       * filter: 'user_id=123/module.module_name_id=module_1_69/semester_id=1'
       */
      getAllStudyPath: function (params) {
        return $http.get($interpolate(URLS.allStudyPath)(params))
          .then(handleSuccess, handleError)
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Student-StudentCourseModule
       * @param  {Number} studentId - uniq id of student
       * @param  {String} courseNameId - uniq name id of course. Ex.: access_to_biomedical_sciences_2
       */
      getStudentModules: function (studentId, courseNameId) {
        return $http.get($interpolate(URLS.studentModules)({
          student_id: studentId,
          course_name_id: courseNameId
        }))
          .then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Tutor-TutorModules
       * @param {Number} id - tutor user id
       */
      getTutorModules: function (id) {
        return $http.get($interpolate(URLS.tutorModules)({id: id}))
          .then(handleSuccess, handleError);
      },

      /**
       * Returns modules of all tutors except this one that have been passed as argument
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Tutor-NotTutorModules
       * @param  {Number} id - id of tutor
       */
      getOtherTutorModules: function (id) {
        return $http.get($interpolate(URLS.otherTutorModules)({id: id}))
          .then(handleSuccess, handleError);
      },

      /**
       * Returns list of student programmes and nested levels with statuses
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Student-StudentPrograms
       * @param  {Number} id - user id of the student
       * @return {Array} - list of programmes
       */
      getStudentProgrammes: function (id) {
        return $http.get($interpolate(URLS.studentProgrammes)({id: id}))
          .then(handleSuccess, handleError);
      },

      /**
       * Attach student file to content
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-File-AttachFileToStudyPathModule
       * @param  {Number} id - Unique study path module ID
       * @param  {Object} params - params of the file
       * @param  {Number} params.content_id - Content ID
       * @param  {Number} params.file_id - File ID
       * @param  {Number} params.created_by - User ID who created this
       * @return {Array} - list of programmes
       */

      attachFile: function (id, params) {
        return $http.post($interpolate(URLS.attachFile)({spModuleId: id}), params)
          .then(handleSuccess, handleError);
      },

      /**
       * Delete student file from content
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Files-DeleteStudentFilesFromContent
       * @param  {Number} id - file id of the file
       */

      deleteStudentFile: function (id) {
        return $http.delete($interpolate(URLS.deleteStudentFile)({fileId: id}))
          .then(handleSuccess, handleError);
      },

      /**
       * Returns list of files attached to StudyPathModule
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-File-GetStudentFilesFromContent
       * @param  {Number} id  - SpModuleId
       */
      getAttachedFiles: function (id) {
        return $http.get($interpolate(URLS.attachedFiles)({id: id}))
          .then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Study_Path-CreateStudyPath
       * @param {Object} params
       * @param {Number} params.user_id
       * @param {Number} params.created_by
       * @param {Number} params.semester_id
       * @param {Number} params.programme_id
       * @param {Number} params.course_type
       */
      createNewStudyPath: function (params) {
        return $http.post($interpolate(URLS.studypath)({id: ''}), params)
          .then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Study_Path-UpdateStudyPath
       * @param {Number} id - unique study path id
       */
      updateStudyPath: function (id, config) {
        return $http.put($interpolate(URLS.studypath)({id: id}), config)
          .then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Study_Path-DeleteStudyPath
       * @param {Number} id - unique study path id
       */
      deleteStudyPath: function (id) {
        return $http.delete($interpolate(URLS.studypath)({id: id}))
          .then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Study_Path-DeleteStudyPath
       * @param {Number} id - unique study path id
       */
      getStudypathModuleEntry: function (id, withStudent) {
        return $http.get($interpolate(URLS.getStudypathModuleEntry)({id: id, withStudent: withStudent}))
          .then(handleSuccess);
      },

      getFilesAttachedToActivity: function (spModuleId, contentId) {
        var url = $interpolate(URLS.getFilesAttachedToActivity)({ spModuleId: spModuleId, contentId: contentId });
        return $http.get(url)
          .then(handleSuccess, handleError);
      },

      /**
       * Returns list of students that belong to tutor with passes *tutorId*
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Tutor-TutorModuleWithStudents
       * @param  {Number} tutorId  - user id of tutor
       * @param  {Number} moduleId - id of module
       */
      getListOfStudentsForTutor: function (tutorId, moduleId) {
        var url = $interpolate(URLS.getListOfStudentsForTutor)({ tutorId: tutorId, moduleId: moduleId });
        return $http.get(url)
          .then(handleSuccess, handleError);
      },

      /**
       * Returns students that don't belong to tutor with passed *tutorId*
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Tutor-NotTutorModuleWithStudents
       * @param  {Number} tutorId  - user id of tutor
       * @param  {Number} moduleId - id of module
       */
      getListOfStudentsForOtherTutors: function (tutorId, moduleId) {
        var url = $interpolate(URLS.getListOfStudentsForOtherTutors)({ tutorId: tutorId, moduleId: moduleId });
        return $http.get(url)
          .then(handleSuccess, handleError);
      },

      /**
       * Returns programme's statuses
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-statuses-ProgrammeStatuses
       */
      getProgrammeStatuses: function () {
        return $http.get(URLS.programmeStatuses)
          .then(handleSuccess, handleError);
      },

      /**
       * Returns level's statuses
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-statuses-LevelStatuses
       */
      getLevelStatuses: function () {
        return $http.get(URLS.levelStatuses)
          .then(handleSuccess, handleError);
      },

      /**
       * Returns module's statuses
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-statuses-ModuleStatuses
       */
      getModuleStatuses: function () {
        return $http.get(URLS.moduleStatuses)
          .then(handleSuccess, handleError);
      },

      /**
       * Updates programme's status
       *
       * @param {Object} params
       * @param {Number} params.created_by - User ID who created this
       * @param {String} params.status - Status for update
       * @param {Number} id - Programme Id
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-Study_Path-UpdateStatus
       */
      updateProgrammeStatus: function (params, id) {
        var url = $interpolate(URLS.updateProgrammeStatus)({ id: id });
        return $http.post(url, params)
          .then(handleSuccess, handleError);
      },

      /**
       * Updates level's status
       *
       * @param {Object} params
       * @param {Number} params.created_by - User ID who created this
       * @param {String} params.status - Status for update
       * @param {String} courseId - Course ID (cour_id)
       * @param {Number} studypathId - Study Path ID
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/#api-statuses-ModuleStatuses
       */
      updateLevelStatus: function (params, courseId, studypathId) {
        var url = $interpolate(URLS.updateLevelStatus)({ courseId: courseId, studypathId: studypathId });
        return $http.post(url, params)
          .then(handleSuccess, handleError);
      },

      // TODO: update url when it's done
      /**
       * Update the status of Study Path Module
       *
       * @param {Object} params
       * @param {Number} params.created_by - User ID who created this
       * @param {String} params.status - Status for update
       * @param {Number} id - Module Id
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/
       */
      updateModuleStatus: function (params, id) {
        var url = $interpolate(URLS.updateModuleStatus)({ id: id });
        return $http.post(url, params)
          .then(handleSuccess, handleError);
      },

      /**
       * Update study path module
       *
       * @param {Object} params
       * @param {Number} id - Module Id
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/
       */
      updateStudyPathModule: function (params, id) {
        var url = $interpolate(URLS.updateStudyPathModule)({ id: id });
        return $http.put(url, params)
          .then(handleSuccess, handleError);
      },

      /**
       * Update study path module
       *
       *
       * @see http://docs.idi-demo.com/apidocs/idi-ms-studypath2/docs/
       */
      getStudentProgrammesWithModules: function (studentId) {
        var url = $interpolate(URLS.getStudentProgrammesWithModules)({ studentId: studentId });
        return $http.get(url)
          .then(handleSuccess);
      }


    };
  }
})();
