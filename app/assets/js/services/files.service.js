(function () {
  'use strict';

  angular
  .module('files.service', [])
  .service('FilesService', FilesService);

  function FilesService ($http, CONFIG, $interpolate, FileUploader) {
    var BASE_URL = CONFIG.url + 'filemanager/';

    var URLS = {
      uploadFile: BASE_URL + 'file/upload/{{ type }}/{{ entity_id }}',
      createMetaForFile: BASE_URL + 'file/meta/{{ type }}/{{ entity_id }}',
      file: BASE_URL + 'file/{{ id }}',
      updateFile: BASE_URL + 'file/{{ id }}/update/',
      folder: BASE_URL + 'folder' +
      '{{ id || id === 0 ? "/" + id : "" }}' +
      '{{ start >= 0 ? "/page/" + start : "" }}' +
      '{{ limit ? "/" + limit : "" }}',
      foldersList: BASE_URL + 'files/folder/{{ id }}' +
      '{{ start >= 0 ? "/page/" + start : "" }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      folderFiles: BASE_URL +
      'folders{{ id || id === 0 ? "/" + id : "" }}/list' +
      '{{ start >= 0 ? "/page/" + start : "" }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getBulkFiles: BASE_URL + 'files/bulk/',
      getFileUrl: BASE_URL + 'file/{{id}}/share/',
      getFileList: BASE_URL + 'files/',
      deleteFile: BASE_URL + 'file/{{id}}'
    };

    var handleSuccess = function (response) {
      return response.data;
    };

    var handleError = function (response) {
      return response;
    };

    return {
      createFileMeta: function (type, entity_id, data) {
        var url = $interpolate(URLS.createMetaForFile)({
          type: type,
          entity_id: entity_id
        });
        return $http.post(url, data)
          .then(handleSuccess, handleError)
      },
      uploadBlob: function (type, entity_id, blob, filename) {
        var url = $interpolate(URLS.uploadFile)({
          type: type,
          entity_id: entity_id
        });
        var formData = new FormData();
        if (filename) {
          formData.append('file', blob, filename);
        } else {
          formData.append('file', blob);
        }
        return $http.post(url, formData, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        }).then(handleSuccess, handleError)
      },
      getUploader: function (type, entity_id, autoUpload) {
        var url = $interpolate(URLS.uploadFile)({
          type: type,
          entity_id: entity_id
        });

        url = url.replace('http://URLS.idi-demo.com/', CONFIG.url);

        return new FileUploader({
          url: url,
          autoUpload: autoUpload
        })
      },
      getFile: function (id) {
        var url = $interpolate(URLS.file)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      getFileList: function(filetype,userID) {
        var data = {
          type: filetype,
          user_id: userID
        };
        return $http.post(URLS.getFileList, data).then(handleSuccess);
      },
      deleteFile: function (id) {
        var url = $interpolate(URLS.file)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getBulkFiles: function (ids) {
        var data = {
          ids: ids
        };
        return $http.post(URLS.getBulkFiles, data).then(handleSuccess, handleError);
      },
      getFileUrl: function (id) {
        var url = $interpolate(URLS.getFileUrl)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      getFolder: function (config) {
        var url = $interpolate(URLS.folder)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      deleteFolder: function (id) {
        var url = $interpolate(URLS.folder)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      updateFolder: function (id, data) {
        var url = $interpolate(URLS.folder)({id: id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      createFolder: function (data) {
        var url = $interpolate(URLS.folder)({id: ''});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      getFilesInFolder: function (config) {
        var opt = config;
        if (!opt.start) {
          opt.start = 0;
        }
        var url = $interpolate(URLS.folderFiles)(opt);

        return $http.get(url).then(handleSuccess, handleError);
      },
      getFoldersList: function (config) {
        var opt = config;
        if (!opt.start) {
          opt.start = 0;
        }
        var url = $interpolate(URLS.foldersList)(opt);

        return $http.get(url).then(handleSuccess, handleError);
      },
      updateFile: function (id, data) {
        var url = $interpolate(URLS.updateFile)({id: id});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      deleteFile: function(id) {
        var url = $interpolate(URLS.deleteFile)({id: id});
        return $http.delete(url).then(handleSuccess)
      }
    };

  }

})();