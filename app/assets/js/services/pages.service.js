(function () {
  'use strict';

  angular
  .module('pages.service', [])
  .service('PagesService', pagesService);

  function pagesService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/
     */
    // TODO: Use config instead of hardcoded string
    var BASE_URL = CONFIG.url + 'pages/';

    var URLS = {
      getPageBySlug: BASE_URL + 'page/{{ slug }}',
      createNewPage: BASE_URL + 'page',
      getPagesByTag: BASE_URL + 'tag/{{tagName}}/page',
      updatePage: BASE_URL + 'page/{{ id }}'
    };

    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }

    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-GetPageBySlug
       * @param {string} slug - Unique page slug
       */
      getPageBySlug: function (slug) {
        var url = $interpolate(URLS.getPageBySlug)({slug: slug});
        return $http.get(url).then(handleSuccess);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-CreateNewPage
       * @param {Object} pageData
       * @param {String} pageData.title
       * @param {String} pageData.slug
       * @param {String} pageData.body
       * @param {Array} pageData.tags
       * @param {Number} pageData.parent
       */
      createNewPage: function (pageData) {
        return $http.post(URLS.createNewPage,pageData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Tag-TagPages
       * @param {string} tagName - Unique tag name
       */
      getPagesByTag: function (tagName) {
        var url = $interpolate(URLS.getPagesByTag)({tagName: tagName});
        return $http.get(url).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-UpdatePage
       * @param {Object} pageData
       * @param {Number} pageData.id - id page
       * @param {String} pageData.title
       * @param {String} pageData.slug
       * @param {String} pageData.body
       * @param {Array} pageData.tags
       * @param {Number} pageData.parent
       */
      updatePage: function (pageData, id) {
        var url = $interpolate(URLS.updatePage)({id: id});
        return $http.put(url,pageData).then(handleSuccess, handleError);
      }
    };
  }
})();