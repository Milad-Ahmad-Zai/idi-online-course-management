(function () {
  'use strict';

  angular
    .module('profiles.service', [])
    .service('ProfilesService', ProfilesService);

  function ProfilesService($http, $interpolate, CONFIG) {
    var BASE_URL = CONFIG.url + 'profiles/';

    var URLS = {
      profile: BASE_URL + 'profile/{{ user_id }}',
      basicProfile: BASE_URL + 'profile/{{ user_id }}/basic/',
      publicProfile: BASE_URL + 'profile/{{ user_id }}/public/',
      getProfileFields: BASE_URL + 'profileFields/',
      getProfileFieldsList: BASE_URL + 'field/list/filter/section=personal/export=1',
      profileImage: BASE_URL + 'profile/{{ id }}/image/',
      coverImage: BASE_URL + 'profile/{{ id }}/cover/',
      getProfileVersions: BASE_URL + 'profile/{{ id }}/versions/{{ start }}/{{ limit }}',
      restoreProfileVersion: BASE_URL + 'profile/restore/version/{{ versionId }}',
      getProgrammeList: BASE_URL + 'programmeList/',
      UH: BASE_URL + 'uh/{{userId}}/{{programmeId}}/',
      getBulkBasicProfiles: BASE_URL + 'profiles/basic/bulk',
      getBulkProfiles: BASE_URL + 'profiles/bulk'
    };

    var getProfile = function (id, params) {
      var defaults = {
        cache: false
      };
      var url = $interpolate(URLS.profile)({user_id: id});
      var req = $http.get(url, angular.extend({}, defaults, params));
      return req.then(handleSuccess, handleError);
    };

    var handleSuccess = function (response) {
      return response.data;
    };

    var handleError = function (response) {
      return response;
    };

    return {
      getProfile: getProfile,
      getBasicProfile: function (id) {
        var url = $interpolate(URLS.basicProfile)({user_id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      getPublicProfile: function (id) {
        var url = $interpolate(URLS.publicProfile)({user_id: id});
        return $http.get(url).then(handleSuccess)
      },
      updateProfile: function (id, profile) {
        var url = $interpolate(URLS.profile)({user_id: id});
        return $http({
          method: 'POST',
          url: url,
          data: profile,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(handleSuccess, handleError);
      },
      getProfileFields: function () {
        return $http.get(URLS.getProfileFields, {}).then(handleSuccess, handleError);
      },
      getProfileFieldsList: function () {
        return $http.get(URLS.getProfileFieldsList, {}).then(handleSuccess, handleError);
      },
      getProfileImage: function (id) {
        var url = $interpolate(URLS.profileImage)({id: id});
        return $http.get(url, {}).then(handleSuccess, handleError);
      },
      deleteProfileImage: function (id) {
        var url = $interpolate(URLS.profileImage)({id: id});
        return $http.delete(url, {}).then(handleSuccess, handleError);
      },
      deleteProfile: function (id) {
        var url = $interpolate(URLS.profile)({id: id});
        return $http.delete(url, {}).then(handleSuccess, handleError);
      },
      updateProfileImage: function (profile_id, file_id) {
        var url = $interpolate(URLS.profileImage)({id: profile_id});
        return $http.post(url, {file_id: file_id}).then(handleSuccess, handleError);
      },
      getCoverImage: function (id) {
        var url = $interpolate(URLS.coverImage)({id: id});
        return $http.get(url, {}).then(handleSuccess, handleError);
      },
      deleteCoverImage: function (id) {
        var url = $interpolate(URLS.coverImage)({id: id});
        return $http.delete(url, {}).then(handleSuccess, handleError);
      },
      updateCoverImage: function (profile_id, file_id) {
        var url = $interpolate(URLS.coverImage)({id: profile_id});
        return $http.post(url, {file_id: file_id}).then(handleSuccess, handleError);
      },
      getProfileVersions: function (id, start, limit) {
        if (!start) start = 0;
        if (!limit) limit = 20;
        var url = $interpolate(URLS.getProfileVersions)({id: id, start: start, limit: limit});
        return $http.get(url).then(handleSuccess, handleError);
      },
      restoreProfileVersion: function (versionId) {
        var url = $interpolate(URLS.restoreProfileVersion)({versionId: versionId});
        return $http.post(url, {}).then(handleSuccess, handleError);
      },
      getProgrammeList: function () {
        return $http.get(URLS.getProgrammeList).then(handleSuccess, handleError);
      },
      getUH: function (programmeId, userId) {
        var url = $interpolate(URLS.UH)({userId: userId, programmeId: programmeId});
        return $http.get(url).then(handleSuccess, handleError);
      },
      saveUH: function (programmeId, userId, data) {
        var url = $interpolate(URLS.UH)({userId: userId, programmeId: programmeId});
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      getBulkBasicProfiles: function(ids) {
        var data = {
          ids: ids
        };
        return $http.post(URLS.getBulkBasicProfiles,data).then(handleSuccess);
      },
      getBulkProfiles: function(ids) {
        var data = {
          ids: ids
        };
        return $http.post(URLS.getBulkProfiles,data).then(handleSuccess);
      }
    }

  }
})();