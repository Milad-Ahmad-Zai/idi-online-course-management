(function () {
  'use strict';

  angular
  .module('forms.service', [])
  .service('FormsService', formsService);

  function formsService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms
     */
    // TODO: Use config instead of hardcoded string
    var BASE_URL = CONFIG.url + 'profiles/form';

    var URLS = {
      addForm: BASE_URL,
      updateForm: BASE_URL + '/{{ id }}',
      deleteForm: BASE_URL + '/{{ id }}',
      getFormBySlug: BASE_URL + '/slug/{{slug}}',
      getFormList: BASE_URL + '/list/page/{{start}}/{{limit}}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}'
    };

    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }

    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms-PostForm
       * @param {Object} formData - Form Object.
       * @param {string} formData.title - Title Form
       * @param {string} formData.slug - Slug Form
       * @param {string} formData.body - Body Form
       */
      addForm: function (formData) {
        return $http.post(URLS.addForm,formData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms-PostForm
       * @param {Object} formData - Form Object.
       * @param {string} formData.id  - Form unique ID.
       * @param {string} formData.title - Title Form
       * @param {string} formData.slug - Slug Form
       * @param {string} formData.body - Body Form
       * @param {string} id - ID Form for update
       */
      updateForm: function (formData, id) {
        var url = $interpolate(URLS.updateForm)({id: id});
        return $http.post(url,formData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms-PostForm
       * @param {Object} formData - Form Object.
       * @param {string} formData.id  - Form unique ID.
       * @param {string} formData.title - Title Form
       * @param {string} formData.slug - Slug Form
       * @param {string} formData.body - Body Form
       * @param {string} id - ID Form for update
       */
      deleteForm: function (id) {
        var url = $interpolate(URLS.deleteForm)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms-PostForm
       * @param {Object} formData - Form Object.
       * @param {string} formData.id  - Form unique ID.
       * @param {string} formData.title - Title Form
       * @param {string} formData.slug - Slug Form
       * @param {string} formData.body - Body Form
       * @param {string} id - ID Form for update
       */
      getFormBySlug: function (slug) {
        var url = $interpolate(URLS.getFormBySlug)({slug: slug});
        return $http.get(url).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms-GetFormList
       */
      getFormList: function(filter){
        var defaults = {
          start: 0,
          limit: 100,
          page: 1
        };
        return $http.get($interpolate(URLS.getFormList)(angular.extend({}, defaults, filter)))
          .then(handleSuccess, handleError)
      }
    };
  }
})();