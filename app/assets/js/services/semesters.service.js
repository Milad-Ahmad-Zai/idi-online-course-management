(function () {
  'use strict';

  angular
  .module('semesters.service', [])
  .service('SemestersService', SemestersService);

  function SemestersService ($http, $interpolate, CONFIG) {
    var BASE_URL = CONFIG.url + 'semesters/semesters/';

    var URLS = {
      semester: BASE_URL,
      deleteSemester: BASE_URL + '{{ id }}',
      getSemestersList: BASE_URL + 'list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getSemester: BASE_URL + '{{ id }}'
    };

    var handleSuccess = function (response) {
      if (response.status === 200) {
        return response.data;
      } else {
        return false;
      }
    };

    var handleError = function (response) {

    };

    return {

      // semesters
      createSemester: function (data) {
        return $http.post(URLS.semester, data).then(handleSuccess, handleError);
      },
      deleteSemester: function (id) {
        var url = $interpolate(URLS.deleteSemester)({ id : id });
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getSemestersList: function (config) {
        var url = $interpolate(URLS.getSemestersList)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      getSemester: function (id) {
        var url = $interpolate(URLS.getSemester)({ id : id });
        return $http.get(url).then(handleSuccess, handleError);
      },
      updateSemester: function (data) {
        return $http.get(URLS.semester, data).then(handleSuccess, handleError);
      }
    };
  }


})();