(function () {
  'use strict';

  angular
  .module('salesforce.service', [])
  .service('SalesforceService', SalesforceService);

  function handleSuccess (response) {
    return response.data.data;
  }

  function handleError (response) {
    return response;
  }

  function SalesforceService ($http, $filter, CONFIG) {
    var BASE_URL = CONFIG.url + 'salesforce/';

    var URLS = {
      sfGetUserList: BASE_URL + 'task/users',
      sfAddTask: BASE_URL + 'task'
    };

    return {
      getUsersList: function () {
        return $http.get(URLS.sfGetUserList)
        .then(handleSuccess, handleError);
      },

      /**
       * @param {Object} params
       * @param {String} params.profile_id - user id which is task about
       * @param {String} params.subject - task subject
       * @param {String} params.comment - task content
       * @param {String} params.assigned_to - sf_id of user
       * @param {Date} params.due_date
       * @param {Date} params.reminder_date
       */
      addTask: function (params) {
        if (params.due_date instanceof Date) {
          params.due_date = $filter('date')(params.due_date, 'yyyy-MM-dd')
        }
        if (params.reminder_date instanceof Date) {
          params.reminder_date = $filter('date')(params.reminder_date, 'yyyy-MM-dd hh:mm:ss')
        }

        return $http.post(URLS.sfAddTask, params)
          .then(handleSuccess, handleError);
      }
    }
  }
})()