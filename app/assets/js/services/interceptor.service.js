(function () {
  'use strict';

  angular
    .module('interceptor.service', [])
    .factory('InterceptorService', function($window, $rootScope, $q, $injector, CONFIG) {
      function extractDomain(url) {
        var domain;
        if (url.indexOf("://") > -1) {
          domain = url.split('/')[2];
        } else {
          domain = url.split('/')[0];
        }
        domain = domain.split(':')[0];
        return domain;
      }

      return {
        request: function (config) {
          config.headers = config.headers || {};
          var extension = config.url.split('.')[(config.url.split('.').length-1)];
          var AuthService = $injector.get('AuthService');
          var $http = $injector.get('$http');

          var ignoreUrls = [
            'auth/token',
            'auth/register',
            'passwordReset',
            'remindEmail',
            /permissions\/available\/\S*\/\S*/, // -> permissions/available/client/abc123
            /\.json$/
          ]

          var ignored = false;
          for (var i = 0; i < ignoreUrls.length; i++) {
            var pattern = new RegExp(ignoreUrls[i]);
            if (pattern.test(config.url)) {
              ignored = true;
              break;
            }
          }

          var addingToken = function () {
            if (!ignored) {
              if ($window.localStorage.token && JSON.parse($window.localStorage.token).timestamp >= new Date().getTime()) {
                if(config.url.indexOf('?') !== -1){
                  config.url = config.url + '&access_token=' + JSON.parse($window.localStorage.token).access_token;
                }else{
                  config.url = config.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
                }
                return config;
              } else {
                var interceptorDeferred = $q.defer();

                AuthService
                  .getAccessToken('refresh_token', CONFIG.client)
                  .then(interceptorDeferred.resolve, interceptorDeferred.reject);

                return interceptorDeferred.promise.then(function() {
                  config.url = config.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
                  return config;
                });
              }
            } else {
              return config;
            }
          };

          var interceptor = function () {
            if (CONFIG.url.indexOf(domain) > -1 && domain.length > 0) {
              return addingToken();
            } else {
              if (domain) {
                var url = CONFIG.url;
                config.url = url.substring(0, url.length - 1) + config.url.split(domain)[1];
              }
              return addingToken();
            }
          };

          var domain = extractDomain(config.url);
          if (extension === 'html') {
            return config;
          } else {
            return interceptor();
          }
        },
        response: function (response) {
          if (response.status === 401) {
            // handle the case where the user is not authenticated
          }
          return response || $q.when(response);
        },
        'responseError': function(response) {
          // Session has expired
          if (response.status == 419) {
            var AuthService = $injector.get('AuthService');
            var $http = $injector.get('$http');
            var deferred = $q.defer();

            AuthService.getAccessToken('refresh_token', CONFIG.client).then(deferred.resolve, deferred.reject);

            return deferred.promise.then(function() {
              return $http(response.config);
            });
          }
          return $q.reject(response);
        }
      };
    });
})();