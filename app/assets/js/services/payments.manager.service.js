(function () {
  'use strict';

  angular
  .module('payments.manager.courses.service', [])
  .service('PaymentsManagerCoursesService', PaymentsManagerCoursesService);

  function PaymentsManagerCoursesService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/
     */
    var BASE_URL = CONFIG.url + 'payments/';

    var URLS = {
      getCoursesList: BASE_URL + 'course',
      updateCourse: BASE_URL + 'course/{{id}}',
      deleteCourse: BASE_URL + 'course/{{id}}',
      createNewCourse: BASE_URL + 'course'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-PagesList
       * @param 
       */
      getCoursesList: function () {
        return $http.get(URLS.getCoursesList).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-UpdatePage
       * @param {number} courseData.id
       * @param {String} courseData.title
       * @param {String} courseData.note
       * @param {bolean} courseData.archived
       */
       
      saveCourse: function (courseData) {
        //update page
        if(courseData.id){
            var url = $interpolate(URLS.updateCourse)({id: courseData.id});
            return $http.put(url, courseData).then(handleSuccess, handleError);
        }else{
            //create new page
            return $http.post(URLS.createNewCourse, courseData).then(handleSuccess, handleError);
        }
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-DeletaPage
       * @param {number} id
       */
      deleteCourse: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deleteCourse)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();