(function () {
  'use strict';

  angular
  .module('payments.manager.cards.service', [])
  .service('PaymentsManagerCardsService', PaymentsManagerCardsService);

  function PaymentsManagerCardsService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-CodeDiscount
     */
    var BASE_URL = CONFIG.url + 'payments/customer/';
    var STRIPE_URL = CONFIG.url + 'payments/';
    var CARD_URL = CONFIG.url + 'payments/customer';

    var URLS = {
      getCardsByUserId: BASE_URL + 'user/{{id}}',
      updateCard: BASE_URL + 'user/{{id}}',
      deleteCard: BASE_URL + 'user/{{id}}',
      createNewCard: CARD_URL,
      removeCard: CARD_URL + "/card",
      makeCardDefault: CARD_URL + "/defaultCard",
      getStripePublicKey: STRIPE_URL + 'key'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      
      getCardsByUserId: function (id) {
        var url = $interpolate(URLS.getCardsByUserId)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },

      getStripePublicKey: function() {
        return $http.get(URLS.getStripePublicKey).then(handleSuccess, handleError);
      },

      createNewCard: function (cardData) {
        //create new card
          return $http.post(URLS.createNewCard, cardData).then(handleSuccess, handleError);
      },

      makeCardDefault: function (cardData) {
          return $http.post(URLS.makeCardDefault, cardData).then(handleSuccess, handleError);
      },

      removeCard: function (requestData) {
          return $http({
              method: 'DELETE',
              url: URLS.removeCard,
              data: requestData,
              headers: {
                  'Content-type': 'application/json;charset=utf-8'
              }
          })
          .then(handleSuccess);
      },
       
      updateDiscount: function (discountData) {
        //update page
        var url = $interpolate(URLS.updateDiscount)({code: discountData.code});
        return $http.put(url, discountData).then(handleSuccess, handleError);
      },

      deleteDiscount: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deleteDiscount)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();