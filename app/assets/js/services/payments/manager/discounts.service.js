(function () {
  'use strict';

  angular
  .module('payments.manager.discounts.service', [])
  .service('PaymentsManagerDiscountsService', PaymentsManagerDiscountsService);

  function PaymentsManagerDiscountsService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-CodeDiscount
     */
    var BASE_URL = CONFIG.url + 'payments/';

    var URLS = {
      getDiscountsList: BASE_URL + 'code_discount',
      updateDiscount: BASE_URL + 'code_discount/{{code}}',
      deleteDiscount: BASE_URL + 'code_discount/{{code}}',
      createNewDiscount: BASE_URL + 'code_discount'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-CodeDiscount-GetCodeDiscountList
       * @param 
       */
      getDiscountsList: function (config) {
        var urlParams = URLS.getDiscountsList +
            '?start={{start}}' +
            '{{limit ? "&limit=" + limit : "" }}' +
            '{{ sort ? "&sort[]=" + sort : "" }}' +
            '{{ filter ? "&filter[]=" + filter : "" }}';

        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(urlParams)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} courseData.id
       * @param {String} courseData.title
       * @param {String} courseData.note
       * @param {bolean} courseData.archived
       */
       
      saveDiscount: function (discountData) {
        //create new page
          return $http.post(URLS.createNewDiscount, discountData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} courseData.id
       * @param {String} courseData.title
       * @param {String} courseData.note
       * @param {bolean} courseData.archived
       */
       
      updateDiscount: function (discountData) {
        //update page
        var url = $interpolate(URLS.updateDiscount)({code: discountData.code});
        return $http.put(url, discountData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-DeleteCourse
       * @param {number} id
       */
      deleteDiscount: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deleteDiscount)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();