(function () {
  'use strict';

  angular
  .module('payments.manager.courses.service', [])
  .service('PaymentsManagerCoursesService', PaymentsManagerCoursesService);

  function PaymentsManagerCoursesService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/
     */
    var BASE_URL = CONFIG.url + 'payments/';

    var URLS = {
      getCoursesList: BASE_URL + 'course',
      updateCourse: BASE_URL + 'course/{{id}}',
      deleteCourse: BASE_URL + 'course/{{id}}',
      createNewCourse: BASE_URL + 'course'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      getCoursesList: function (config) {
        var urlParams = URLS.getCoursesList +
            '?start={{start}}' +
            '{{limit ? "&limit=" + limit : "" }}' +
            '{{ sort ? "&sort[]=" + sort : "" }}' +
            '{{ filter ? "&filter[]=" + filter : "" }}';

        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(urlParams)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} courseData.id
       * @param {String} courseData.title
       * @param {String} courseData.note
       * @param {bolean} courseData.archived
       */
       
      saveCourse: function (courseData) {
        //create new page
        return $http.post(URLS.createNewCourse, courseData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} courseData.id
       * @param {String} courseData.title
       * @param {String} courseData.note
       * @param {bolean} courseData.archived
       */
       
      updateCourse: function (courseData) {
        //update page
        var url = $interpolate(URLS.updateCourse)({id: courseData.id});
        return $http.put(url, courseData).then(handleSuccess, handleError);
      },      

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-DeleteCourse
       * @param {number} id
       */
      deleteCourse: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deleteCourse)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();