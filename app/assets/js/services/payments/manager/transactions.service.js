(function () {
  'use strict';

  angular
  .module('payments.manager.transactions.service', [])
  .service('PaymentsManagerTransactionsService', PaymentsManagerTransactionsService);

  function PaymentsManagerTransactionsService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/
     */
    var BASE_URL = CONFIG.url + 'payments/';

    var URLS = {
      getTransactionsList: BASE_URL + 'transaction',
      updateTransaction: BASE_URL + 'transaction/{{id}}',
      deleteTransaction: BASE_URL + 'transaction/{{id}}',
      takeTransaction: BASE_URL + 'transaction/{{id}}/take',
      createNewTransaction: BASE_URL + 'transaction',
      getTransactionsListCSV: BASE_URL + 'transaction/csv'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      getTransactionsList: function (config) {
        var urlParams = URLS.getTransactionsList +
            '?start={{start}}' +
            '{{limit ? "&limit=" + limit : "" }}' +
            '{{ sort ? "&sort[]=" + sort : "" }}' +
            '{{ filter ? "&filter[]=" + filter : "" }}';

        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(urlParams)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} planData.id
       * @param {String} planData.title
       * @param {String} planData.note
       * @param {bolean} planData.enabled
       * @param {Number} planData.price
       * @param {Number}  planData.admin_fee.
       * @param {String} planData.admin_fee_type
       * @param   String  start_date
       * @param  Number  course_id
       * @param  String  currency_code
       * @param   Object[]  installments
       * @param  Number (installments) amount
       * @param  String  (installments) date
       * @param Object[] early_discounts
       * @param  String (early_discounts) type (percent, value)
       * @param Number (early_discounts) amount
       * @param String (early_discounts) date  
      */
       
      saveCustomer: function (customerData) {
    
        //create new plan
        return $http.post(URLS.createNewCustomer, customerData).then(handleSuccess, handleError);
    
      },

      updateTransaction: function (customerData) {
        //update plan
    
        var url = $interpolate(URLS.updateTransaction)({id: customerData.id});
        return $http.put(url, customerData).then(handleSuccess, handleError);
    
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      getTransactionsListCSV: function (list) {

        var url = URLS.getTransactionsListCSV + "?filter[]=id=IN=" + list;
        return $http.get(url).then(handleSuccess, handleError);

      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      takeTransaction: function (id) {

        var url = $interpolate(URLS.takeTransaction)({id: id});
        return $http.post(url).then(handleSuccess);

      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-DeleteCourse
       * @param {number} id
       */
      deleteCustomer: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deleteCustomer)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();