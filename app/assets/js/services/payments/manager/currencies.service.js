(function () {
  'use strict';

  angular
  .module('payments.manager.currencies.service', [])
  .service('PaymentsManagerCurrenciesService', PaymentsManagerCurrenciesService);

  function PaymentsManagerCurrenciesService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Currency
     */
    var BASE_URL = CONFIG.url + 'payments/';

    var URLS = {
      getCurrenciesList: BASE_URL + 'currency',
      updateCurrency: BASE_URL + 'currency/{{code}}',
      deleteCurrency: BASE_URL + 'currency/{{code}}',
      createNewCurrency: BASE_URL + 'currency'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Currency-GetCurrenciesList
       * @param 
       */
      getCurrenciesList: function (config) {
        var urlParams = URLS.getCurrenciesList +
            '?start={{start}}' +
            '{{limit ? "&limit=" + limit : "" }}' +
            '{{ sort ? "&sort[]=" + sort : "" }}' +
            '{{ filter ? "&filter[]=" + filter : "" }}';

        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(urlParams)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Currency-CreateNewCurrency
       * @param {number} courseData.id
       * @param {String} courseData.title
       * @param {String} courseData.note
       * @param {bolean} courseData.archived
       */
       
      saveCurrency: function (courseData) {
        //create new currency
        return $http.post(URLS.createNewCurrency, courseData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Currency-CreateNewCurrency
       * @param {number} courseData.id
       * @param {String} courseData.title
       * @param {String} courseData.note
       * @param {bolean} courseData.archived
       */
       
      updateCurrency: function (courseData) {
          //update currency
          var url = $interpolate(URLS.updateCurrency)({code: courseData.code});
          return $http.put(url, courseData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Currency-DeleteCurrency
       * @param {number} id
       */
      deleteCurrency: function (code) {
        var data = {code: code};
        var url = $interpolate(URLS.deleteCurrency)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();