(function () {
  'use strict';

  angular
  .module('payments.manager.plans.service', [])
  .service('PaymentsManagerPlansService', PaymentsManagerPlansService);

  function PaymentsManagerPlansService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/
     */
    var BASE_URL = CONFIG.url + 'payments/';

    var URLS = {
      getPlansList: BASE_URL + 'plan',
      updatePlan: BASE_URL + 'plan/{{id}}',
      deletePlan: BASE_URL + 'plan/{{id}}',
      createNewPlan: BASE_URL + 'plan'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      getPlansList: function (config) {
        var urlParams = URLS.getPlansList +
            '?start={{start}}' +
            '{{limit ? "&limit=" + limit : "" }}' +
            '{{ sort ? "&sort[]=" + sort : "" }}' +
            '{{ filter ? "&filter[]=" + filter : "" }}';

        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(urlParams)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} planData.id
       * @param {String} planData.title
       * @param {String} planData.note
       * @param {bolean} planData.enabled
       * @param {Number} planData.price
       * @param {Number}  planData.admin_fee.
       * @param {String} planData.admin_fee_type
       * @param   String  start_date
       * @param  Number  course_id
       * @param  String  currency_code
       * @param   Object[]  installments
       * @param  Number (installments) amount
       * @param  String  (installments) date
       * @param Object[] early_discounts
       * @param  String (early_discounts) type (percent, value)
       * @param Number (early_discounts) amount
       * @param String (early_discounts) date  
      */
       
      savePlan: function (planData) {
        //create new plan
        return $http.post(URLS.createNewPlan, planData).then(handleSuccess, handleError);
      },

      updatePlan: function (planData) {
        //update plan
        var url = $interpolate(URLS.updatePlan)({id: planData.id});
        return $http.put(url, planData).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-DeleteCourse
       * @param {number} id
       */
      deletePlan: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deletePlan)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();