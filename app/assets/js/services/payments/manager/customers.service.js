(function () {
  'use strict';

  angular
  .module('payments.manager.customers.service', [])
  .service('PaymentsManagerCustomersService', PaymentsManagerCustomersService);

  function PaymentsManagerCustomersService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/
     */
    var BASE_URL = CONFIG.url + 'payments/';
    var AUTH_URL = CONFIG.url + 'auth/users/';

    var URLS = {
      getCustomersList: BASE_URL + 'subscription',
      updateCustomer: BASE_URL + 'subscription/{{id}}',
      deleteCustomer: BASE_URL + 'subscription/{{id}}',
      createNewCustomer: BASE_URL + 'subscription',
      getCustomersListCSV: BASE_URL + 'subscription/csv',
      getUsersList: AUTH_URL + 'list',
      takePayment: BASE_URL + 'subscription',
      recalculatePlan: BASE_URL + 'plan/',
      chargeInstallment: BASE_URL + 'subscription/'
    };

        
    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      getCustomersList: function (config) {
        var urlParams = URLS.getCustomersList +
            '?start={{start}}' +
            '{{limit ? "&limit=" + limit : "" }}' +
            '{{ sort ? "&sort[]=" + sort : "" }}' +
            '{{ filter ? "&filter[]=" + filter : "" }}';

        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(urlParams)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);

      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} planData.id
       * @param {String} planData.title
       * @param {String} planData.note
       * @param {bolean} planData.enabled
       * @param {Number} planData.price
       * @param {Number}  planData.admin_fee.
       * @param {String} planData.admin_fee_type
       * @param   String  start_date
       * @param  Number  course_id
       * @param  String  currency_code
       * @param   Object[]  installments
       * @param  Number (installments) amount
       * @param  String  (installments) date
       * @param Object[] early_discounts
       * @param  String (early_discounts) type (percent, value)
       * @param Number (early_discounts) amount
       * @param String (early_discounts) date  
      */
       
      saveCustomer: function (customerData) {
    
        //create new plan
        return $http.post(URLS.createNewCustomer, customerData).then(handleSuccess, handleError);
    
      },

      updateCustomer: function (customerData) {
        //update plan
    
        var url = $interpolate(URLS.updateCustomer)({id: customerData.id});
        return $http.put(url, customerData).then(handleSuccess, handleError);
    
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      getCustomersListCSV: function (list) {

        var url = URLS.getCustomersListCSV + "?filter[]=id=IN=" + list;
        return $http.get(url).then(handleSuccess, handleError);

      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      getUsersList: function (filter) {
        var url = URLS.getUsersList + "/filter/search=" + filter;
        return $http.get(url).then(handleSuccess, handleError);

      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-CreateNewCourse
       * @param {number} planData.id
       * @param {String} planData.title
       * @param {String} planData.note
       * @param {bolean} planData.enabled
       * @param String (early_discounts) date  
      */
       
      takePayment: function (paymentData) {
    
        //create new plan
        return $http.post(URLS.takePayment, paymentData).then(handleSuccess, handleError);
    
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      recalculatePlan: function (requestData) {
        var url = URLS.recalculatePlan + requestData.id + "/recalculate/" + requestData.code;
        return $http.get(url).then(handleSuccess, handleError);

      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
       * @param 
       */
      chargeInstallment: function (requestData) {
        var url = URLS.chargeInstallment + requestData.subscription_id + "/installment/" + requestData.id + "/take";
        return $http.post(url).then(handleSuccess, handleError);

      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-DeleteCourse
       * @param {number} id
       */
      deleteCustomer: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deleteCustomer)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();