(function () {
  'use strict';

  angular
  .module('payments.student.service', [])
  .service('PaymentsStudentService', PaymentsStudentService);

  function PaymentsStudentService ($http, $interpolate, CONFIG) {

    var BASE_URL = CONFIG.url + 'payments/';

    var URLS = {
      getCourseList: BASE_URL + 'course',
      getPlanList: BASE_URL + 'plan',
      getCurrencyList: BASE_URL + 'currency',
      getRecalculatedPlan: BASE_URL + 'plan/{{id}}/recalculate/{{code}}',
      getRecalculatedPlanNoCode: BASE_URL + 'plan/{{id}}/recalculate',
      addCustomerByCard: BASE_URL + 'customer',
      getUserByID: BASE_URL + 'customer/user/{{id}}',
      setDefaultCard: BASE_URL + 'customer/defaultCard',
      createSubscription: BASE_URL + 'subscription',
      getUserSubscriptions: BASE_URL + 'subscription/user/{{id}}',
      getStripePublicKey: BASE_URL + 'key'
    };


    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Course-GetCoursesList
      **/
      getCourseList: function () {
        return $http.get(URLS.getCourseList).then(handleSuccess, handleError);
      },

      /**
       * http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Plan-GetPlanList
      **/
      getPlanList: function () {
        return $http.get(URLS.getPlanList).then(handleSuccess, handleError);
      },

      /**
       * http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Currency-GetCurrenciesList
      **/
      getCurrencyList: function () {
        return $http.get(URLS.getCurrencyList).then(handleSuccess, handleError);
      },

      /**
       * http://docs.idi-demo.com/apidocs/ms-payments/docs/#api-Plan-GetRecalculatedPlan
       */
      getRecalculatedPlan: function(planID, discountCode) {
        if (discountCode) {
          var url = $interpolate(URLS.getRecalculatedPlan)({id: planID, code: discountCode});
        } else {
          var url = $interpolate(URLS.getRecalculatedPlanNoCode)({id: planID});
        }
        return $http.get(url).then(handleSuccess, handleError);
      },

      addCustomerByCard: function (data) {
        return $http.post(URLS.addCustomerByCard,data).then(handleSuccess, handleError);
      },

      getUserByID: function(userID) {
        var url = $interpolate(URLS.getUserByID)({id: userID});
        return $http.get(url).then(handleSuccess, handleError);
      },

      setDefaultCard: function (data) {
        return $http.post(URLS.setDefaultCard,data).then(handleSuccess, handleError);
      },

      createSubscription: function(data) {
        return $http({
          method: 'POST',
          data: data,
          url: URLS.createSubscription,
          headers: {
            'Content-Type': 'application/json'
          }
        }).then( handleSuccess, handleError);
      },

      getUserSubscriptions: function(userID) {
        var url = $interpolate(URLS.getUserSubscriptions)({id: userID});
        return $http.get(url).then(handleSuccess, handleError);
      },

      getStripePublicKey: function() {
        return $http.get(URLS.getStripePublicKey).then(handleSuccess, handleError);
      }
    };
  }
})();