(function () {
  'use strict';

  angular
    .module('auth.service', [])
    .service('AuthService', AuthService);

  function AuthService($http, $cacheFactory, $interpolate, $window, $q, PermissionStore, CONFIG) {
    var BASE_URL = CONFIG.url + 'auth/';

    var URLS = {
      generateRegistrationKey: BASE_URL + 'register/key',
      registerUser: BASE_URL + 'register',
      getPermissions: BASE_URL + 'permissions/',
      getAvailablePermissions: BASE_URL + 'permissions/available',
      getAvailablePermissionsByClient: BASE_URL + 'permissions/available/{{ client }}/{{ secret }}',
      getAvailablePermissionsSimplify: BASE_URL + 'permissions/available/simplify',
      getAccessToken: BASE_URL + 'token/',
      me: BASE_URL + 'me/',
      getApi: BASE_URL + 'api/',
      initAuth: BASE_URL + 'init/',
      user: BASE_URL + 'users/',
      getUser: BASE_URL + 'users/{{ id }}',
      updateUser: BASE_URL + 'users/{{ id }}',
      deleteUser: BASE_URL + 'users/{{ id }}',
      getFullUser: BASE_URL + 'users/{{ id }}/full',
      getUserGroups: BASE_URL + 'users/{{ user_id }}/groups',
      getUserPermissions: BASE_URL + 'users/{{ user_id }}/permissions/',
      getUsersList: BASE_URL +
      'users/list/page/{{start}}' +
      '{{limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      group: BASE_URL + 'groups/',
      getGroup: BASE_URL + 'groups/{{ id }}',
      updateGroup: BASE_URL + 'groups/{{ id }}',
      deleteGroup: BASE_URL + 'groups/{{ id }}',
      getGroupsList: BASE_URL +
      'groups/list/page/{{start}}' +
      '{{limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      sendEmail: BASE_URL + 'users/{{ name }}/passwordReset/sendEmail',
      checkPasswordResetRequest: BASE_URL + 'users/passwordReset/{{ hash }}',
      passwordReset: BASE_URL + 'users/passwordReset/{{ hash }}/{{ password }}',
      remindEmail: BASE_URL + 'users/{{ email }}/remindEmail',
      getSettingsTree: BASE_URL + 'settings/'
    };

    var localUser = {};

    var toParams = function (obj) {
      var p = [];
      for (var key in obj) {
        p.push(key + '=' + encodeURIComponent(obj[key]));
      }
      return p.join('&');
    };


    var handleSuccess = function (res) {
      return res.data;
    };

    var handleError = function (res) {
      return res;
    };

    return {
      me: function () {
        return $http.get(URLS.me).then(handleSuccess, handleError);
      },

      generateRegistrationKey: function (params) {
        return $http.post(URLS.generateRegistrationKey, params).then(handleSuccess);
      },

      registerUser: function (params) {
        return $http.post(URLS.registerUser, params).then(handleSuccess);
      },
      /**
       * Checks if user is authenticated and token isn't expired
       *
       * @return {Boolean}
       */
      isAuthenticated: function() {
        return $window.localStorage.token
          && angular.fromJson($window.localStorage.token).timestamp > new Date().getTime()
      },

      getAccessToken: function (grantType, config) {
        var dataToSend = angular.copy(config);
        if (grantType == 'refresh_token') {
          // if (!$window.localStorage.token || !$window.localStorage.token.refresh_token)
          //   throw new Error('No refresh token found');
          dataToSend.grant_type = 'refresh_token';
          if (!config.refresh_token) {
            dataToSend.refresh_token = angular.fromJson($window.localStorage.token).refresh_token;
          }
        } else {
          dataToSend.grant_type = grantType;
        }
        return $http({
          method: 'POST',
          url: URLS.getAccessToken,
          data: toParams(dataToSend),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (res) {
          var token = res.data.data;
          token.timestamp = new Date().getTime() + token.expires_in * 1000;
          $window.localStorage.token = JSON.stringify(token);
          return res.data;
        });
      },

      getUser: function (id, params) {
        var defaults = {
          cache: false
        };
        var url = $interpolate(URLS.getUser)({id: id});
        return $http.get(url, angular.extend({}, defaults, params)).then(handleSuccess, handleError);
      },

      getFullUser: function (id) {
        var url = $interpolate(URLS.getFullUser)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },

      createUser: function (user) {
        var dataToSend = {
          username: user.username,
          name: user.name,
          email: user.email,
          password: user.password
        };

        if (user.groups && user.groups.length != 0) dataToSend.groups = _.pluck(user.groups, 'id');
        if (user.permissions && user.permissions.length != 0) dataToSend.permissions = user.permissions;

        return $http({
          method: 'POST',
          url: URLS.user,
          data: dataToSend,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(handleSuccess, handleError);
      },

      updateUser: function (user) {
        var dataToSend = {
          username: user.username,
          name: user.name,
          email: user.email,
          groups: [],
          permissions: [],
          deleted: user.deleted
        };

        var url = $interpolate(URLS.updateUser)({id: user.id});

        if (user.groups && user.groups.length != 0) dataToSend.groups = _.pluck(user.groups, 'id');
        if (user.permissions && user.permissions.length != 0) dataToSend.permissions = user.permissions;
        if (user.password && user.password.length != 0) dataToSend.password = user.password;

        return $http({
          method: 'PATCH',
          url: url,
          data: dataToSend,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(handleSuccess, handleError);
      },

      deleteUser: function (id) {
        var url = $interpolate(URLS.deleteUser)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },

      getGroup: function (id) {
        var url = $interpolate(URLS.getGroup)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },

      createGroup: function (group) {
        var dataToSend = {
          name: group.name,
          description: group.description
        };

        if (group.permissions && group.permissions.length != 0) dataToSend.permissions = group.permissions;

        return $http({
          method: 'POST',
          url: URLS.group,
          data: dataToSend,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(handleSuccess, handleError);
      },

      /**
       * Patches group with data that passed to
       * this method
       *
       * @param  {Object} group
       * @param  {Number} id - id of group
       */
      updateGroup: function (group) {
        var url = $interpolate(URLS.updateGroup)({id: group.id});

        return $http({
          method: 'PATCH',
          url: url,
          data: group,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
      },

      deleteGroup: function (id) {
        var url = $interpolate(URLS.deleteGroup)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },

      getPermissions: function (params) {
        var config = angular.extend({}, {
          cache: false
        }, params)

        return $http.get(URLS.getPermissions, config)
          .then(handleSuccess)
          .then(function (res) {
            if (res.data && res.data.permissions) {
              localUser.permissions = res.data.permissions;
            }
            return res;
          });
      },

      getPermissionsList: function () {
        return $http.get(URLS.getAvailablePermissions, {}).then(handleSuccess, handleError);
      },

      /**
       * Returns list of all available permissions even if user isn't logged in
       *
       * @param  {String} client - client_id from config
       * @param  {String} secret - client_secret from config
       *
       * @see http://docs.idi-demo.com/apidocs/ms-auth/docs/#api-Authentication_Server-Get_All_Available_Permissions_by_Client
       */
      getPermissionsListByClient: function(client, secret) {
        var url = $interpolate(URLS.getAvailablePermissionsByClient)({client: client, secret: secret})
        return $http.get(url).then(handleSuccess);
      },

      getPermissionsListSimplify: function () {
        return $http.get(URLS.getAvailablePermissionsSimplify, {}).then(handleSuccess, handleError);
      },

      /**
       * We fill PermissionStore with default permissions and all avaiable permissions
       * Must be called only once
       */
      definePermissions: function() {
        var AuthService = this;

        PermissionStore.clearStore();
        PermissionStore.definePermission('anonymous', function permissionCheck(name, transitionProperties) {
          return !AuthService.isAuthenticated()
        });
        /**
         * Non existing permission
         * We use it to manage redirecting with angular-permission library
         * @see app.admin.index state @ routes.js
         */
        PermissionStore.definePermission('non.existsing.permission', function permissionCheck() {
          return false;
        })

        /**
         * Main permission checking code
         * We define a list of all available permissions and check is user have required permission
         *
         * @warning Don't forget to reset $http cache with logout
         */
        return AuthService.getPermissionsListByClient(CONFIG.client.client_id, CONFIG.client.client_secret)
        .then(function(res) {
          PermissionStore.defineManyPermissions(res.data.permissions, function(permissionName, stateParams) {
            return AuthService.getPermissions({cache: true})
            .then(function(res) {
              return res.data.permissions.indexOf(permissionName) > -1
              ? $q.resolve()
              : $q.reject()
            })
          })
        })
      },

      getUsersList: function (config) {
        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(URLS.getUsersList)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      getGroupsList: function (config) {
        var config = config;
        if (!config.start) {
          config.start = 0;
        }
        var url = $interpolate(URLS.getGroupsList)(config);
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      getHelp: function () {
        var help = [];

        return help;
      },

      getUserInfo: function () {
        return localUser;
      },

      getUserGroups: function (id) {
        var url = $interpolate(URLS.getUserGroups)({user_id: id});
        return $http.get(url, {}).then(handleSuccess, handleError);
      },

      sendEmail: function (name) {
        var url = $interpolate(URLS.sendEmail)({name: name});
        return $http.get(url).then(handleSuccess, handleError);
      },

      checkPasswordResetRequest: function (hash) {
        var url = $interpolate(URLS.checkPasswordResetRequest)({hash: hash});
        return $http.get(url).then(handleSuccess, handleError);
      },

      passwordReset: function (config) {
        var url = $interpolate(URLS.passwordReset)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },

      remindEmail: function (email) {
        var url = $interpolate(URLS.remindEmail)({email: email});
        return $http.get(url).then(handleSuccess, handleError);
      },


      getCountryLocal: function () {
        return $http.get(URLS.getCountryLocal).then(handleSuccess, handleError);
      },

      getSettingsTree: function (email) {
        return $http.get(URLS.getSettingsTree).then(handleSuccess, handleError);
      }
    };
  }
})();