(function () {
  'use strict';

  angular
  .module('notes.service', [])
  .service('NotesService', NotesService);

  function NotesService ($http, $interpolate, CONFIG) {
    var BASE_URL = CONFIG.url + 'notes/';

    var URLS = {
      createNote: BASE_URL + 'note',
      note: BASE_URL + 'note/{{ id }}',
      getLatestNote: BASE_URL + 'latest/{{ note_id }}',
      getLatestNoteSourceId: BASE_URL + 'source/{{ source }}/{{ source_id }}'
    };

    var handleSuccess = function (res) {
      return res.data;
    };

    var handleError = function (res) {
      return res;
    };

    return {
      createNote: function (data) {
        return $http.post(URLS.createNote, data).then(handleSuccess, handleError);
      },
      getNote: function (id) {
        var url = $interpolate(URLS.note)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      deleteNote: function (id) {
        var url = $interpolate(URLS.note)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      updateNote: function (id, data) {
        var url = $interpolate(URLS.note)({id: id});
        return $http.put(url, data).then(handleSuccess, handleError);
      },
      getLatestNote: function (note_id) {
        var url = $interpolate(URLS.getLatestNote)({note_id: note_id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      getLatestNoteSourceId: function (source_id, source) {
        var url = $interpolate(URLS.getLatestNoteSourceId)({source_id: source_id, source: source});
        return $http.get(url).then(handleSuccess, handleError);
      }
    };
  }
})();