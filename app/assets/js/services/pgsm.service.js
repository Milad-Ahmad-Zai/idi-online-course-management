(function () {
  'use strict';

  angular
  .module('pgsm.service', [])
  .service('PgsmService', PgsmService);

  function PgsmService ($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/
     */
    var BASE_URL = CONFIG.url + 'pages/';

    var URLS = {
      getPagesList: BASE_URL + 'page',
      updatePage: BASE_URL + 'page/{{id}}',
      deletePage: BASE_URL + 'page/{{id}}',
      createNewPage: BASE_URL + 'page'
    };

    function unflatten(arr) {
      var tree = [],
          mappedArr = {},
          arrElem,
          mappedElem;

      // First map the nodes of the array to an object -> create a hash table.
      for(var i = 0, len = arr.length; i < len; i++) {
        arrElem = arr[i];
        mappedArr[arrElem.id] = arrElem;
        mappedArr[arrElem.id]['children'] = [];
      }


      for (var id in mappedArr) {
        if (mappedArr.hasOwnProperty(id)) {
          mappedElem = mappedArr[id];
          // If the element is not at the root level, add it to its parent array of children.
          if (mappedElem.parent_id) {
            mappedArr[mappedElem['parent_id']]['children'].push(mappedElem);
          }
          // If the element is at the root level, add it to first level elements array.
          else {
            tree.push(mappedElem);
          }
        }
      }
      return tree;
    }

    function handlePageListSuccess (response) {
      return unflatten(response.data.data);
    }

    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }


    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-PagesList
       * @param 
       */
      getPagesList: function () {
        return $http.get(URLS.getPagesList).then(handlePageListSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-UpdatePage
       * @param {number} pageData.id
       * @param {String} pageData.title
       * @param {String} pageData.slug
       * @param {String} pageData.body
       * @param {Array} pageData.tags
       * @param {Number} pageData.parent
       */
       
      savePage: function (pageData) {
        //update page
        if(pageData.id){
            var url = $interpolate(URLS.updatePage)({id: pageData.id});
            return $http.put(url, pageData).then(handleSuccess, handleError);
        }else{
            //create new page
            return $http.post(URLS.createNewPage,pageData).then(handleSuccess, handleError);
        }
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-pages/docs/#api-Page-DeletaPage
       * @param {number} id
       */
      deletePage: function (id) {
        var data = {id: id};
        var url = $interpolate(URLS.deletePage)(data);
        return $http.delete(url, data).then(handleSuccess, handleError);
      }

    };
  }
})();
