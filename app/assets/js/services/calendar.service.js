(function () {
  'use strict';

  angular
    .module('calendar.service', [])
    .service('CalendarService', calendarService);

  function calendarService($http, $interpolate, CONFIG) {
    /**
     * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms
     */
    var BASE_URL = CONFIG.url + 'calendars/';

    var URLS = {
      //calendar
      getCalendar: BASE_URL + 'calendar{{ id }}',
      getUserDefaultCalendar: BASE_URL + 'calendar/user/{{ id }}/default',
      createCalendar: BASE_URL + 'calendar',
      deleteCalendar: BASE_URL + 'calendar/{{ id }}',
      updateCalendar: BASE_URL + 'calendar/{{ id }}',
      getCalendarList: BASE_URL + 'calendar',
      makeCalendarDefault: BASE_URL + 'calendar/{{ id }}/default',
      renewCalendarToken: BASE_URL + 'calendar/{{ id }}/token/renew',

      //events
      getEvent: BASE_URL + 'event/{{uuid}}',
      createEvent: BASE_URL + 'event',
      updateEvent: BASE_URL + 'event/{{uuid}}',
      deleteEvent: BASE_URL + 'event/{{uuid}}',
      getEventList: BASE_URL + 'event'
    };

    function handleSuccess(response) {
      return response.data;
    }

    function handleError(response) {
      return response;
    }

    return {
      /**
       * @see http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Calendar-GetCalendar
       * @param {Number} id - Calendar id.
       */
      getCalendar: function (id) {
        var url = $interpolate(URLS.getCalendar)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Calendar-GetUserDefaultCalendar
       * @param {Number} id - User id
       */
      getUserDefaultCalendar: function (id) {
        var url = $interpolate(URLS.getUserDefaultCalendar)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Calendar-CreateNewCalendar
       * @param {String} name - Calendar name.
       * @param {Number} user_id - User id, if missing willl be user id of loggeduser
       * @param {Boolean} defaultCalendar - Is callendar default, if missing true for first calendar.
       */
      createCalendar: function (name, user_id, defaultCalendar) {
        var params = {};
        params.name = name;
        if (user_id) params.user_id = user_id;
        if (defaultCalendar) params.default = defaultCalendar;

        return $http.post(URLS.createCalendar, params)
          .then(handleSuccess, handleError)
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Calendar-DeleteCalendar
       * @param {Number} id - Calendar id.
       */
      deleteCalendar: function (id) {
        var url = $interpolate(URLS.deleteCalendar)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },

      /**
       * http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Calendar-UpdateCalendar
       * @param {Number} id - Calendar id.
       * @param {String} name - Calendar name.
       */
      updateCalendar: function (id, name) {
        var url = $interpolate(URLS.updateCalendar)({id: id});
        return $http.put(url, {id: id, name: name}).then(handleSuccess, handleError);
      },

      /**
       * http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Calendar-RenewCalendarToken
       * @param {Number} id - Calendar id.
       */
      renewCalendarToken: function (id) {
        var url = $interpolate(URLS.renewCalendarToken)({id: id});
        return $http.post(url, {id: id}).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-profiles/docs/#api-ProfileForms-GetFormList
       */
      getCalendarList: function (filter) {
        var defaults = {
          'page[start]': 0,
          'page[limit]': 100
        };
        return $http.get(URLS.getCalendarList, {
          params: defaults
        }).then(handleSuccess, handleError)
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Event-GetEventList
       */
      getEventList: function (filter) {
        var defaults = {
          'page[start]': 0,
          'page[limit]': 100
        };
        return $http.get(URLS.getEventList, {
          params: angular.extend({}, defaults, filter)
        }).then(handleSuccess, handleError)
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Event-CreateNewEvent
       * @param {Object} event - Event title.
       * @param {String} event.title - Event title.
       * @param {Number} [event.calendar_id] - Calendar ID to with will be Event added
       * @param {Number} [event.user_id] - User id, to with default Calendar will be Event added if calendat_id is missing.
       * @param {String} [event.description] - Event description.
       * @param {String} [event.start] - Event start date.
       * @param {String} event.end  - Event end date.
       * @param {Boolean} [event.fullday] - Is Event a full day Event. options
       */
      createEvent: function (event) {
        return $http.post(URLS.createEvent, event)
          .then(handleSuccess, handleError)
      },

      /**
       * http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Event-UpdateEvent
       * @param {Object} event - Event object.
       */
      updateEvent: function (event) {
        var url = $interpolate(URLS.updateEvent)({uuid: event.uuid});
        return $http.put(url, {
          uuid:event.uuid,
          title:event.title,
          description:event.description,
          start:event.start,
          end:event.end,
          fullday:event.fullday
        }).then(handleSuccess, handleError);
      },

      /**
       * @see http://docs.idi-demo.com/apidocs/ms-calendar/docs/#api-Event-DeleteEvent
       * @param {Number} uuid - Event uuid.
       */
      deleteEvent: function (uuid) {
        var url = $interpolate(URLS.deleteEvent)({uuid: uuid});
        return $http.delete(url).then(handleSuccess, handleError);
      }
    }
  }
})();