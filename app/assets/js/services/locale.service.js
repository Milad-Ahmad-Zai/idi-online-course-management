(function () {
  'use strict';
  angular
  .module('locale.service', [])
  .service('LocaleService', LocaleService);

  function LocaleService($http) {
    var BASE_URL = '/assets/locale/';

    var URLS = {
      //calendar
      getLocaleDefault: BASE_URL + 'locale.default.json',
      getLocale: BASE_URL + 'locale.json'
    };

    var handleSuccess = function (res) {
      if (res.status == 204) {
        return res;
      }
      return res.data;
    };
    var handleError = function (res) {
      return res;
    };

    return {
      getLocaleDefault: function () {
        return $http.get(URLS.getLocaleDefault).then(handleSuccess,handleError);
      },
      getLocale: function () {
        return $http.get(URLS.getLocale).then(handleSuccess,handleError);
      }
    };
  }
})();