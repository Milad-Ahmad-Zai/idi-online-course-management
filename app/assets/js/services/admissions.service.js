(function () {
  'use strict';

  angular
  .module('admissions.service', [])
  .service('AdmissionsService', AdmissionsService);

  function AdmissionsService ($http, $interpolate, CONFIG) {
    var BASE_URL = CONFIG.url + 'admissions/';

    var URLS = {
      // admissions
      getTerms: BASE_URL + 'terms/{{ level }}/{{ userId }}/',
      acceptTerms: BASE_URL + 'acceptTerms/{{ level }}/{{ userId }}/',
      deleteTerms: BASE_URL + 'terms/{{ userId }}/',

      getCountries: BASE_URL + 'uh/data/countries/list/',
      getApplicants: BASE_URL +
      'admissions/{{start}}' +
      '{{limit ? "/" + limit : "" }}',
      setTemperature: BASE_URL + 'admission/temperature/',
      getFileCategoryId: BASE_URL + 'file/category/id/',
      deleteAdmission: BASE_URL + 'admission',
      admission: BASE_URL + 'admission/{{ id }}',
      admissionFile: BASE_URL + 'admission/{{ admissionId }}/file',
      deleteAdmissionFile: BASE_URL + 'file/{{ id }}',
      getAdmissionFiles: BASE_URL +
      'files/{{start}}' +
      '{{limit ? "/" + limit : "" }}',
      updateAdmissionFile: BASE_URL + 'admission/{{ admissionId }}/file/{{ admissionFileId }}',
      supportDocuments: BASE_URL +
      'supportDocuments/{{start}}' +
      '{{limit ? "/" + limit : "" }}',
      supportDocument: BASE_URL + 'supportDocument',
      getFaq: BASE_URL + 'faq',
      bulkMethod: BASE_URL + 'admission/bulk/',
      getOptionsByField: BASE_URL + 'field/options/{{ field }}',

      // sheets

      getActiveSummary: BASE_URL + 'sheet/active/summary/',
      getUserSummarySheets: BASE_URL + 'sheet/{{ id }}/summary/all/',
      getUserDecisionSheets: BASE_URL + 'sheet/{{ id }}/decision/all/',
      summary: BASE_URL + 'sheet/summary{{ id ? "/" + id : "" }}',
      decision: BASE_URL + 'sheet/decision{{ id ? "/" + id : "" }}'
    };

    var toParams = function (obj) {
      var p = [];
      for (var key in obj) {
        p.push(key + '=' + encodeURIComponent(obj[key]));
      }
      return p.join('&');
    };

    var handleSuccess = function (res) {
      if (res.status == 204) {
        return res;
      }
      return res.data;
    };

    var handleError = function (res) {
      return res;
    };

    var activities = [
      {
        name: 'Management',
        value: 'management'
      },
      {
        name: 'Cash Handling',
        value: 'cash_handling'
      },
      {
        name: 'Project Management',
        value: 'project_management'
      },
      {
        name: 'Invoicing Payments',
        value: 'invoicing_payments'
      },
      {
        name: 'Spreadsheets and Databases',
        value: 'spreadsheets_databases'
      },
      {
        name: 'Team Leading',
        value: 'team_leading'
      },
      {
        name: 'Report Writing',
        value: 'report_writing'
      },
      {
        name: 'Administration',
        value: 'administration'
      },
      {
        name: 'Accounts and Bookkeeping',
        value: 'accounts_bookkeeping'
      },
      {
        name: 'Events Management',
        value: 'events_management'
      },
      {
        name: 'Supervision of others',
        value: 'supervision_of_others'
      },
      {
        name: 'External Communication',
        value: 'external_communications'
      },
      {
        name: 'Design and Testing',
        value: 'design_testing'
      },
      {
        name: 'Record Keeping',
        value: 'record_keeping'
      },
      {
        name: 'Articles and Newsletters',
        value: 'articles_newsletters'
      },
      {
        name: 'Training or Instruction',
        value: 'training_or_instructions'
      },
      {
        name: 'Presentations',
        value: 'presentations'
      },
      {
        name: 'Monitoring and Assessment',
        value: 'monitoring_assessment'
      },
      {
        name: 'Data Processing',
        value: 'data_processing'
      },
      {
        name: 'Consultancy',
        value: 'consultancy'
      },
      {
        name: 'Other',
        value: 'other',
        additional: '(please give details in box below)'
      }
    ];

    return {
      getActivities: function () {
        return activities;
      },
      getCountries: function () {
        return $http.get(URLS.getCountries).then(handleSuccess, handleError);
      },
      getApplicants: function (config, data) {
        var url = $interpolate(URLS.getApplicants)(config);
        return $http.post(url, data).then(handleSuccess, handleError);
      },
      setTemperature: function (ids, temperature) {
        var config = {
          ids: ids,
          temperature: temperature
        };
        return $http.post(URLS.setTemperature, config).then(handleSuccess, handleError);
      },
      deleteAdmission: function (ids) {
        return $http.delete(URLS.deleteAdmission, {data: {ids: ids}}).then(handleSuccess, handleError);
      },
      getAdmission: function (id) {
        var url = $interpolate(URLS.admission)({id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },
      saveAdmission: function (id, user) {
        var url = $interpolate(URLS.admission)({id: id});
        return $http.post(url, JSON.stringify(user)).then(handleSuccess, handleError);
      },
      getFileCategoryId: function (title) {
        return $http.post(URLS.getFileCategoryId, {title: title}).then(handleSuccess, handleError);
      },
      postAdmissionFile: function (admissionId, fileId, categoryId) {
        var url = $interpolate(URLS.admissionFile)({admissionId: admissionId});
        return $http.post(url, {file_id: fileId, category_id: categoryId}).then(handleSuccess, handleError);
      },
      deleteAdmissionFile: function (id) {
        var url = $interpolate(URLS.deleteAdmissionFile)({id: id});
        return $http.delete(url).then(handleSuccess, handleError);
      },
      getAdmissionFiles: function (admissionId, config) {
        var url = $interpolate(URLS.getAdmissionFiles)(config);
        return $http.post(url, {admission_id: admissionId}).then(handleSuccess, handleError);
      },

      updateAdmissionFile: function (data) {
        var url = $interpolate(URLS.updateAdmissionFile)({
          admissionId: data.admissionId,
          admissionFileId: data.admissionFileId
        });

        var data = {
          file_id: data.fileId,
          category_id: data.categoryId,
          seen: data.seen
        };

        return $http.post(url, data).then(handleSuccess, handleError);
      },

      getFaq: function () {
        return $http.get(URLS.getFaq).then(handleSuccess, handleError);
      },

      // Summary Sheet

      getActiveSummarySheet: function () {
        return $http.get(URLS.getActiveSummary).then(handleSuccess, handleError);
      },
      getUserSummarySheets: function (userId) {
        var url = $interpolate(URLS.getUserSummarySheets)({id: userId});
        return $http.get(url).then(handleSuccess, handleError);
      },
      deleteUserSummarySheet: function (ids) {
        var url = $interpolate(URLS.summary)({id: ''});
        return $http.delete(url, ids).then(handleSuccess, handleError);
      },
      updateUserSummarySheet: function (userId, data, summaryId) {
        var url = $interpolate(URLS.summary)({id: summaryId});
        data.user_id = userId;
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // Decision Sheet

      getUserDecisionSheets: function (userId) {
        var url = $interpolate(URLS.getUserDecisionSheets)({id: userId});
        return $http.get(url).then(handleSuccess, handleError);
      },
      deleteUserDecisionSheet: function (ids) {
        return $http({
          url: $interpolate(URLS.decision)({id: ''}),
          method: "DELETE",
          data: JSON.stringify({ "ids" : ids }),
          headers: {'Content-Type': 'application/json'}
        }).then(handleSuccess, handleError);
      },
      updateUserDecisionSheet: function (data, id, decisionId) {
        var url = $interpolate(URLS.decision)({id: decisionId});
        data.user_id = id;
        return $http.post(url, data).then(handleSuccess, handleError);
      },

      // Supporting Documents

      getSupportingDocuments: function (config) {
        var url = $interpolate(URLS.supportDocuments)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },
      addSupportingDocument: function (data) {
        return $http.post(URLS.supportDocument, data).then(handleSuccess, handleError);
      },
      deleteSupportingDocument: function (id) {
        var url = URLS.supportDocument + '/' + id;
        return $http.delete(url).then(handleSuccess, handleError);
      },

      // Terms

      getTerms: function () {
        return $http.get(URLS.getTerms, {}).then(handleSuccess, handleError);
      },
      acceptTerms: function (id) {
        var url = $interpolate(URLS.acceptTerms)({id: id});
        return $http.post(url, {}).then(handleSuccess, handleError);
      },
      // Bulk
      bulkMethod: function (data) {
        return $http.post(URLS.bulkMethod, data).then(handleSuccess, handleError);
      },
      getOptionsByField: function (field) {
        var url = $interpolate(URLS.getOptionsByField)({field: field});
        return $http.get(url).then(handleSuccess, handleError);
      }
    };
    }
})();