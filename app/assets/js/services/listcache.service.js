(function () {
  'use strict';

  angular
  .module('listcache.service', [])
  .service('ListcacheService', ListcacheService);

  function ListcacheService ($http, CONFIG, $interpolate, $window) {
    var BASE_URL = CONFIG.url + 'listcache/';

    var URLS = {
      getApplicantsCachedList: BASE_URL +
      'admissions/list/page/{{start}}' +
      '{{limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter" + filter : "" }}',
      getStudentsCachedList: BASE_URL +
      'students/list' +
      '{{ start >= 0 ? "/page/" + start : "" }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getAllApplicantsReport: BASE_URL +
      'reports/admissions/submitted' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '/filter{{ filter ? filter : "" }}/export=1'
    };

    var toParams = function (obj) {
      var p = [];
      for (var key in obj) {
        p.push(key + '=' + encodeURIComponent(obj[key]));
      }
      return p.join('&');
    };

    var handleSuccess = function (res) {
      if (res.status == 204) {
        return res;
      }
      return res.data;
    };

    var handleError = function (res) {
      return res;
    };

    return {
      getApplicantsCachedList: function (config) {
        var opt = config;
        if (!opt.start) {
          opt.start = 0;
        }
        var url = $interpolate(URLS.getApplicantsCachedList)(opt);

        return $http.get(url).then(handleSuccess, handleError);
      },
      getStudentsCachedList: function (config) {
        var url = $interpolate(URLS.getStudentsCachedList)(config);

        return $http.get(url).then(handleSuccess, handleError);
      },
      getReportUrl: function (config) {
        var url = $interpolate(URLS.getAllApplicantsReport)(config) + '?access_token=' + angular.fromJson($window.localStorage.token).access_token;
        return url.replace('http://api.idi-demo.com/', CONFIG.url);
      }
    };
  }
})();