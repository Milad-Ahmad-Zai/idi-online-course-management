(function () {
  angular
  .module('services.assessments', [])
  .factory('AssessmentsService', assessmentsService)

  function assessmentsService ($interpolate, $http, CONFIG) {
    var BASE_URL = CONFIG.url + 'assessments/';
    var ENROLMENT_URL = BASE_URL + 'enrolment/';

    var URLS = {
      enrolment: ENROLMENT_URL + '{{ id }}',
      getFiles: BASE_URL + 'files/list/page/{{start}}/{{limit}}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getFolders: BASE_URL + 'folder/list/page/{{start}}/{{limit}}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      addFiles: BASE_URL + 'file'
    };

    function handleSuccess (response) {
      return response.data;
    }

    function handleError (response) {
      return response;
    }

    return {
      /**
       * Returns enrolment
       *
       * @see http://docs.idi-demo.com/apidocs/ms-assessments/docs/#api-Enrolment-GetEnrolment
       * @param  {Number} id
       */
      getEnrolment: function (id) {
        return $http.get($interpolate(URLS.enrolment)({id: id}))
          .then(handleSuccess, handleError)
      },

      getFiles: function(filter){
        var defaults = {
            start: 0,
            limit: 10,
            page: 1
          };
        return $http.get($interpolate(URLS.getFiles)(angular.extend({}, defaults, filter)))
          .then(handleSuccess, handleError)
      },

      getFolders: function(filter){
          var defaults = {
            start: 0,
            limit: 100,
            page: 1
          };
        return $http.get($interpolate(URLS.getFolders)(angular.extend({}, defaults, filter)))
          .then(handleSuccess, handleError)
      },

      addFiles: function(data){
        return $http.post(URLS.addFiles,data)
          .then(handleSuccess, handleError)
      }
    }
  }
})()