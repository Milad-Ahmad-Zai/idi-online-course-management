(function () {
  'use strict';

  angular
  .module('permissions.service', [])
  .service('PermissionsService', function ($rootScope) {

    /**
     * Checks if user has permission
     * @param  {String} permission - permission that should be checked
     * @return {Boolean} true if user has permission
     * 
     * @example
     * PermissionsService.has('forums.postAny.get')
     */
    function has(permission) {
      if (arguments.length > 1)
        throw new Error('Use #hasAny or #hasAll methods for more then one permission')
      return $rootScope.currentPermissions.indexOf(permission) > -1;
    }

    /**
     * Checks if user has any of required permissions
     * @param  {...String} permissions - list of permissions
     * @return {Boolean} true if at least one permission is exist
     *
     * @example
     * PermissionsService.hasAny('forums.postAny.get', 'forums.postSelf.get')
     */
    function hasAny() {
      for (var i = arguments.length - 1; i >= 0; i--) {
        if (has(arguments[i]))
          return true;
      }
      
      return false;
    }

    /**
     * Checks if user has all of required permissions
     * @param  {...String} permissions - list of permissions
     * @return {Boolean} true if user has all of required permissions
     *
     * @example
     * PermissionsService.hasAll('forums.postAny.get', 'forums.postSelf.get')
     */
    function hasAll() {
      for (var i = arguments.length - 1; i >= 0; i--) {
        if (!has(arguments[i]))
          return false;
      }

      return true;
    }

    return {
      has: has,
      hasAny: hasAny,
      hasAll: hasAll
    }
  })
})()