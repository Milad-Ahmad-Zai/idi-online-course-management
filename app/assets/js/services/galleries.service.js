(function () {
  'use strict';

  angular
  .module('galleries.service', [])
  .service('GalleriesService', GalleriesService)

  function GalleriesService ($http, $interpolate, $q, CONFIG) {
    var BASE_URL = CONFIG.url + 'gallery/';

    var URLS = {
      getStudiosList: BASE_URL + 'studio/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getStudioGalleries: BASE_URL + 'studio/{{ id }}/galleries/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}',
      getStudio: BASE_URL + 'studio/{{ id }}',
      getGalleryAlbums: BASE_URL + 'gallery/{{ id }}/albums/list/page/{{ start }}' +
      '{{ limit ? "/" + limit : "" }}' +
      '{{ sort ? "/sort/" + sort : "" }}' +
      '{{ filter ? "/filter/" + filter : "" }}'
    };

    var handleSuccess = function (response) {
      return response.data;
    }

    var handleError = function (response) {
      return $q.reject(response.data);
    }

    return {
      // galleries

      getStudio: function (id) {
        var url = $interpolate(URLS.getStudio)({ id: id});
        return $http.get(url).then(handleSuccess, handleError);
      },

      getStudiosList: function (config) {
        var url = $interpolate(URLS.getStudiosList)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },

      getStudioGalleries: function (config) {
        var url = $interpolate(URLS.getStudioGalleries)(config);
        return $http.get(url).then(handleSuccess, handleError);
      },

      getGalleryAlbums: function (config) {
        var url = $interpolate(URLS.getGalleryAlbums)(config);
        return $http.get(url).then(handleSuccess, handleError);
      }
    }

  }
})()