(function () {
  'use strict';

  angular
    .module('customCurrency.filter', [])
    .filter('customCurrency', function(){
      return function(input, symbol, place){
        if(isNaN(input)){
          return input;
        } else {
          var symbol = symbol || '£';
          var place = place === undefined ? true : place;
          if(place == 'left'){
            return symbol + parseFloat(input).toFixed(2);
          } else{
            return parseFloat(input).toFixed(2) + symbol;
          }
        }
      }
    })
})();