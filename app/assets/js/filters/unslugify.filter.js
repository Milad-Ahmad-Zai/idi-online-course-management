(function () {
  'use strict';

  angular
    .module('unslugify.filter', [])
    .filter('unslugify', [function () {
      return function (input, sub) {
        var new_string = '';
        if (!sub) {
          sub = '-';
        }

        var inputSplit = '';
        if (input) {
          inputSplit = input.split(sub);
        }

        for (var i = 0; i < inputSplit.length; i++) {
          new_string += inputSplit[i].charAt(0).toUpperCase() + inputSplit[i].slice(1) + ' ';
        }

        return new_string;
      };
    }])
    
})();