(function () {
  'use strict';

  angular
    .module('moment.filter', [])
    .filter('moment', function() {
      return function(dateString,dateFormat) {
        return moment(new Date(dateString)).format(dateFormat)
      }
    });
})();