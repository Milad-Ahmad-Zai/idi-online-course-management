(function () {
  'use strict';

  angular
    .module('unique.filter', [])
    .filter('unique', ['$parse', function uniqFilter($parse) {
      var isUndefined = angular.isUndefined,
        isObject = angular.isObject,
        isArray = angular.isArray,
        equals = angular.equals;

      function toArray(object) {
        return isArray(object)
          ? object
          : Object.keys(object).map(function(key) {
          return object[key];
        });
      }
      
      return function (collection, property) {

        collection = isObject(collection) ? toArray(collection) : collection;

        if (!isArray(collection)) {
          return collection;
        }

        //store all unique identifiers
        var uniqueItems = [],
          get = $parse(property);

        return (isUndefined(property))
          //if it's kind of primitive array
          ? collection.filter(function (elm, pos, self) {
          return self.indexOf(elm) === pos;
        })
          //else compare with equals
          : collection.filter(function (elm) {
          var prop = get(elm);
          if(some(uniqueItems, prop)) {
            return false;
          }
          uniqueItems.push(prop);
          return true;
        });

        //checked if the unique identifier is already exist
        function some(array, member) {
          if(isUndefined(member)) {
            return false;
          }
          return array.some(function(el) {
            return equals(el, member);
          });
        }
      }
    }])
})();

