(function () {
  'use strict';

  angular
  .module('app.run', ['ui.router', 'oc.lazyLoad', 'permission','mgcrea.ngStrap'])
  .run(run)
  .config(angularStrapConfig);

  function run ($rootScope, $location, $state, PermissionStore, AuthService, $timeout, $q, $window, CONFIG) {
    $rootScope.config = CONFIG;
    $rootScope.currentPermissions = $rootScope.currentPermissions || [];

    if (CONFIG.ga_tracking) {
      $window.ga('create', CONFIG.ga_tracking.id, 'auto');
    }

    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
      //$rootScope.loading = true;
      var isLogin = toState.name === 'app.login';
      var is404 = toState.name === 'app.404';
      if (isLogin) {
        return; // no need to redirect
      } else if (is404) {
        $timeout(function () {
          $state.go('app.admin.profile.public');
        }, 5000);
      } else if (toState.name == 'app.admin.admissions.student' && toParams.id.length) {
        return;
      }
    });

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
      var state = AuthService.isAuthenticated() ? 'app.admin.error.404' : 'app.login';
      $rootScope.previousState = {
        state: toState,
        params: toParams
      }
      $state.go(state)
    });


    $rootScope.$on('$stateChangePermissionDenied', function(event, toState, toParams, options) {
      if ($window.localStorage.token) {
        $rootScope.previousState = {
          state: toState,
          params: toParams
        }
      }
      console.error('You do not have permissions to visit this page', arguments)
    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      if (toState.name != 'app.login') {
        $rootScope.previousState = null;
      }
      if (CONFIG.ga_tracking) {
        for (var i in CONFIG.ga_tracking.states) {
          if (toState.name.indexOf(CONFIG.ga_tracking.states[i]) > -1) {
            $window.ga('send', 'pageview', $location.path());
          }
        }
      }
    })
  }

  function angularStrapConfig ($datepickerProvider, $timepickerProvider) {
    angular.extend($datepickerProvider.defaults, {
      container: 'body',
      iconLeft: 'fa fa-chevron-left',
      iconRight: 'fa fa-chevron-right'
    });
    angular.extend($timepickerProvider.defaults, {
      container: 'body',
      iconUp: 'fa fa-chevron-up',
      iconDown: 'fa fa-chevron-down'
    });
  }

})();