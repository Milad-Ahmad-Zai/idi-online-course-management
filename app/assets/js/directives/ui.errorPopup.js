(function () {
  'use strict';

  angular
    .module('ui.errorPopup', [])
    .directive('uiErrorPopup', ['$sce', '$window', '$rootScope', function ($sce, $window, $rootScope) {
      return {
        restrict: 'C',
        scope: {
          type: '=type'
        },
        link: function ($scope, $el, $attr) {
          $rootScope.socialSelectionOn = false;
          $rootScope.errorElement = {};
          angular.element($el).on('mouseup', function (e) {
            var clickedElement = e.target;
            var elementClasses = clickedElement.classList;
            var parentElementClasses = clickedElement.parentElement !== null && clickedElement.parentElement.classList;
            var grandParentElementClasses = clickedElement.parentElement !== null && clickedElement.parentElement.parentElement !== null && clickedElement.parentElement.parentElement.classList;
            var isPencil = elementClasses.contains('error-button') || clickedElement.parentElement.classList.contains('error-button') || elementClasses.contains('popover-error') || (parentElementClasses && parentElementClasses.contains('popover-error')) || (grandParentElementClasses && grandParentElementClasses.contains('popover-error'));
            var selection = $window.getSelection();
            var text = selection.toString();

            if (isPencil) {
              return;
            } else if (text.length > 0) {
              $rootScope.errorTitle = angular.copy(text);
            } else {
              $rootScope.errorTitle = '';
            }
          });
        }
      };
    }]);
})();