(function () {
  'use strict';
  
  angular
    .module('ui.message', ['message.service', 'angularFileUpload'])
    .directive('editReplyForm', ['$sce', '$window', '$rootScope', 'cfpLoadingBar', 'MessageService', 'FilesService', 'FileUploader', 'Notification', function ($sce, $window, $rootScope, cfpLoadingBar, MessageService, FilesService, FileUploader, Notification) {
      return {
        restrict: 'E',
        templateUrl: '/partials/messages/edit-reply-form.html',
        scope: {
          message : '=',
          files : '='
        },
        link: function ($scope, element, attrs) {
          $scope.message.tempMessage = $scope.message.text + '';
          $scope.tempFileIds = [];

          $scope.cancelEdit = function () {
            $scope.message.isEdit = false;
            $scope.message.tempMessage = $scope.message.text;
          };

          // add attachments
          var uploader = $scope.uploader = FilesService.getUploader('message', 'message', true);

          uploader.onCompleteItem = function(fileItem, response, status, headers) {
            if (!response.error || status == 200) {
              $scope.tempFileIds.push(response.data.id);
              $scope.files[response.data.id] = response.data;
            } else {
              fileItem.msg = response.msg;
            }
          };

          uploader.onBeforeUploadItem = function(item) {
            item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
          };

          FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
            return false; // true|false
          };

          $scope.removeMessageFiles = function (id) {
            var index = $scope.message.files.indexOf(id);
            if (index > -1) {
              $scope.message.files.splice(index, 1);
            }

            FilesService.deleteFile(id).then(function(res) {});
          };

          $scope.removeFileId = function (item) {
            var index = uploader.queue.indexOf(item);
            var fileId = $scope.tempFileIds[index];
            if (fileId) {
              $scope.tempFileIds.splice(index, 1);
              FilesService.deleteFile(fileId).then(function(res) {});
            }
          };
          
          $scope.updateReplyMessage = function (form) {
            if (form.$invalid) {
              return;
            }

            $scope.editReplyFormSubmitted = true;
            cfpLoadingBar.start();
            var fileIds = _.union($scope.message.files, $scope.tempFileIds);

            var data = {
              'text': $scope.message.tempMessage,
              'modified_by' : $rootScope.userId,
              'topic': 'topic text 1',
              'file_ids': fileIds
            };
            
            MessageService.updateMessage($scope.message.id, data).then(function (res) {
              if (res) {
                uploader.clearQueue();
                $scope.tempFileIds = [];
                $scope.message.text = $sce.trustAsHtml($scope.message.tempMessage);
                $scope.message.isEdit = false;
                $scope.message.files = fileIds;
                $scope.message.modifiedDate = new Date();
                $scope.message.isUpdated = true;
                
                Notification.success({ 
                  message: 'Your message has been successfully updated.',
                  title: "Success"
                });
              } else {
                Notification.error({ 
                  message: "There's an error processing your request.",
                  title: "Error"
                });
              }
              
              $scope.editReplyFormSubmitted = false;
              cfpLoadingBar.complete();
            });
          }
        }
      }
    }]);
})();