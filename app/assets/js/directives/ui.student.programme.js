(function () {
  'use strict';

  angular
    .module('ui.student.programme', [])
    .directive('editStudentProgramme', function ($rootScope, cfpLoadingBar, Notification, StudyPath2Service) {
      return {
        restrict: 'E',
        templateUrl: '/partials/students/edit-programme-form.html',
        scope: {
          programme : '=',
          semesters : '=',
          statuses : '=',
          onStatusChange: '&'
        },
        link: function (scope, element, attrs) {
          scope.semesters.forEach(function (sem) {
            /*if (sem.id == scope.programme.semester.semester_id) {
              scope.prog.prog_semester = sem;
            }*/
          });
          scope.editStudentProgramme = function (form) {
            // update course status
            cfpLoadingBar.start();

            var data = {
              status: scope.programme.status,
              created_by: $rootScope.userId
            };

            StudyPath2Service.updateProgrammeStatus(data, scope.programme.id).then(function (res) {
              cfpLoadingBar.complete();
              form.$setPristine();
              form.$setUntouched();

              if (!res.error) {
                scope.programme.isEdit = false;
                Notification.success({
                  message: 'Programme status successfully updated.',
                  title: "Success"
                });
                scope.onStatusChange();
              } else {
                Notification.error({
                  message: 'There was an error processing your request.',
                  title: "Error"
                });
              }
            });
          };

          scope.cancel = function () {
            scope.prog = {};
            scope.programme.isEdit = false;
          };
        }
      }
    });
})();