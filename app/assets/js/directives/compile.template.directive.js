(function () {
  'use strict';

  angular
    .module('compile.template.directive', [])
    .directive('compile', ['$compile', '$parse', function ($compile, $parse) {
      return function (scope, element, attr) {
        scope.$watch(attr.compile, function () {
          element.html($parse(attr.compile)(scope));
          $compile(element.contents())(scope);
        }, true);
      }
    }])
})();


