(function () {
  'use strict';

  angular
    .module('ui.file.thumbnail', ['message.service'])
    .directive('uiFileThumbnail', directive);

  function directive($sce) {
    return {
      restrict: 'E',
      templateUrl: '/partials/files/thumbnail--file.html',
      scope: {
        item: '='
      },
      link: function (scope, element, attrs) {
        if (!scope.item)
          return;

        init();

        function isImage() {
          return /^(jpg|jpeg|png|bmp|gif)$/.test(scope.item.ext)
        };

        function isDoc() {
          return /^(doc|docx|odt)$/.test(scope.item.ext);
        };

        function isExcel() {
          return /^(xls|xlsx|ods)$/.test(scope.item.ext);
        };

        function isKeynote() {
          return /^(ppt|pptx)$/.test(scope.item.ext);
        };

        function isRecording() {
          return /^(wav|mp3)$/.test(scope.item.ext);
        };

        function isVideo() {
          return /^(mov|mp4|wmv)$/.test(scope.item.ext);
        };

        function isPdf () {
          return /^pdf$/.test(scope.item.url);
        };

        function isTxt () {
          return /^txt$/.test(scope.item.url);
        }

        function init() {
          switch (true) {
          case (isImage()):
            scope.type = 'image';
            break;
          case (isPdf()):
            scope.type = 'book';
            break;
          default:
            scope.type = 'doc';
          }
        }

        if (scope.item.created) {
          var created = moment(scope.item.created);
          scope.item.uploaded = created.toDate();
        }
      }
    };
  }
})();