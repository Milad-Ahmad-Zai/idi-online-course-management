(function () {
  'use strict';

  angular
    .module('ui.student.module', [])
    .directive('editStudentModule', function ($rootScope, cfpLoadingBar, Notification, StudyPath2Service) {
      return {
        restrict: 'E',
        templateUrl: '/partials/students/edit-module-form.html',
        scope: {
          module : '=',
          moduleStatuses : '=',
          tutors : '=',
          onStatusChange: '&'
        },
        link: function (scope, element, attrs) {
          scope.editStudentModule = function (form) {
            // update module status
            cfpLoadingBar.start();
            StudyPath2Service.updateStudyPathModule(scope.module, scope.module.id).then(function (res) {
              cfpLoadingBar.complete();
              form.$setPristine();
              form.$setUntouched();

              if (!res.error) {
                scope.module.isEdit = false;
                Notification.success({
                  message: 'Module successfully updated.',
                  title: "Success"
                });

                scope.onStatusChange();
              } else {
                Notification.error({
                  message: 'There was an error processing your request.',
                  title: "Error"
                });
              }
            });
          };

          scope.cancel = function () {
            scope.mod = {};
            scope.module.isEdit = false;
          };
        }
      }
    });
})();