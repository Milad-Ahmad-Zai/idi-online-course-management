(function () {
  'use strict';

  angular
    .module('ui.student.course', [])
    .directive('editStudentCourse', function ($rootScope, cfpLoadingBar, Notification, StudyPath2Service) {
      return {
        restrict: 'E',
        templateUrl: '/partials/students/edit-course-form.html',
        scope: {
          coursestatus : '=',
          course: '=',
          semesters : '=',
          levelStatuses : '=',
          studypathid : '=',
          onStatusChange: '&'
        },
        link: function (scope, element, attrs) {
          scope.levelStatus = scope.coursestatus;

          console.log(scope.course)

          scope.editStudentCourse = function (form) {
            // update course status
            cfpLoadingBar.start();

            var data = {
              status: scope.levelStatus,
              created_by: $rootScope.userId
            };

            StudyPath2Service.updateLevelStatus(data, scope.course.cour_id, scope.studypathid).then(function (res) {
              cfpLoadingBar.complete();
              form.$setPristine();
              form.$setUntouched();

              if (!res.error) {
                scope.course.isEdit = false;
                Notification.success({
                  message: 'Course status successfully updated.',
                  title: "Success"
                });
                scope.onStatusChange();
              } else {
                Notification.error({
                  message: 'There was an error processing your request.',
                  title: "Error"
                });
              }
            });
          };

          scope.cancel = function () {
            scope.prog = {};
            scope.course.isEdit = false;
          };
        }
      }
    });
})();