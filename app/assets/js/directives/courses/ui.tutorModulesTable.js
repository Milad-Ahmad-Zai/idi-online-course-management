(function () {
  'use strict';

  angular
  .module('ui.courses.tutorModulesTable', [])
  .directive('tutorModulesTable', directive);

  function directive ($filter, NgTableParams, StudyPath2Service) {
    return {
      templateUrl: '/partials/directives/courses/ui.tutorModulesTable.html',
      scope: {
        userId: '='
      },
      link: function (scope, element, attrs) {
        var table = element.find('table');
        scope.isMyCourses = true;

        function getCourses() {
          var getMethod = scope.isMyCourses 
            ? StudyPath2Service.getTutorModules 
            : StudyPath2Service.getOtherTutorModules;

          return getMethod(scope.userId);
        }

        scope.$watch('isMyCourses', function (newValue, oldValue) {
          table.css('opacity', 0.4);
          getCourses().then(function (res) {
            scope.modules = res.data;
            table.css('opacity', 1);
          })
        })
      }
    }
  }
})();