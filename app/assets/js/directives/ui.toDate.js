(function () {
  'use strict';

  angular
  .module('ui.toDate', [])
  .directive('toDate', ['$timeout', function ($timeout) {
    return {
      restrict : 'A',
        scope : {
            ngModel : '='
        },
        link: function (scope) {
            if (scope.ngModel) scope.ngModel = new Date(scope.ngModel);
        }
    };
  }]);
})();