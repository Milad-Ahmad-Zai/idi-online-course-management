(function () {
  'use strict';
  angular
    .module('ui.dashboard.todo', [])
    .directive('dashboardTodo', directive);

  function directive(TodoService, $window, $timeout) {
    return {
      templateUrl: '/assets/js/directives/dashboard/todo/idi.todo-list.html',
      scope: {
        userId: '=',
      },
      link: function(scope,elem,attr,ngModel) {
        scope.tasktext = '';
        scope.viewOptions = {
          type: "select",
          name: "taskView",
          value: "false"
        }

        init();

        function init () {
          scope.editing = false;
          getTasks();
        }

        /**
         * Enables editing of an individual task
         * @param  {taskId} - id of task to be edited
         */
        scope.startEditing = function (taskId) {
          var element = $window.document.getElementById('editTask-' + taskId);
          $timeout(function () {
            element.focus();
          });
        }

        /**
         * Updates task with new task title
         *
         * @param  {task} - task to be updated
         * @param  {newTitle} - new title for task
         */
        scope.updateTask = function(task, newTitle) {
          scope.editing = false;
          if (newTitle == task.title) {
            return
          }

          var data = {
            title: newTitle,
            user_id: scope.userId
          };

          TodoService.updateTodoTask(task.id, data)
          .then(getTasks);
        }

        /**
         * Get the users list of tasks and display them
         */
        function getTasks () {
          return TodoService.getTodoUserTaskList(scope.userId).then(function(res) {
            scope.tasks = res.data;
          })
        }

        /**
         * Set task as complete
         * @param {taskId} - id of task to be set complete
         */
        scope.setComplete = function (taskId) {
          TodoService.setTodoTaskComplete(taskId)
          .then(getTasks);
        }

        /**
         * Deletes task
         * @param  {taskId} - id of task to be deleted
         */
        scope.deleteTask = function (taskId) {
          TodoService.deleteTodoTask(taskId)
          .then(getTasks);
        }

        /**
         * Add new task
         */
        scope.addTask = function() {
          if (scope.tasktext.trim() < 0) return;

          var data = {
            title: scope.tasktext,
            user_id: scope.userId
          };

          TodoService.addTodoTask(data)
          .then(getTasks);
          scope.tasktext = "";
        }
      }
    }
  }
})();
