(function () {
  'use strict';

  angular
    .module('ui.dashboard.today', [])
    .directive('dashboardToday', directive)

  function directive($rootScope) {
    return {
      templateUrl: '/assets/js/directives/dashboard/today/ui.dashboardToday.html',
      scope: {
        date: '=',
        events: '=',
        todayClick: '&'
      },
      link: function (scope, element, attrs) {
        scope.clicked = function(id) {
          scope.todayClick({id: id})
        }
      }
    }
  }
})();