(function () {
  'use strict';

  angular
  .module('ui.dashboard.programmesTable', [])
  .directive('programmesTable', directive);

  function directive ($filter, NgTableParams, StudyPath2Service) {
    return {
      templateUrl: '/assets/js/directives/dashboard/programmesTable/ui.programmesTable.html',
      scope: {
        userId: '='
      },
      link: function (scope, element, attrs) {
        init();

        function init() {
          scope.tableParams = new NgTableParams({

          })

          StudyPath2Service
            .getStudentProgrammes(scope.userId)
            .then(function (programmes) {
              programmes.map(function (programme) {
                programme.programme.courses.map(function (course) {
                  course.start_date = $filter('date')(new Date(course.start_date), 'd/M/y');
                })
              })

              scope.programmes = programmes;
              
            })
        }
      }
    }
  }
})();