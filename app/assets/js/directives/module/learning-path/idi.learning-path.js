/**
 * @directive idi.module.learning-path
 * @description
 * Dispalys sections of learning path and allows
 * to navigate throw them.
 *
 * @param {Number} moduleId - id of current module
 * @param {Number} parentId - parent of current section list
 * @param {Number} sectionId - selected section
 * @param {Number} contentId - selected Learning Path or Activity item of section
 * @param {Number} spModuleId - Sp Module Id
 * @param {Function} onItemSelect - callback that is called when any content item
 *                                  has been selected
 * @param {Function} onFileSelect - callback that is called when file have been
 *                                  selected
 * @param {String} mode - 'activities' for activities only, nothing for default
 *                        behaviour
 */

(function () {
  'use strict';

  angular
    .module('idi.module.learning-path', ['courses.service'])
    .directive('idiLearningPath', directive);

  function directive ($q, CoursesService, StudyPath2Service, FilesService) {
    return {
      templateUrl: '/assets/js/directives/module/learning-path/idi.learning-path.html',
      scope: {
        moduleId: '=',
        parentId: '=?',
        sectionId: '=?',
        contentId: '=?',
        spModuleId: '=',
        onItemSelect: '&',
        onFileSelect: '&',
        flat: '='
      },
      link: function link(scope, element, attrs) {
        scope.isActivitiesMode = attrs.mode === 'activities';

        /**
         * Initialization
         */
        init();

        scope.$on('lp.sections:update-files', updateFiles);

        /**
         * Gets list of sections
         */
        function getSections (parentId, moduleId) {
          return CoursesService.getSectionsList(moduleId, parentId)
          .then(function (res) {
            return res.data.list;
          })
        }

        /**
         * Gets content for specific section
         */
        function getContent (id) {
          return CoursesService.getSectionContent(id)
          .then(function (res) {
            return res.data.list[0].content;
          })
        }

        /**
         * Returns files assigned to this SpModule
         * @param {Number} spModuleId
         * @description
         * Gets list of files assigned to SpModule and processes it. At the end
         * returns dictionary where key is content_id and value is file data
         *
         * @returns {Object} - dictionary of files
         */
        function getFiles (spModuleId) {
          if (!spModuleId) return $q.reject();
          return StudyPath2Service.getAttachedFiles(spModuleId)
          .then(function (res) {
            return res.data;
          })
          .then(function (files) {
            var fileIds = files.map(function (file) { return file.file_id });
            return FilesService.getBulkFiles(fileIds).then(function (res) {
              var filesData = res.data.reduce(function (prev, current) {
                prev[current.id] = current;

                return prev;
              }, {});
              files = files.reduce(function (prev, current) {
                filesData[current.file_id].attached_messages = current.attached_messages;

                prev[current.content_id] = prev[current.content_id] || [];
                prev[current.content_id].push(filesData[current.file_id])
                filesData[current.file_id].attached_messages = current.attached_messages;

                return prev;
              }, {})

              return files;
            })
          })
        }

        /**
         * Updates list of sections in the view and some
         * variables
         */
        function applySections (sections) {
          scope.sections = sections;
          if (!sections.length) {
            return;
          }
          scope.parentId = sections[0].parent_id;
          scope.backId = sections[0].parent.parent_id;

          return sections;
        }

        /**
         * Filters and adds content
         */
        function applyContent(section, content) {
          section.content = content;
          if( !scope.flat ) {
            section.activities = content.filter(function (item) {
              return item.content_type_name == 'Activities';
            });
            if (scope.isActivitiesMode) return
            section.learningMaterials = content.filter(function (item) {
              return item.content_type_name == 'Learning Materials';
            });
          } else {
            // When flat is true, we do not separate activities and learning materials into separate sub-sections
            if (scope.isActivitiesMode) {
              section.allContent = content.filter(function (item) {
                return item.content_type_name == 'Activities';
              })
            } else {
              section.allContent = content.filter(function (item) {
                return item.content_type_name == 'Activities' || item.content_type_name == 'Learning Materials';
              })
            }
          }
        }


        function updateFiles(digest) {
          getFiles(scope.spModuleId)
          .then(function (files) {
            scope.files = files;
          })
        }

        scope.getSectionById = function (id) {
          getSections(id, scope.moduleId).then(applySections)
        };

        /**
         * If section has content that method loads it.
         * Otherwise it loads inner sections
         */
        scope.getSection = function (section) {
          if (section.has_content) {
            if (section.content && section.content.length) {
              section.content = [];
              section.learningMaterials = [];
              section.activities = [];
            } else {
              getContent(section.id).then(function (content) {
                applyContent(section, content);
                scope.sectionId = section.id;
              });
            }
          } else {
            getSections(section.id, scope.moduleId).then(applySections);
          }
        };


        scope.selectItem = function (item, section) {
          scope.contentId = item.id;
          scope.onItemSelect({item: item, section: section});
        };

        scope.selectFile = function (file, content, $event) {
          scope.onFileSelect({file: file, content: content, $event: $event});
        }

        function init () {
          scope.parentId = scope.parentId || 0;
          if (scope.isActivitiesMode) {
            updateFiles();
          }

          getSections(scope.parentId, scope.moduleId)
          .then(function (sections) {
            // if some element of section is selected
            if (scope.sectionId) {
              return getContent(scope.sectionId).then(function (content) {
                var choosenSection;
                sections.map(function (section) {
                  if (section.id == scope.sectionId) {
                    applyContent(section, content);
                    choosenSection = section;
                  }
                });

                if (scope.contentId) {
                  content.map(function (item) {
                    if (item.id == scope.contentId) {
                      scope.onItemSelect({item: item, section: choosenSection});
                    }
                  })
                }
                return sections
              })
            } else {
              return sections;
            }

          })
          .then(applySections);
        }
      }
    }
  }
})();
