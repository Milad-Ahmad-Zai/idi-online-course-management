(function () {
  'use strict';

  angular
    .module('srs.filter', [])
    .directive('srsFilter', ['$window', function ($window) {
      return {
        restrict: 'C',
        template:
        '<div class="srs_filter__multiselect" ng-if="item.type == \'multiselect\'">' +
          '<div ng-dropdown-multiselect="" options="item.options" events="{onItemSelect: studentsList.reloadFilters, onItemDeselect: studentsList.reloadFilters, onSelectAll: studentsList.reloadFilters, onDeselectAll: studentsList.reloadFilters}"  selected-model="studentsList.filters[item.filterId]"></div>' +
        '</div>' +
        '<div class="srs_filter__select" ng-if="item.type == \'select\'">' +
          '<label class="col-lg-6 col-md-6 col-sm-12 col-xs-12 control-label">{{item.name}}</label>' +
          '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">' +
            '<select class="form-control" ng-change="studentsList.reloadFilters()" ng-options="key as value for (key, value) in item.options" ng-model="studentsList.filters[item.filterId]">' +
              '<option value="" selected>Select</option>' +
            '</select>' +
          '</div>' +
        '</div>' +
        '<div class="srs_filter__text" ng-if="item.type == \'text\'">' +
          '<input type="search" class="form-control" placeholder="Search" value="" ng-change="studentsList.reloadFilters()" ng-model="studentsList.filters[item.filterId]">' +
        '</div>',
        link: function (scope, element, attributes) {

        }
      };
    }]);

})();