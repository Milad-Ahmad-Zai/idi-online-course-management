/**
 * @directive stripeCardExpire
 * @description
 * Validates expiration date of card. It requires month and year to be able
 * to validate. So we pass year to input for month and pass month to year input
 *
 * @using
 * <input ng-model="card.month" stripe-card-expire stripe-card-year="card.year"/>
 * <input ng-model="card.year" stripe-card-expire stripe-card-month="card.month"/>
 * @return {[type]} [description]
 */
(function () {
  angular
  .module('validators.stripe-card-expire', [])
  .directive('stripeCardExpire', stripeCardExpire);

  function stripeCardExpire() {
    return {
      require: ['ngModel', '?stripeCardMonth', '?stripeCardYear'],
      scope: {
        stripeCardMonth: '=?',
        stripeCardYear: '=?'
      },
      link: function(scope, element, attrs, ctrls) {
        var ngModelCtrl = ctrls[0];

        scope.$watch('stripeCardMonth', ngModelCtrl.$validate);
        scope.$watch('stripeCardYear', ngModelCtrl.$validate);

        ngModelCtrl.$validators.stripeCardExpire = function(modelValue, viewValue) {
          var month = scope.stripeCardMonth || modelValue;
          var year = scope.stripeCardYear || modelValue;

          return Stripe.card.validateExpiry(month, year);
        }

      }
    }
  }
})()