/**
 * @directive stripeCardNumber
 * @description
 * Validates card number with helping of Stripe library
 *
 * @using
 * <form name="form">
 *   <input type="text" name="number" ng-model="card.number" stripe-card-number/>
 *   <span ng-show="form.number.$error.stripeCardNumber">Number isn't correct</span>
 * </form>
 * @requires Stripe
 */

(function () {
  angular
  .module('validators.stripe-card-number', [])
  .directive('stripeCardNumber', stripeCardNumber);

  function stripeCardNumber() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ctrl) {
        ctrl.$validators.stripeCardNumber = function(modelValue, viewValue) {
          if(!angular.isUndefined(modelValue)) {
            var stripeCheck = Stripe.card.validateCardNumber(modelValue);
            var lengthCheck = modelValue.replace(/-|\s/g,"").length == 16;
            return stripeCheck&&lengthCheck;
          }
        }
      }
    }
  }
})()