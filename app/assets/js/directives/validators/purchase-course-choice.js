(function() {
  angular
  .module('validators.purchase-course-choice', [])
  .directive('purchaseCourseChoice', purchaseCourseChoice);

  function purchaseCourseChoice() {
    return {
      require: 'ngModel',
      scope: {
        initialVals : '='
      },
      link: function(scope, element, attrs, ctrl) {
        ctrl.$validators.purchaseCourseChoice = function(modelValue, viewValue) {
          // Check that field isn't undefined, null or an initial value
          var check1 = angular.isUndefined(modelValue);
          var check2 = scope.initialVals.indexOf(modelValue) > -1;
          var check3 = modelValue == null;
          if (!check1 && !check2 && !check3) {
            return true;
          } else {
            return false;
          }
        }
      }
    }
  }
})()