/**
 * @directive stripeCardCvc
 * @description
 * Checks card CVC with help of Stripe library
 *
 * @using
 * <form name="form">
 *   <input name="cvc" ng-model="card.cvc" stripe-card-cvc/>
 *   <span ng-show="form.cvc.$error.stripeCardCvc">CVC isn't correct</span>
 * </form>
 *
 * @requires Stripe
 */

(function() {
  angular
  .module('validators.stripe-card-cvc', [])
  .directive('stripeCardCvc', stripeCardCvc);

  function stripeCardCvc() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ctrl) {
        ctrl.$validators.stripeCardCvc = function(modelValue, viewValue) {
          return Stripe.card.validateCVC(modelValue);
        }
      }
    }
  }
})()