(function () {
  'use strict';

  angular
  .module('idi.assessments.assessments', [])
  .directive('idiAssessments', directive);

  function directive () {
    return {
      templateUrl: '/assets/js/directives/assessments/assessments/idi.assessments.html',
      scope: {
        items: '='
      },
      link: function (scope, element, attrs) {

      }
    }
  }

})()