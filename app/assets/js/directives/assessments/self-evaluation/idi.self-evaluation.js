(function () {
  'use strict';

  angular
    .module('idi.assessments.self-evaluation', [])
    .directive('idiSelfEvaluation', directive);

  function directive($window, $modal) {
    return {
      templateUrl: '/assets/js/directives/assessments/self-evaluation/idi.self-evaluation.html',
      scope: {
        onSubmitted: '&'
      },
      link: function (scope, element, attrs) {
        scope.onSubmitted = scope.onSubmitted || angular.noop;
        scope.selfEvaluationText = $window.localStorage.selfEvaluationText || '';

        scope.save = function () {
          $window.localStorage.selfEvaluationText = scope.selfEvaluationText;
        };

        var selfEvaluationModalAccept = $modal({
          scope: scope,
          template: '/partials/modals/assessments.self-evaluation-accept.modal.html',
          templateUrl: '/partials/modals/assessments.self-evaluation-accept.modal.html',
          show: false
        });

        var selfEvaluationModalSubmit = $modal({
          scope: scope,
          template: '/partials/modals/assessments.self-evaluation-submit.modal.html',
          templateUrl: '/partials/modals/assessments.self-evaluation-submit.modal.html',
          show: false
        });

        scope.openModalAccept = function () {
          selfEvaluationModalAccept.$promise.then(selfEvaluationModalAccept.show);
        };

        scope.openModalSubmit = function () {
          selfEvaluationModalSubmit.$promise.then(selfEvaluationModalSubmit.show);
        };
      }
    }
  }

})()