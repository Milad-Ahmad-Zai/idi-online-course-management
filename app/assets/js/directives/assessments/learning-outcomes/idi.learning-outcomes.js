
(function () {
  'use strict';
  angular
  .module('idi.assessments.learning-outcomes', [])
  .directive('idiLearningOutcomes', directive)

  function directive () {
    return {
      templateUrl: '/assets/js/directives/assessments/learning-outcomes/idi.learning-outcomes.html',
      scope: {
        items: '='
      },
      link: function (scope, element, attrs) {

      }
    }
  }

})()