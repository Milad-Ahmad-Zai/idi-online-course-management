/**
 * @directive idi-topics-list
 * 
 * Combines three directives idi-topics-group with pagination
 *
 * @param {Array<Topic>} topics       - list of topics
 * @param {Object} pagination         - object with data for pagination
 * @param {Number} pagination.limit   - items per page
 * @param {Number} pagination.total   - total amount of items
 * @param {Number} pagination.page    - current page         
 * @param {String} topicState         - state for topic links 
 */
(function () {
  'use strict';

  angular
  .module('idi.forums.topics-list', [])
  .directive('idiTopicsList', directive);

  function directive ($state) {
    return {
      templateUrl: '/assets/js/directives/forums/topics-list/idi.topics-list.html',
      scope: {
        topics: '=',
        pagination: '=',
        topicState: '@'
      },
      link: function (scope, element, attrs) {

        var url = $state.href($state.current.name, {page: 'PAGE_VAR'});
        scope.pageUrl = url.replace('PAGE_VAR', '{page}');


        scope.$watch('topics', function (topics) {
          if (angular.isUndefined(topics)) {
            return;
          }

          if (!topics) {
            scope.hasNoTopics = true;
            return
          }

          scope.hasNoTopics = !(topics.length > 0);

          scope.stickyTopics = topics.filter(function (item) {
            return item.sticky == '1';
          });

          scope.learnTopics = topics.filter(function (item) {
            return item.learn_path == '1';
          });

          scope.userTopics = topics.filter(function (item) {
            return item.learn_path == '0' && item.sticky == '0';
          })
          
        })

      }
    }
  }
})()