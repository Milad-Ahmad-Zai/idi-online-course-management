/**
 * @directive idi-topics-group
 *
 * @param {Array<Topic>} topics   - list of topics
 *                                  Example: [{
 *                                    subject: 'Topic title',
 *                                    posts_count: 12,
 *                                    hits: 329,
 *                                    last_replied_date: <Date>,
 *                                    last_replied_user: 'Sam',
 *                                    pages: 15 // missing in response now
 *                                  }]
 *
 * @param {String} name           - name of the table. Will be show in header.
 *                                  Example: 'Sticky Topics'
 * @param {String} tableClasses   - list of classes separated by space which will be
 *                                  applied to the table
 *                                  Example: 'sticky-table disabled'
 * @param {String} topicState     - state of one topic
 * @param {Boolean} dragDisabled  - disables drag-n-drop and removes dragging icons from the table
 */

(function () {
  'use strict';

  angular
  .module('idi.forums.topics-group', [])
  .directive('idiTopicsGroup', directive);

  function directive ($state, NgTableParams, ForumsService, PermissionsService, $location) {
    return {
      templateUrl: '/assets/js/directives/forums/topics-group/idi.topics-group.html',
      // template: '<h1>test</h1>',
      scope: {
        topics: '=',
        name: '@',
        dragDisabled: '=',
        tableClasses: '@',
        topicState: '@'
      },
      link: function (scope, element, attrs) {
        scope.$watch('topics', function (nv) {
          scope.table = new NgTableParams({}, {
            dataset: scope.topics
          })
        })

        scope.rowClick = function (url) {
          $location.path(url);
        }

        scope.$state = $state;

        scope.getNumber = function (number) {
          return new Array(number);
        }

        scope.treeOptions = {
          dragStop: function ($event) {
            var newOrdering = scope.table.data.reduce(function (prev, current) {
              prev.push({
                id: current.id,
                ordering: prev.length + 1
              })

              return prev;
            }, []);

            ForumsService.postTopicsOrder(newOrdering);
          }
        };

        scope.hasPermissions = function(permission){
          return PermissionsService.has(permission);
        };
      }
    }
  }

})();