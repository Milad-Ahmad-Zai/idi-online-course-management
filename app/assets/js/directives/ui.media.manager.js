(function () {
  'use strict';

  angular
    .module('ui.media.manager', [])
    .directive('mediaManager', function ($modal, $rootScope, $window, FilesService, FileUploader, Notification, cfpLoadingBar) {
      return {
        restrict: 'E',
        templateUrl: '/partials/tpl/media.manager.html',
        scope: {
          files: '=',
          activeNode: '=?'
        },
        link: function ($scope, $el, $attr) {
          var table = angular.element($el.find('table')[0]);
          var tree = angular.element($el.find('aside')[0]);

          $scope.activeNode = $scope.files[0];
          $scope.allChecked = false;
          $scope.copied = false;
          $scope.copiedNodes = [];
          $scope.fileUploaderLoaded = false;

          $scope.getFolder = function (id) {
            var config = {
              id: id,
              start: 0,
              limit: 100
            };
            FilesService.getFolder(config).then(function (res) {
              $scope.activeNode.nodes = [];
              res.data.structure.list.forEach(function (folder) {
                var node = folder;
                if (node.type === 'folder') {
                  node.nodes = [];
                }
                $scope.activeNode.nodes.push(node);
              });
            });
          };

          $rootScope.fileUploaderDeferred.promise.then(function () {
            var uploader = $scope.uploader = FilesService.getUploader('content', '0', true);

            uploader.onCompleteItem = function(fileItem, response, status, headers) {
              cfpLoadingBar.complete();
              if (!response.error || status === 200) {
                var dataToSend = {
                  id: response.data.id,
                  folder_id: $scope.activeNode.id
                };
                FilesService.updateFile(response.data.id, dataToSend).then(function (res) {
                  $scope.getFolder($scope.activeNode.id);
                })
              } else {
                fileItem.msg = response.msg;
                Notification.error({title: 'Error', message: response.msg});
              }
            };

            uploader.onBeforeUploadItem = function(item) {
              item.url = item.url + '?access_token=' + angular.fromJson($window.localStorage.token).access_token;
              cfpLoadingBar.start();
            };

            FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
              return false; // true|false
            };

            $scope.fileUploaderLoaded = true;
          });

          $scope.unFocus = function (files) {
            for (var i = 0; i < files.length; i++) {
              files[i].edit = false;
              if (files[i].nodes && files[i].nodes.length > 0) {
                $scope.unFocus(files[i].nodes);
              }
            }
          };

          $scope.tableClickHandler = function (e) {
            var target = angular.element(e.target);
            if (target.scope()) {
              var node = target.scope().node;
              var files = target.scope().files;
              var nodeName = target[0].nodeName;
              if (node && !node.edit && ( nodeName === 'TD' || nodeName === 'SPAN' || nodeName === 'DIV' ||
                  nodeName === 'I' && !target.hasClass('edit-name') && !target.hasClass('icon-x'))) {
                $scope.activeNode = node;
                $scope.getFolder($scope.activeNode.id);
                $scope.allChecked = false;
              } else if (node && (target.hasClass('edit-name') || nodeName === 'INPUT' && target[0].type !== 'checkbox')) {
                node.edit = true;
                //var td = target.parent();
                //target.parent().find('input')[0].focus();
              } else if (target.hasClass('icon-x')) {
                delete node.note;
              } else if (target.hasClass('media-manager-add')) {
                $scope.editNode(target.scope());
              } else {
                $scope.unFocus(files);
              }
            }
          };

          $scope.treeClickHandler = function (e) {
            var target = angular.element(e.target);
            if (target.scope()) {
              var node = target.scope().node;
              var files = target.scope().files;
              var nodeName = target[0].nodeName;
              if (node && ( nodeName === 'TD' || nodeName === 'SPAN' || nodeName === 'DIV' || nodeName === 'I')) {
                $scope.activeNode = node;
                $scope.getFolder($scope.activeNode.id);
                $scope.allChecked = false;
              }
              $scope.unFocus(files);
            }
          };

          table.on('click', $scope.tableClickHandler);
          tree.on('click', $scope.treeClickHandler);

          $scope.onBlur = function (e) {
            var target = angular.element(e.target);
            var scope = target.scope();
            var node = scope && scope.node;
            var dataToSend = {};
            if (node && node.type === 'folder') {
              dataToSend = {
                id: node.id,
                folder_id: scope.activeNode.id,
                name: node.name
              };
              FilesService.updateFolder(node.id, dataToSend).then(function (res) {
                $scope.getFolder($scope.activeNode.id);
              });
            } else if (node && node.type !== 'folder') {
              dataToSend = {
                id: node.id,
                folder_id: scope.activeNode.id,
                title: node.name
              };
              FilesService.updateFile(node.id, dataToSend).then(function (res) {
                $scope.getFolder($scope.activeNode.id);
              });
            }
            node.edit = false;
          };

          $scope.createFolder = function (e) {
            if (!$scope.activeNode.nodes) {
              $scope.activeNode.nodes = [];
            }
            var folder = {
              name: angular.element(e.target).scope().folderName || 'untitled',
              type: 'folder',
              nodes: []
            };
            var dataToSend = {
              parent_id: $scope.activeNode.id,
              name: angular.element(e.target).scope().folderName || 'untitled'
            };
            FilesService.createFolder(dataToSend).then(function (res) {
              $scope.getFolder($scope.activeNode.id);
            });
          };

          $scope.addFiles = function (files) {

            // $scope.addedFiles = _.cloneDeep(files);
            // if (files) {
            //   if (!$scope.activeNode.nodes) {
            //     $scope.activeNode.nodes = [];
            //   }
            //
            //   for (var i = 0; i < $scope.addedFiles.length; i++) {
            //     $scope.activeNode.nodes.push($scope.addedFiles[i]);
            //   }
            //   files = [];
            //   $scope.addedFiles = [];
            // }
          };

          $scope.checkAllInput = function (e) {
            var state = false;
            if (angular.element(e.target)[0].checked) state = true;

            if ($scope.activeNode.nodes && $scope.activeNode.nodes.length > 0) {
              for (var i = 0; i < $scope.activeNode.nodes.length; i++) {
                $scope.activeNode.nodes[i].checked = state;
              }
            }
          };

          $scope.pasteSelected = function () {
            if (!$scope.activeNode.nodes) {
              $scope.activeNode.nodes = [];
            }
            console.log($scope.copiedNodes);
            for (var i = 0; i < $scope.copiedNodes.length; i++) {

              $scope.activeNode.nodes.push(_.cloneDeep($scope.copiedNodes[i]));
            }
          };

          $scope.copySelected = function () {
            $scope.copiedNodes = $scope.activeNode.nodes.filter(function (node) {
              return node.checked === true;
            });
            $scope.copied = true;
          };

          $scope.cutSelected = function () {
            $scope.copiedNodes = _.remove($scope.activeNode.nodes, function (node) {
              return node.checked === true;
            });

            $scope.copied = true;
          };

          $scope.deleteSelected = function () {
            var filteredNodes = $scope.activeNode.nodes.filter(function (node) {
              return node.checked === true;
            });
            var numberOfRequests = filteredNodes.length;

            function checkRequestsAndUpdate() {
              numberOfRequests -= 1;
              if (numberOfRequests === 0) {
                $scope.getFolder($scope.activeNode.id);
              }
            }

            $scope.activeNode.nodes.forEach(function (node) {
              if (node.checked === true) {
                if (node.type === 'folder') {
                  FilesService.deleteFolder(node.id).then(function (res) {
                    checkRequestsAndUpdate();
                  });
                } else {
                  FilesService.deleteFile(node.id).then(function (res) {
                    checkRequestsAndUpdate();
                  });
                }
              }
            });
            // _.remove($scope.activeNode.nodes, function (node) {
            //   return node.checked === true;
            // });
          };

          $scope.editNode = function (scope) {
            // Pre-fetch an external template populated with a custom scope
            var modal = $modal({
              scope: scope,
              template: 'partials/modals/media.manager.note.modal.html',
              templateUrl: 'partials/modals/media.manager.note.modal.html',
              show: false
            });

            modal.$promise.then(modal.show);
          };
        }
      };
    });
})();