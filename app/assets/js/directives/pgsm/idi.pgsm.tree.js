(function () {
  'use strcit';

  angular
  .module('idi.pgsm.tree', [])
  .directive('nodeTree', [function () {
    return {
      template: '<node ng-repeat="node in tree"></node>',
      replace: true,
      restrict: 'E',
      scope: {
        tree: '=children'
      }
    };
  }])

  .directive('node', function ($compile) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: '/assets/js/directives/pgsm/idi.pgsm.tree.html',
      link: function (scope, element) {
        /*
         * Here we are checking that if current node has children then compiling/rendering children.
         * */
        if (scope.node && scope.node.children && scope.node.children.length > 0) {
          scope.node.childrenVisibility = true;
          var childNode = $compile('<ul class="tree" ng-if="!node.childrenVisibility"><node-tree children="node.children"></node-tree></ul>')(scope);
          element.append(childNode);
        } else {
          scope.node.childrenVisibility = false;
        }
      },
      controller: function ($scope, $rootScope) {
        // This function is for just toggle the visibility of children
        $scope.toggleVisibility = function (node) {
          if (node.children) {
            node.childrenVisibility = !node.childrenVisibility;
          }
        };
        
        // Here We are marking check/un-check all the nodes.
        $scope.checkNode = function (node) {
          node.checked = !node.checked;
          function checkChildren(c) {
            angular.forEach(c.children, function (c) {
              c.checked = node.checked;
              checkChildren(c);
            });
          }

          checkChildren(node);
        };
      }
    };
  });


})();
