(function () {
  'use strict';
  
  angular
    .module('ui.conversationScope', [])
    .directive('conversationScope', [function() {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var cat = attrs.conversationScope;
          var className = '';

          switch (cat) {
            case 'tutor':
              className = 'icon-paper-plane';
              break;
            case 'finance':
              className = 'icon-wallet';
              break;
            case 'course-coordinator':
              className = 'icon-calc';
              break;
            case 'student-support':
              className = 'icon-apps1';
              break;
            case 'technical-support':
              className = 'icon-gear';
              break;
            case 'academic-administrator':
              className = 'icon-layer';
              break;
            case 'student':
              className = 'icon-paper-plane';
              break;
            case 'private-messages':
              className = 'icon-letter';
              break;
            case 'other':
              className = 'icon-cards';
              break;
            case 'announcements':
              className = 'icon-megaphone';
              break;
            default:
              className = 'icon-paper-plane';
              break;
          }
          element[0].className = className;
        }
      }
    }]);
})();