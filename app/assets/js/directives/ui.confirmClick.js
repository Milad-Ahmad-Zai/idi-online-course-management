(function () {
  'use strict';
  
  angular
    .module('ui.confirmClick', [])
    .directive('ngConfirmClick', [function () {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          element.bind('click', function(e) {
            var message = attrs.ngConfirmClick;
            if (message && !confirm(message)) {
              e.stopImmediatePropagation();
              e.preventDefault();
            }
          });
        }
      }
    }]);
})();