(function () {
  'use strict';

  angular
    .module('idi.notifications.widget', [])
    .directive('notificationsWidget', directive)

  function directive($rootScope, $interval) {
    return {
      templateUrl: '/assets/js/directives/common/notifications/idi.notificationsWidget.html',
      scope: {
        events: '=',
        widget: '=',
        typeFilter: '=?',
        searchFilter: '=?'
      },
      link: function (scope, element, attrs) {
        scope.$watch('typeFilter', function(newValue, oldValue, scope) {
          if(newValue=='all') {
            scope.tFilter = {type: ''};
          } else {
            scope.tFilter = {type: newValue};
          }
        });

        scope.$watch('searchFilter', function(newValue, oldValue, scope) {
          scope.sFilter = newValue;
        });

        scope.getPrettyTime = function(date) {
          var temp = moment(new Date(date)).fromNow();
          return temp.replace('minutes', 'mins')
        }
      }
    }
  }
})();