(function () {
  'use strict';

  angular
    .module('idi.profile.widget', [])
    .directive('profileWidget', directive)

  function directive($rootScope, $interval) {
    return {
      templateUrl: '/assets/js/directives/common/profile/idi.profileWidget.html',
      scope: {
        date: '=',
        social: '=',
        profile: '=',
        link: '='
      },
      link: function (scope, element, attrs) {
        scope.noCover = false;
        scope.noImage = false;

        if(scope.profile.cover_image.ext=='css') {
          scope.noCover = true;
        }

        if(scope.profile.profile_image.ext=='css') {
          scope.noImage = true;
        }

        if(scope.date==true) {
          getDate();
          // update every 10 seconds for clock
          $interval(getDate, 10000);
        }

        function getDate() {
          scope.clockTime = new Date();
        }
      }
    }
  }
})();