(function () {
  'use strict';

  var template;
  var result;

  angular
  .module('idi.bioCard', [])
  .service('cardCache', cardCache)
  .directive('idiBioCard', idiBioCard);

  function cardCache($cacheFactory) {
    return $cacheFactory('cardCache');
  }

  function idiBioCard ($compile, $document, $templateRequest, $templateCache, ProfilesService, cardCache) {
    var templateUrl = '/assets/js/directives/common/bio-card/idi.bio-card.html';
    $templateRequest(templateUrl);

    return {
      restrict: "AE",
      scope:{
        idiBioCard: "@"
      },

      link: function (scope, element, attrs) {
        var card;
        var isHovered = false;
        template = template || $templateCache.get(templateUrl);

        element.css("cursor", "pointer");

        /**
         * First of all we try to get card for this id from cache
         * If there is no such card, we create new one and put it
         * to cache
         */
        element.on('mouseenter', function(e) {
          card = cardCache.get(scope.idiBioCard);

          if (card) {
            showCard(card)
          } else {
            ProfilesService
            .getPublicProfile(scope.idiBioCard, {
              cache: true
            })
            .then(function(res) {
              scope.profile = res.data
              if (scope.profile.about!=null) {
                var temp = scope.profile.about.replace(/(\r\n|\n|\r)/gm," ");
                scope.profile.short = temp.replace(/(([^\s]+\s\s*){15})(.*)/,"$1…");
              }
            })

            scope.$apply(function() {
              var result = $compile(template)(scope);

              card = $document[0].body.appendChild(result[0]);
              card.addEventListener('mouseleave', onHoverLeave);
              cardCache.put(scope.idiBioCard, card);

              showCard(card);
            })
          }
        });


        function showCard(card) {
          var rect = getOffset(element[0]);

          isHovered = true;
          card.style.display = 'block';
          card.style.position = 'absolute';
          card.style.top = rect.top + 'px'
          card.style.left = rect.left + 'px';
        }


        function getOffset(element) {
          var rect = element.getBoundingClientRect();

          var body = document.body;
          var docEl = document.documentElement;

          var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
          var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

          var clientTop = docEl.clientTop || body.clientTop || 0;
          var clientLeft = docEl.clientLeft || body.clientLeft || 0;

          return {
            top: rect.top + scrollTop - clientTop,
            left: rect.left + scrollLeft - clientLeft
          }
        }


        function onHoverLeave(e) {
          if(!scope.keepDisplay){
            scope.isHovered = false;
            card.style.display = 'none';
          }
        }

        scope.$on('$destroy', function() {
          var card = cardCache.get(scope.idiBioCard)
          if (card) {
            card.removeEventListener('mouseleave', onHoverLeave);
            $document[0].body.removeChild(card);
          }
          cardCache.remove(scope.idiBioCard);
        });

      }
    }
  }
})();