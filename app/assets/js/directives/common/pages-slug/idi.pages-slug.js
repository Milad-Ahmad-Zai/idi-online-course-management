/**
 * @directive idi-pages-slug
 *
 * @description
 * Loads and displays content from Pages
 *
 * @param {String} idi-pages-slug - uniq slug to get a content
 * @param {Boolean} show-error - display error message when fails
 *                               to load the content. Default is true
 *
 * @using
 * <div idi-pages-slug="'uniq-slug-name'" show-error="false">
 * </div>
 */
(function() {

  angular
  .module('idi.pages-slug', [])
  .directive('idiPagesSlug', function(PagesService) {
    return {
      restrict: 'A',
      template: '<div ng-bind-html="content"></div>',
      link: function(scope, element, attrs) {
        var slug = scope.$eval(attrs.idiPagesSlug);
        var showError = scope.$eval(attrs.showError);

        init();

        function init() {
          PagesService.getPageBySlug(slug)
          .then(function(res) {
            scope.content = res.data.body
          })
          .catch(function(res) {
            if (showError === true || angular.isUndefined(showError)) {
              scope.content = '<div class="bg-danger panel-body">Error loading page content. Please contact technical support.</div>'
            }
          })
        }
      }
    }
  })
})()