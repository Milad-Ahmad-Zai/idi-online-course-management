(function () {
  'use strict';

  angular
    .module('idi.locale', [])
    .directive('idiLocale', directive);

  function directive ($rootScope, CONFIG) {
    return {
      scope: {
        localeName: '@idiLocale'
      },
      link: function (scope, element, attrs) {
        var localeValue = getLocale();
        if(!CONFIG.debug){
          if(localeValue) element.text(localeValue);
        }else{
          if(localeValue){
            element.html("<kbd>'" + scope.localeName + "'</kbd> " + localeValue);
          }else{
            element.html("<kbd class='text-danger'>'" + scope.localeName + "'</kbd>");
          }
        }

        function getLocale(){
          var path = $rootScope.locale;
          var localeNameArr = scope.localeName.split('.');
          for (var i = 0; i < localeNameArr.length; i++) {
            if(path[localeNameArr[i]]){
              path = path[localeNameArr[i]];
            }else{
              path = undefined;
              break;
            }
          }

          return path;
        }
      }
    }
  }

})()