(function () {
  'use strict';

  function idiCommonProfileImageDirective () {
    return {
      restrict: "EA",
      templateUrl: "/assets/js/directives/common/profile-image/idi.profile-image.html",
      scope:{
        coverHeight: "@",
        coverWidth: "@",
        coverClass: "@",
        coverUrl: "@",
        profile: "=?"
      },
      link: function (scope,element,attrs) {
        scope.noImage = false;
        if(scope.profile) {
          if(scope.profile.profile_image.ext=='css') {
            scope.noImage = true;
          }
          if(scope.profile.gender && scope.profile.gender.toLowerCase() == 'female') {
            scope.gender = 'female';
          } else {
            scope.gender = 'male';
          }
        }
      }
    };
  }

  angular
    .module('idi.common.profileImage', [])
    .directive('idiCommonProfileImage', [idiCommonProfileImageDirective]);
})();