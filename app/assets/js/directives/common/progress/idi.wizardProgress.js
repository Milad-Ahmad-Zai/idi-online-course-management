(function () {
  'use strict';

  angular
    .module('idi.wizard.progress', [])
    .directive('idiWizardProgress', directive)

  function directive($rootScope) {
    return {
      templateUrl: '/assets/js/directives/common/progress/idi.wizardProgress.html',
      scope: {
        tabs: '=',
        current: '='
      },
      link: function (scope, element, attrs) {
        scope.$watch('current', function() {
          var width = (100 / (scope.tabs.length*2)) + (scope.current * (100 / scope.tabs.length));
          scope.barWidth = width + '%';
          updateTabs(element, scope.current);
        });
      }
    }
  }

  function updateTabs(element, index) {
    var stages = element.querySelectorAll('.tabs li');
    [].forEach.call(stages, function(stage, i) {
      if (i === index) {
        stage.classList.add('active');
        stage.classList.remove('visited');
      } else {
        if (i < index) {
          stage.classList.add('visited');
          stage.classList.remove('active');
        } else {
          stage.classList.remove('visited');
          stage.classList.remove('active');
        }
      }
    });
  }
})();