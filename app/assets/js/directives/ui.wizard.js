(function () {
  'use strict';

  function formWizardDirective ($compile, $timeout) {
    return {
      restrict: 'AE',
      priority: 100,
      scope: {
        index: '=?'
      },
      link: function ($scope, $el, $attr) {
        $scope.progress = 0;
        $scope.index = $scope.index || 0;
        if($attr.viewingMode) {
          $scope.viewingMode = true;
        }

        var links = $el[0].querySelectorAll('.tabs li > button'),
          lis = $el[0].querySelectorAll('.tabs li'),
          panes = $el[0].querySelectorAll('.wizard_pane');
        $scope.numberOfTabs = lis.length;

        var tabs = $el[0].querySelector('.tabs');
        if ($scope.viewingMode) {
          var progress = $compile('<div class="bar" style="width: {{ 100 / numberOfTabs }}%; margin-left: {{100 * index / numberOfTabs}}%"></div>')($scope)
        } else {
          var progress = $compile('<div class="bar" style="width: {{ progress }}%"></div>')($scope)
        }

        //if (!$scope.viewingMode) {
          angular.element(tabs).after(progress);
        //}

        angular.element(links).on('click', function (e) {
          e.preventDefault();
          $scope.index = _getIndex.call(this);
        });

        $scope.$next = function (e) {
          if(e) e.preventDefault();
          window.scrollTo(0,0);
          if ($scope.index < links.length-1) $scope.index++;
        };

        $scope.$prev = function (e) {
          if(e) e.preventDefault();
          window.scrollTo(0,0);
          if ($scope.index > 0) $scope.index--;
        };

        $scope.$first = function (e) {
          if (e) e.preventDefault();
          $scope.index = 0;
        };

        $scope.$last = function (e) {
          if (e) e.preventDefault();
          $scope.index = links.length - 1;
        };

        function _getIndex () {
          return [].indexOf.call(links, this);
        }

        function _nodeWalk (index) {
          if ( index === (lis.length-1) ) {
            $scope.progress = 100;
          } else {
            var p = 1/lis.length * 100;
            $scope.progress = p * index + p/2;
          }

          if ($scope.viewingMode) {
            [].forEach.call(lis, function (node, i) {
              node.classList.remove('active');
              node.querySelector(':first-child').disabled = false;
            });
            lis[index].classList.add('active');
          } else {
            [].forEach.call(lis, function (node, i) {
              if (i <= index ) {
                node.classList.add('active');
                node.querySelector(':first-child').disabled = false;
              } else {
                node.classList.remove('active');
                node.querySelector(':first-child').disabled = true;
              }
            });
          }
        }

        function _paneWalk (index) {
          [].forEach.call(panes, function (pane, i) {
            if ( i === index ) pane.classList.add('active');
            else pane.classList.remove('active');
          });
        }

        $scope.$watch('index', function (newPosition) {
          $scope.$parent.hasChanged = false;
          _nodeWalk(newPosition);
          _paneWalk(newPosition);
        });
      }
    };
  }

  angular
    .module('ui.wizard', [])
    .directive('uiFormWizard', ['$compile', '$timeout', formWizardDirective]);

})();