(function () {
  'use strict';

  angular
    .module('idi.payments.card', [])
    .directive('idiPaymentCard', directive)

  function directive($rootScope) {
    return {
      templateUrl: '/assets/js/directives/payments/card/idi.paymentCard.html',
      scope: {
        card: '='
      },
      link: function (scope, element, attrs) {
        scope.$watch('card', function() {
          
        })
      }
    }
  }
})();