(function() {
  'use strict';

  angular
  .module('idi.payments.card-form', [])
  .directive('idiPaymentCardForm', idiPaymentCardForm);

  function idiPaymentCardForm() {
    return {
      templateUrl: '/assets/js/directives/payments/card-form/idi.paymentCardForm.html',
      scope: {
        card: '=',
        billing: '=',
        countries: '=',
        onSubmitted: '&',
        goBack: '&',
        addingCard: '=',
        cancelText: '@',
        submitText: '@',
        newCustomer: '='
      },
      link: function(scope, element, attrs) {
        init();

        function init() {

          scope.yearRange = getYearRange();

          scope.card.expiryMonth = parseInt(new Date().getMonth()) + 1;
          scope.card.expiryYear = new Date().getFullYear();

          scope.frozen = false;
          if (scope.newCustomer == false) {
            scope.card.expiryMonth = scope.card.exp_month;
            scope.card.expiryYear = scope.card.exp_year;
            scope.card.number = "**** **** **** " + scope.card.last4;
            scope.card.cvc = "***";
            scope.frozen = true;
          }
        }

        scope.onSubmit = function() {
          if( scope.newCustomer == false ) {
            // If existing customer return without validating as no details were taken
            scope.onSubmitted({card: scope.card, billing: scope.billing})
          } else {
            if (scope.form.$valid) {
              scope.onSubmitted({card: scope.card, billing: scope.billing})
            }
          }
        }

        scope.prevStep = function() {
          scope.goBack();
        }

        function getYearRange() {
          var currentYear = new Date().getFullYear();
          var years = [];
          for (var i = 0; i < 11; i++){
              years.push(currentYear + i);
          }
          return years;
        }
      }
    }
  }
})();