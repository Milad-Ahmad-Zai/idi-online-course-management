(function () {
  'use strict';

  angular
    .module('idi.payments.summary', [])
    .directive('idiPaymentSummary', directive)

  function directive($rootScope) {
    return {
      templateUrl: '/assets/js/directives/payments/summary/idi.paymentSummary.html',
      scope: {
        plan: '='
      },
      link: function (scope, element, attrs) {

      }
    }
  }
})();