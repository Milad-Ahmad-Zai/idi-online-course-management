(function () {
  'use strict';

  angular
    .module('idi.payments.subscriptions', [])
    .directive('idiPaymentSubscriptions', directive)

  function directive($rootScope, CONFIG, $window) {
    return {
      templateUrl: '/assets/js/directives/payments/subscriptions/idi.paymentSubscriptions.html',
      scope: {
        subscriptions: '='
      },
      link: function (scope, element, attrs) {
        // set access token
        scope.accessToken = JSON.parse($window.localStorage.token).access_token;

        scope.getPDF = function(id) {
          return CONFIG.url + 'payments/subscription/' + id + '.pdf?access_token=' + scope.accessToken;
        }
      }
    }
  }
})();