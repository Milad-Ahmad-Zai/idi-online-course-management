(function () {
  'use strict';

  angular
    .module('ui.chat', ['angularFileUpload'])
    .directive('uiChatToggle', [function () {
      return {
        restrict: 'AC',
        link: function ($scope, $el, $attr) {
          $el.on('click', function (e) {
            e.preventDefault();
            document.querySelector('body').classList.toggle('chat-open');
          });
        }
      }
    }])
    .directive('uiChat', function ($rootScope, $sce, $window, $modal, user, profile, AuthService, ProfilesService, MessageService, FilesService, FileUploader, Notification) {
      return {
        restrict: 'E',
        prority: 10,
        scope: {
          // users: '=ngModel'
          messageScopes: '='
        },
        replace: true,
        templateUrl: '/partials/chat.html',
        link: function($scope, $el, $attr, ngModel) {
          $scope.users = [];
          $scope.message = {};
          $scope.messageScope = {};
          $scope.conversation = [];
          $scope.tempFileIds = [];
          $scope.profile = profile;
          
          // add attachments
          var uploader = $scope.uploader = FilesService.getUploader('message', 'message', true);
          
          uploader.onCompleteItem = function(fileItem, response, status, headers) {
            if (!response.error || status == 200) {
              $scope.tempFileIds.push(response.data.id);
            } else {
              fileItem.msg = response.msg;
            }
          };
          
          uploader.onBeforeUploadItem = function(item) {
            item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
          };
          
          FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
            return false; // true|false
          };

          if ($scope.profile && $scope.profile.profile_image && $scope.profile.profile_image.url) {
            $scope.profile.image = $sce.trustAsResourceUrl($scope.profile.profile_image.url);
          }
          
          $scope.loadUserProfiles = function (users) {
            users.forEach(function (user) {
              var userId = user.user_id;
              
              if (userId == $scope.userId) {
                $scope.conversationUserId = user.id;
              }
              
              if (!$scope.users[userId]) {
                $scope.users[userId] = {};
                ProfilesService.getProfile(userId).then(function (res) {
                  if (res) {
                    if (res.data.first_name && res.data.last_name) {
                      $scope.users[userId].name = (res.data.first_name || '') + ' ' + (res.data.last_name || '');
                    } else {
                      $scope.users[userId].name = 'Unknown User';
                    }

                    if (res.data.file && res.data.file.type == 'profileimage') {
                      $scope.users[userId].profileImage = $sce.trustAsResourceUrl(res.data.file.url);
                    }
                  }
                });
              }
            });
          };
          
          $scope.sendMessage = function (e) {
            if (e.which === 13) {
              if ($scope.message.text.length > 0) {
                $scope.sendingMessage = true;
                
                var data = {
                  'conversation_id': $scope.messageScope.conversationId,
                  'user_id': $scope.profileId,
                  'text': $scope.message.text + '',
                  'created_at_local': new Date(),
                  'deleted': 0,
                  'file_ids': $scope.tempFileIds
                };
                
                if (!$scope.messageScope.conversationId) {
                  data.conversation_id = null;
                  data.scope_id = $scope.messageScope.id;
                  data.source_id = $scope.profileId;
                  data.topic = $scope.messageScope.name;
                }
                
                MessageService.sendMessage(data).then(function (res) {
                  if (res) {
                    if (!$scope.messageScope.conversationId) {
                      MessageService.getMessage(res.data.id).then(function (res) {
                        $scope.messageScopes.forEach(function (item) {
                          if (item.id == $scope.messageScope.id) {
                            item.conversationId = res.data.conversation_id;
                            $scope.messageScope.conversationId = res.data.conversation_id;
                          }
                        });
                      });
                    }
                    
                    var attachments = [];
                    if ($scope.tempFileIds.length > 0) {
                      $scope.tempFileIds.forEach(function (item) {
                        attachments.push({
                          file_id: item
                        });
                      });
                    }
                    
                    $scope.conversation.push({
                      conversation_id: $scope.messageScope.conversationId,
                      date: new Date(),
                      deleted: 0,
                      text: $scope.message.text,
                      user_id: $scope.profileId,
                      id: res.data.id,
                      hasAttachments: $scope.tempFileIds.length > 0,
                      MessageAttachments: attachments
                    });
                    
                    Notification.success({
                      message: 'Your message has been successfully sent.',
                      title: "Success"
                    });
                  } else {
                    Notification.error({
                      message: "There's an error processing your request.",
                      title: "Error"
                    });
                  }
                  
                  $scope.sendingMessage = false;
                  $scope.message.text = '';
                  $scope.tempFileIds = [];
                  uploader.clearQueue();
                });
              }
            }
          };
          
          $scope.displayAttachedFiles = function (message) {
            message.files = [];
            message.displayAttachments = true;
            
            message.MessageAttachments.forEach(function (item, index) {
              FilesService.getFile(item.file_id).then(function (res) {
                if (res.data.id > 0) {
                  message.files.push(res.data);
                }
              });
            });
          };
          
          $scope.enter = function (e, messageScope) {
            e.preventDefault();
            
            $scope.messageScope = messageScope;
            
            if (messageScope.conversationId) {
              MessageService.getConversation(messageScope.conversationId).then(function (res) {
                var message = res.data;
                
                var messages = [];
                message.Messages.forEach(function (mes) {
                  if (mes.deleted == 0) {
                    var createdMoment = moment.utc(mes.created_at);
                    mes.date = createdMoment.toDate();

                    mes.text = $sce.trustAsHtml(mes.text);

                    if (mes.modified_at) {
                      var modifiedMoment = moment.utc(mes.modified_at);
                      mes.modifiedDate = modifiedMoment.toDate();
                      mes.isUpdated = true;
                    }
                    
                    mes.hasAttachments = false;
                    if (mes.MessageAttachments.length > 0) {
                      mes.MessageAttachments.forEach(function (item) {
                        if (item.deleted == 0) mes.hasAttachments = true;
                      });
                    }
                    
                    messages.push(mes);
                  }
                });
                $scope.conversation = messages;
                
                $scope.conversationUsers = message.ConversationUsers;
                $scope.loadUserProfiles($scope.conversationUsers);
                
                $el[0].classList.add('open-conversation');
              });
            } else {
              $el[0].classList.add('open-conversation');
            }
          };
          
          $scope.back = function (e) {
            e.preventDefault();
            $el[0].classList.remove('open-conversation');
          };
        }
      };
    });
})();