(function () {
  'use strict';
  
  angular
    .module('ui.post', ['angularFileUpload'])
    .directive('editPostForm', ['$sce', '$window', '$rootScope', 'cfpLoadingBar', 'ForumsService', 'FilesService', 'FileUploader', 'Notification', function ($sce, $window, $rootScope, cfpLoadingBar, ForumsService, FilesService, FileUploader, Notification) {
      return {
        restrict: 'AE',
        templateUrl: '/partials/forums/edit.post.form.html',
        scope: {
          post : '=',
          files : '=',
          tpModel : '=',
        },
        replace: true,
        link: function ($scope, element, attrs) {
            
            
        var topicsListConfig = {
                id: $rootScope.forum.id,
                start: 0,
                limit: 999999,
                filter: 'export=1'
            };
        
        var currentTopic = null;
            
          $scope.reply = { message: $scope.post.text };
          $scope.tempMessage = $scope.post.text;
          $scope.tempFileIds = [];
          $scope.files = [];
          $scope.deletedFileIds = [];
          
          $scope.cancelEdit = function () {
            $scope.post.isEdit = false;
            $scope.post.text = $scope.tempMessage;
          };
          
          $scope.updatePost = function (form) {
            cfpLoadingBar.start();
            
            if ($scope.tpModel.selected.id)  {
                if ($scope.tpModel.selected.id != currentTopic.id) {
                    $scope.hide = $scope.post.id;
                    $scope.post.hide = true;
                }
            }
            
            var updated = new Date();
            var replyObj = {
              reply_to: $scope.post.reply_to,
              subject: $scope.post.subject,
              text: $scope.reply.message,
              updated_at: updated,
              topic_id: $scope.tpModel.selected.id
            };
              
            ForumsService.updateSavePost(replyObj, $scope.post.id).then(function (resPost) {
              if (resPost) {
                if (!resPost.error) {
                  $scope.post.textHtml = $scope.reply.message.replace(/\r?\n/g, '<br />');
                  $scope.post.updated_date = updated;
                  $scope.reply = {};
                  form.$setPristine();
                  form.$setUntouched();
                  
                  // post attachments if any
                  if ($scope.tempFileIds.length > 0) {

                    $scope.tempFileIds.forEach(function (item, index) {
                      var data = {
                        file_id: item,
                        entity_id: resPost.data.id,
                        post_id: resPost.data.id,
                        type: 'post',
                        topic_id: $scope.tpModel.selected.id
                      };
                           
                      ForumsService.attachFileToPostTopic(data).then(function (attachRes) {
                        if (index == ($scope.tempFileIds.length - 1)) {
                          $scope.post.attachments = $scope.post.attachments.concat($scope.files);                            
                            var first = attachRes.data.id - $scope.tempFileIds.length + 1;
                            
                           $scope.files.forEach(function (element, index) {
                               element.id = first + index;
                            });
                            
                          $scope.tempFileIds = [];
                          uploaderEdit.clearQueue();
                          $scope.post.isEdit = false;  
                          
                          $scope.displayNotification(true, 'Post successfully updated.');
                        }
                      });
                    });
                  } else {
                    $scope.post.isEdit = false;
                    $scope.displayNotification(true, 'Post successfully updated.');
                  }
                } else {
                  $scope.displayNotification(true, restPost.msg);
                }
              } else {
                $scope.displayNotification(true, 'There was a problem processing your request.');
              }
              
              cfpLoadingBar.complete();
            });
          };
        
          var uploaderEdit = $scope.uploaderEdit = FilesService.getUploader('forumattachment', 'forumattachment', true);
          
          uploaderEdit.onBeforeUploadItem = function(item) {
            item.url = item.url + '?access_token=' + JSON.parse($window.localStorage.token).access_token;
          };
          
          FileUploader.FileSelect.prototype.isEmptyAfterSelection = function() {
            return false;
          };
          
          uploaderEdit.onCompleteItem = function(fileItem, response, status, headers) {
            if (!response.error || status == 200) {
              $scope.tempFileIds.push(response.data.id);
              $scope.files.push({ file: response.data });
            } else {
              fileItem.msg = response.msg;
            }
          };
          
        $scope.removeFileId = function (item) {
            var index = uploaderEdit.queue.indexOf(item);
            var fileId = $scope.tempFileIds[index];
            if (fileId) {
              $scope.tempFileIds.splice(index, 1);
              FilesService.deleteFile(fileId).then(function(res) {});
            }
            
            _.remove($scope.files, function (n) {
              return n.file.id == fileId;
            });
          };
          
        $scope.deleteAttachment = function (item) {
            _.remove($scope.post.attachments, function (n) {
              if (n.id == item.id) {
                ForumsService.deleteAttachment(n.id ).then(function (res) {});
                FilesService.deleteFile(n.file.id).then(function(res) {});
                return true;
              }
            });
          };
          
          $scope.displayNotification = function (isSuccess, msg) {
            if (isSuccess) {
              Notification.success({
                title: 'Success',
                message: msg
              });
            } else {
              Notification.error({
                title: 'Error',
                message: msg
              });
            }
          };
            
            
            var topicsListConfig = {
                id: $rootScope.forum.id,
                start: 0,
                limit: 999999,
                filter: 'export=1'
            };

            $scope.topics = [];

            ForumsService.getForumTopics(topicsListConfig).then(function (res) {
                  if (res) {
                        $scope.topicsList = res.data.list;

                        angular.forEach($scope.topicsList, function (item, key) {
                            var topic = {
                                subject: item.subject,
                                id: item.id
                            }
                            $scope.topics.push(topic);
                        
                            if (item.id == $scope.tpModel.id) {
                                $scope.tpModel.selected = {
                                    subject: item.subject,
                                    id: item.id
                                };
                                currentTopic = $scope.tpModel.selected;
                            }
                            
                        });
                      

                    } else {
                       return false; 
                }
            });
            
        }
      }
    }]);
})();