(function () {
  'use strict';

  angular
    .module('idi.messages.message', [])
    .directive('idiMessage', directive)

  function directive($rootScope, MessageService) {
    return {
      templateUrl: '/assets/js/directives/messages/message/idi.message.html',
      scope: {
        message: '=',
        studio: '=',
        onAttachmentClick: '&',
        onMessageDeleted: '&'
      },
      link: function (scope, element, attrs) {

        scope.onAttachmentClick = scope.onAttachmentClick || angular.noop;
        scope.onMessageDeleted = scope.onMessageDeleted || angular.noop;


        scope.currentPermissions = $rootScope.currentPermissions;
        scope.isEditable = isEditable($rootScope, scope.message);
        scope.message.isEdit = false;
        scope.createdDate = moment.utc(scope.message.created_at).toDate()


        /**
         * Removes attachment from current message
         * @param  {File} file  - file that should be removed
         * @return {[type]}      [description]
         */
        scope.removeAttachment = function (attachment) {
          var file = attachment.file;
          var permissions = $rootScope.currentPermissions;

          // list all attachments except one that
          // we're going to delete
          var attachments = scope.message.MessageAttachments
            .reduce(function (result, attachment) {
              if (attachment.file.id !== file.id) {
                result.push(attachment.file.id);
              }
              return result;
            }, []);

          // post updated list of attachments
          // TODO:  Replace with method for removing attachments
          //        as soon as it implemented on backend
          var data = {
            'text': scope.message.text,
            'modified_by' : $rootScope.userId,
            'file_ids': attachments
          };

          MessageService.updateMessage(scope.message.id, data);
        };


        /**
         * Deletes current message and calls
         * `onMessageDeleted` callback
         */
        scope.deleteMessage = function () {
          scope.message.deleted = '1';

          MessageService.deleteMessage(scope.message.id)
            .then(scope.onMessageDeleted.bind(null, {message: scope.message}))
        }
      }
    }
  };

  function isEditable($rootScope, message) {
    var flag = false;
    var permissions = $rootScope.currentPermissions;

    if (message.deleted == '1') { return false; }

    if ((permissions.indexOf('messages.messageAny.delete') > -1 || permissions.indexOf('messages.messageAny.edit') > -1) && message.deleted == 0) {
      flag = true;
    } else if ($rootScope.user.id == message.user_id) {
      var diffMs = new Date() - message.date;
        var diffMins = diffMs / 60000; // minutes

        if (diffMins < 10) { flag = true; }
    }
    return flag;
  };
})();