(function () {
angular
  .module('ui.routes', ['ui.router', 'oc.lazyLoad'])
  .config(providerConfig)
  .config(permissionsConfig)
  .config(routesConfig)
  .run(definePermissions)


/**
 * Here we decorate every state to add default
 * redirects. We can override it in state definition
 */
function permissionsConfig($stateProvider, $provide) {
  /**
   * Original method in angular-permissions library
   * replaces `redirectTo` param value of child state with
   * `redirectTo` of his parent. This decorator extends
   * child's object with parent's `redirectTo`
   */
  $provide.decorator('StatePermissionMap', function($delegate) {
    var originalExtend = $delegate.prototype.extendPermissionMap;
    $delegate.prototype.extendPermissionMap = function(permissionMap) {
      var originalRedirectTo = this.redirectTo;
      originalExtend.call(this, permissionMap);
      if (angular.isDefined(originalRedirectTo))
        this.redirectTo = originalRedirectTo;
    }

    return $delegate;
  })

  /**
   * Setting default permission and redirects rules for every
   * state. We want to redirect anonymous users to login page
   * and the rest users to 403 page
   */
  $stateProvider.decorator('data', function (state, parent) {
    var defaultData = parent(state);

    var data = angular.merge({}, {
      permissions: {
        redirectTo: {
          anonymous: 'app.login',
          default: 'app.admin.error.403'
        }
      }
    }, defaultData);

    return data;
  });
}

function providerConfig($httpProvider, $urlRouterProvider) {
  $httpProvider.interceptors.push('InterceptorService');
  $urlRouterProvider.deferIntercept();
}

function definePermissions(AuthService, $urlRouter) {
  AuthService.definePermissions()
  .then(function() {
    $urlRouter.sync()
    $urlRouter.listen()
  })
}

function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $stateProvider

  // root route
    .state('app', {
      abstract: true,
      templateUrl: '/partials/home.html',
      controller: 'AppCtrl',
      resolve: {
        localeService: function (load, $rootScope, LocaleService) {
          var localeDefault, locale;
          LocaleService.getLocaleDefault()
            .then(function (res) {
              localeDefault = res.locale;
              return LocaleService.getLocale();
            })
            .then(function (res) {
              locale = res.locale || {};
              $rootScope.locale = _.merge(localeDefault, locale);
            })
        },
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'cfp.loadingBar', files: ['/assets/js/vendor/loading-bar.js']},
            {name: 'ui-notification', files: ['/assets/js/vendor/angular/angular-ui-notification.js']},
            {name: 'angular.filter', files: ['/assets/js/filters/angular-filter.js']},
            {name: 'mb-scrollbar', files: ['/assets/js/vendor/mb-scrollbar.js']},
            {name: 'ctrl.app', files: ['/assets/js/controllers/app.ctrl.js']},

            // ui elements
            {name: 'ui.c3', files: ['/assets/js/directives/charts/c3.js']},
            {name: 'ui.sidemenu', files: ['/assets/js/directives/ui.sidemenu.js']},
            {name: 'ui.panels', files: ['/assets/js/directives/ui.panels.js']},
            {name: 'ui.dropdown', files: ['/assets/js/vendor/bootstrap/ui.dropdown.js']},
            {name: 'ui.tabs', files: ['/assets/js/vendor/bootstrap/ui.tab.js']},
            {name: 'ui.collapse', files: ['/assets/js/vendor/bootstrap/ui.collapse.js']},
            {name: 'ui.bootstrap.carousel', files: ['/assets/js/vendor/bootstrap/ui.carousel.js']},
            {
              name: 'ui.code',
              files: ['/assets/js/directives/ui.code.js', '/assets/js/vendor/thirdparty/highlight.pack.js']
            },
            {name: 'ui.tree', files: ['/assets/js/vendor/angular/angular-ui-tree.js']},
            {name: 'ui.mask', files: ['/assets/js/vendor/bootstrap/ui.mask.js']},
            {name: 'ui.select', files: ['/assets/js/vendor/angular/angular-ui-select.js']},
            {name: 'ui.media.manager', files: ['/assets/js/directives/ui.media.manager.js']},
            {
              name: 'ui.markdown',
              files: ['/assets/js/directives/ui.markdown.js', '/assets/js/vendor/thirdparty/marked.js']
            },
            {name: 'ui.multiselect', files: ['/assets/js/directives/ui.multiselect.js']},
            {name: 'ui.tags', files: ['/assets/js/directives/ui.tags.js']},
            {name: 'ui.wizard', files: ['/assets/js/directives/ui.wizard.js']},
            {name: 'ui.chat', files: ['/assets/js/directives/ui.chat.js']},
            {name: 'ui.countTo', files: ['/assets/js/directives/ui.countTo.js']},
            {name: 'ui.message', files: ['/assets/js/directives/ui.message.js']},
            {name: 'ui.thumbnail.directive', files: ['/assets/js/directives/ui.thumbnail.directive.js']},
            {name: 'ui.conversationScope', files: ['/assets/js/directives/ui.conversationScope.js']},
            {name: 'ui.confirmClick', files: ['/assets/js/directives/ui.confirmClick.js']},
            {name: 'ui.file.thumbnail', files: ['/assets/js/directives/ui.file.thumbnail.js']},
            {name: 'ui.post', files: ['/assets/js/directives/ui.post.js']},
            {name: 'ui.student.programme', files: ['/assets/js/directives/ui.student.programme.js']},
            {name: 'ui.student.course', files: ['/assets/js/directives/ui.student.course.js']},
            {name: 'ui.student.module', files: ['/assets/js/directives/ui.student.module.js']},
            {
              name: 'idi.common.profileImage',
              files: ['/assets/js/directives/common/profile-image/idi.profile-image.js']
            },

            // editor
            {name: 'ngQuill', files: ['/assets/js/vendor/ng-quill.js', '/assets/js/vendor/thirdparty/quill.js']},

            // genie.js
            {name: 'uxGenie', files: ['/assets/js/vendor/angular/angular-lamp.js']},

            // vendor
            {name: 'ngTable', files: ['/assets/js/vendor/ng-table.js']},
            {name: 'vr.directives.slider', files: ['/assets/js/vendor/angular/angular-slider.js']},
            {name: 'angularFileUpload', files: ['/assets/js/vendor/angular/angular-file-upload.min.js']},
            {name: 'angular-svg-round-progress', files: ['/assets/js/vendor/angular/roundProgress.js']},
            {name: 'ngclipboard', files: ['/assets/js/vendor/ngclipboard.js']},


            // widgets
            {name: 'month.widget', files: ['/assets/js/directives/widgets/month.widget.js']},
            {name: 'balance.widget', files: ['/assets/js/directives/widgets/balance.widget.js']},
            {name: 'planner.widget', files: ['/assets/js/directives/widgets/planner.widget.js']},

            // thirdparty
            {
              files: [
                '/assets/js/vendor/thirdparty/d3.js',
                '/assets/js/vendor/thirdparty/c3.js'
              ]
            },

            // directives
            {name: 'compile.template.directive', files: ['/assets/js/directives/compile.template.directive.js']},
            {name: 'idi.locale', files: ['/assets/js/directives/common/locale/idi.locale.js']},
            {name: 'idi.notifications.widget', files: ['/assets/js/directives/common/notifications/idi.notificationsWidget.js']},

            // services
            {name: 'message.service', files: ['/assets/js/services/message.service.js']},
            {name: 'profiles.service', files: ['/assets/js/services/profiles.service.js']},
            {name: 'files.service', files: ['/assets/js/services/files.service.js']},
            {name: 'admissions.service', files: ['/assets/js/services/admissions.service.js']},
            {name: 'notes.service', files: ['/assets/js/services/notes.service.js']},
            {name: 'listcache.service', files: ['/assets/js/services/listcache.service.js']},
            {name: 'courses.service', files: ['/assets/js/services/courses.service.js']},
            {name: 'forums.service', files: ['/assets/js/services/forums.service.js']},
            {name: 'galleries.service', files: ['/assets/js/services/galleries.service.js']},
            {name: 'semesters.service', files: ['/assets/js/services/semesters.service.js']},
            {name: 'salesforce.service', files: ['/assets/js/services/salesforce.service.js']},
            {name: 'permissions.service', files: ['/assets/js/services/helpers/permissions.service.js']},
            {name: 'services.studypath2', files: ['/assets/js/services/studypath2.service.js']},
            {name: 'locale.service', files: ['/assets/js/services/locale.service.js']},
            {name: 'todo.service', files: ['/assets/js/services/todo.service.js']},
            {name: 'pages.service', files: ['/assets/js/services/pages.service.js']},
            {name: 'forms.service', files: ['/assets/js/services/forms.service.js']},
            {name: 'payments.student.service', files: ['/assets/js/services/payments/student/payments.service.js'] },
            {name: 'services.assessments', files: ['/assets/js/services/assessments.service.js'] },

            // filters
            {name: 'age.filter', files: ['/assets/js/filters/age.filter.js']},
            {name: 'unslugify.filter', files: ['/assets/js/filters/unslugify.filter.js']},
            {name: 'moment.filter', files: ['/assets/js/filters/moment.filter.js'] },
            {name: 'customCurrency.filter', files: ['/assets/js/filters/customCurrency.filter.js'] }
          ])
        }
      }
    })

    // admin panel
    .state('app.admin', {
      abstract: true,
      views: {
        'sidebar': {
          templateUrl: '/partials/sidebar.html',
          controller: 'MainCtrl'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.main', files: ['/assets/js/controllers/main.ctrl.js']}
          ])
        },
        permissions: function ($rootScope, $state, AuthService, PermissionStore) {
          return AuthService.getPermissions()
            .then(function (res) {
              var permissions = res.data.permissions;
              return $rootScope.currentPermissions = permissions;
            })
            .catch(function(res) {
              $state.go('app.login')
            })
        },
        user: function (load, $rootScope, AuthService) {
          return AuthService.me().then(function (res) {
            if (res.status !== 403) {
              // we need this for Admissions in Profiles, will delete it if we rewrite Admissions
              $rootScope.user = res.data.user;
              $rootScope.userId = res.data.user.id;
            }
            return res.data.user;
          });
        },
        profile: function (load, user, ProfilesService) {
          return ProfilesService.getProfile(user.id).then(function (res) {
            return res.data;
          });
        }
      },
      data: {
        permissions: {
          except: ['anonymous']
        }
      }
    })

    .state('app.admin.index', {
      url: '/',
      data: {
        permissions: {
          only: ['non.existing.permission'],
          redirectTo: {
            'admissions.role.advisor' : 'app.admin.admissions.advisor',
            'admissions.role.applicant':'app.admin.admissions.student',
            'anonymous' : 'app.login',
            'default' : 'app.admin.dashboard'
          }
        }
      }
    })

    // Media Manager
    .state('app.admin.files', {
      url: '/files',
      views: {
        'content@app': {
          controller: 'FilesCtrl',
          templateUrl: '/partials/files/index.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'filter.bytes', files: ['/assets/js/filters/bytes.filter.js']},
            {name: 'ctrl.files', files: ['/assets/js/controllers/files.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['filemanager.folder.get'],
          redirectTo: {
            default: 'app.admin.admissions.student'
          }
        }
      }
    })
    // end

    // Forums Old
    .state('app.admin.forumsold', {
      url: '/forums',
      views: {
        'content@app': {
          controller: 'ForumsOldCtrl',
          templateUrl: '/partials/forums-old/index.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.forums', files: ['/assets/js/controllers/forums-old.ctrl.js']
            }
          ])
        }
      },
      data: {
        permissions: {
          except: ['anonymous'],
          redirectTo: {
            'default': 'app.login'
          }
        }
      }
    })
    // end

    // Forums
    .state('app.admin.forums', {
      url: '/forums-new',
      abstract: true,
      views: {
        'content@app': {
          controller: 'ForumsCtrl',
          templateUrl: '/partials/forums/index.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.forums', files: ['/assets/js/controllers/forums/forums.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.get', 'forums.forumSelf.get'],
          redirectTo: {
            'default': 'app.login'
          }
        }
      }
    })

    .state('app.admin.forums.manager', {
      url: '/manager',
      views: {
        'subcontent@app.admin.forums': {
          controller: 'ForumsManagerCtrl',
          templateUrl: '/partials/forums/manager.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.forums.manager', files: ['/assets/js/controllers/forums/forums.manager.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.edit', 'forums.forumSelf.edit', 'forums.forumAny.delete', 'forums.forumSelf.delete', 'forums.forum.publish', 'forums.forum.unpublish'],
        }
      }
    })

    .state('app.admin.forums.manager.edit', {
      url: '/:id',
      views: {
        'subcontent@app.admin.forums': {
          controller: 'ForumsManagerCtrl',
          templateUrl: '/partials/forums/manager.edit.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.forums.manager', files: ['/assets/js/controllers/forums/forums.manager.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.edit', 'forums.forumSelf.edit', 'forums.forumAny.delete', 'forums.forumSelf.delete', 'forums.forum.publish', 'forums.forum.unpublish'],
        }
      }
    })

    .state('app.admin.forums.list', {
      url: '/list',
      views: {
        'subcontent@app.admin.forums': {
          controller: 'ForumsListCtrl',
          templateUrl: '/partials/forums/forums.list.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.forums.list', files: ['/assets/js/controllers/forums/forums.list.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.get', 'forums.forumSelf.get'],
        }
      }
    })

    .state('app.admin.forums.view', {
      url: '/:forumId?page',
      views: {
        'subcontent@app.admin.forums': {
          controller: 'TopicsListCtrl',
          templateUrl: '/partials/forums/topics.list.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'idi.forums.topics-group', files: ['/assets/js/directives/forums/topics-group/idi.topics-group.js']},
            {name: 'idi.forums.topics-list', files: ['/assets/js/directives/forums/topics-list/idi.topics-list.js']},
            {name: 'bw.paging', files: ['/assets/js/vendor/angular/paging.js']},
            {name: 'ctrl.topics.list', files: ['/assets/js/controllers/forums/topics.list.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.get', 'forums.forumSelf.get'],
        }
      }
    })

    .state('app.admin.forums.topic', {
      url: '/topic/:topicId/:pageNo',
      params: {
        fromStudio: null
      },
      views: {
        'subcontent@app.admin.forums': {
          controller: 'TopicPostsCtrl',
          templateUrl: '/partials/forums/topic.posts.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.topic.posts', files: ['/assets/js/controllers/forums/topic.posts.ctrl.js']},
            {name: 'idi.bioCard', files: ['/assets/js/directives/common/bio-card/idi.bio-card.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['forums.forumAny.get', 'forums.forumSelf.get'],
        }
      }
    })
    // end

    /**
     * ### Galleries ###
     *
     * /galleries - list of studios
     * /galleries/studio/:studioId - studio, list of galleries
     * /galleries/:galleryId - gallery, list of albums
     * /galleries/album/:albumId - album, list of files
     */

    .state('app.admin.galleries', {
      url: '/galleries',
      abstract: true,
      views: {
        'content@app': {
          controller: 'GalleriesCtrl',
          templateUrl: '/partials/galleries/index.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.galleries', files: ['/assets/js/controllers/galleries/galleries.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['gallery.studioAny.get', 'gallery.studioSelf.get'],
        }
      }
    })

    /**
     * @url /galleries
     * @ctrl StudiosListCtrl
     */
    .state('app.admin.galleries.list', {
      url: '',
      views: {
        'subcontent@app.admin.galleries': {
          controller: 'StudiosListCtrl',
          templateUrl: '/partials/galleries/studios.list.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'ctrl.studios.list', files: ['/assets/js/controllers/galleries/studios.list.ctrl.js']}
          ]);
        }
      },
      data: {
        permissions: {
          only: ['gallery.studioAny.get', 'gallery.studioSelf.get'],
        }
      }
    })

    /**
     * @url /galleries/studio/:studioId
     * @ctrl GalleriesListCtrl
     */
    .state('app.admin.galleries.studio', {
      url: '/studio/:studioId',
      views: {
        'subcontent@app.admin.galleries': {
          controller: 'GalleriesListCtrl',
          templateUrl: '/partials/galleries/galleries.list.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.galleries.list', files: [
              '/assets/js/controllers/galleries/galleries.list.ctrl.js'
            ]
            }
          ])
        }
      },
      data: {
        permissions: {
          only: ['gallery.galleryAny.get', 'gallery.gallerySelf.get'],
        }
      }
    })

    /**
     * @url /galleries/:galleryId
     * @ctrl AlbumsListCtrl
     */
    .state('app.admin.galleries.gallery', {
      url: '/:galleryId',
      views: {
        'subcontent@app.admin.galleries': {
          controller: 'AlbumsListCtrl',
          templateUrl: '/partials/galleries/albums.list.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.galleries.albums.list', files: [
              '/assets/js/controllers/galleries/albums.list.ctrl.js'
            ]
            }
          ])
        }
      },
      data: {
        permissions: {
          only: ['gallery.galleryAny.get', 'gallery.gallerySelf.get'],
        }
      }
    })

    /**
     * @url /galleries/album/:albumId
     * @ctrl AlbumCtrl
     */
    .state('app.admin.galleries.album', {
      url: '/album/:albumId',
      views: {
        'subcontent@app.admin.galleries': {
          controller: 'AlbumCtrl',
          templateUrl: '/partials/galleries/album.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.galleries.albums.view', files: [
              '/assets/js/controllers/galleries/album.ctrl.js'
            ]
            }
          ])
        }
      },
      data: {
        permissions: {
          only: ['gallery.galleryAny.get', 'gallery.gallerySelf.get'],
        }
      }
    })

    // start
    .state('app.admin.courses', {
      abstract: true,
      controller: 'CoursesCtrl',
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.courses', files: ['/assets/js/controllers/courses/courses.ctrl.js']
            },
            {
              name: 'ui.errorPopup', files: ['/assets/js/directives/ui.errorPopup.js']
            }
          ])
        }
      }
    })
    .state('app.admin.courses.level', {
      url: '/level/:id',
      views: {
        'content@app': {
          controller: 'LevelCtrl',
          templateUrl: '/partials/courses/level.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.level', files: ['/assets/js/controllers/courses/level.ctrl.js'],
            },
            {
              name: 'idi.bioCard', files: ['/assets/js/directives/common/bio-card/idi.bio-card.js']
            }
          ])
        }
      },
      data: {
        permissions: {
          only: ['courses.module.search'],
        }
      }
    })
    .state('app.admin.courses.myStudents', {
      url: '/my/students/:moduleNameId',
      views: {
        'content@app': {
          controller: 'TutorStudentsCtrl as tutorStudents',
          templateUrl: '/partials/courses/tutor.students.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.tutor.students', files: ['/assets/js/controllers/courses/tutor.students.ctrl.js']
            },
            {
              name: 'services.studypath2', files: ['/assets/js/services/studypath2.service.js']
            }
          ])
        },
        students: function (load, $stateParams, $rootScope, StudyPath2Service, user) {
          return StudyPath2Service.getListOfStudentsForTutor(user.id, $stateParams.moduleNameId)
        }
      },
      data: {
        permissions: {
          only: ['studypath.role.tutor'],
        }
      }
    })
    .state('app.admin.courses.otherStudents', {
      url: '/other/students/:moduleNameId',
      views: {
        'content@app': {
          controller: 'TutorStudentsCtrl as tutorStudents',
          templateUrl: '/partials/courses/tutor.students.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {
              name: 'ctrl.tutor.students', files: ['/assets/js/controllers/courses/tutor.students.ctrl.js']
            },
            {
              name: 'services.studypath2', files: ['/assets/js/services/studypath2.service.js']
            }
          ])
        },
        students: function (load, $stateParams, $rootScope, StudyPath2Service, AuthService) {
          return AuthService.me().then(function (res) {
            return StudyPath2Service.getListOfStudentsForOtherTutors(res.data.user.id, $stateParams.moduleNameId)
          })
        }
      },
      data: {
        permissions: {
          only: ['studypath.role.tutor'],
        }
      }
    })

    // pages

    .state('app.register', {
      url: '/register',
      views: {
        '@': {
          controller: 'RegisterCtrl as register',
          templateUrl: '/partials/pages/registration.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'register.ctrl', files: ['/assets/js/controllers/register.ctrl.js']}
          ]);
        }
      }
    })
    .state('app.login', {
      url: '/login/:accessToken/:refreshToken/:path',
      params: {
        accessToken: {squash: true, value: null},
        refreshToken: {squash: true, value: null},
        path: {squash: true, value: null},
        login: null,
        password: null
      },
      views: {
        '@': {
          controller: 'LoginCtrl',
          templateUrl: '/partials/pages/login.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'login.ctrl', files: ['/assets/js/controllers/login.ctrl.js']}
          ]);
        }
      }
    })

    .state('app.forgotpassword', {
      url: '/forgotPassword',
      views: {
        '@': {
          controller: 'LoginCtrl',
          templateUrl: '/partials/pages/login.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'login.ctrl', files: ['/assets/js/controllers/login.ctrl.js']}
          ]);
        }
      }
    })

    .state('app.recoverUsername', {
      url: '/recoverUsername',
      views: {
        '@': {
          controller: 'LoginCtrl',
          templateUrl: '/partials/pages/login.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'login.ctrl', files: ['/assets/js/controllers/login.ctrl.js']}
          ]);
        }
      }
    })

    .state('app.resetpassword', {
      url: '/passwordReset/:hash',
      views: {
        '@': {
          controller: 'LoginCtrl',
          templateUrl: '/partials/pages/login.html'
        }
      },
      resolve: {
        load: function ($ocLazyLoad) {
          return $ocLazyLoad.load([
            {name: 'login.ctrl', files: ['/assets/js/controllers/login.ctrl.js']}
          ]);
        }
      }
    })


    .state('app.page', {
      url: '/pages',
      abstract: true
    })
    .state('app.page.blank', {
      url: '/blank',
      views: {
        '@': {
          templateUrl: '/partials/pages/blank.html'
        }
      },
      data: {
        permissions: {
          except: ['anonymous'],
        }
      }
    })
    .state('app.page.lock', {
      url: '/lock',
      views: {
        '@': {
          templateUrl: '/partials/pages/lock.html'
        }
      },
      data: {
        permissions: {
          except: ['anonymous'],
        }
      }
    })
    .state('app.page.loading', {
      url: '/loading',
      views: {
        '@': {
          templateUrl: '/partials/pages/loading.html'
        }
      },
      data: {
        permissions: {
          except: ['anonymous'],
        }
      }
    });

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
};
})()
