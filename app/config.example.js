(function () {
  angular.module('app.config', []).constant('CONFIG', {
    client: {
      client_id: "XXX",
      client_secret: "XXX"
    },
    url: "XXX",
    metadata: {
      title: "XXX"
    },
    ga_tracking:{
      id: "UA-XXXXXXXX-X",
      states : ["xxxxx"]
    },
    debug:false,
    student_statuses:["Studying", "Semester Break", "Resting", "Alumini"]
  })
})()